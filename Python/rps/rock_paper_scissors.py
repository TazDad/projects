#! /usr/bin/env python3.11

"""
This program implements a two player version of the rock, paper, scissors
game.
"""

# imports required libraries
import sys
from getpass import getpass as secret


def get_winner(chosen_options: list) -> int:
    """
    This function determines if there was a winner and returns that value
    """
    # If the values are equal, the game is a draw
    if chosen_options[0] == chosen_options[1]:
        # Returns -1 to indicate a tie
        return -1
    # If player 1 wins
    if f'{chosen_options[0]}{chosen_options[1]}' in ['rs', 'sp', 'pr']:
        # Return 0 for player 1 to win
        return 0
    # Return 1 for player 2 to win
    return 1


def main() -> None:
    """
    This function exedutes the game
    """
    # Sets player names
    player_names = ['Player 1', 'Player 2']
    # Sets play options
    options = {'r': 'rock', 'p': 'paper', 's': 'scissors', 'e': 'exit'}
    # Gets options to present
    choices = ', '.join([f'{v} ({k})' for k, v in options.items()])

    # Iterates through four times to allow for four plays in the game
    for _ in range(4):
        # Sets the player pieces to nothing
        player_options = ['', '']
        # Steps through each player
        for index in range(2):
            # While the player does not enter a valid value
            while player_options[index] not in options:
                # Prompt for a valid value to be entered from the player
                # The input is from getpass and hides the player's input
                player_options[index] = secret(f'{player_names[index]}, enter '
                                               f'a move to play: {choices}: ')
                # If the player entered an e, exit the program
                if player_options[index] == 'e':
                    # Exits the program
                    sys.exit()

        # Checks to see who won and displays it
        check_winner(player_names, get_winner(player_options))


def check_winner(players: list[str], winner_index: int) -> None:
    """
    This function determines the winner and prints a message
    """
    # If the index is a -1, the game is a tie
    if winner_index == -1:
        # Informs the users
        print('The game is a tie.')
    # A player won
    else:
        # Informs the users
        print(f'{players[winner_index]} won.')


# If the program was not imported.
if __name__ == '__main__':
    # Executes the program
    main()

This is a simple number guessing game using Python. Here are some factors to
consider in building the program.

- By default, the number to guess is a number between 0 and 10, inclusively
- Allow for the option to increase the maximum number from 10 to another
value by passing in the new number into the function
- Let the user know if they guessed too high or low until they guess the
correct number
- When guessed, let the user know that they guessed correctly and how many
times that it took them to guess it
- The user should only be allowed to guess a number once
- Allow the user to play again, if they choose

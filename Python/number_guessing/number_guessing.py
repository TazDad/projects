#! /usr/bin/env python3.8

"""
This program is a number guessing game. By default, the game plays a random
number from 1 through 10. It prompts the user for a number to guess and
informs the user if the value that they entered is too high, too low, or
matches the generated value. Game play continues until the user no longer
wishes to continue playing.
"""

# pylint: disable=W0102

# Imports the required library
import random


def test_values(rand_num, guess):
    """
    This function compares the randomly generated number to the one provided
    by the user, displays an appropriate message, and returns True, if the
    guess matched or False, if the guess did not match.
    Args:
        rand_num is the randomly generated number
        guess is the number the user guessed
        high is the highest value that a number could be
    Returns:
        True if the user guessed the randomly generated number
        False if the user did not guess the randomly generated number
    """
    # If the random number matches the guessed number
    if rand_num == guess:
        # Informs the user
        print(f'Congratulations. You guessed the number: {rand_num}')
        # Returns True
        return True
    # If the number guessed is greater than the random number
    if rand_num < guess:
        # Informs the user
        print(f'Your guess was too high.')
    # If the number guessed is less than the random number
    else:
        # Informs the user
        print(f'Your guess was too low.')
    # Returns False
    return False


def get_user_number(high, attempts=[], reset=False):
    """
    This function retrieves a number to guess from the user. It ensures that
    the number entered is within the range of what the number can be and that
    the user has not already guessed the value. As attempts is only set once,
    reset is included to reset the list to allow for multiple games.
    Args:
        high is the highest number that a guess can be
        attempts is the list that is used to keep track of guesses to ensure
                 that a number is only guessed once. as attempts is mutable
                 and initialized within the definition line, it is only
                 initiated once regardless of how many times the function is
                 called; therefore, the reset variable was added to be able
                 to clear the list for any games past the first one.
        reset is used to clear the attemps list at the beginning of each game
    Returns:
        None if the attempts list is being cleared
        otherwise, the number the user guessed
    """
    # If the attempts list is to be cleared
    if reset:
        # Clears the list of all values
        attempts.clear()
        # Returns control back to the area of code that the function was
        # called
        return None
    # Set to indicate that a user has not entered a valid number
    guess_valid = False
    # While the guess is not valid
    while not guess_valid:
        # Tries the block of code
        try:
            # Retrieves the user's guess and attempts to convert it to an
            # integer
            guess = int(input(f'Enter a number from 1 through {high} not '
                              'already guessed: '))
            # If the number has not been guessed and the guess is within
            # the valid range
            if 0 < guess <= high and guess not in attempts:
                # Set to indicate that the guess was valid
                guess_valid = True
        # If a value other than an integer was entered
        except ValueError:
            # Set to indicate that a user has not entered a valid number
            guess_valid = False
    # Appends a valid guess to the list to keep track of them
    attempts.append(guess)
    # Returns the valid guess
    return guess


def get_random_number(high):
    """
    This function generates and returns a random number between 1 and the
    maximum value.
    Args:
        high is the highest number that a guess can be
    Returns:
        a random number between one and the high number (inclusive)
    """
    # Calculates and returns an integer between 1 and the high value
    # (inclusive)
    return random.randint(1, high)


def get_response(message):
    """
    This function retrieves a yes or no response from the user and returns
    True if the user enters any veriation of yes and False if they enter any
    variation of no.
    Args:
        high is the highest number that a guess can be
    Returns:
        True if the user responds yes to the message displayed
        False if the user responds no to the message displayed
    """
    # Creates an infinite loop
    while True:
        # Retrieves the response from the user, keeping only the first
        # character, and removing its case
        response = input(message)[0].casefold()
        # If the response is yes
        if response == 'y':
            # Return True
            return True
        # If the response is no
        if response == 'n':
            # Return False
            return False


def main(high=10):
    """
    This function executes the code to play the game.
    Args:
        high is the highest number that a guess can be
    Returns:
        None
    """
    # Sets to play the game
    continue_game = True
    # While the user wants to play the game
    while continue_game:
        # Sets to indicate that the game is not over
        game_over = False
        # Generates a random number from 1 through the high value passed in
        rand_num = get_random_number(high)
        # Counter to keep track of the number of guesses a user attempted
        guesses = 0
        # Executes to reset (clear) the list in the function used to store
        # the previous attempts
        get_user_number(high, reset=True)
        # While the game is not over
        while not game_over:
            # Retrieves the number guessed by the user
            guess = get_user_number(high)
            # Determines if the numbers matched and sets the game_over value
            game_over = test_values(rand_num, guess)
            # Increments the number of guesses
            guesses += 1
        # Informs the user of the number of attempts it took them to guess
        # the number
        print(f'You completed the game in {guesses} guesses')
        # Retrieves whether the user would like to play again
        continue_game = get_response('Would you like to play again? ')


# If the program was executed by calling its name
if __name__ == '__main__':
    # Executes the main function
    main()

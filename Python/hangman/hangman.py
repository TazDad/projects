#! /usr/bin/env python3.8

"""
This program implements a version of the game: hangman
It attempts to read phrases found on this website by default:
    https://www.phrases.org.uk/meanings/phrases-and-sayings-list.html
If the website is unreachable or a valid status code is not received, the
built-in phrases are used for the game.
"""

# Imports required libraries
import os
import random
import string
import argparse
import requests
import click
from bs4 import BeautifulSoup as bs


def clear_screen():
    """
    This function clears the screen.
    Args:
        None
    Returns:
        None
    """
    # Clears the screen
    click.clear()


def is_file_readable(file):
    """
    This function validates that a file is readable by the player
    Args:
        file is a filename to validate that it is readable by the player
    Returns:
        returns True if the file is readable by the player
        returns False if the file is not readable by the player
    """
    # Returns the test of checking to see if a file is readable
    return os.access(file, os.R_OK)


def parse_command_line(**kwargs):
    """
    This function reads the command line to retrieve the necessary values
    to conduct a port scan. The command line options can be obtained by
    calling the program name with the -h option.

    Args:
        show_help if True is passed in, displays the command line help
        kwargs is an optional dictionary of the keyword/value pairs
               equivalent to those required on the command line
    Returns:
        a dictionary containing the retrieved keyword/value pairs
    """
    # Defaults the allowable options
    options = {'url': 'https://www.phrases.org.uk/meanings/phrases-and-'
                      'sayings-list.html',
               'phrase_file': 'hangman.dat',
               'recover': 'web',
               'min_words': 2}

    # Creates an argument parser
    parser = argparse.ArgumentParser(formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    # Sets the possible input arguments
    parser.add_argument('-u', '--url', dest='url', default=options['url'],
                        help='The website to retrieve a list of phrases that '
                        'are packaged within <a href=''>phrase</a> HTML tags',
                        metavar='URL', type=str)
    parser.add_argument('-p', '--phrase_file', dest='phrase_file',
                        default=options['phrase_file'], help='The file found '
                        'on the local system that contains a list of phrases.',
                        metavar='FILE', type=str)
    parser.add_argument('-r', '--recover', dest='recover',
                        default=options['recover'],
                        help='The recovery method of the list of phrases.',
                        metavar='RECOVER', type=str)
    parser.add_argument('-m', '--min_words', dest='min_words',
                        default=options['min_words'], help='The minimum '
                        'number of words the phrase is required to contain.',
                        metavar='MIN_PHRASE', type=int)

    # Parses the command line
    args = parser.parse_args()

    # If the help message is to be displayed or the length is not set
    # Extracts the variables from the Namespace into a dictionary
    options = vars(args)

    # If keyword/value pairs were passed in
    if len(kwargs.keys()) > 0:
        # Steps through any optional arguments passed in
        # If the argument exists, the value is taken over those on the command
        # line; if no value, the command line and/or default values are kept
        for key, value in kwargs.items():
            # If a key is not the length and it is not a valid key
            if key not in options.keys():
                # Informs the player
                print(f"\n\nUnknown argument '{key}' passed, exiting...\n\n")
            # Overwrites the command line value or default value with the
            # optional value passed into the function
            options[key] = value

    # Returns a dictionary with the keyword and associated value retrieved
    return options


def retrieve_phrases(options: dict) -> list:
    """
    This function recovers the phrases to use in the game.
    Args:
        options is the dictionary retrieves from attempting to reac from the
                command line
    Returns:
        A list of phrases to use in the game
    """
    if options['recover'] == 'web':
        # Attempts to retrieve the phrases from the specified website
        try:
            # If the min_words is less than 1 or greater than 6,
            # set the default to 2
            if 6 < options['min_words'] < 1:
                # Sets the default to 2
                options['min_words'] = 2
            # Retrieves the site information
            site = requests.get(options['url'])
            # If a valid status code was returned
            if site.status_code == 200:
                # Format the website data using BeautifulSoup
                soup = bs(site.content, "html.parser")
                # Retrieves the list of phrases that contain at least the
                # minimum number of words
                phrases = [str(link.string).strip() for link in
                           soup.findAll('a')
                           if len(str(link.string).strip().split())
                           >= options['min_words']]
                # If there were phrases recovered
                if len(phrases) > 0:
                    # Return the phrases
                    return phrases
        # If there was no internet or unable to establish a connection with
        # the website
        except requests.exceptions.ConnectionError:
            # Skips
            pass
    # If the file is readable
    if is_file_readable(options['phrase_file']):
        # Reads the file, removes the newline character, keeps non-empty
        # lines, and returns any with the minimum number of words
        phrases = [p.strip() for p in
                   open(options['phrase_file'], 'r').readlines()
                   if len(p) > 2 and len(p.split()) >= options['min_words']]
        # If there were phrases recovered
        if len(phrases) > 0:
            # Return the phrases
            return phrases
    # Returns the default phrase list if there were no phrases recovered, a
    # valid status code was not received or a connection could not be
    # established to the website
    return ['To be or not to be.', 'To boldly go.',
            'Always believe in yourself!',
            'Sometimes you feel like a nut,',
            'Go forth and do good things.']


def set_phrase(phrases) -> str:
    """
    This function picks a random phrase from a list of phrases.
    Args:
        phrases is a list of phrases
    Returns:
        A randomly selected phrase from the list
    """
    # Returns a randomly selected phrase from the list
    return random.choice(phrases)


def check_in_phrase(letter: str, phrase: str) -> bool:
    """
    This function checks to see if a letter is within a phrase.
    Args:
        letter is a letter to see if it is in phrase
        phrase is a phrase to see if the letter is in phrase
    Returns:
        True if the letter is in the phrase
        False if the letter is not in the phrase
    """
    # Checks to see if a letter is within a phrase, if it is, return True
    # if it is not, return False
    return letter in phrase.casefold()


def display_chosen(characters: dict) -> str:
    """
    This function returns a string of characters separated by a comma (,) and
    space that represents the characters the player has already tried within
    their attempts to guess the phrase.
    Args:
        characters is a dictionary that has in it every letter within the
                   alphabet as the key and a sub-dictionary that contains
                   a key of 'guessed'. If it has been tried before, the value
                   will be set to True, if it has not, the value will be False
    Returns:
        A string of characters separated by a comma (,) and space that
    represents the characters the player has already tried within their
    attempts to guess the phrase. If no characters have been tried, an empty
    string is returned.
    """
    # Checks the value in the dictionary to see if it has been set to True
    # if it has, return it in the string, separating each chosen character
    # with a comma (,) followed by a space.
    # if no characters are found, an empty string is returned.
    return 'Characters already guessed: ' +\
           ', '.join([c for c, d in characters.items() if d['guessed']])


def get_letter(characters: dict, letter: str = '') -> str:
    """
    This function retrieves a new letter from the player in their guessing
    of the letters in the phrase.
    Args:
        characters is a dictionary that has in it every letter within the
                   alphabet as the key and a sub-dictionary that contains
                   a key of 'guessed'. If it has been tried before, the value
                   will be set to True, if it has not, the value will be False
        letter is a letter to see if it is in phrase, defaulting to an
               empty string
    Returns:
        A letter that the player has not already attempted in their guess
    """
    # Displays the letters that the player has already guessed
    print(display_chosen(characters))
    # Until the player provides them with a valid character that is within the
    # keys of the dictionary and has not already been guessed, they will
    # be prompted to provide them a letter
    while letter not in characters.keys() or characters[letter]['guessed']:
        # Asks the player for a letter and casefolds it.
        letter = input('Enter a letter [a-z]: ').casefold()
    # Returns the valid letter provided by the player
    return letter


def create_characters(phrase: str) -> dict:
    """
    This function creates the dictionary used within the game to keep track
    of whether a character is within the phrase and the guesses that the player
    has attempted.
    Args:
        phrase is a phrase the player is trying to guess
    Returns:
        A dictionary that has in it every letter within the alphabet as the
    key and a sub-dictionary that contains a keys of 'guessed' and 'in_string'.
    'guessed' will be defaulted to False. 'in_string' will be True if the
    letter is in the string; otherwise, it will be False.
    """
    return {l: {'in_string': check_in_phrase(l, phrase), 'guessed': False}
            for l in string.ascii_lowercase}


def get_response(message: str, response: str = 'a') -> bool:
    """
    This function retrieves a yes or no response from the player.
    Args:
        message is the message to be displayed to the player
        response is the player's response to the message
    Returns:
        True if the player entered any variation of yes
        False if the player entered any variation of no
    """
    # Until the player enters a n or y
    while response not in 'ny':
        # Prompts them for a response, retrieves the first character, and
        # and casefolds it
        response = input(message)[0].casefold()
    # Returns True if the response was a y and False otherwise
    return response == 'y'


def check_game_over(failed_guesses: int, phrase: str, guess: str) -> bool:
    """
    This function checks to see if the game is over.
    Args:
        failed_guesses is an integer marking the number of times the player
                       guessed a letter that was not in the phrase
        phrase is the phrase the player is trying to guess
        guess is the state of the guesses within the phrase made by the player
    Returns:
        A tuple with True or False as the first element to indicate that the
    game is over (True) or not (False). The second element indicates whether
    the player won, lost, or the game is still in session.
    """
    # If the phrase and guess match
    if phrase == guess:
        # Returns a tuple indicating that the game is over (True) and the
        # player won the game
        return (True, 'Win')
    # If the maximum number of guesses has been reached
    if failed_guesses == 6:
        # Returns a tuple indicating that the game is over (True) and the
        # player lost the game
        return (True, 'Loss')
    # Returns a tuple to indicate the game is not over (False) and still in
    # session (Not over)
    return (False, 'Not over')


def set_guess(phrase: str) -> str:
    """
    This function converts all the letters in the string to underscores (_)
    to allow the player to have a phrase to guess.
    Args:
        phrase is the phrase the player is trying to guess
    Returns:
        A string as a guess phrase that has converted all the letters to an
    underscore (_) so that the player can guess them.
    """
    # Steps through each letter in the phrase and replaces any letter (a-z)
    # that is found with an underscore (_) in its place. Any other characters
    # are left as they are within the phrase. Returns the new string.
    return ''.join(['_' if c in string.ascii_lowercase +
                    string.ascii_uppercase else c for c in phrase])


def set_letter(letter: str, phrase: str, guess: list) -> str:
    """
    This function replaces underscores (_) within the guess string with the
    guessed letter when that underscore (_) is represented by the letter.
    Args:
        letter is the letter guessed by the player
        phrase is the phrase the player is trying to guess
        guess is the phrase used in the player's guessing
    Returns:
        A new guessing string with the underscores (_) replaced by the letter
    where it is found within the phrase being guessed.
    """
    # Returns the guess phrase with the underscore (_) replaced by the letter
    # guessed where they fall within the phrase
    return ''.join([l if l.casefold() == letter else guess[i]
                    for i, l in enumerate(phrase)])


def print_part(which_part: str, times: int = 6) -> None:
    """
    This function prints the specified part to the hangman area of the game.
    Args:
        which_part indicates which part of the hangman area is to be printed
        times for empty areas only containing the gallow or the torso, it
              represents the number of times to print the same area
    Returns:
        None
    """
    # If the part to print is the head
    if which_part == 'head':
        # Prints the head
        print('|', ' ' * 4, '-' * 5)
        print('|', ' ' * 3, '/ o o \\')
        print('|', ' ' * 3, '\\ --- /')
        print('|', ' ' * 4, '-' * 5)
    # If the part to print is the torso
    elif which_part == 'torso':
        # Prints the torso area
        print(f'| {" " * 6} |\n' * times, end='')
    # If the part to print is the right arm
    elif which_part == 'right_arm':
        # Prints the right arm
        print(f'| {" " * 6}/|')
        print(f'| {" " * 5}/ |')
        print(f'| {" " * 4}() |')
    # If the part to print is the arms
    elif which_part == 'arms':
        # Prints the arms
        print(f'| {" " * 6}/|\\')
        print(f'| {" " * 5}/ | \\')
        print(f'| {" " * 4}() | ()')
    # If the part to print is the right leg
    elif which_part == 'right_leg':
        # Prints the right leg
        print(f'| {" " * 6}/ ')
        print(f'| {" " * 5}/  ')
        print(f'| {" " * 2}---   ')
    # If the part to print is the legs
    elif which_part == 'legs':
        # Prints the legs
        print(f'| {" " * 6}/ \\')
        print(f'| {" " * 5}/   \\')
        print(f'| {" " * 2}---     ---')
    # If the part to print is the gallow
    elif which_part == 'gallow':
        # Prints the gallow the specified number of times
        print('|\n' * times, end='')


def print_board(failed_guesses: int) -> None:
    """
    This function prints the appropriate hangman area based on the failed
    guesses.
    Args:
        failed_guesses is the number of times the player has guessed a letter
                       that was not in the phrase
    Returns:
        None
    """
    # Clears the screen
    clear_screen()
    # Prints the top of the gallow
    print(' ', '_' * 8, sep='')
    # Prints the start of the gallow
    print('|', ' ' * 6, '|')
    # If there have been no failed guesses
    if failed_guesses == 0:
        # Print the middle of the gallow
        print_part('gallow', 15)
    # If there has been 1 failed guess
    elif failed_guesses == 1:
        # Print the head
        print_part('head')
        # Print the remaining portion of the gallow
        print_part('gallow', 11)
    # If there has been 2 failed guesses
    elif failed_guesses == 2:
        # Print the head
        print_part('head')
        # Print the torso
        print_part('torso')
        # Print the remaining portion of the gallow
        print_part('gallow', 5)
    # If there has been 3 failed guesses
    elif failed_guesses == 3:
        # Print the head
        print_part('head')
        # Prints the right arm
        print_part('right_arm')
        # Print the remaining torso
        print_part('torso', 2)
        # Print the remaining portion of the gallow
        print_part('gallow', 5)
    # If there has been 4 failed guesses
    elif failed_guesses == 4:
        # Print the head
        print_part('head')
        # Print the neck
        print_part('neck')
        # Print the arms
        print_part('arms')
        # Print the remaining torso
        print_part('torso', 2)
        # Print the remaining portion of the gallow
        print_part('gallow', 5)
    # If there has been 5 failed guesses
    elif failed_guesses == 5:
        # Print the head
        print_part('head')
        # Print the neck
        print_part('neck')
        # Print the arms
        print_part('arms')
        # Print the remaining torso
        print_part('torso', 2)
        # Print the right leg
        print_part('right_leg')
        # Print the remaining portion of the gallow
        print_part('gallow', 2)
    # If there has been 6 failed guesses
    elif failed_guesses == 6:
        # Print the head
        print_part('head')
        # Print the neck
        print_part('neck')
        # Print the arms
        print_part('arms')
        # Print the remaining torso
        print_part('torso', 2)
        # Print the legs
        print_part('legs')
        # Print the remaining portion of the gallow
        print_part('gallow', 2)
    # Print the bottom of the gallow
    print('|', '_' * 14, sep='')


def main() -> None:
    """
    This function executes the game.
    Args:
        None
    Returns:
        None
    """
    # Retrieve the command line arguments
    command_line_options = parse_command_line()
    # Retrieves a list of found phrases using the options retrieved from the
    # parse_command_line() function
    phrases = retrieve_phrases(command_line_options)
    # Set to True to play the game
    play_game = True
    # While the game is being played
    while play_game:
        # Set to False to indicate that the game is not over
        game_over = False
        # Sets the phrase to be used in the game
        phrase = set_phrase(phrases)
        # Sets the guessing phrase for the game
        guess = set_guess(phrase)
        # Sets the dictionary to be used within the game for the phrase
        characters = create_characters(phrase)
        # Sets to indicate that the player has not guessed incorrectly
        failed_guesses = 0
        # While the game is not over
        while not game_over:
            # Print the hangman area
            print_board(failed_guesses)
            # Prints the guess phrase in its current state
            print('\n', guess, sep='')
            # Retrieves a guess fromt the player
            letter = get_letter(characters)
            # Sets to indicate that the letter has been guessed
            characters[letter]['guessed'] = True
            # If the letter is in the string
            if characters[letter]['in_string']:
                # Updates the guess with the letter guessed
                guess = set_letter(letter, phrase, list(guess))
            # The letter is not in the string
            else:
                # Increments the number of failed guesses
                failed_guesses += 1
            # Checks to see if the game is over and the status of the game
            (game_over, status) = check_game_over(failed_guesses,
                                                  phrase, guess)
        # Print the hangmane area
        print_board(failed_guesses)
        # If the game is over by winning
        if status == 'Win':
            # Prints a win message to the player
            print('Congratulations. You guessed the phrase.')
        # The game is over by losing
        else:
            # Prints a loss message to the player
            print('Sorry, You did not guess the phrase.')
        # Prints the state of the guessing
        print('\nYour guess: ', guess, sep='')
        # Prints the phrase that the player was trying to guess
        print('Phrase:    ', phrase, '\n')
        # Prompts the user to determine if they would like to play again
        play_game = get_response('Would you like to play again (Y/N): ')


# If the program is being called and not imported
if __name__ == '__main__':
    # Executes the program
    main()

#! /usr/bin/env python3.11

"""
This program implements a database in Python.
"""

# pylint: disable=R0902
# pylint: disable=R0904

# Modules to import
import sys
import os
import re
import sqlite3
from string import punctuation as P
from string import ascii_uppercase as UC
from string import ascii_lowercase as LC
from string import digits as D
import time
import random
from getpass import getpass
import click
import bcrypt


class Database():
    """
    This is a class that implements a database in Python.
    """
    # Sets the attributes to enable hiding
    __initialize_db = False
    __username = ''
    __password = ''
    __pass_hash = ''
    __connection = ''
    __cursor_object = ''
    __valid_user = False
    __user_access_level = 'Invalid User'

    def __init__(self, database_name='my_db.db', username='new',
                 password='old'):
        """
        This method initializes a database class in Python.
        """
        # Sets the database name
        self.database_name = self.get_db_name(database_name)
        # Sets to indicate that the database should not be initialized
        self.__initialize_db = False
        # If the database is not readable or writeable
        if not self.is_db_usable():
            # Sets to indicate the database should be initialized
            self.__initialize_db = True
        # Sets the username
        self.__username = self.check_username(username)
        # Sets the password for the username
        self.__password = self.set_password(password, self.__username)
        # Sets the hash for the retrieved password
        self.__pass_hash = self.get_password_hash(self.__password)
        # Sets the connection to the database
        self.__connection = self.database_connection()
        # Sets the cursor object to the database
        self.__cursor_object = self.database_cursor()
        # If the database needs to be initialized
        if self.__initialize_db:
            # Creates the user table
            self.create_user_table()
            # Initializes the database table
            self.initialize_user_table()
        # Sets whether the user's password is valid
        self.__valid_user = self.is_valid_user()
        # Sets to indicate that the user's access is invalid
        self.__user_access_level = 'Invalid User'
        # If the user's password is valid
        if self.__valid_user:
            # Retrieves the user's access level
            self.__user_access_level = self.retrieve_user_access_type()
            # If the user is required to change their password
            if self.is_password_reset_required():
                # Informs the user
                print('You are required to change your password upon login.')
                # Runs the method to change their password
                self.change_password()
                # Prompts to wait until the user hits enter to allow them
                # to see their password, if it was auto generated
                input('Hit enter to continue:')
        # The user has been validated.
        # The password and password hash are no longer required.
        # They are deleted to avoid them from being retrieved later.
        self.__password = ''
        self.__pass_hash = ''

    def is_db_usable(self):
        """
        This method validates that a file is usable (readable and wrtitable)
        by the user.
        """
        # Returns the result of testing if the file is readable and writable
        return os.access(self.database_name, os.R_OK | os.W_OK)

    def print_username_rules(self):
        """
        This method displays the rules surrounding username creation.
        """
        # Clears the screen
        self.clear_screen()
        # Prints the username rules
        print('Username rules: ')
        print('  o must be betwen 5 and 24 (inclusive) in length')
        print('  o must begin with either an uppercase or lowercase letter')
        print('  o can only contain letters (uppercase or lowercase), '
              'numbers, digits, and underscores')
        print('  o must not end with an underscore')

    def check_username(self, username):
        """
        This method validates a username.
        """
        # Set to indicate that the rules have not been printed
        printed = False
        # While the username is not valid
        while not self.valid_username(username):
            # If the rules have not been printed
            if not printed:
                # Print the username rules
                self.print_username_rules()
            # Retrieves a username from the user
            username = input('Enter a username: ')
            # Set to indicate that the rules have been printed
            printed = True
        # Returns the username
        return username

    @staticmethod
    def get_response(message):
        """
        This method gets a yes/no response from the user.
        """
        # Sets response to an invalid response
        response = 'a'
        # While the response is not a 'n' or a 'y'
        while response not in 'ny':
            # Sets the response from the user
            response = input(message)[0].casefold()
        # Returns True if the user entered 'y', False otherwise
        return response == 'y'

    def is_valid_user_password(self):
        """
        This method validates the password entered to see if it matches
        the stored password hash for the user.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash FROM tblUsers WHERE '
                                     'username=?', (self.__username,))
        # Retrieves the value from the query
        value = self.__cursor_object.fetchone()
        # If a value was returned
        if value:
            # Compares the user password to what was retrieved from the
            # database and returns True of False depending on whether they
            # matched
            return bcrypt.checkpw(self.__password.encode('utf-8'), value[0])
        # If no value was retrieved, return False
        return False

    def initialize_user_table(self):
        """
        This method allows for the creation of the database, if there are
        no users in the database and informs the user they do not have
        access if their username is not in the database.
        """
        # If the username is not in the database
        if not self.is_user_in_user_table(self.__username):
            # If there are users in the database, inform the user that they
            # do not have access because they were not in the database
            if self.any_users_in_user_table():
                # Informs the user
                print(f'{self.__username}, you are not in the database.'
                      'Contact an admin to get access.')
            # There are no users in the database
            else:
                # Informs the user
                print('There are no users in the database.')
                # If the user wants to create the database, adding themselves
                # as an admin because the database did not exist before this
                if self.get_response('Would you like to do you add yourself '
                                     'to the database as an admin? '):
                    # Inserts the user into the database as an admin
                    self.insert_user_into_table(self.__username,
                                                self.__pass_hash, 'admin', '1')

    def insert_user_into_table(self, user, pass_hash, access, reset):
        """
        This method inserts a user into the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('INSERT INTO tblUsers (username, '
                                     'pass_hash,  access_type, reset) VALUES ('
                                     ':username, :pass_hash, :access_type, '
                                     ':reset)', (user, pass_hash, access,
                                                 reset))
        # Commits the query
        self.__connection.commit()

    def any_users_in_user_table(self):
        """
        This method returns True if there are users in the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT username FROM tblUsers')
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def is_password_reset_required(self):
        """
        This method returns True if the user must reset their password and
        False if they do not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT reset FROM tblUsers WHERE '
                                     'username=?', (self.__username,))
        # Returns True of the user must reset their password and False if
        # they do not
        return bool(int(self.__cursor_object.fetchone()[0]))

    def is_user_in_user_table(self, user):
        """
        This method returns True if the user is in the database and False if
        it is not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash FROM tblUsers WHERE '
                                     'username=?', (user,))
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def retrieve_user_access_type(self):
        """
        This method retrieves the user's access level from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT access_type FROM tblUsers WHERE '
                                     'username=?', (self.__username,))
        # Returns the user's access type
        return self.__cursor_object.fetchone()[0]

    @staticmethod
    def get_db_name(db_name):
        """
        This method retrieves a database name from the user.
        """
        # While the database name does not end with a .db extension
        while db_name[-3:] != '.db':
            # Retrieves the database name
            db_name = input('Enter database name (must end with .db): ')
        # Returns the database name
        return db_name

    @staticmethod
    def valid_username(username):
        """
        This method checks the username to see if it matches the validation
        criteria.
        """
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_]*$'
        # The value is between 5 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, and underscores
        # The value does not end with an underscore
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(username) > 1 and all([4 < len(username) < 25,
                                          username[0].isalpha(),
                                          re.match(valid_chars, username),
                                          username[-1] != '_'])

    @staticmethod
    def valid_password(password):
        """
        This method checks the password to see if it matches the validation
        criteria.
        """
        # Sets the valid search parameters
        search = '[a-z]{3,}|[A-Z]{3,}|[0-9]{3,}|['+P+'}]{3,}'
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_'+P+']*$'
        # The value is between 12 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, punctuation, and
        # underscores
        # The value does not end with a punctuation character
        # The value does not contain three or more characters of the same
        # character set in a row
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(password) > 1 and all([11 < len(password) < 25,
                                          password[0].isalpha(),
                                          re.match(valid_chars, password),
                                          re.search(search, password) is None,
                                          password[-1] not in P])

    def generate_password(self):
        """
        This method generates a password based upon the rules.
        """
        # Sets a random length value between two numbers lower than the upper
        # and lower limits
        body_length = random.randint(10, 22)
        # Sets the variables and shuffles its contents
        random.shuffle(char_list_start := list(UC + LC))
        random.shuffle(char_list_body := list(UC + LC + P + D))
        random.shuffle(char_list_end := list(UC + LC + D))
        # Sets an invalid password
        gen_password = ''
        # While the password is not valid
        while not self.valid_password(gen_password):
            # Generate a password
            gen_password = random.choice(char_list_start) +\
                           ''.join(random.choices(char_list_body,
                                                  k=body_length)) +\
                           random.choice(char_list_end)
        # Returns the generated password
        return gen_password

    def print_password_rules(self):
        """
        This method displays the rules for passwords.
        """
        # Clears the screen
        self.clear_screen()
        # Prints the password rules to the user
        print('Password rules: ')
        print('  o must be betwen 12 and 24 (inclusive) in length')
        print('  o must begin with either an uppercase or lowercase letter')
        print('  o can only contain character from the character sets: '
              'letters (uppercase or lowercase), numbers, digits, and '
              'punctuation characters')
        print('  o must not contain any more than two characters from the'
              'same character set')
        print('  o must not end with punctuation')

    def is_valid_user(self):
        """
        This method checks to see if the user's password is valid.
        """
        # Returns True if the user's password matches, False otherwise
        return self.is_valid_user_password()

    def set_password(self, value, user):
        """
        This method retrieves a password from the user until they enter
        a valid one.
        """
        # Sets to indicate the rules have not been printed
        printed = False
        # While it is not a valid password
        while not self.valid_password(value):
            # If the rules have not been printed
            if not printed:
                # Print the password rules
                self.print_password_rules()
            # Retrieves a password from the user
            value = getpass(f'Enter a password for {user}: ')
            # Set to indicate that the rules have been printed
            printed = True
        # Returns the retrieved password
        return value

    @staticmethod
    def clear_screen():
        """
        This method clears the screen.
        """
        # Clears the screen
        click.clear()

    def set_username(self, username):
        """
        This method retrieves a username from the user until they enter
        a valid one.
        """
        # Sets the username
        self.__username = username if self.valid_username(username) else None

    @staticmethod
    def get_password_hash(password):
        """
        This method generates a hash of the password using bcrypt
        """
        # If there is a password
        if password:
            # Creates a hash of the password
            return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # Returns None if there was no password
        return None

    def database_connection(self):
        """
        This method establishes a connecgtion to the database
        """
        # Returns a connection to the database
        return sqlite3.connect(self.database_name)

    def database_cursor(self):
        """
        This method establishes the cursor to a place in the database
        """
        # Returns the cursor to the database
        return self.__connection.cursor()

    def create_user_table(self):
        """
        This method creates the user table within the database.
        """
        # Sets the SQL code to create the database table
        self.__cursor_object.execute('CREATE TABLE IF NOT EXISTS tblUsers '
                                     '(user_id INTEGER PRIMARY KEY '
                                     'AUTOINCREMENT, username CHAR(25) NOT '
                                     'NULL UNIQUE, pass_hash CHAR(128) NOT '
                                     'NULL, access_type CHAR(10) '
                                     'check(access_type = "admin" or '
                                     'access_type = "user" or access_type = '
                                     '"reviewer") NOT NULL, reset CHAR(1) '
                                     'check(reset = "0" or reset = "1") NOT '
                                     'NULL)')

    def retrieve_users_with_access_type(self):
        """
        This method retrieves the usernames and their access type from the
        database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT username, access_type FROM '
                                     'tblUsers')
        # Returns the results of the query
        return self.__cursor_object.fetchall()

    def view_users(self):
        """
        This method prints the users and their access type retrieved from the
        database.
        """
        # Steps through each result retrieved
        for user, access in self.retrieve_users_with_access_type():
            # Prints the username and their access type
            print(f'{user}\t{access}')
        # Prints an empty line
        print()

    def get_username(self):
        """
        This method retrieves a username to make modifications.
        """
        # Sets the user list to exclude the current user
        user_list = [info[0] for info in
                     self.retrieve_users_with_access_type()
                     if info[0] != self.__username]
        # If the user list is not empty
        if user_list:
            # Clears the screen
            self.clear_screen()
            # Steps through each user in the list
            for user in user_list:
                # Print the username
                print(f'{user}')
            # Sets to an invalid value
            user = 'a'
            # Wihle the user is not valid
            while user not in user_list:
                # Retrieve the username from the user
                user = input('From the choices above, enter a username to '
                             'modify: ')
            # Returns the username
            return user
        # Returns None if no users were found
        return None

    def set_new_password_hash(self, password, user):
        """
        This method sets the user's password based upon a response from the
        admin indicating that they will set the password or let it be
        generated.
        """
        # If the user wants to enter the new password
        if self.get_response('Manually reset the user\'s password? '):
            print('Enter the password: ')
            # Retrieves the new password from the user
            new_pass = self.set_password(password, user)
        # Auto generate the password
        else:
            # Generates the new password
            new_pass = self.generate_password()
            # Provides the current user with the user's password; otherwise
            # it is not retrievable
            print(f'{user}\'s new password is: {new_pass}')
        # Returns the password hash
        return self.get_password_hash(new_pass)

    def change_password(self):
        """
        This method retrieves a new password before updating it in the
        database.
        """
        # Retrieves the hash for the password
        new_pass_hash = self.set_new_password_hash('', self.__username)
        # Updates the user's password in the database
        self.update_user_password(self.__username, new_pass_hash, "0")

    def set_new_username(self):
        """
        This method allows for a new username to be set.
        """
        # Sets to keep retrieving a value
        keep_retrieving = True
        # While the username entered is not valid
        while keep_retrieving:
            # Retrieves a user name
            new_user = self.check_username('')
            # If the username is in the database
            if self.is_user_in_user_table(new_user):
                # Informs the user
                print('The username must be one not already in the database.')
                # Sleeps for two seconds so the user can see the message
                time.sleep(2)
            # The username is not in the database
            else:
                # Sets to stop retrieving a username
                keep_retrieving = False
        # Returns the retrieved username
        return new_user

    @staticmethod
    def get_user_access_type(user):
        """
        This method retrieves the user access type from the admin.
        """
        # Sets the access type possibilities
        access_types = ['admin', 'user', 'reviewer']
        # Sets to an invalid access type
        access = 'a'
        # Prompts the user
        print(f'Enter the number associated with the access type for {user}:')
        # Steps through each access type and prints them to the user
        for index, access_type in enumerate(access_types, start=1):
            # Prints the index and the access type
            print(f'\t{index}\t{access_type}')
        # While access is not a valid access type
        while access not in access_types:
            # Attempts
            try:
                # If a valid number was entered to set the access
                access = access_types[int(input())-1]
            # A valid number was not entered
            except (ValueError, IndexError):
                # Sets to skip the exception
                pass
        # Returns the access type retrieved
        return access

    def reset_user_password(self, user):
        """
        This method sets a user's reset value based upon the admin's response.
        """
        # Returns a string 1 if the user wants to force a user to change their
        # password when they login to the database again
        return "1" if self.get_response(f'Force {user} to change their '
                                        'password the first time that they '
                                        'login? ') else "0"

    def add_user(self):
        """
        This method retrieves the username, sets their password, and reset
        value before updating the database.
        """
        # Retrieves a valid username
        new_user = self.set_new_username()
        # Retrieves a password hash
        new_pass_hash = self.set_new_password_hash('', new_user)
        # Retrieves an access type for the username
        access = self.get_user_access_type(new_user)
        # Determimes if the new user is required to change their password
        # when they login
        reset = self.reset_user_password(new_user)
        # Updates the database
        self.insert_user_into_table(new_user, new_pass_hash, access, reset)

    def update_user_password(self, user, password, reset):
        """
        This method updates the user's password, and reset value.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET pass_hash = (?), '
                                     'reset = (?) WHERE username=(?);',
                                     (password, reset, user))
        # Commits the change to the database
        self.__connection.commit()

    def change_user_password(self):
        """
        This method retrieves the username to change their password and
        retrieves the new password before updating it int he database.
        """
        # Retrieves the username to modify
        user_to_modify = self.get_username()
        # Retrieves the new password hash
        new_pass_hash = self.set_new_password_hash('', user_to_modify)
        # Determines if the user is required to change their password when
        # they login
        reset = self.reset_user_password(user_to_modify)
        # Updates the database
        self.update_user_password(user_to_modify, new_pass_hash, reset)

    def update_user_access_level(self, user, access):
        """
        This method updates the user's access level.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET access_type = (?) '
                                     'WHERE username=(?);',
                                     (access, user))
        # Commits the change to the database
        self.__connection.commit()

    def change_access_level(self):
        """
        This method retrieves the username to change their password and
        retrieves the new password before updating it int he database.
        """
        # Retrieves the username to modify
        user_to_modify = self.get_username()
        # Retrieves an access type for the username
        access = self.get_user_access_type(user_to_modify)
        self.update_user_access_level(user_to_modify, access)

    def delete_user_from_table(self, user):
        """
        This method deletes the specified user from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('DELETE FROM tblUsers WHERE '
                                     'username=(?);', (user,))
        # Commits the change to the database
        self.__connection.commit()

    def delete_user(self):
        """
        This method retrieves the user to delete from the database and then
        calls the method to delete them.
        """
        # Retrieves the username to delete
        user_to_delete = self.get_username()
        # Updates the database
        self.delete_user_from_table(user_to_delete)

    def force_password_change_in_table(self, user):
        """
        This method updates the user's access level.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET reset = "1" '
                                     'WHERE username=(?);',
                                     (user,))
        # Commits the change to the database
        self.__connection.commit()

    def force_password_change(self):
        """
        This method retrieves the user to delete from the database and then
        calls the method to delete them.
        """
        # Retrieves the username to delete
        user_to_force = self.get_username()
        # Updates the database
        self.force_password_change_in_table(user_to_force)

    def menu(self):
        """
        This method creates a menu for the user to execute commands and
        manipulate the database.
        """
        # Sets to indicate the user wants to perform more actions
        more_actions = True
        # Sets the possible actions
        actions = {'1': self.view_users,
                   '2': self.change_password,
                   '5': self.add_user,
                   '6': self.change_user_password,
                   '7': self.delete_user,
                   '8': self.change_access_level,
                   '9': self.force_password_change,
                   'q': sys.exit
                   }
        # If the user's access level is not valid
        if self.__user_access_level not in ['admin', 'user', 'reviewer']:
            # Informs the user
            print('You do not have access to the database.')
            print('Contact an administrator for access. Exiting...')
            # Exits
            sys.exit()
        # Clears the screen
        self.clear_screen()
        # While the user is still executing actions
        while more_actions:
            # Prints the menu according to access values
            print('What action would you like to perform?')
            print('1    View users and their access type')
            print('2    Change your password')
            # If the user's access level is in the list, print these command
            # possibilities
            if self.__user_access_level in ['user', 'admin']:
                print('3    feature to implement')
                print('4    feature to implement')
            # If the user's access level is admin, print these command
            # possibilities
            if self.__user_access_level == 'admin':
                print('5    add a user')
                print('6    change a user\'s password')
                print('7    delete a user')
                print('8    change a user\'s access level')
                print('9    force a user to reset password upon login')
            # Informs the user how to quit
            print('q    quit the program')
            # Retrieves the response from the user
            response = input().casefold()[0]
            # If the response is valid
            if response in actions.keys():
                # Execute the appropriate action
                actions[response]()


# This executes the code if the program name was executed and not imported
if __name__ == '__main__':
    # When a username and password is set, it could be added here
    # USERNAME = ''
    # PASSWORD = ''
    # Add the username and password to the Database
    # MY_DB = Database(username=USERNAME, password=PASSWORD)
    # Initializes a database
    MY_DB = Database()
    # Displays the menu
    MY_DB.menu()

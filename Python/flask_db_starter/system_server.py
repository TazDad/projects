#! /usr/bin/env python3.11


"""
This program creates a series of web pages to login and manage a database
for a web site.
"""

# Imports required libraries
from flask import Flask, request, redirect, session
from web import Database


# Creates an APP for Flask
APP = Flask(__name__, static_url_path="/static")
# Sets the secret key to use and sign cookies
APP.secret_key = 'Database Server'


def get_file_contents(filename):
    """
    This function opens the passed file and returns the contents of the file
    """
    # Returns the contents of the file
    return open("." + APP.static_url_path + filename, "r").read()


# Creates the base page
@APP.route('/')
def index():
    """
    This Flask function serves the landing page for the web system.
    """
    # Returns the retrieved contents of the file
    return get_file_contents("/templates/index.html")


# Creates the admin page
@APP.route('/admin')
def admin():
    """
    This Flask function serves the admin page for the web system.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Returns the retrieved contents of the file
        return get_file_contents("/templates/admin.html")
    # The logged in user is not an admin and is redirected to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates the page to check the password
@APP.route('/check_password', methods=['POST'])
def check_password():
    """
    This Flask function serves the page to change a password
    """
    # Retrieves the posted form data
    form = request.form
    # Establishes a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])

    # If the passwords match and the password was changed
    if all([form['password'] == form['new_password'],
            data_base.password_change(form['username'], form['password'])]):
        # Redirects to the main page
        return redirect('/main')
    # Redirects to the change password page to allow the user to try again
    return redirect('/change_password?message=<p>Password provided does '
                    'not meet all the requirements. Try again.</p>')


# Creates the page to check the values on the edit page
@APP.route('/check_edit', methods=['POST'])
def check_edit():
    """
    This Flask function processes the edit page to validate the values
    entered into the edit page
    """
    # Retrieves the posted form data into a dictionary to allow for change
    form = dict(request.form)
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])
    # If the password field was left empty
    if len(form['password']) == 0:
        # Delete password from the form data
        del form['password']
    # If the values entered on the edit page are valid
    if data_base.valid_user_values(form):
        # If the password is still in the form data
        if 'password' in form:
            # Change the password
            data_base.password_change(form['username'], form['password'])
            # Deletes the password from the form data
            del form['password']
        # Updates the user information with the data provided on the edit page
        data_base.update_user_info(form)
        # Redirects to the main page
        return redirect('/main')
    # One or more values added were incorrect and the user will be redirected
    # back to the edit page to try again
    return redirect('/edit?message=<p>Invalid values entered.</p>&'
                    f'user={form["username"]}')


# Creates the page to retrieve the password information to change a user's
# password
@APP.route('/change_password', methods=['GET'])
def change_password():
    """
    This Flask function serves the page to enable a password to be changed.
    """
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])
    # Retrieves the passed data
    get = request.args
    # Creates an empty message
    message = ''
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']

    # Returns the retrieved contents of the file
    page = get_file_contents("/templates/change_password.html")
    # Sets the newline character for web display
    data_base.set_newline_char('<br>')
    # Replaces placeholders with appropriate data
    page = page.replace('{message}', message)
    page = page.replace('{rules}', data_base.get_password_rules())
    page = page.replace('{username}', session['username'])
    # Returns page
    return page


# Creates the page to check login credentials
@APP.route('/check_login', methods=['POST'])
def check_login():
    """
    This Flask function processes the data from the login page to allow a
    user to login.
    """
    # Establish a connection to the database
    data_base = Database()
    # Retrieves the posted form data
    form = request.form
    # If the username was in the database with the provided password
    if data_base.login(form['username'], form['password']):
        # Store the username in a cookie
        session['username'] = form['username']
        # Sets a pointer to the database function to retrieve the access
        # type of the user from the database. This was done to facilitate
        # keeping the line that follows below 80 characters for PEP8 compliance
        retrieve = data_base.retrieve_user_access_type
        # Retrieves the user's access level
        session['access'] = retrieve(session['username'])
        # If the user is required to reset their password
        if data_base.is_password_reset_required():
            # Redirects the user to the page to force a password change
            return redirect('/change_password')
        # Redirects to the main page
        return redirect('/main')
    # Redirects to the login page
    return redirect('/login?message=<p>Invalid username or password</p>')


# Creates the login page to the database
@APP.route('/login', methods=['GET'])
def login():
    """
    This Flask function serves a page to allow a user to login to the database.
    """
    # Retrieves the passed data
    get = request.args
    # Creates an empty message
    message = ''
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']
    # Retrieves the contents of the file
    page = get_file_contents("/templates/login.html")
    # Replaces the placeholder with the appropriate data
    page = page.replace('{message}', message)
    # Returns the page
    return page


# Creates a page to allow a user to signup for access to the database
@APP.route('/signup', methods=['GET'])
def signup():
    """
    This Flask function creates a page to retrieve information for signing
    up a user for access to the database.
    """
    # Establish a connection to the database
    data_base = Database()
    # Retrieves the passed data
    get = request.args
    # Sets the message for the user
    message = 'Welcome. Add your information below to sign up.'
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']
    # Retrieves the contents of the file
    page = get_file_contents("/templates/new_user.html")

    # Sets the default access and reset values
    access_input = '<input type="hidden" name="access" value="user">'
    reset_input = '<input type="hidden" name="reset" value="0">'

    # Sets the newline character for web display
    data_base.set_newline_char('<br>')

    # Replaces the placeholders with the appropriate data
    page = page.replace('{title}', 'Database Signup')
    page = page.replace('{action}', '/check_signup')
    page = page.replace('{button}', 'Signup')
    page = page.replace('{access_input}', access_input)
    page = page.replace('{reset_input}', reset_input)
    page = page.replace('{username_rules}', data_base.get_username_rules())
    page = page.replace('{rules}', data_base.get_password_rules())
    page = page.replace('{message}', message)

    # Returns the page
    return page


def is_valid_email(email_address):
    """
    This function tests the input to see if it matches the email formatting.
    """
    # Returns True if the value is greater than 4, the @ symbol is in the
    # passed value, a period (.) is in the value, and the end matches one
    # of the values in the list; otherwise, False is returned
    return all([len(email_address) > 4, '@' in email_address,
                '.' in email_address,
                email_address.split('.')[-1] in ['edu', 'mil', 'com', 'org']])


# Creates the page to add the user
@APP.route('/add_user', methods=['POST', 'GET'])
def add_user():
    """
    This Flask function serves the page to add a user to the database.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database()
        # Retrieves the passed data
        get = request.args
        # Sets the message for the user
        message = 'Enter the details for the new user below:'
        # If there were arguments passed and a message was passed
        if len(get.keys()) > 0 and 'message' in get.keys():
            # Retrieve the passed message
            message = get['message']
        # Retrieves the contents of the file
        page = get_file_contents("/templates/new_user.html")

        # Sets the newline character for web display
        data_base.set_newline_char('<br>')

        # Replaces the placeholders with the appropriate data
        page = page.replace('{title}', 'Add User')
        page = page.replace('{action}', '/check_add')
        page = page.replace('{button}', 'Add User')
        page = page.replace('{username_rules}', data_base.get_username_rules())
        page = page.replace('{rules}', data_base.get_password_rules())
        page = page.replace('{message}', message)
        page = page.replace('{access_input}', access_dropdown())
        page = page.replace('{reset_input}', reset_dropdown())

        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


def access_dropdown(access='user'):
    """
    This function sets the dropdown options for the access level.
    """
    dropdown = '<p><label for="access">Access Level:</label>\n'
    dropdown += '  <select name="access" id="access">\n'
    # If the user's access type is an admin
    if access == 'admin':
        dropdown += '    <option value="admin" selected>admin</option>\n'
        dropdown += '    <option value="user">user</option>\n'
    # The user is not an admin
    else:
        dropdown += '    <option value="admin">admin</option>\n'
        dropdown += '    <option value="user" selected>user</option>\n'
    dropdown += '  </select></p>\n'

    # Returns the dropdown list
    return dropdown


def reset_dropdown(reset='1'):
    """
    This function sets the dropdown options for the reset options.
    """
    dropdown = '<p><br><br><label for="reset">Require Reset:</label>\n'
    dropdown += '  <select name="reset" id="reset">\n'
    # If the reset value is a 1 to force a reset
    if reset == '1':
        dropdown += '    <option value="1" selected>Yes</option>\n'
        dropdown += '    <option value="0">No</option>\n'
    # The reset value is not a 1
    else:
        dropdown += '    <option value="1">Yes</option>\n'
        dropdown += '    <option value="0" selected>No</option>\n'
    dropdown += '  </select></p>\n'

    # Returns the dropdown list
    return dropdown


# Creates the edit form
@APP.route('/edit', methods=['GET'])
def edit():
    """
    This Flask function serves the form to edit a user's database information.
    """
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])

    # Retrieves the passed data
    get = request.args
    # Creates an empty message
    message = ''
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']

    # If the user is not in the passed data
    if 'user' not in get:
        # Retrieves the user database information based on the user logged in
        user_data = data_base.get_user_data(session['username'])
    # The user is not in the passed data
    else:
        # Retrieves the user database information based on the passed username
        user_data = data_base.get_user_data(get['user'])

    # Retrieves the contents of the file
    page = get_file_contents("/templates/edit.html")

    # Replaces the placeholders with the appropriate data
    page = page.replace('{message}', message)
    page = page.replace('{header}', 'Edit User Information')
    page = page.replace('{username}', user_data['username'])
    page = page.replace('{first_name}', user_data['first_name'])
    page = page.replace('{last_name}', user_data['last_name'])
    page = page.replace('{email_address}', user_data['email_address'])

    # Sets the default access and reset values
    access_data = '<input type="hidden" name="access" value="user">'
    reset_data = '<input type="hidden" name="reset" value="0">'

    # If the logged in user is an admin
    if is_admin_user():
        # Sets the access and reset values to the dropdown option
        access_data = access_dropdown(user_data['access_type'])
        reset_data = reset_dropdown(user_data['reset'])

    # Replaces the placeholders with the appropriate data
    page = page.replace('{access_input}', access_data)
    page = page.replace('{reset_input}', reset_data)

    # Returns the page
    return page


# Creates the function to delete a user from the database
@APP.route('/delete', methods=['GET'])
def delete():
    """
    This Flask function deletes the selected user from the database.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database()
        # Retrieves the passed data
        get = request.args
        # Deletes the selected user from the database
        data_base.delete_user_from_table(get['user_to_delete'])
        # Returns the page
        return redirect('/users')
    # Redirects the user to the login page so they can try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates a function to check the values to sign up
@APP.route('/check_signup', methods=['POST'])
def check_signup():
    """
    This Flask function checks the values entered during signup to see if
    they are legitimate and sign up the user
    """
    # Establish a connection to the database
    data_base = Database()
    # Retrieves the posted form data
    form = request.form
    # If all the values entered on the form are valid
    if data_base.valid_user_values(form):
        # Adds the user to the database
        data_base.create_user(dict(form))
        # Stores the user's username and access information into a cookie
        session['username'] = form['username']
        session['access'] = form['access']
        # If the user is required to reset their password
        if data_base.is_password_reset_required():
            # Returns the page to allow them to change their password
            return redirect('/change_password')
        # Returns the page
        return redirect('/main')
    # Redirects the user to the signup page so they can try again
    return redirect('/signup?message=Invalid parameters entered. Enter '
                    'your information and try again.')


# Creates a function to check the values entered on the add user page
@APP.route('/check_add', methods=['POST'])
def check_add():
    """
    This function validates the values entered on the add user page.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the posted form data
        form = request.form
        # If valid values were entered on the add form
        if data_base.valid_user_values(form):
            # Adds the user to the database
            data_base.create_user(dict(form))
            # Redirects to the admin page
            return redirect('/admin')
    # Redirects to the add user page to try again
    return redirect('/add_user?message=Invalid parameters entered. Enter '
                    'the information again.')


# Creates a function to logout a user
@APP.route('/logout')
def logout():
    """
    This Flask function removes the username and access from the cookie
    to log out a user.
    """
    # If the username is in the session
    if 'username' in session:
        # Deletes the username from the session (cookie)
        del session['username']
    # If the access is in the session
    if 'access' in session:
        # Deletes the access from the session (cookie)
        del session['access']
    # Redirects the user to the login page
    return redirect('/login')


def is_admin_user():
    """
    This function checks to see if the logged in user is an admin
    """
    # Returns True if the username and access are in the session and the
    # access is admin; otherwise, returns False
    return all(['username' in session, 'access' in session,
                session['access'] == 'admin'])


# Creates a function to display the user information in the database
@APP.route('/users')
def users():
    """
    This Flask function retrieves all the users from the database and displays
    their information to the admin user that requested it.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the header information and the user data from the
        # database
        header, user_data = data_base.all_users_in_user_table()
        # Creates the web page to display
        page = '<html>\n<head><title>User Information</title>\n'
        page += '<link href="static/css/style.css" rel="stylesheet" '
        page += 'type="text/css" />\n'
        page += '</head>\n'
        page += '<body>\n'
        page += '<center><h1><b><u>Database Users</u></b></h1></center>'
        page += '<table border="10%" align="center">\n<tr><th align="center">'
        # Joins the header information for display
        page += '</th>\n<th align="center">'.join(header)
        page += '</th>\n'
        # Adds a column to delete the user
        page += '<th align="center">Delete</th>\n'
        page += '</tr>\n'
        # Steps through each user
        for user in user_data:
            page += '<tr><td>'
            # Joins the user information for display
            page += '</td>\n<td>'.join(user)
            page += '</td>\n'
            # Makes the username a link to allow their information to be
            # edited
            page = page.replace(f'<td>{user[0]}</td>',
                                f'<td><a href="/edit?user={user[0]}">'
                                f'{user[0]}</a></td>\n')
            # Adds a link to allow for the deletion of the user
            page += f'<td><a href="/delete?user_to_delete={user[0]}" '
            page += 'onclick="return confirm(\'Are you sure you want to '
            page += 'delete?\');">Delete</a></td>'
            page += '</tr>\n'
        page += '</table>\n'
        page += '<table align="center">\n'
        page += '<tr><td align="center">\n'
        page += '<form action="/main">\n'
        page += '<p><button type="submit">Main</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '<td align="center">\n'
        page += '<form action="/">\n'
        page += '<p><button type="submit">Home</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '<td align="center">\n'
        page += '<form action="/logout">\n'
        page += '<p><button type="submit">Logout</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '</tr></table>\n'
        page += '</body>\n</html>'
        # Returns the page
        return page
    # Redirects the user back to the login page to try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates a function for the landing page that is displayed after login
@APP.route('/main')
def main():
    """
    This Flask function serves the main page. This page is the landing page
    for the web system after a user is logged in.
    """
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Retrieves the contents of the file
        page = get_file_contents("/templates/main.html")

        # Replaces the placeholders with the appropriate data
        page = page.replace('{user}', session['username'])

        # Sets to empty the button areas
        users_button = ''
        admin_button = ''

        # If the logged in user is an admin
        if is_admin_user():
            # Set the admin button
            admin_button += '<form action = "/admin">\n'
            admin_button += '<p><button type="submit">Admin Page'
            admin_button += '</button></p>\n'
            admin_button += '</form>\n'

        # Replaces the placeholders with the appropriate data
        page = page.replace('{users}', users_button)
        page = page.replace('{admin}', admin_button)

        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# If the program is not being imported
if __name__ == '__main__':
    # Execute Flask to serve the pages
    APP.run(host='0.0.0.0', port=8080)

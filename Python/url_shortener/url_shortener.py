#! /usr/bin/env python3.11


# https://realpython.com/build-a-python-url-shortener-with-fastapi/#demo-your-python-url-shortener

from url_shortener_app.config import get_settings
print(get_settings().base_url)
print(get_settings().db_url)


#! /usr/bin/env python3.11


"""
This program calculates the years with the associated ages where all of the
ages of the individuals entered are all odd and all even. It will print out
the year and the ages for both scenarios. NOTE: The more birthdays added, the
less likely there will be a time where all the ages are odd or even. In
testing, three birthdays worked, but the dates tested for four did not produce
any all odd or all even results.
"""


# Import the required libraries
import sys
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import click


def clear_screen():
    """
    This function clears the screen.
    """
    # Clears the screen
    click.clear()


def get_number(message='What is the number of people for calculation? ',
               number=0):
    """
    This function retrieves the number of people to use in the calculation.
    The minimum is two and the maximum is nine.
    """
    # While the number of personnel is less than two or greater than 10
    while number < 2 or number > 10:
        # Try to execute the block of code
        try:
            # Retrieves the integer version of the number entered by the user
            number = int(input(message))
        # The user quit the program via CTRL^C
        except KeyboardInterrupt:
            # Informs the user
            print("\nCTRL^C, exiting...")
            # Exit the program
            sys.exit()
        # Catches a ValueError
        except ValueError:
            # Informs the user that an invalid number was entered
            print('Invalid number entered.')
    # Returns the number
    return number


def set_list(number):
    """
    This function returns a list with a default value in the specified
    number of slots.
    """
    # Returns a list with a default value in each element
    return [0]*number


def is_leap_year(year):
    """
    This function returns True if the year is a leap year and False otherwise.
    """
    # If the year is divisible by four and not divisible by 100 or if it is
    # divisible by 400.
    if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        # Returns True
        return True
    # Returns False
    return False


def is_valid_date(year, month, day):
    """
    This function determines if the year, month, and day entered by a user
    is valid. If it is, it returns True; otherwise, it returns False.
    """
    # Sets the number of days in each month
    days = [31, 29 if is_leap_year(year) else 28, 31, 30, 31, 30, 31, 31, 30,
            31, 30, 31]
    # Sets the numeric value of each month
    months = [n + 1 for n in range(0, 12)]
    # If the month is valid and the day falls in to the range for the month
    if month-1 in months and all([month in months, days[month-1] > 0,
                                  day <= days[month-1]]):
        # Returns True
        return True
    # Returns False
    return False


def get_birthdays(number):
    """
    This function retrieves birthdays for the specified number of people.
    """
    # Sets an array to store the retrieved birthdays
    birthdays = []
    # Steps through each birthday to retrieve
    for num in range(number):
        # Sets to indicate that it is not a valid birthday
        is_valid_birth = False
        # While it is not a valid birthday
        while not is_valid_birth:
            # Executes a try block
            try:
                # Retrieves the day
                day = int(input('Enter the day of birth for person number '
                                f'{num + 1}: '))
                # Retrieves the month
                month = int(input('Enter the month (number of the month) of '
                                  f'birth for person number {num + 1}: '))
                # Retrieves the year
                year = int(input('Enter the four digit year of birth for '
                                 f'person number {num + 1}: '))
                # If it is a valid date
                if is_valid_date(year, month, day):
                    # Adds the birthdate to the list
                    birthdays.append(datetime(year, month, day))
                    # Sets to exit the while loop
                    is_valid_birth = True
            # The user quit the program via CTRL^C
            except KeyboardInterrupt:
                # Informs the user
                print("CTRL^C, exiting...")
                # Exit the program
                sys.exit()
            # Catches a ValueError
            except ValueError:
                # Informs the user that the birthday entered was invalid
                print('Invalid birthday entered.')
    # Returns the list of birthdays
    return birthdays


def get_max_age(message="What is the maximum age for calculation? "):
    """
    This function retrieves the maximum age to perform the calculation.
    """
    # Set to indicate that a valid age has not been found
    is_valid_age = False
    # While the age is not valid
    while not is_valid_age:
        # Executes a try block
        try:
            # Retrieves the maximum age for calculation
            max_age = int(input(message))
            # Sets to indicate that the maximum age is valid
            is_valid_age = True
        # The user quit the program via CTRL^C
        except KeyboardInterrupt:
            # Informs the user
            print("CTRL^C, exiting...")
            # Exit the program
            sys.exit()
        # Catches a ValueError
        except ValueError:
            # Informs the user
            print('Invalid age entered. The age must be an integer.')
    # Returns the maximum age value
    return max_age


def check_if_valid_date(date_to_check, birthdays):
    """
    This function returns True if the date to start checking from is a year
    or more from the youngest person's birthday.
    """
    # Returns True if the date to start checking from is a year or more
    # from the youngest person's birthday; otherwise, returns False
    return all((relativedelta(date_to_check, birthday).years >= 1
                for birthday in birthdays))


def get_start_date(birthdays):
    """
    This function retrieves the date to start performing calculations. The
    date must be one year (exactly or any number of days) or more from the
    youngest person's birthday.
    """
    # Sets to indicate that the date is not valid
    valid_date = False
    # While the date is not valid
    while not valid_date:
        # Executes a try block
        try:
            # Sets the year
            year = int(input('Enter the four digit year to start from: '))
            # Sets the month
            month = int(input('Enter the number of the month to start from: '))
            # Sets the day
            day = int(input('Enter the day to start from: '))
            # If the date entered is valid
            if is_valid_date(year, month, day):
                # Sets the start date
                start_date = datetime(year, month, day)
                # Checks to see if the start date is valid
                valid_date = check_if_valid_date(start_date, birthdays)
        # The user quit the program via CTRL^C
        except KeyboardInterrupt:
            # Informs the user
            print("CTRL^C, exiting...")
            # Exit the program
            sys.exit()
        # Catches a ValueError
        except ValueError:
            # Informs the user
            print('The date entered is invalid. It must be greater than all '
                  'the birthdays by at least one year and must result in the '
                  'maximum age not being exceeded.')
    # Returns the start date
    return start_date


def set_ages(birthdays, start_date):
    """
    This function sets the ages of each of the people based upon the start
    date supplied and returns the ages in a list.
    """
    # Creates a list to store ages
    age_data = []
    # Steps through each of the birthdays
    for birthday in birthdays:
        # Sets the age for the person
        age_data.append(relativedelta(start_date, birthday).years)
    # Returns the list of ages
    return age_data


def find_matches(start_date, birthdays, max_age, odd=True):
    """
    This function determines the years at which all of the ages match either
    odd or even, if any, and returns them in a dictionary.
    """
    # Sets to indicate a max age has not been retrieved
    found_max_age = 0
    # Sets the working date to calculate the ages
    working_date = start_date
    # Creates a dictionary to store the year with the ages retrieved in the
    # year
    matches = {}
    # While the maximum age is less than or equal to the maximum age
    while found_max_age <= max_age:
        # Sets the ages at the current moment in time
        current_ages = set_ages(birthdays, working_date)
        # If the agges are all even or odd, depending on the request and
        # the maximum age found is less than or equal to the max age allowed
        if all([age % 2 == int(odd) for age in current_ages] +
               [found_max_age <= max_age]):
            # Stores the year with associated ages
            matches[working_date.year] = current_ages[:]
        # Sets the new maximum age
        found_max_age = max(current_ages)
        # Increments the working date by one day
        working_date += timedelta(days=1)
    # Returns the found matches
    return matches


def format_matches(matches, print_type):
    """
    This function displays match information, if any, to the user.
    """
    # If there were matches found
    if len(matches) > 0:
        # Informs the user
        print('Here are the years and ages where all of the individual\'s '
              f'ages are {print_type}:')
        # Steps through each matched year
        for year in matches:
            # Prints the year and associated ages
            print(f'{year}: {", ".join([str(y) for y in matches[year]])}')
    # There were no matches found
    else:
        # Informs the user
        print(f'There are no years where all the ages are {print_type}.')
    # Prints an extra new line character
    print()


def main():
    """
    This function performs the necessary actiosn to run the program.
    """
    # Clears the screen
    clear_screen()
    # Sets the number of people to calculate their ages
    people = get_number('Enter the number of people to compare '
                        '(2-10 inclusive): ')
    # Retrieves the birthdays for each of the people
    birthdays = get_birthdays(people)
    # Retrieves the maximum age used for calculations
    max_age = get_max_age()
    # Retrieves the date to use when starting to perform age calculations
    start_date = get_start_date(birthdays)
    # Clears the screen
    clear_screen()
    # Prints the result for each type
    format_matches(find_matches(start_date, birthdays, max_age), 'odd')
    format_matches(find_matches(start_date, birthdays, max_age, False), 'even')


# If this program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

#! /usr/bin/env python3.11

"""
This program creates a class that implements a sqlite3 database in Python for
use via web pages.
"""

# pylint: disable=R0902
# pylint: disable=R0904


# Modules to import
import re
import os
import sqlite3
from string import punctuation as P
from datetime import datetime
import bcrypt


class Database():
    """
    This is a class that implements a database in Python.
    """
    # Sets the attributes to enable hiding
    __initialize_db = False
    __username = ''
    __password = ''
    __pass_hash = ''
    __connection = ''
    __newline_char = '\n'
    __cursor_object = ''
    __valid_user = False
    __user_access_level = 'Invalid User'

    def __init__(self, database_name='db/accounts_sqlite3.db', username='new',
                 access=''):
        """
        This method initializes a database class in Python.
        """
        # Sets the database name
        self.database_name = database_name
        # If the database needs to be initialized
        if not self.is_db_usable():
            # If the OS separator value is in the database name
            if os.sep in self.database_name:
                # Determines the current working directory
                cur_dir = os.getcwd()
                # Steps through each directory in the database name
                for base_dir in self.database_name.split(os.sep)[0:-1]:
                    # Creates the directory found
                    os.mkdir(base_dir)
                    # Changes directory to the directory found
                    os.chdir(base_dir)
                # Returns back to the original directory
                os.chdir(cur_dir)
            # Creates the user table
            self.create_user_table()
        # Assigns the passed access value
        self.__user_access_level = access
        # Assigns the passed username value
        self.__username = username
        # Sets the connection to the database
        self.__connection = self.database_connection()
        # Sets the cursor object to the database
        self.__cursor_object = self.database_cursor()

    def set_newline_char(self, newline_char):
        """
        This method allows the setting of the newline character to either a
        newline or the break for web pages
        """
        # If the passed newline character is in the allowable options
        if newline_char in ['\n', '<br>']:
            # Sets the newline character value
            self.__newline_char = newline_char

    def get_newline_char(self):
        """
        This method retrieves the newline character
        """
        # Returns the newline character value
        return self.__newline_char

    def is_db_usable(self):
        """
        This method validates that a file is usable (readable and wrtitable)
        by the user.
        """
        # Returns the result of testing if the file is readable and writable
        return os.access(self.database_name, os.R_OK | os.W_OK)

    def get_username_rules(self):
        """
        This method displays the rules surrounding username creation.
        """
        # Sets the username rules
        rules = f'Username rules: {self.__newline_char}'
        rules += '  o must be betwen 5 and 24 (inclusive) in '
        rules += f'length {self.__newline_char}'
        rules += '  o must begin with either an uppercase or lowercase '
        rules += f'letter {self.__newline_char}'
        rules += '  o can only contain letters (uppercase or lowercase), '
        rules += f'numbers, digits, and underscores {self.__newline_char}'
        rules += f'  o must not end with an underscore {self.__newline_char}'
        # Returns the username rules
        return rules

    def check_username(self, username):
        """
        This method validates a username.
        """
        # Sets if the username is valid
        status = self.is_valid_username(username)
        # If the username was valid
        if status:
            self.__username = username
        # Returns the status
        return status

    def is_valid_user_password(self):
        """
        This method validates the password entered to see if it matches
        the stored password hash for the user.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash FROM tblUsers WHERE '
                                     'username=?', (self.__username,))
        # Retrieves the value from the query
        value = self.__cursor_object.fetchone()
        # If a value was returned
        if value:
            # Compares the user password to what was retrieved from the
            # database and returns True of False depending on whether they
            # matched
            return bcrypt.checkpw(self.__password.encode('utf-8'), value[0])
        # If no value was retrieved, return False
        return False

    def insert_user_into_table(self, user_info):
        """
        This method inserts a user into the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('INSERT INTO tblUsers (username, '
                                     'pass_hash,  first_name, last_name, '
                                     'email_address, access_type, reset, '
                                     'date_added, last_login) VALUES ('
                                     ':username, :pass_hash, :first_name, '
                                     ':last_name, :email_address, '
                                     ':access_type, :reset, :date_added,'
                                     ':last_login)',
                                     (user_info['username'],
                                      user_info['pass_hash'],
                                      user_info['first_name'],
                                      user_info['last_name'],
                                      user_info['email_address'],
                                      user_info['access'],
                                      user_info['reset'],
                                      user_info['date_added'],
                                      user_info['last_login']))

        # Commits the query
        self.__connection.commit()

    def update_user_info(self, user_info):
        """
        This method inserts a user into the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET first_name = (?), '
                                     'last_name = (?), email_address = (?), '
                                     'access_type = (?), reset = (?) '
                                     'WHERE username = (?);',
                                     (user_info['first_name'],
                                      user_info['last_name'],
                                      user_info['email_address'],
                                      user_info['access'],
                                      user_info['reset'],
                                      user_info['username']))

        # Commits the query
        self.__connection.commit()

    def any_users_in_user_table(self):
        """
        This method returns True if there are users in the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT username FROM tblUsers')
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def all_users_in_user_table(self):
        """
        This method retrieves requested information for all users from
        the database.
        """
        # Sets the order of items to retrieve
        order = ['username', 'first_name', 'last_name', 'email_address',
                 'access_type', 'reset', 'date_added', 'last_login']
        # Sets the header formatted of the items retrieved
        order_headers = [' '.join(header.split('_')).title()
                         if '_' in header else header.title()
                         for header in order]
        # SQL statement
        sql = f'SELECT {", ".join(order)} FROM tblUsers;'
        # Executes the SQL code
        self.__cursor_object.execute(sql)
        # Returns the header and the retrieved user information
        return(order_headers, self.__cursor_object.fetchall())

    def get_user_data(self, username):
        """
        This method retrieves requested information for the requested user
        from the database.
        """
        order = ['username', 'first_name', 'last_name', 'email_address',
                 'access_type', 'reset', 'date_added', 'last_login']
        # SQL statement
        sql = f'SELECT {", ".join(order)} FROM tblUsers WHERE '
        sql += f'username="{username}";'
        # Executes the SQL code
        self.__cursor_object.execute(sql)
        # Returns the information retrieved for the user as a dictionary
        return dict(zip(order, self.__cursor_object.fetchone()))

    def is_password_reset_required(self):
        """
        This method returns True if the user must reset their password and
        False if they do not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT reset FROM tblUsers WHERE '
                                     'username=?', (self.__username,))
        # Returns True of the user must reset their password and False if
        # they do not
        return bool(int(self.__cursor_object.fetchone()[0]))

    def is_user_in_user_table(self, user):
        """
        This method returns True if the user is in the database and False if
        it is not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash FROM tblUsers WHERE '
                                     'username=?', (user,))
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def retrieve_user_access_type(self, username):
        """
        This method retrieves the user's access level from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT access_type FROM tblUsers WHERE '
                                     'username=?', (username,))
        # Returns the user's access type
        return self.__cursor_object.fetchone()[0]

    @staticmethod
    def is_valid_username(username):
        """
        This method checks the username to see if it matches the validation
        criteria.
        """
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_]*$'
        # The value is between 5 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, and underscores
        # The value does not end with an underscore
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(username) > 1 and all([4 < len(username) < 25,
                                          username[0].isalpha(),
                                          re.match(valid_chars, username),
                                          username[-1] != '_'])

    def password_change(self, username, password):
        """
        This method changes the password for the user
        """
        # If the user is changing their password or the user is an admin
        # changing another user and the password is valid
        if all([any([self.__username == username,
                     self.__user_access_level == 'admin']),
                self.is_valid_password(password)]):
            # Retrieves the hash for the password
            new_pass_hash = self.get_password_hash(password)
            # If the password hash is not None (empty)
            if new_pass_hash is not None:
                # Updates the user's password in the database
                self.update_user_password(username, new_pass_hash, "0")
                # Return True to indicate that the password was changed
                return True
        # Return False to indicate that the password was not changed
        return False

    def update_user_password(self, user, pass_hash, reset='0'):
        """
        This method updates the user's password, and reset value.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET pass_hash = (?), '
                                     'reset = (?) WHERE username=(?);',
                                     (pass_hash, reset, user))
        # Commits the change to the database
        self.__connection.commit()

    @staticmethod
    def is_valid_password(password):
        """
        This method checks the password to see if it matches the validation
        criteria.
        """
        # Sets the valid search parameters
        search = '[a-z]{3,}|[A-Z]{3,}|[0-9]{3,}|['+P+'}]{3,}'
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_'+P+']*$'
        # The value is between 12 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, punctuation, and
        # underscores
        # The value does not end with a punctuation character
        # The value does not contain three or more characters of the same
        # character set in a row
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(password) > 1 and all([11 < len(password) < 25,
                                          password[0].isalpha(),
                                          re.match(valid_chars, password),
                                          re.search(search, password) is None,
                                          password[-1] not in P])

    def get_password_rules(self):
        """
        This method returns the rules for passwords.
        """
        # Sets the password rules to the user
        rules = f'Password rules: {self.__newline_char}'
        rules += '  o must be betwen 12 and 24 (inclusive) in '
        rules += f'length{self.__newline_char}'
        rules += '  o must begin with either an uppercase or lowercase '
        rules += f'letter{self.__newline_char}'
        rules += '  o can only contain character from the character sets: '
        rules += 'letters (uppercase or lowercase), numbers, digits, and '
        rules += f'punctuation characters{self.__newline_char}'
        rules += '  o must not contain any more than two characters from the '
        rules += f'same character set{self.__newline_char}'
        rules += f'  o must not end with punctuation{self.__newline_char}'
        return rules

    def is_valid_user(self):
        """
        This method checks to see if the user's password is valid.
        """
        # Returns True if the user's password matches, False otherwise
        return self.is_valid_user_password()

    def database_connection(self):
        """
        This method establishes a connecgtion to the database
        """
        # Returns a connection to the database
        return sqlite3.connect(self.database_name)

    def database_cursor(self):
        """
        This method establishes the cursor to a place in the database
        """
        # Returns the cursor to the database
        return self.__connection.cursor()

    def create_user_table(self):
        """
        This method creates the user table within the database.
        """
        # Sets the connection to the database
        data_base = sqlite3.connect(self.database_name)
        # Sets the SQL code to create the database table
        data_base.execute('CREATE TABLE IF NOT EXISTS tblUsers '
                          '(user_id INTEGER PRIMARY KEY AUTOINCREMENT, '
                          'username CHAR(25) NOT NULL UNIQUE, '
                          'pass_hash CHAR(128) NOT NULL, '
                          'first_name CHAR(64) NOT NULL, '
                          'last_name CHAR(64) NOT NULL, '
                          'email_address CHAR(128) NOT NULL, '
                          'access_type CHAR(10) check(access_type = "admin" '
                          'or access_type = "user" or '
                          'access_type = "reviewer") '
                          'NOT NULL, reset CHAR(1) '
                          'check(reset = "0" or reset = "1") NOT NULL, '
                          'date_added real NOT NULL, last_login real '
                          'NOT NULL)')
        # Closes the database
        data_base.close()

    def login(self, username, password):
        """
        This method performs the actions to log a user into the database.
        """
        # If the username is not valid
        if not self.check_username(username):
            # Returns False to not allow the user to not login
            return False
        # Sets the password variable
        self.__password = password
        # Sets to see if the password provided matches the stored password
        self.__valid_user = self.is_valid_user()
        # If the provided password does not match the one in the database
        if not self.__valid_user:
            # Returns False to not allow the user to not login
            return False
        # Empties the password and hash values
        self.__password = ''
        self.__pass_hash = ''
        # Sets the user's access level
        self.__user_access_level =\
            self.retrieve_user_access_type(self.__username)
        # Updates the last login time for the user
        self.update_last_login(self.__username)
        # Returns True to indicate that the user has logged in
        return True

    @staticmethod
    def get_password_hash(password):
        """
        This method generates a hash of the password using bcrypt
        """
        # If there is a password
        if password:
            # Creates a hash of the password
            return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # Returns None if there was no password
        return None

    def create_user(self, user_info, access='user', reset='1'):
        """
        This method retrieves the username, sets their password, and reset
        value before updating the database.
        """
        # If the username is not valid
        if not self.check_username(user_info['username']):
            # Returns False to not allow the user to be created
            return False
        # Sets the access type possibilities
        access_types = ['admin', 'user', 'reviewer']
        # If no access value was provided in the dictionary
        if 'access' not in user_info or len(user_info['access']) == 0:
            # Sets to the default access value
            user_info['access'] = access
        # If there are no users in the database
        if not self.any_users_in_user_table():
            # Sets to indicate that the user will be an admin
            # The first person will be an admin by default
            user_info['access'] = 'admin'
        # If no reset value was provided in the dictionary
        if 'reset' not in user_info or len(user_info['reset']) == 0:
            # Sets to the default reset value
            user_info['reset'] = reset
        # The user is in the database or the password is not valid or
        # the access value is not valid
        if any([self.is_user_in_user_table(self.__username),
                not self.is_valid_password(user_info['password']),
                user_info['access'] not in access_types]):
            # Returns False to not allow the user to be created
            return False
        # Sets and stores the password hash
        user_info['pass_hash'] = self.get_password_hash(user_info['password'])
        # Sets the date the user was added
        user_info['date_added'] = f'{datetime.today()}'
        # If the username being added is the same as the user that is logged in
        if user_info['username'] == self.__username:
            # Sets the date of the last login value
            user_info['last_login'] = f'{datetime.today()}'
        # Adds the user to the database
        self.insert_user_into_table(user_info)
        # Empties the password and hash values
        self.__password = ''
        self.__pass_hash = ''
        # Set to indicaet that the user is valid
        self.__valid_user = True
        # Sets to have access to the user's access level
        self.__user_access_level = access
        # Returns True to indicate that the user was added to the database
        return True

    def update_last_login(self, user):
        """
        This method updates the user's password, and reset value.
        """
        # Executes the SQL code
        self.__cursor_object.execute('UPDATE tblUsers SET last_login = (?) '
                                     'WHERE username=(?);',
                                     (f'{datetime.today()}', user))

        # Commits the change to the database
        self.__connection.commit()

    def valid_user_values(self, user_data):
        """
        This method checks all the user data values to see if they are valid
        """
        # Creates an empty list
        valid_values = []
        # Steps through each of the keys
        for key in user_data.keys():
            # If the key is the username
            if key == 'username':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_username(user_data[key]))
            # If the key is the password
            if key == 'password':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_password(user_data[key]))
            # If the key is the first name
            if key == 'first_name':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_name(user_data[key]))
            # If the key is the last name
            if key == 'last_name':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_name(user_data[key]))
            # If the key is the email address
            if key == 'email_address':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_email(user_data[key]))
            # If the key is the access
            if key == 'access':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_access(user_data[key]))
            # If the key is the reset
            if key == 'reset':
                # Test the value to see if it is valid
                valid_values.append(self.is_valid_reset(user_data[key]))
        # Returns True if all values resolved to True; otherwise, it returns
        # False
        return all(valid_values)

    @staticmethod
    def is_valid_name(name_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the length of the value is greater than 1; otherwise,
        # return False
        return len(name_value) > 1

    @staticmethod
    def is_valid_email(email_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the length of the value is greater than 4,
        # the value contains a period (.), and the ending matches one
        # of the allowed endings; otherwise, returns False
        return all([len(email_value) > 4, '@' in email_value,
                    '.' in email_value,
                    email_value.split('.')[-1] in ['edu', 'mil', 'com',
                                                   'org']])

    @staticmethod
    def is_valid_access(access_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the value is in the allowed list; otherwise, returns
        # False
        return access_value in ['admin', 'user', 'reviewer']

    @staticmethod
    def is_valid_reset(reset_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the value is in the allowed list; otherwise, returns
        # False
        return reset_value in '01'

    def delete_user_from_table(self, user):
        """
        This method deletes the specified user from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('DELETE FROM tblUsers WHERE '
                                     'username=(?);', (user,))
        # Commits the change to the database
        self.__connection.commit()

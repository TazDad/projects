#! /usr/bin/env python3.8

"""
Implements an OOP iteration of Tic Tac Toe
"""

# pylint: disable=C0302
# pylint: disable=R0902
# pylint: disable=R0913


import re
import itertools
import os.path
import random
import json
from os import system, name


class TicTacToe():
    """
    Name:
    TicTacToe

    Description:
    Class to implement Tic Tac Toe.

    Parameters:
    None

    Returns:
    None
    """
    def __init__(self, player1_name="Player 1", player1_piece="X",
                 number_of_players=1, player2_name="Computer",
                 player2_piece="O", player_start=1,
                 record_data="ttt.json"):
        """
        Name:
        __init__

        Description:
        Constructor for the TicTacToe class.
        Sets up all the values (with defaults) required to establish a game.

        Parameters:
        self            - the instantiation of the current object
        player1_name     - sets the name of the first player
        player1_piece    - sets the piece value of the first player
        number_of_players      - sets the number of players; 1 or 2
        player2_name     - sets then name of the second player
        player2_piece    - sets the piece value of the second player
        record_data      - sets the file that contains records of game play

        Returns:
        None
        """
        # The default record file
        self._default_data_file = "ttt.json"
        # The player names (player 1/player 2)
        self._names = [check_player_name(name) for name in [player1_name,
                                                            player2_name]]
        # The player pieces (player 1/player 2)
        self._pieces = self.__check_player_pieces(player1_piece, player2_piece)
        # The number of players playing the game
        self._number_of_players =\
            self.__check_number_of_players(number_of_players)
        # The player's turn that it currently is
        self._player_turn = self.__check_player_value(player_start)
        # Set the gameboard position values
        self._position_values = self.__reset_position_values()
        # Sets the possible winning combinations
        self._winning_combinations = self.__set_winning_combinations()
        # Sets the record data file
        self._record_data =\
            self.__set_record_file(record_data)
        # Retrieves the records from the record data file
        self._stats = self.__read_records
        # Check whether a game exists between the two players,
        # if not create one
        self.__check_stats_exist()
        # Sets to allow game play
        self._play_game = True
        # Sets to indicate that the game is not over
        self._game_over = False

    @property
    def set_game_values(self):
        """
        Name:
        set_game_values

        Description:
        Allows for all the values required to establish a game to be set
        by the user.

        Parameters:
        self - the instantiation of the current object

        Returns:
        None
        """
        # Attempt to see if the dictionary exists and is not empty
        try:
            # If the dictionary exists and is not empty
            if bool(self._stats):
                # Writes the current records
                self.__write_records()
        # The dictionary did not exist
        except NameError:
            pass
        # Create an empty name list
        self._names = ["", ""]
        # Create an empty pieces list
        self._pieces = ["", ""]
        # Sets the number of players to be something other than 1 or 2
        self._number_of_players = -1
        # Sets the players turn to be something other than 1 or 2
        self._player_turn = -1
        # Sets the default records file name
        self._default_data_file = "ttt.json"
        # Sets the default file with the path
        self._record_data = f"./{self._default_data_file}"
        # Checks/Sets the default file with the path
        self._record_data =\
            self.__set_record_file(self._record_data)
        # Sets the possible winning combinations
        self._winning_combinations = self.__set_winning_combinations()
        # Sets the playable position values
        self._position_values = self.__reset_position_values()
        # Runs the series of prompts to set the required values
        self.__establish_parameters()
        # Read the records file and gets the records
        self._stats = self.__read_records()
        # Checks to see if the players have a record, if not create one
        self.__check_stats_exist()

    def __check_stats_exist(self):
        """
        Name:
        __check_stats_exist

        Description:
        Checks to see if there is a record of the players, if not
        it creates an empty one.

        Parameters:
        self - the instantiation of the current object

        Returns:
        None
        """
        # Sets the key value of the two players
        key = f"{self._names[0]}_{self._names[1]}"
        # If the records file was empty, create the dictionary
        if not isinstance(self._stats, dict):
            # Creates the dictionary with an empty entry for the two players
            self._stats = {key: {self._names[0]: [0, 0, 0],
                                 self._names[1]: [0, 0, 0]}}
        # If the key is not in the dictionary
        elif key not in self._stats:
            # Creates an empty entry for the two players
            self._stats[key] = {self._names[0]: [0, 0, 0],
                                self._names[1]: [0, 0, 0]}

    @staticmethod
    def __prompt_for_response(message):
        """
        Name:
        __prompt_for_response

        Description:
        Prompts the user with a yes (Y), returns True, or
        no (N), returns False, question

        Parameters:
        message - the Y/N question that prompts the user for a response

        Returns:
        True if the user enters "Y"
        False if the user enters "N"
        """
        # Defaults the response value
        response = "n"
        # While response is not a "Y" or "N"
        while not re.fullmatch("^[NY]$", response):
            # Prompts the user with the message, converts the response
            # to upper case, and stores the response
            response = input(message).upper()
        # If the response value was a "Y"
        if response == "Y":
            # Returns True
            return True
        # If the response value was a "N"
        # Returns False
        return False

    @staticmethod
    def __can_create_file(filename, remove="Y"):
        """
        Name:
        __can_create_file

        Description:
        Checks to see if a file can be created

        Parameters:
        filename    - the filename to create
        remove      - if the remove value is set to "Y",
                        the file will be removed after creation

        Returns:
        True if the file could be created
        False if the file could not be created
        """
        # Attempts to create a file
        try:
            # Attempts to open a file for writing
            tmp_file = open(filename, "w")
            # Closes the file
            tmp_file.close()
            # If the remove value is set to remove the file
            if remove == "Y":
                # Removes the file
                os.remove(filename)
            # Returns True to indicate that the file could be created
            return True
        # The file could not be created
        except PermissionError:
            # Returs False to indicate that the file could not be created
            return False

    @staticmethod
    def __set_record_file(record_data):
        """
        Name:
        __set_record_file

        Description:
        Checks to see if a file is readable, writable, or both depending
        on the option specified in the check_type

        Parameters:
        record_data          - the filename of the record data

        Returns:
        The record data file name with the path to the file
        Raises an error, if the file file or directory is not found or able
            to be written to
        """
        # Checks to see if the file is readable and writable
        if check_if_file_is(record_data, "b"):
            return record_data
        # The file is not readable or is not writable
        raise SystemExit("Unable to read or write to record file "
                         f"{record_data}, exiting...")

    @property
    def __change_player(self):
        """
        Name:
        __change_player

        Description:
        Changes the active player

        Parameters:
        None

        Returns:
        The opposite player
        """
        return int(not self._player_turn)

    def __check_valid_position(self, position):
        """
        Name:
        __check_valid_position

        Description:
        Checks to see if the position attempting to play has already been
        played (False) or if it is free to play (True)

        Parameters:
        position    - index position of the square attempting to play

        Returns:
        True if the position has not already been played
        False if the position has already been played
        """
        # If the position index value is equal to the index plus one
        if self._position_values[position] == position + 1:
            return True
        # The position index value is anything else
        return False

    @property
    def __get_position_to_play(self):
        """
        Name:
        __get_position_to_play

        Description:
        Prompts the player for the positon number that they would like
        to play until such time that they choose a position that has not
        already been played.

        Parameters:
        None

        Returns:
        A position that a player wants to play
        """
        # Sets to stay in the loop
        played = False
        # Loop until a valid playable position has been entered
        while not played:
            # Retrieves a position to play from the player
            pos = input("What position would you like to play? ")
            # If there was a value entered by the player
            if len(pos) > 0:
                # If the value entered was a number
                if pos.isdigit():
                    # Converts the number entered to a position index
                    pos = int(pos) - 1
                    # Determines whether the index has been played
                    played = self.__check_valid_position(pos)
        # Returns the position value entered
        return pos

    def __check_win(self, position, piece):
        """
        Name:
        __check_win

        Description:
        Check the piece against the possible winning combinations for the
        position entered

        Parameters:
        position    - The position the player would like to play
        piece       - The player's piece to play in the position specified

        Returns:
        True if the piece allows the player to win in the position specified
        False if the piece does not produce a winning combination
        """
        # Steps through each winning combination possibility for the position
        for j in self._winning_combinations[position]:
            # If the winning combinations possibilities matches three of
            # the pieces entered
            if(f"{self._position_values[position]}"
               f"{self._position_values[j[0]]}"
               f"{self._position_values[j[1]]}" == f"{piece}{piece}{piece}"):
                # Returns True to indicate the position entered would allow
                # the player to win
                return True
        # Returns False to indicate the position does not allow the player to
        # win
        return False

    @property
    def __check_tie(self):
        """
        Name:
        __check_tie

        Description:
        Checks to see if all the positions have been played. It does not
        check whether any player has won, only that all postions have
        been played.

        Parameters:
        None

        Returns:
        True if any of the positions are a number indicating they have not
                been played yet
        False if none of the positions are a number indicating there are
                positions that have not been played yet
        """
        # If there are no positions left to play
        if len([x for x in self._position_values if str(x).isdigit()]) == 0:
            return True
        # There are positions left to play
        return False

    def check_game_over(self, pos):
        """
        Name:
        check_game_over

        Description:
        Determines if the game is over due to a player winning or
        the game is a tie

        Parameters:
        pos     - The position to check against

        Returns:
        True if there is a winner or a tie along with the appropriate message
        False if the game is not over along with the appropriate message
        """
        # Sets to make the calls smaller later
        key = f"{self._names[0]}_{self._names[1]}"
        # If there is a winner
        if self.__check_win(pos, self._pieces[self._player_turn]):
            # Increases the current player's turn winning record by one
            self._stats[key][self._names[self._player_turn]][0] += 1
            # Increases the other player's turn losses record by one
            self._stats[key][self._names[int(not self._player_turn)]][1] += 1
            # Returns True to indicate that the game is over and a
            # message saying who won
            return (True, f"{self._names[self._player_turn]} won")
        # If the game is a tie
        if self.__check_tie:
            # Increases each player's tie record by one
            self._stats[key][self._names[self._player_turn]][2] += 1
            self._stats[key][self._names[int(not self._player_turn)]][2] += 1
            # Returns True to indicate that the game is over and a
            # message saying the game is a tie
            return (True, "The game is a tie.")
        # Returns False to indicate that the game is not over and a
        # message saying that the games was not over
        return (False, "The game is not over.")

    def play_game(self):
        """
        Name:
        play_game

        Description:
        Performs the actions required in order to play Tic Tac Toe

        Parameters:
        None

        Returns:
        None
        """
        # Sets to allow game play
        self._play_game = True
        # While the player(s) want to continue playing the game
        while self._play_game:
            # Resets the position values
            self._position_values = self.__reset_position_values()
            # Sets to indicate that the game is not over
            self._game_over = False
            # Empties message variable
            message = ""
            # While the game is not over
            while not self._game_over:
                # If the current player is the computer
                if self._names[self._player_turn] == "Computer":
                    # Run the computer play and get the game status
                    (self._game_over, message) = self.__computer_play
                # It is a human player's turn
                else:
                    # Clears the screen
                    clear_screen()
                    # Prints the stats for the users at play
                    self.print_stat_area(self.get_game_key())
                    # Prints the gameboard with the current positional values
                    self.print_game_board(self._position_values)
                    # Prints whose turn it is
                    print(f"{self._names[self._player_turn]}'s turn:")
                    # Retrieves the position the player wants to play
                    position = self.__get_position_to_play
                    # Sets the position value to the current player's piece
                    self._position_values[position] =\
                        self._pieces[self._player_turn]
                    # Checks to get the status of the game
                    (self._game_over, message) = self.check_game_over(position)
                # If the game is not over
                if not self._game_over:
                    # Change the current player
                    self._player_turn = self.__change_player
            # Clear the screen
            clear_screen()
            # Prints the stats for the users at play
            self.print_stat_area(self.get_game_key())
            # Prints the gameboard with the current positional values
            self.print_game_board(self._position_values)
            # Prints the game ending message
            print(message)
            # Prompts the user to determine if they want to play again
            self._play_game = self.__prompt_for_response("Would you like to "
                                                         "play again (Y/N): ")
            # Change the current player to allow for a new player to start
            # each game
            self._player_turn = self.__change_player
        # Writes the current status of records
        self.__write_records()

    def __write_records(self):
        """
        Name:
        __write_records

        Description:
        Writes the current records to the record file

        Parameters:
        None

        Returns:
        None
        """
        # Opens the file for writing
        record_file = open(self._record_data, "w")
        # Prints the stats to the file
        json.dump(self._stats, record_file)
        # Closes the file
        record_file.close()

    @property
    def __computer_play(self):
        """
        Name:
        __computer_play

        Description:
        Runs the routines in order for the comuter to play

        Parameters:
        None

        Returns:
        True if the game is over and the computer won
        False if the game is not over
        """
        # Retrieves the position for the computer to play
        pos = self.__get_computer_position
        # Sets the position to the computer's game piece
        self._position_values[pos] = self._pieces[self._player_turn]
        # Returns that the game is over and the computer won or
        # that the game is not over
        return self.check_game_over(pos)

    @property
    def __get_computer_position(self):
        """
        Name:
        __get_computer_position

        Description:
        Determines a position for the computer to play

        Parameters:
        None

        Returns:
        The position for the computer to play
        """
        # Set to indicate that a position has not been found
        position_found = False
        # Retrieves a position where the computer can win
        pos = self.get_win_position(self._pieces[1])
        # If the position is greater than 0
        if pos >= 0:
            # Set to indicate that a position has been found
            position_found = True
        # The computer does not have a position where it can win
        else:
            # Retrieves a position where player can win
            pos = self.get_win_position(self._pieces[0])
        # If the position is greater than 0
        if pos >= 0:
            # Set to indicate that a position has been found
            position_found = True
        # If the middle position is open
        elif self._position_values[4] == 5:
            # Sets the position to the middle position
            pos = 4
            # Set to indicate that a position has been found
            position_found = True
        # While no position has been found
        while not position_found:
            # Select a random index
            pos = random.choice(list(range(9)))
            # Determines whether the position is an open position
            position_found = self.__check_valid_position(pos)
        # Returns the position found
        return pos

    def get_win_position(self, piece):
        """
        Name:
        get_win_position

        Description:
        Determines if there is a position where the piece tested can allow a
        player to win.

        Parameters:
        piece       - The player's piece to test if there is a position where
                        it could be played to allow a user to win

        Returns:
        The position value where the piece could be played for a win or
        a -1 to indicate that there is no position where the piece would
        allow a player to win
        """
        # Step through each key (index/position)
        for i in self._winning_combinations:
            # Step through each winning combination
            for j in self._winning_combinations[i]:
                # If there are two of the pieces together and the
                # key (index/position) has not been played
                if all([self._position_values[i] == i + 1,
                        f"{self._position_values[j[0]]}"
                        f"{self._position_values[j[1]]}"
                        == f"{piece}{piece}"]):
                    # Return the position that would allow a user to win
                    return i
        # Return -1 to indicate there is no position that would allow a
        # user to win
        return -1

    def print_stat_area(self, game_key):
        """
        Name:
        print_stat_area

        Description:
        Prints the stored records for the game key specified

        Parameters:
        game_key     - the key for the records to print

        Returns:
        None
        """
        # Sets the stats information to determine the length next
        stats = itertools.chain.from_iterable(self._stats[game_key].values())
        # Determines the length of the largest win, lose, draw record
        # for formatting when printing
        records_length = len(str(max(stats)))
        # The display order for the records
        order = ("Wins", "Losses", "Draws")
        # Determines the largest order value for formatting when printing
        order_max = max([len(x) for x in order])
        # Gets the players from the record
        players = list(self._stats[game_key].keys())
        # Prints the header (names of players)
        print(" " * (order_max+2),
              f"{players[0]}".center(max([len(players[0]), records_length])),
              " " * 2,
              f"{players[1]}".center(max([len(players[1]), records_length])))
        # Sets the beginning of the order list
        order_index = 0
        # While there are values in the order index
        while order_index < len(order):
            # Prints the players records information for each order
            print(f"{order[order_index]}".rjust(order_max), ":",
                  f"{self._stats[game_key][players[0]][order_index]}"
                  .center(max([len(players[0]), records_length])), " " * 2,
                  f"{self._stats[game_key][players[1]][order_index]}"
                  .center(max([len(players[1]), records_length])))
            # Increments the order index by one
            order_index += 1
        # Prints a new line
        print()

    @staticmethod
    def __set_winning_combinations():
        """
        Name:
        __set_winning_combinations

        Description:
        Returns the possible winning combinations

        Parameters:
        None

        Returns:
        A dictionary of the possible winning combinations
        """
        # Returns a dictionary of the possible winning combinations
        return({0: [[1, 2], [3, 6], [4, 8]],
                1: [[0, 2], [4, 7]],
                2: [[0, 1], [4, 6], [5, 8]],
                3: [[0, 6], [4, 5]],
                4: [[0, 8], [1, 7], [2, 6], [3, 5]],
                5: [[2, 8], [3, 4]],
                6: [[0, 3], [2, 4], [7, 8]],
                7: [[1, 4], [6, 8]],
                8: [[0, 4], [2, 5], [6, 7]]})

    @staticmethod
    def __reset_position_values():
        """
        Name:
        __reset_position_values

        Description:
        Returns default values of the positions

        Parameters:
        None

        Returns:
        The default position values
        """
        return [1, 2, 3, 4, 5, 6, 7, 8, 9]

    @staticmethod
    def print_game_board(position_values):
        """
        Name:
        print_game_board

        Description:
        Prints the gameboard with the current values of the positions

        Parameters:
        position_values      - The current position values

        Returns:
        None
        """
        # The first index position of the position values
        index = 0
        # Steps through each position value
        while index < len(position_values):
            # If the index matches
            if index in [0, 3, 6]:
                # Prints the line
                print(" " * 4, "|", " " * 3, "|", " " * 4)
            # If the index matches
            elif index in [2, 5, 8]:
                # Prints the line
                print(" " * 4, "|", " " * 3, "|", " " * 4)
                # If it is not the last element
                if index != 8:
                    # Prints the line
                    print("-" * 17)
            # Any other index value
            else:
                # Prints the position values
                print("{:^5}|{:^5}|{:^5}".format(position_values[index-1],
                                                 position_values[index],
                                                 position_values[index+1]))
            # Increments the index value by 1
            index += 1
        # Prints a newline
        print()

    @staticmethod
    def __check_number_of_players(number_of_players):
        """
        Name:
        __check_number_of_players

        Description:
        Validates that the number of players is a valid number

        Parameters:
        number_of_players     - The current position values

        Returns:
        Returns the number of players, if valid or
        Raises an error if the value passed is not valid
        """
        # If the number of players passed is either 1 or 2
        if number_of_players in [1, 2]:
            # Returns the number of players
            return number_of_players
        # The number of players is not valid
        # Raises an error if an invalid number was entered
        raise SystemExit("Invalid number of players entered "
                         f"({number_of_players}). Must be 1 "
                         "(human vs. computer) or 2 (human vs. human), "
                         "exiting...")

    @staticmethod
    def __check_player_value(player_value):
        """
        Name:
        __check_player_value

        Description:
        Checks that the player value is valid

        Parameters:
        player_value     - The player number

        Returns:
        Returns the index value of the players or
        Raises an error
        """
        # If the player value is one or two
        if player_value in [1, 2]:
            # Returns the index value of the players
            return player_value - 1
        # If the player value is not valid
        # Raises an error if an invalid number was entered
        raise SystemExit(f"Invalid player value entered ({player_value}). "
                         "Must be 1 (for Player 1) or 2 (for Player 2), "
                         "exiting...")

    @staticmethod
    def __check_player_pieces(player1_piece, player2_piece):
        """
        Name:
        __check_player_pieces

        Description:
        Validates that the player pieces are valid and they are different

        Parameters:
        player1_piece    - The piece selected for player 1
        player2_piece    - The piece selected for player 2

        Returns:
        Returns the player pieces in a list or
        Raises an error if the values are not valid pieces or they are the
                same
        """
        # If the pieces are valid and not the same piece
        if all([re.fullmatch("^[OX]$", player1_piece),
                re.fullmatch("^[OX]$", player2_piece),
                player1_piece != player2_piece]):
            # Returns the list of player pieces
            return ([player1_piece, player2_piece])
        # The player pieces are not valid or are the same value
        # Raises an error if the pieces are not valid or the same
        raise SystemExit("Invalid player pieces entered. Valid options "
                         "are 'X' or 'O' unique to each player. The "
                         f"values entered are '{player1_piece}' "
                         f"(for Player 1) and '{player2_piece}' "
                         "(for Player 2), exiting...")

    @property
    def __establish_parameters(self):
        """
        Name:
        __establish_parameters

        Description:
        Runs the routines to establish the values necessary to play a game

        Parameters:
        None

        Returns:
        None
        """
        # Retrieves and validates the number of players
        self._number_of_players =\
            self.__check_number_of_players(self.__get_number_of_players)
        # Creates the players for the game
        self.__create_players()
        # Gets the player whose turn was chosen to start the game
        self.__select_player_turn()
        # Sets the player's piece values
        self.__select_piece()

    @property
    def __select_player_turn(self):
        """
        Name:
        __select_player_turn

        Description:
        Prompts the players to determine whose turn it should be

        Parameters:
        None

        Returns:
        None
        """
        # Sets the player turn to the first player
        self._player_turn = 0
        # The first player does not want to go first
        if not self.__prompt_for_response(f"{self._names[0]}, would you "
                                          "like to go 1st: (Y/N) "):
            # Sets the player turn to the second player
            self._player_turn = 1

    @property
    def __select_piece(self):
        """
        Name:
        __select_piece

        Description:
        Prompts the players to determine which piece they are going to be

        Parameters:
        None

        Returns:
        None
        """
        # Sets the first player to X and second to O
        self._pieces = ["X", "O"]

        # If the first player would like to play as O
        if not self.__prompt_for_response(f"{self._names[0]}, would you "
                                          "like to play as X: (Y/N) "):
            # Sets the first player to X and second to O
            self._pieces = ["O", "X"]

    @property
    def __get_number_of_players(self):
        """
        Name:
        __get_number_of_players

        Description:
        Gets the number of players to play the game

        Parameters:
        None

        Returns:
        The number of players to play the game
        """
        # Sets the number of players to an invalid value
        number_of_players = 0
        # While the number of players entered is not a 1 or 2
        while number_of_players not in [1, 2]:
            # Gets the number of players from the player
            number_of_players = input("Enter the number of players: ")
            # If the value entered is a digit and the value was a 1 or 2
            if number_of_players.isdigit() and number_of_players in "12":
                # Sets the number of players
                number_of_players = int(number_of_players)
            # The value entered was invalid
            else:
                # Sets the number of players to an invalid value
                number_of_players = 0
        # Returns the number of players
        return number_of_players

    def __create_players(self):
        """
        Name:
        __create_players

        Description:
        Creates the chosen number of players

        Parameters:
        None

        Returns:
        None
        """
        # Retrieves the name of player 1
        self._names[0] = check_player_name(input(f"Enter the name"
                                                 " of player 1:  "))
        # If there are two players
        if self._number_of_players == 2:
            # Retrieves the name of player 2
            self._names[1] = check_player_name(input(f"Enter the name"
                                                     " of player 2:  "))
        # There is one player
        else:
            # Sets name of player 2 to the computer
            self._names[1] = "Computer"

    def get_player_name(self, player):
        """
        Name:
        get_player_name

        Description:
        Retrieves the name of the player

        Parameters:
        player      - the player number

        Returns:
        The name of the player in the position specified
        """
        # The name of the player in the position specified
        return self._names[player-1]

    def get_game_key(self):
        """
        Name:
        get_game_key

        Description:
        Get the key to the game statistics currently being played

        Parameters:
        None

        Returns:
        The key storing the statistics for this game
        """
        # Returns the key that stores the statistics for this game
        return self.get_player_name(1) + "_" + self.get_player_name(2)

    @property
    def write_records(self):
        """
        Name:
        write_records

        Description:
        Writes the statistics to the record data file

        Parameters:
        None

        Returns:
        None
        """
        # Opens the record data file for writing
        record = open(self._record_data, "w")
        # Writes the records to the file
        json.dump(self._stats, record)
        # Closes the record file
        record.close()

    @property
    def __read_records(self):
        """
        Name:
        read_records

        Description:
        Reads the statistics from the record data file

        Parameters:
        None

        Returns:
        None
        """
        # Opens the record data file for reading
        record = open(self._record_data, "r")
        # Reads the records from the file
        records = json.load(record)
        # Closes the record file
        record.close()
        # Returns records found
        return records


def check_if_file_is(filename, check_type):
    """
    Name:
    check_if_file_is

    Description:
    Checks to see if a file is readable, writable, or both depending
    on the option specified in the check_type

    Parameters:
    filename     - the filename to check
    check_type   - r, checks if the file is readable
                   w, checks if the file is writable
                   b or a, checks if the file is both readable and writable

    Returns:
    True if the file matches the criteria in check_type
    False if the file does not match the criteria in check_type
    """
    # If readable is to be checked and the file is readable
    if all([check_type == "r", os.access(filename, os.R_OK)]):
        return True
    # If writable is to be checked and the file is writable
    if all([check_type == "w", os.access(filename, os.W_OK)]):
        return True
    # If writable and readable are to be checked and the file is
    # able to be read and written to
    if all([check_type in "ab", os.access(filename, os.R_OK),
            os.access(filename, os.W_OK)]):
        return True
    # The action requested cannot be performed on the specified file
    return False


def clear_screen():
    """
    Name:
    clear_screen

    Description:
    Clear the screen

    Parameters:
    None

    Returns:
    None
    """
    # If the operating system is Windows
    if name == 'nt':
        # Execute the windows clear screen command
        system('cls')
    # The operating system is UNIX/LINUX
    else:
        # Execute the UNIX/LINUX clear screen command
        system('clear')


def check_player_name(player_name):
    """
    Name:
    check_player_name

    Description:
    Validates a player's name and returns it

    Parameters:
    player_name      - The proposed player name

    Returns:
    The players name if it matches the criteria or
    Raises an error if the criteria does not match
    """
    # If the length of the player name is greater than or equal to 2 and
    # the player name is only alphabet characters of any case
    while any([len(player_name) < 2,
               re.match("^[a-zA-Z]+$", player_name) is None]):
        player_name = input("Enter player name: ")
    # Returns the player name
    return player_name

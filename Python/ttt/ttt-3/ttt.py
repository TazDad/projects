#!/usr/bin/env python3.8

"""
Implements a version of Tic Tac Toe
"""

import signal
import sys
from tic_tac_toe import TicTacToe


def handle_signal(signum, stack_frame):
    """
    Name:
    handle_signal

    Description:
    Handles the user hitting CTRL+C

    Parameters:
    None

    Returns:
    None
    """
    # Informs the user that CTRL+C was hit
    print(f"\nYou pressed CTRL+C (signal: {signum})")
    print("Stack Frame:")
    print(f"\t{stack_frame}")
    print("Exiting...")
    # Exits the program
    sys.exit()


def main():
    """
    Name:
    main

    Description:
    Executes the main functions to call and initiate the TicTacToe class

    Parameters:
    None

    Returns:
    None
    """
    # Sets the function to handle the CTRL+C
    signal.signal(signal.SIGINT, handle_signal)
    # Attempts to execute the selected lines of code
    try:
        # Creates an instantiation of the TicTacToe class setting the
        # player 1 name
        new_game = TicTacToe()
        # Resets the game values
        # new_game.set_game_values
        # Plays the game
        new_game.play_game()
    # Catches the SystemExit exception
    except SystemExit as system_exit:
        # Prints information about the exception
        print(f"Caught an exception... {system_exit.args}")
    # Catches the EOFError exception
    except EOFError:
        # Prints information about the exception
        print("CTRL+D stopped the execution of the program.")


# If the program is running as the main program
if __name__ == "__main__":
    # Calls the main function
    main()

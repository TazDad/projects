#! /usr/bin/env python3.8

"""
Implements a game of Tic Tac Toe
"""
# pylint: disable=R0913
# pylint: disable=C0302

import re
import sys
import itertools
import os.path
import random
from os import system, name
import signal
import json


def set_game_values(game_values):
    """
    Name:
    set_game_values

    Description:
    Allows for all the values required to establish a game to be set
    by the user.

    Parameters:
    game_values - the dictionary used for the current game values

    Returns:
    The dictionary of the current game values
    """
    # Attempt to see if the dictionary exists and is not empty
    try:
        # If the dictionary exists and is not empty
        if bool(game_values["game_stats"]):
            # Writes the current records
            write_records(game_values["game_stats"],
                          game_values["record_data"])
    # The dictionary did not exist
    except NameError:
        pass
    # Create an empty name list
    game_values["player_names"] = ["", ""]
    # Create an empty pieces list
    game_values["player_pieces"] = ["", ""]
    # Sets the number of players to be something other than 1 or 2
    game_values["number_of_players"] = -1
    # Sets the players turn to be something other than 1 or 2
    game_values["player_turn"] = -1
    # Sets the default records file name
    default_data_file = "ttt.json"
    # Sets the default file with the path
    game_values["record_data"] = f"{default_data_file}"
    # Checks/Sets the default file with the path
    game_values["record_data"] = set_record_file(game_values["record_data"])
    # Sets the possible winning combinations
    game_values["winningCombinations"] = set_winning_combinations()
    # Sets the playable position values
    game_values["position_values"] = reset_position_values()
    # Runs the series of prompts to set the required values
    game_values = establish_parameters(game_values)
    # Read the records file and gets the records
    game_values["game_stats"] = read_records(game_values["record_data"])
    # Checks to see if the players have a record, if not create one
    game_values["game_stats"] =\
        check_stats_exist(get_game_key(game_values),
                          game_values["game_stats"],
                          game_values['player_names'][0],
                          game_values['player_names'][1])
    # The dictionary of the game variables
    return game_values


def check_stats_exist(key, stats, player1_name, player2_name):
    """
    Name:
    check_stats_exist

    Description:
    Checks to see if there is a record of the players, if not
    it creates an empty one.

    Parameters:
    key   - the key to look for in the dictionary
    stats - the statistics dictionary
    player1_name - player 1's name
    player2_name - player 2's name

    Returns:
    The statistics dictionary
    """
    # If the statistics is not a dictionary, create the dictionary
    if not isinstance(stats, dict):
        # Creates the dictionary with an empty entry for the two players
        stats = {key: {player1_name: [0, 0, 0],
                       player2_name: [0, 0, 0]}}
    # If the key is not in the dictionary
    elif key not in stats:
        # Creates an empty entry for the two players
        stats[key] = {player1_name: [0, 0, 0],
                      player2_name: [0, 0, 0]}
    # Returns the statistics dictionary
    return stats


def prompt_for_response(message):
    """
    Name:
    prompt_for_response

    Description:
    Prompts the user with a yes (Y), returns True, or
    no (N), returns False, question

    Parameters:
    message - the Y/N question that prompts the user for a response

    Returns:
    True if the user enters "Y"
    False if the user enters "N"
    """
    # Defaults the response value
    response = "n"
    # While response is not a "Y" or "N"
    while not re.fullmatch("^[NY]$", response):
        # Prompts the user with the message, converts the response
        # to upper case, and stores the response
        response = input(message).upper()
    # If the response value was a "Y"
    if response == "Y":
        # Returns True
        return True
    # If the response value was a "N"
    # Returns False
    return False


def check_if_file_is(filename, check_type):
    """
    Name:
    check_if_file_is

    Description:
    Checks to see if a file is readable, writable, or both depending
    on the option specified in the check_type

    Parameters:
    filename    - the filename to check
    check_type   - r, checks if the file is readable
                  w, checks if the file is writable
                  b or a, checks if the file is both readable and writable

    Returns:
    True if the file matches the criteria in check_type
    False if the file does not match the criteria in check_type
    """
    # If readable is to be checked and the file is readable
    if check_type == "r" and os.access(filename, os.R_OK):
        return True
    # If writable is to be checked and the file is writable
    if check_type == "w" and os.access(filename, os.W_OK):
        return True
    # If writable and readable are to be checked and the file is
    # able to be read and written to
    if check_type in 'ab' and os.access(filename, os.R_OK) and\
       os.access(filename, os.W_OK):
        return True
    # The action requested cannot be performed on the specified file
    return False


def set_record_file(record_data):
    """
    Name:
    set_record_file

    Description:
    Checks to see if a file is readable, writable, or both depending
    on the option specified in the check_type

    Parameters:
    record_data          - the filename of the record data

    Returns:
    The record data file name
    Raises an error, if the file found, cannot be read or written to
    """
    # Checks to see if the file is readable and writable
    if check_if_file_is(record_data, "b"):
        return record_data
    # The file is not readable or is not writable
    raise SystemExit(f"Unable to read or write to record file {record_data},"
                     " exiting...")


def change_player(player_turn):
    """
    Name:
    change_player

    Description:
    Changes the active player

    Parameters:
    None

    Returns:
    The opposite player
    """
    # Returns the opposite player value
    return int(not player_turn)


def check_valid_position(position_values, position):
    """
    Name:
    check_valid_position

    Description:
    Checks to see if the position attempting to play has already been
    played (False) or if it is free to play (True)

    Parameters:
    position    - index position of the square attempting to play

    Returns:
    True if the position has not already been played
    False if the position has already been played
    """
    # If the position index value is equal to the index plus one
    if position_values[position] == position+1:
        return True
    # The position index value is anything else
    return False


def get_position_to_play(position_values):
    """
    Name:
    get_position_to_play

    Description:
    Prompts the player for the positon number that they would like
    to play until such time that they choose a position that has not
    already been played.

    Parameters:
    The current position values

    Returns:
    A position that a player wants to play
    """
    # Sets to stay in the loop
    played = False
    # Loop until a valid playable position has been entered
    while not played:
        # Retrieves a position to play from the player
        pos = input("What position would you like to play? ")
        # If there was a value entered by the player
        if len(pos) > 0:
            # If the value entered was a number
            if pos.isdigit():
                # Converts the number entered to a position index
                pos = int(pos) - 1
                # Determines whether the index has been played
                played = check_valid_position(position_values, pos)
    # Returns the position value entered
    return pos


def clear_screen():
    """
    Name:
    clear_screen

    Description:
    Clear the screen

    Parameters:
    None

    Returns:
    None
    """
    # If the operating system is Windows
    if name == 'nt':
        # Execute the windows clear screen command
        system('cls')
    # The operating system is UNIX/LINUX
    else:
        # Execute the UNIX/LINUX clear screen command
        system('clear')


def check_win(game_values, position):
    """
    Name:
    check_win

    Description:
    Check the piece against the possible winning combinations for the
    position entered

    Parameters:
    game_values  - The dictionary containing the game's information
    position     - The position the player would like to play

    Returns:
    True if the piece allows the player to win in the position specified
    False if the piece does not produce a winning combination
    """
    # Steps through each winning combination possibility for the position
    for j in game_values["winningCombinations"][position]:
        # If the winning combinations possibilities matches three of
        # the pieces entered
        if(f"{game_values['position_values'][position]}"
           f"{game_values['position_values'][j[0]]}"
           f"{game_values['position_values'][j[1]]}" ==
           f"{game_values['player_pieces'][game_values['player_turn']]}"
           f"{game_values['player_pieces'][game_values['player_turn']]}"
           f"{game_values['player_pieces'][game_values['player_turn']]}"):
            # Returns True to indicate the position entered would allow
            # the player to win
            return True
    # Returns False to indicate the position does not allow the player to win
    return False


def check_tie(position_values):
    """
    Name:
    check_tie

    Description:
    Checks to see if all the positions have been played. It does not
    check whether any player has won, only that all postions have
    been played.

    Parameters:
    position_values - the current value of the posiitons

    Returns:
    True if any of the positions are a number indicating they have not
            been played yet
    False if none of the positions are a number indicating there are
            positions that have not been played yet
    """
    # If there are no positions left to play
    if len([x for x in position_values if str(x).isdigit()]) == 0:
        return True
    # There are positions left to play
    return False


def check_game_over(game_values, pos):
    """
    Name:
    check_game_over

    Description:
    Determines if the game is over due to a player winning or
    the game is a tie

    Parameters:
    game_values - The dictionary containing the game's information
    pos         - The position to check against

    Returns:
    True if there is a winner or a tie along with the appropriate message
    False if the game is not over along with the appropriate message
    """
    # If there is a winner
    if check_win(game_values, pos):
        # Increases the current player's turn winning record by one
        set_stat_values(game_values["game_stats"],
                        get_game_key(game_values),
                        game_values["player_names"]
                        [game_values["player_turn"]],
                        0)
        # Increases the other player's turn losses record by one
        set_stat_values(game_values["game_stats"],
                        get_game_key(game_values),
                        game_values["player_names"]
                        [int(not game_values["player_turn"])], 1)
        # Sets the game_over key to True
        game_values["game_over"] = True
        # Returns the game_values dictionary and a message saying who won
        return game_values,\
            f"{game_values['player_names'][game_values['player_turn']]} won"
    # If the game is a tie
    if check_tie(game_values["position_values"]):
        # Increases each player's tie record by one
        game_values["game_stats"] = set_stat_values(game_values["game_stats"],
                                                    get_game_key(game_values),
                                                    game_values["player_names"]
                                                    [game_values["player_"
                                                                 "turn"]], 2)
        game_values["game_stats"] = set_stat_values(game_values["game_stats"],
                                                    get_game_key(game_values),
                                                    game_values["player_names"]
                                                    [int(not
                                                         game_values["player_"
                                                                     "turn"])],
                                                    2)
        # Sets the game_over key to True
        game_values["game_over"] = True
        # Returns the game_values dictionary and a message saying
        # the game is a tie
        return game_values, "The game is a tie."
    # Sets the game_over key to False
    game_values["game_over"] = False
    # Returns the game_values dictionary and a message saying that
    # the game was not over
    return game_values, "The game is not over."


def set_stat_values(stats, game_key, player, index):
    """
    Name:
    set_stat_values

    Description:
    Increments the desired statistic by one

    Parameters:
    stats        - The statistics dictionary
    game_key     - The key of the game being played
    player       - The player value to increment
    index        - The index of the position to increment

    Returns:
    The game's statistics dictionary with the value incremented by one
    """
    # Increments the specified value by one
    stats[game_key][player][index] += 1
    # Returns the statistics dictionary
    return stats


def play_game(game_values):
    """
    Name:
    play_game

    Description:
    Performs the actions required in order to play Tic Tac Toe

    Parameters:
    game_values - The dictionary containing the game's information

    Returns:
    None
    """
    # Sets to allow game play
    game_values["play_game"] = True
    # While the player(s) want to continue playing the game
    while game_values["play_game"]:
        # Resets the position values
        game_values["position_values"] = reset_position_values()
        # Sets to indicate that the game is not over
        game_values["game_over"] = False
        # Empties message variable
        message = ""
        # While the game is not over
        while not game_values["game_over"]:
            # If the current player is the computer
            if game_values["player_names"][game_values["player_turn"]] ==\
                    "Computer":
                # Run the computer play and get the game status
                (game_values, message) = computer_play(game_values)
            # It is a human player's turn
            else:
                # Clears the screen
                clear_screen()
                # Prints the stats for the users at play
                print_stat_area(game_values["game_stats"],
                                get_game_key(game_values))
                # Prints the gameboard with the current positional values
                print_game_board(game_values["position_values"])
                # Used to keep the print and val line below 80 characters
                step = game_values['player_turn']
                # Prints whose turn it is
                print(f"{game_values['player_names'][step]}'s turn:")
                # Retrieves the position the player wants to play
                position = get_position_to_play(game_values["position_values"])
                # Sets the position value to the current player's piece
                val = f"{game_values['player_pieces'][step]}"
                game_values["position_values"][position] = val
                # Checks to get the status of the game
                (game_values, message) = check_game_over(game_values, position)
            # If the game is not over
            if not game_values["game_over"]:
                # Change the current player
                game_values["player_turn"] =\
                 change_player(game_values["player_turn"])
        # Clear the screen
        clear_screen()
        # Prints the stats for the users at play
        print_stat_area(game_values["game_stats"], get_game_key(game_values))
        # Prints the gameboard with the current positional values
        print_game_board(game_values["position_values"])
        # Prints the game ending message
        print(message)
        # Prompts the user to determine if they want to play again
        game_values["play_game"] = prompt_for_response("Would you like to "
                                                       "play again (Y/N): ")
        # Change the current player to allow for a new player to start
        # each game
        game_values["player_turn"] = change_player(game_values["player_turn"])
    # Writes the current status of records
    write_records(game_values["game_stats"], game_values["record_data"])


def write_records(stats, record_data):
    """
    Name:
    write_records

    Description:
    Writes the current records to the record file

    Parameters:
    stats       - The statistics dictionary
    record_data  - The file to write the statistics to

    Returns:
    None
    """
    # Opens the file for writing
    record_file = open(record_data, "w")
    # Prints the stats to the file
    json.dump(stats, record_file)
    # Closes the file
    record_file.close()


def computer_play(game_values):
    """
    Name:
    computer_play

    Description:
    Runs the routines in order for the comuter to play

    Parameters:
    game_values  - The dictionary containing the game's information

    Returns:
    True if the game is over and the computer won or
    False if the game is not over
    And the message of the game
    """
    # Retrieves the position for the computer to play
    position = get_computer_position(game_values)
    # Sets the player's piece
    player_piece = game_values["player_pieces"][game_values["player_turn"]]
    # Sets the position to the computer's game piece
    game_values["position_values"][position] = player_piece
    # Returns the game_values dictionary and a game status message
    return check_game_over(game_values, position)


def get_computer_position(game_values):
    """
    Name:
    get_computer_position

    Description:
    Determines a position for the computer to play

    Parameters:
    game_values  - The dictionary containing the game's information

    Returns:
    The position for the computer to play
    """
    # Set to indicate that a position has not been found
    position_found = False
    # Retrieves a position where the computer can win
    pos = get_win_position(game_values, game_values["player_pieces"][1])
    # If the position is greater than 0
    if pos >= 0:
        # Set to indicate that a position has been found
        position_found = True
    # The computer does not have a position where it can win
    else:
        # Retrieves a position where player can win
        pos = get_win_position(game_values, game_values["player_pieces"][0])
    # If the position is greater than 0
    if pos >= 0:
        # Set to indicate that a position has been found
        position_found = True
    # If the middle position is open
    elif game_values["position_values"][4] == 5:
        # Sets the position to the middle position
        pos = 4
        # Set to indicate that a position has been found
        position_found = True
    # While no position has been found
    while not position_found:
        # Select a random index
        pos = random.choice(list(range(9)))
        # Determines whether the position is an open position
        position_found = check_valid_position(game_values["position_"
                                                          "values"], pos)
    # Returns the position found
    return pos


def get_win_position(game_values, piece):
    """
    Name:
    get_win_position

    Description:
    Determines if there is a position where the piece tested can allow a
    player to win.

    Parameters:
    game_values  - The dictionary containing the game's information
    piece       - The player's piece to test if there is a position where
                    it could be played to allow a user to win

    Returns:
    The position value where the piece could be played for a win or
    a -1 to indicate that there is no position where the piece would
    allow a player to win
    """
    # Step through each unplayed position
    for i in [x-1 for x in game_values["position_values"]
              if str(x).isdigit()]:
        # Step through each winning combination of the unplayed position
        for j in game_values["winningCombinations"][i]:
            # If there are two of the player's pieces together
            if(f"{game_values['position_values'][j[0]]}"
               f"{game_values['position_values'][j[1]]}" ==
               f"{piece}"*2):
                return i
    # Return -1 to indicate there is no position that would allow a
    # user to win
    return -1


def print_stat_area(stats, game_key):
    """
    Name:
    print_stat_area

    Description:
    Prints the stored records for the game key specified

    Parameters:
    stats    - the statistics for the current game
    game_key - the key for the records to print

    Returns:
    None
    """
    # Determines the length of the largest win, lose, draw record
    # for formatting when printing
    records_length = len(str(max(itertools.chain.from_iterable(
        stats[game_key].values()))))
    # The display order for the records
    order = ("Wins", "Losses", "Draws")
    # Determines the largest order value for formatting when printing
    order_max = max([len(x) for x in order])
    # Gets the players from the record
    players = list(stats[game_key].keys())
    # Prints the header (names of players)
    print(" " * (order_max+2), f"{players[0]}".center(max([len(players[0]),
                                                           records_length])),
          " " * 2, f"{players[1]}".center(max([len(players[1]),
                                               records_length])))
    # Sets the beginning of the order list
    order_index = 0
    # While there are values in the order index
    while order_index < len(order):
        # Prints the players records information for each order
        print(f"{order[order_index]}".rjust(order_max), ":",
              f"{stats[game_key][players[0]][order_index]}"
              .center(max([len(players[0]), records_length])), " " * 2,
              f"{stats[game_key][players[1]][order_index]}"
              .center(max([len(players[1]), records_length])))
        # Increments the order index by one
        order_index += 1
    # Prints a new line
    print()


def set_winning_combinations():
    """
    Name:
    set_winning_combinations

    Description:
    Returns the possible winning combinations

    Parameters:
    None

    Returns:
    A dictionary of the possible winning combinations
    """
    # Returns a dictionary of the possible winning combinations
    return({0: [[1, 2], [3, 6], [4, 8]],
            1: [[0, 2], [4, 7]],
            2: [[0, 1], [4, 6], [5, 8]],
            3: [[0, 6], [4, 5]],
            4: [[0, 8], [1, 7], [2, 6], [3, 5]],
            5: [[2, 8], [3, 4]],
            6: [[0, 3], [2, 4], [7, 8]],
            7: [[1, 4], [6, 8]],
            8: [[0, 4], [2, 5], [6, 7]]})


def reset_position_values():
    """
    Name:
    reset_position_values

    Description:
    Returns default values of the positions

    Parameters:
    None

    Returns:
    The default position values
    """
    return [1, 2, 3, 4, 5, 6, 7, 8, 9]


def check_player_name(player_name):
    """
    Name:
    check_player_name

    Description:
    Validates a player's name and returns it

    Parameters:
    player_name      - The proposed player name

    Returns:
    The players name if it matches the criteria or
    Raises an error if the criteria does not match
    """
    # If the length of the player name is greater than or equal to 2 and
    # the player name is only alphabet characters of any case
    while len(player_name) < 2 or re.match("^[a-zA-Z]+$", player_name) is None:
        player_name = create_players(1)[0]
    # Returns the player name
    return player_name


def print_game_board(position_values):
    """
    Name:
    print_game_board

    Description:
    Prints the gameboard with the current values of the positions

    Parameters:
    position_values      - The current position values

    Returns:
    None
    """
    # The first index position of the position values
    index = 0
    # Steps through each position value
    while index < len(position_values):
        # If the index matches
        if index in [0, 3, 6]:
            # Prints the line
            print(" " * 4, "|", " " * 3, "|", " " * 4)
        # If the index matches
        elif index in [2, 5, 8]:
            # Prints the line
            print(" " * 4, "|", " " * 3, "|", " " * 4)
            # If it is not the last element
            if index != 8:
                # Prints the line
                print("-" * 17)
        # Any other index value
        else:
            # Prints the position values
            print("{:^5}|{:^5}|{:^5}".format(position_values[index-1],
                                             position_values[index],
                                             position_values[index+1]))
        # Increments the index value by 1
        index += 1
    # Prints a newline
    print()


def check_num_players(number_of_players):
    """
    Name:
    check_num_players

    Description:
    Validates that the number of players is a valid number

    Parameters:
    number_of_players     - The current position values

    Returns:
    Returns the number of players, if valid or
    Raises an error if the value passed is not valid
    """
    # If the number of players passed is either 1 or 2
    if number_of_players in [1, 2]:
        # Returns the number of players
        return number_of_players
    # The number of players is not valid
    # Raises an error if an invalid number was entered
    raise SystemExit("Invalid number of players entered "
                     f"({number_of_players}). Must be 1 "
                     "(human vs. computer) or 2 (human vs. human), "
                     "exiting...")


def check_player_value(player_value):
    """
    Name:
    check_player_value

    Description:
    Checks that the player value is valid

    Parameters:
    player_value     - The player number

    Returns:
    Returns the index value of the players or
    Raises an error
    """
    # If the player value is one or two
    if player_value in [1, 2]:
        # Returns the index value of the players
        return player_value - 1
    # If the player value is not valid
    # Raises an error if an invalid number was entered
    raise SystemExit(f"Invalid player value entered ({player_value}). "
                     "Must be 1 (for Player 1) or 2 (for Player 2), "
                     "exiting...")


def check_player_pieces(player1_piece, player2_piece):
    """
    Name:
    check_player_pieces

    Description:
    Validates that the player pieces are valid and they are different

    Parameters:
    player1_piece    - The piece selected for player 1
    player2_piece    - The piece selected for player 2

    Returns:
    Returns the player pieces in a list or
    Raises an error if the values are not valid pieces or they are the same
    """
    # If the pieces are valid and not the same piece
    if(re.fullmatch("^[OX]$", player1_piece) and
       re.fullmatch("^[OX]$", player2_piece) and
       player1_piece != player2_piece):
        # Returns the list of player pieces
        return [player1_piece, player2_piece]
    # The player pieces are not valid or are the same value
    # Raises an error if the pieces are not valid or the same
    raise SystemExit("Invalid player pieces entered. Valid options "
                     "are 'X' or 'O' unique to each player. The "
                     f"values entered are '{player1_piece}' "
                     f"(for Player 1) and '{player2_piece}' "
                     "(for Player 2), exiting...")


def establish_parameters(game_values):
    """
    Name:
    establish_parameters

    Description:
    Runs the routines to establish the values necessary to play a game

    Parameters:
    game_values  - The dictionary containing the game's information

    Returns:
    None
    """

    # Retrieves and validates the number of players
    game_values["number_of_players"] =\
        check_num_players(get_number_of_players())
    # Creates the players for the game
    game_values["player_names"] = create_players(game_values["number_of_"
                                                             "players"])
    # Gets the player whose turn was chosen to start the game
    game_values["player_turn"] = select_player_turn(game_values["player"
                                                                "_names"][0])
    # Sets the player's piece values
    game_values["player_pieces"] = select_piece(game_values["player_"
                                                            "names"][0])
    # Returns the dictionary of the current game
    return game_values


def select_player_turn(player_name):
    """
    Name:
    select_player_turn

    Description:
    Prompts the players to determine whose turn it should be

    Parameters:
    player_name - the name of the player to ask

    Returns:
    The number of the player who will go first
    """
    # Sets the player turn to the first player
    player_turn = 0
    # If the first player wants to go second
    if not prompt_for_response(f"{player_name}, would you like"
                               " to go 1st: (Y/N) "):
        # Sets the player turn to the second player
        player_turn = 1
    # Returns the number of the player to go first
    return player_turn


def select_piece(player_name):
    """
    Name:
    select_piece

    Description:
    Prompts the players to determine which piece they are going to be

    Parameters:
    player_name - the name of the player to ask

    Returns:
    The list of the pieces for the players
    """
    # Sets the first player to X and the second player to O
    pieces = ["X", "O"]
    # If the first player would like to play as O
    if not prompt_for_response(f"{player_name}, would you like "
                               "to play as X: (Y/N) "):
        # Sets the first player to O
        pieces[0] = "O"
        # Sets the second player to X
        pieces[1] = "X"
    # Returns the list of the pieces of the players
    return pieces


def get_number_of_players():
    """
    Name:
    get_number_of_players

    Description:
    Gets the number of players to play the game

    Parameters:
    None

    Returns:
    The number of players to play the game
    """
    # Sets the number of players to an invalid value
    number_of_players = 0
    # While the number of players entered is not a 1 or 2
    while number_of_players not in [1, 2]:
        # Gets the number of players from the player
        number_of_players = input("Enter the number of players: ")
        # If the value entered is a digit and the value was a 1 or 2
        if number_of_players.isdigit() and number_of_players in "12":
            # Sets the number of players
            number_of_players = int(number_of_players)
        # The value entered was invalid
        else:
            # Sets the number of players to an invalid value
            number_of_players = 0
    # Returns the number of players
    return number_of_players


def create_players(number_of_players):
    """
    Name:
    create_players

    Description:
    Creates the chosen number of players

    Parameters:
    number_of_players - the number of players to create

    Returns:
    The list of the players
    """
    names = ["", "Computer"]
    # Retrieves the name of player 1
    names[0] = check_player_name(input(f"Enter the name of player 1:  "))
    # If there are two players
    if number_of_players == 2:
        # Retrieves the name of player 2
        names[1] = check_player_name(input(f"Enter the name of player 2:  "))
    # Returns the list of the players in the game
    return names


def get_player_name(game_values, player):
    """
    Name:
    get_player_name

    Description:
    Retrieves the name of the player

    Parameters:
    game_values  - The dictionary containing the game's information
    player      - the player number

    Returns:
    The name of the player in the position specified
    """
    # The name of the player in the position specified
    return game_values["player_names"][player-1]


def get_game_key(game_values):
    """
    Name:
    get_game_key

    Description:
    Get the key to the game statistics currently being played

    Parameters:
    game_values  - The dictionary containing the game's information

    Returns:
    The key storing the statistics for this game
    """
    # Returns the key that stores the statistics for this game
    return get_player_name(game_values, 1) + "_" +\
        get_player_name(game_values, 2)


def read_records(record_file):
    """
    Name:
    read_records

    Description:
    Reads the statistics from the record data file

    Parameters:
    record_file - the records file containing the statistics

    Returns:
    Returns the statistics found
    """
    # Opens the record data file for reading
    record = open(record_file, "r")
    # Reads the records from the file
    try:
        stats = json.load(record)
        # Closes the record file
        record.close()
    except (TypeError, SyntaxError):
        pass
    # Returns records found
    return stats


def handle_signal(signum, stack_frame):
    """
    Name:
    handle_signal

    Description:
    Handles the user hitting CTRL+C

    Parameters:
    None

    Returns:
    None
    """
    # Informs the user that CTRL+C was hit
    print(f"\nYou pressed CTRL+C (signal: {signum})")
    print("Stack Frame:")
    print(f"\t{stack_frame}")
    print("Exiting...")
    # Exits the program
    sys.exit()


def establish_game_parameters(player1_name="Player 1", player2_name="Computer",
                              number_of_players=1, player_start=1,
                              player1_piece="X", player2_piece="O",
                              record_data="ttt.json"):
    """
    Name:
    establish_game_parameters

    Description:
    Sets up all the values (with defaults) required to establish a game.

    Parameters:
    player1_name     - sets the name of the first player
    player2_name     - sets then name of the second player
    number_of_players      - sets the number of players; 1 or 2
    player_start     - sets the player number who will start the game
    player1_piece    - sets the piece value of the first player
    player2_piece    - sets the piece value of the second player
    record_data      - sets the file that contains records of game play

    Returns:
    The dictionary containing the game values
    """
    game_values = {
        "number_of_players": check_num_players(number_of_players),
        "player_names": [check_player_name(player1_name),
                         check_player_name(player2_name)],
        "player_pieces": check_player_pieces(player1_piece, player2_piece),
        "record_data": set_record_file(record_data),
        "player_turn": check_player_value(player_start),
        "position_values": reset_position_values(),
        "winningCombinations": set_winning_combinations(),
        "play_game": True,
        "game_over": False
        }

    # Check whether a game exists between the two players,
    # if not create one
    game_values["game_stats"] =\
        check_stats_exist(f"{game_values['player_names'][0]}_"
                          f"{game_values['player_names'][1]}",
                          read_records(game_values["record_data"]),
                          game_values["player_names"][0],
                          game_values["player_names"][1])
    # Returns the dictionary containing the game values
    return game_values


def main():
    """
    Name:
    main

    Description:
    Executes the main functions to call and initiate the TicTacToe class

    Parameters:
    None

    Returns:
    None
    """
    # Sets the function to handle the CTRL+C
    signal.signal(signal.SIGINT, handle_signal)

    # Attempts to execute the game
    try:
        game_values = establish_game_parameters()
        # Resets the game values
        # game_values = set_game_values(game_values)
        # Plays the game
        play_game(game_values)
    # Catches the SystemExit exception
    except SystemExit as system_exit:
        # Prints information about the exception
        print(f"Caught an exception... {system_exit.args}")
    # Catches the EOFError exception
    except EOFError:
        # Prints information about the exception
        print("CTRL+D stopped the execution of the program.")


# If the program is running as the main program
if __name__ == "__main__":
    # Calls the main function
    main()

#! /usr/bin/env python3.8


"""
Implements the game of Tic Tac Toe
"""

import re
import sys
from os import system, name
import signal


def prompt_for_response(message):
    """
    Name:
    prompt_for_response

    Description:
    Prompts the user with a yes (Y), returns True, or
    no (N), returns False, question

    Parameters:
    message - the Y/N question that prompts the user for a response

    Returns:
    True if the user enters "Y"
    False if the user enters "N"
    """
    # Defaults the response value
    response = "n"
    # While response is not a "Y" or "N"
    while not re.fullmatch("^[NY]$", response):
        # Prompts the user with the message, converts the response
        # to upper case, and stores the response
        response = input(message).upper()
    # If the response value was a "Y"
    if response == "Y":
        # Returns True
        return True
    # If the response value was a "N"
    # Returns False
    return False


def change_player(player_turn):
    """
    Name:
    change_player

    Description:
    Changes the active player

    Parameters:
    None

    Returns:
    The opposite player
    """
    # Returns the opposite player value
    return int(not player_turn)


def check_valid_position(position_values, position):
    """
    Name:
    check_valid_position

    Description:
    Checks to see if the position attempting to play has already been
    played (False) or if it is free to play (True)

    Parameters:
    position    - index position of the square attempting to play

    Returns:
    True if the position has not already been played
    False if the position has already been played
    """
    # If the position index value is equal to the index plus one
    if position_values[position] == position + 1:
        return True
    # The position index value is anything else
    return False


def get_position_to_play(position_values):
    """
    Name:
    get_position_to_play
     Description:
    Prompts the player for the positon number that they would like
    to play until such time that they choose a position that has not
    already been played.

    Parameters:
    The current position values

    Returns:
    A position that a player wants to play
    """
    # Sets to stay in the loop
    played = False
    # Loop until a valid playable position has been entered
    while not played:
        # Retrieves a position to play from the player
        pos = input("What position would you like to play? ")
        # If there was a value entered by the player
        if len(pos) > 0:
            # If the value entered was a number
            if pos.isdigit():
                # Converts the number entered to a position index
                pos = int(pos) - 1
                # Determines whether the index has been played
                played = check_valid_position(position_values, pos)
    # Returns the position value entered
    return pos


def clear_screen():
    """
    Name:
    clear_screen

    Description:
    Clear the screen

    Parameters:
    None

    Returns:
    None
    """
    # If the operating system is Windows
    if name == 'nt':
        # Execute the windows clear screen command
        system('cls')
    # The operating system is UNIX/LINUX
    else:
        # Execute the UNIX/LINUX clear screen command
        system('clear')


def check_win(game_values, position):
    """
    Name:
    check_win

    Description:
    Check the piece against the possible winning combinations for the
    position entered

    Parameters:
    game_values  - The dictionary containing the game's information
    position    - The position the player would like to play

    Returns:
    True if the piece allows the player to win in the position specified
    False if the piece does not produce a winning combination
    """
    # Steps through each winning combination possibility for the position
    for j in game_values["winning_combinations"][position]:
        # If the winning combinations possibilities matches three of
        # the pieces entered
        if(f"{game_values['position_values'][position]}"
           f"{game_values['position_values'][j[0]]}"
           f"{game_values['position_values'][j[1]]}" ==
           f"{game_values['player_pieces'][game_values['player_turn']]}"
           f"{game_values['player_pieces'][game_values['player_turn']]}"
           f"{game_values['player_pieces'][game_values['player_turn']]}"):
            # Returns True to indicate the position entered would allow
            # the player to win
            return True
    # Returns False to indicate the position does not allow the player to
    # win
    return False


def check_tie(position_values):
    """
    Name:
    check_tie

    Description:
    Checks to see if all the positions have been played. It does not
    check whether any player has won, only that all postions have
    been played.

    Parameters:
    position_values - the current value of the posiitons

    Returns:
    True if any of the positions are a number indicating they have not
            been played yet
    False if none of the positions are a number indicating there are
            positions that have not been played yet
    """
    # If there are no positions left to play
    if len([x for x in position_values if str(x).isdigit()]) == 0:
        return True
    # There are positions left to play
    return False


def check_game_over(game_values, pos):
    """
    Name:
    check_game_over

    Description:
    Determines if the game is over due to a player winning or
    the game is a tie

    Parameters:
    game_values - The dictionary containing the game's information
    pos        - The position to check against

    Returns:
    True if there is a winner or a tie along with the appropriate message
    False if the game is not over along with the appropriate message
    """
    # If there is a winner
    if check_win(game_values, pos):
        # Sets the game_over key to True
        game_values["game_over"] = True
        # Returns the game_values dictionary and a message saying who won
        return(game_values,
               f"{game_values['player_names'][game_values['player_turn']]}"
               " won")
    # If the game is a tie
    if check_tie(game_values["position_values"]):
        # Sets the game_over key to True
        game_values["game_over"] = True
        # Returns the game_values dictionary and a message saying
        # the game is a tie
        return(game_values, "The game is a tie.")

    # Sets the game_over key to False
    game_values["game_over"] = False
    # Returns the game_values dictionary and a message saying that
    # the game was not over
    return game_values, "The game is not over."


def play_game(game_values):
    """
    Name:
    play_game

    Description:
    Performs the actions required in order to play Tic Tac Toe

    Parameters:
    game_values - The dictionary containing the game's information

    Returns:
    None
    """
    # Sets to allow game play
    game_values["play_game"] = True
    # While the player(s) want to continue playing the game
    while game_values["play_game"]:
        # Resets the position values
        game_values["position_values"] = reset_position_values()
        # Sets to indicate that the game is not over
        game_values["game_over"] = False
        # Empties message variable
        message = ""
        # While the game is not over
        while not game_values["game_over"]:
            # Clears the screen
            clear_screen()
            # Prints the gameboard with the current positional values
            print_game_board(game_values["position_values"])
            # Prints whose turn it is
            print(f"{game_values['player_names'][game_values['player_turn']]}"
                  "'s turn:")
            # Retrieves the position the player wants to play
            position = get_position_to_play(game_values["position_values"])
            # Sets the position value to the current player's piece
            val = f"{game_values['player_pieces'][game_values['player_turn']]}"
            game_values["position_values"][position] = val
            # Checks to get the status of the game
            (game_values, message) = check_game_over(game_values, position)
            # If the game is not over
            if not game_values["game_over"]:
                # Change the current player
                game_values["player_turn"] =\
                 change_player(game_values["player_turn"])
        # Clear the screen
        clear_screen()
        # Prints the gameboard with the current positional values
        print_game_board(game_values["position_values"])
        # Prints the game ending message
        print(message)
        # Prompts the user to determine if they want to play again
        game_values["play_game"] = prompt_for_response("Would you like to "
                                                       "play again (Y/N): ")
        # Change the current player to allow for a new player to start
        # each game
        game_values["player_turn"] = change_player(game_values["player_turn"])


def get_win_position(game_values, piece):
    """
    Name:
    get_win_position

    Description:
    Determines if there is a position where the piece tested can allow a
    player to win.

    Parameters:
    game_values  - The dictionary containing the game's information
    piece       - The player's piece to test if there is a position where
                    it could be played to allow a user to win

    Returns:
    The position value where the piece could be played for a win or
    a -1 to indicate that there is no position where the piece would
    allow a player to win
    """
    # Step through each unplayed position
    for i in [x-1 for x in game_values["position_values"] if str(x).isdigit()]:
        # Step through each winning combination of the unplayed position
        for j in game_values["winning_combinations"][i]:
            # If there are two of the player's pieces together
            if(f"{game_values['position_values'][j[0]]}"
               f"{game_values['position_values'][j[1]]}" ==
               f"{piece}"*2):
                return i
    # Return -1 to indicate there is no position that would allow a
    # user to win
    return -1


def set_winning_combinations():
    """
    Name:
    set_winning_combinations

    Description:
    Returns the possible winning combinations

    Parameters:
    None

    Returns:
    A dictionary of the possible winning combinations
    """
    # Returns a dictionary of the possible winning combinations
    return({0: [[1, 2], [3, 6], [4, 8]],
            1: [[0, 2], [4, 7]],
            2: [[0, 1], [4, 6], [5, 8]],
            3: [[0, 6], [4, 5]],
            4: [[0, 8], [1, 7], [2, 6], [3, 5]],
            5: [[2, 8], [3, 4]],
            6: [[0, 3], [2, 4], [7, 8]],
            7: [[1, 4], [6, 8]],
            8: [[0, 4], [2, 5], [6, 7]]})


def reset_position_values():
    """
    Name:
    reset_position_values

    Description:
    Returns default values of the positions

    Parameters:
    None

    Returns:
    The default position values
    """
    return [1, 2, 3, 4, 5, 6, 7, 8, 9]


def check_player_name(player_name):
    """
    Name:
    check_player_name

    Description:
    Validates a player's name and returns it

    Parameters:
    player_name      - The proposed player name

    Returns:
    The players name if it matches the criteria or
    Raises an error if the criteria does not match
    """
    # If the length of the player name is greater than or equal to 2 and
    # the player name is only alphabet characters of any case
    if len(player_name) >= 2 and re.match("^[a-zA-Z]+$", player_name):
        # Returns the player name
        return player_name
    # The player name does not match the criteria
    # Raises an error
    raise SystemExit("Invalid player name entered. The length of a"
                     " name must be two or more characters and "
                     "valid letters of the alphabet, exiting...")


def print_game_board(pos_values):
    """
    Name:
    print_game_board

    Description:
    Prints the gameboard with the current values of the positions

    Parameters:
    pos_values      - The current position values

    Returns:
    None
    """
    # The first index position of the position values
    index = 0
    # Steps through each position value
    while index < len(pos_values):
        # If the index matches
        if index in [0, 3, 6]:
            # Prints the line
            print(" " * 4, "|", " " * 3, "|", " " * 4)
        # If the index matches
        elif index in [2, 5, 8]:
            # Prints the line
            print(" " * 4, "|", " " * 3, "|", " " * 4)
            # If it is not the last element
            if index != 8:
                # Prints the line
                print("-" * 17)
        # Any other index value
        else:
            # Prints the position values
            print("{:^5}|{:^5}|{:^5}".format(pos_values[index-1],
                                             pos_values[index],
                                             pos_values[index+1]))
        # Increments the index value by 1
        index += 1
    # Prints a newline
    print()


def check_player_value(player_value):
    """
    Name:
    check_player_value

    Description:
    Checks that the player value is valid

    Parameters:
    player_value     - The player number

    Returns:
    Returns the index value of the players or
    Raises an error
    """
    # If the player value is one or two
    if player_value in [1, 2]:
        # Returns the index value of the players
        return player_value - 1
    # If the player value is not valid
    # Raises an error if an invalid number was entered
    raise SystemExit("Invalid player value entered "
                     f"({player_value}). Must be 1 "
                     "(for Player 1) or 2 (for Player 2), "
                     "exiting...")


def check_player_pieces(player1_piece, player2_piece):
    """
    Name:
    check_player_pieces

    Description:
    Validates that the player pieces are valid and they are different

    Parameters:
    player1_piece    - The piece selected for player 1
    player2_piece    - The piece selected for player 2

    Returns:
    Returns the player pieces in a list or
    Raises an error if the values are not valid pieces or they are the same
    """
    # If the pieces are valid and not the same piece
    if(re.fullmatch("^[OX]$", player1_piece) and
       re.fullmatch("^[OX]$", player2_piece) and
       player1_piece != player2_piece):
        # Returns the list of player pieces
        return [player1_piece, player2_piece]
    # The player pieces are not valid or are the same value
    # Raises an error if the pieces are not valid or the same
    raise SystemExit("Invalid player pieces entered. Valid options "
                     "are 'X' or 'O' unique to each player. The "
                     f"values entered are '{player1_piece}' "
                     f"(for Player 1) and '{player2_piece}' "
                     "(for Player 2), exiting...")


def select_player_turn(player_name):
    """
    Name:
    select_player_turn

    Description:
    Prompts the players to determine whose turn it should be

    Parameters:
    player_name - the name of the player to ask

    Returns:
    The number of the player who will go first
    """
    # If the first player wants to go first
    if prompt_for_response(f"{player_name}, would you like to go 1st: (Y/N) "):
        # Sets the player turn to the first player
        player_turn = 0
    # The first player does not want to go first
    else:
        # Sets the player turn to the second player
        player_turn = 1
    # Returns the number of the player to go first
    return player_turn


def select_piece(player_name):
    """
    Name:
    select_piece

    Description:
    Prompts the players to determine which piece they are going to be

    Parameters:
    player_name - the name of the player to ask

    Returns:
    The list of the pieces for the players
    """
    pieces = ["", ""]
    # If the first player would like to play as X
    if prompt_for_response(f"{player_name}, would you like "
                           "to play as X: (Y/N) "):
        # Sets the first player to X
        pieces[0] = "X"
        # Sets the second player to O
        pieces[1] = "O"
    # The first player does not ant to play as X
    else:
        # Sets the first player to O
        pieces[0] = "O"
        # Sets the second player to X
        pieces[1] = "X"
    # Returns the list of the pieces of the players
    return pieces


def get_player_name(game_values, player):
    """
    Name:
    get_player_name

    Description:
    Retrieves the name of the player

    Parameters:
    game_values  - The dictionary containing the game's information
    player      - the player number

    Returns:
    The name of the player in the position specified
    """
    # The name of the player in the position specified
    return game_values["player_names"][player-1]


def get_game_key(game_values):
    """
    Name:
    get_game_key

    Description:
    Get the key to the game statistics currently being played

    Parameters:
    game_values  - The dictionary containing the game's information

    Returns:
    The key storing the statistics for this game
    """
    # Returns the key that stores the statistics for this game
    return get_player_name(game_values, 1) + "_" +\
        get_player_name(game_values, 2)


def handle_signal(signum, stack_frame):
    """
    Name:
    handle_signal

    Description:
    Handles the user hitting CTRL+C

    Parameters:
    None

    Returns:
    None
    """
    # Informs the user that CTRL+C was hit
    print(f"\nYou pressed CTRL+C (signal: {signum})")
    print("Stack Frame:")
    print(f"\t{stack_frame}")
    print("Exiting...")
    # Exits the program
    sys.exit()


def establish_game_parameters(player1_name="PlayerOne",
                              player2_name="PlayerTwo",
                              player_start=1, player1_piece="X",
                              player2_piece="O"):
    """
    Name:
    establish_game_parameters

    Description:
    Sets up all the values (with defaults) required to establish a game.

    Parameters:
    player1_name     - sets the name of the first player
    player2_name     - sets then name of the second player
    player_start     - sets the player number who will start the game
    player1_piece    - sets the piece value of the first player
    player2_piece    - sets the piece value of the second player

    Returns:
    The dictionary containing the game values
    """
    game_values = {
        "number_of_players": 2,
        "player_names": [check_player_name(player1_name),
                         check_player_name(player2_name)],
        "player_pieces": check_player_pieces(player1_piece,
                                             player2_piece),
        "player_turn": check_player_value(player_start),
        "position_values": reset_position_values(),
        "winning_combinations": set_winning_combinations(),
        "play_game": True,
        "game_over": False
        }

    # Returns the dictionary containing the game values
    return game_values


def main():
    """
    Name:
    main

    Description:
    Executes the main functions to call and initiate the TicTacToe class

    Parameters:
    None

    Returns:
    None
    """

    # Sets the function to handle the CTRL+C
    signal.signal(signal.SIGINT, handle_signal)

    # Attempts to execute the game
    try:
        # Creates a game with the chosen first name
        game_values = establish_game_parameters("Apple")
        # Plays the game
        play_game(game_values)
    # Catches the SystemExit exception
    except SystemExit as system_exit:
        # Prints information about the exception
        print(f"Caught an exception... {system_exit.args}")
    # Catches the EOFError exception
    except EOFError:
        # Prints information about the exception
        print("CTRL+D stopped the execution of the program.")


# If the program is running as the main program
if __name__ == "__main__":
    # Calls the main function
    main()

#! /usr/bin/env python3

"""
This program is a simple server that establishes a HTTPS server using
supplied .key and .crt files.
"""

from flask import Flask
from werkzeug.serving import make_server


# Instantiates a Flask app
app = Flask(__name__)


# Sets the base location
@app.route("/")
def hello_world():
    """
    This function provides a default message.
    """
    return "Congratulations, if you are seeing this message without needing "\
           "to bypass security features, you have setup a secure connection "\
           "on an HTTPS server. Feel free to add to the server. Have fun!"


def main() -> None:
    """
    This is the main function to execute the program.
    """
    # Sets the key and cert files to be used
    key_file = 'server.key'
    cert_file = 'server.crt'

    # Sets up the SSL context tuple used in creating the HTTPS server
    context = (cert_file, key_file)

    # Create a new server instance with HTTPS support
    server = make_server('0.0.0.0', 4443, app, ssl_context=context)

    # Run the server
    server.serve_forever()


# If the program was executed by calling its name and not imported
if __name__ == "__main__":
    # Executes the main function
    main()

#! /usr/bin/env python3


"""
This program performs the actions required to set up a self-signed SSL server.
In addition, it sets up the system to treat it as though it is in the middle
of a meddler-in-the-middle (MITM) attack by enabling the SSL keys to be
written to a file by creating an environmental variable within the
.bash_aliases file.
Global variables are used because of how the functions are called during
execution.
"""

# pylint: disable=W0601

# Imports the required libraries
import subprocess
import os
import sys
import shutil
import time
import pwd
import grp
import platform
import json
from functools import partial
from pathlib import Path
from argparse import ArgumentParser
from argparse import ArgumentTypeError


def is_valid_file(file: str) -> str:
    """
    This function tests a file entered on the command line to see if it is
    valid.
    """
    # If the file is not value
    if not is_valid_check(file):
        # Indicates an error with a descriptive message
        raise ArgumentTypeError(f"File '{file}' does not exist.")
    # Returns the file
    return file


def parse_command_line() -> dict:
    """
    This function parses the command line.
    """
    # Creates a ArgumentParse with a desription
    parser = ArgumentParser(prog=sys.argv[0],
                            description='Parser to set up parameters for '
                                        'creating a HTTPS server with a '
                                        'self-signed certificate with '
                                        'optional meddler-in-the-middle '
                                        'setup.')

    # Sets the system to setup for a meddler-in-the-middle (MITM) environment
    parser.add_argument('-m', '--mitm', action='store_true',
                        help='If present, the server is set up for a '
                        'meddler-in-the-middle (MITM) environment.')

    # Sets to retrieve a JSON parameter file for the certificate
    parser.add_argument("-f", "--file", type=is_valid_file,
                        help='JSON parameter file for certificate parameters '
                        'to read.')

    # Sets to retrieve a JSON parameter file for the certificate
    parser.add_argument('-t', '--tfile', help='Write default JSON parameters '
                        'to the specified template file and exit.')

    # Sets for the program to be verbose providing output
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='If present, the system prints messages during '
                        'execution.')

    # Sets for the program to be verbose providing output
    parser.add_argument('-n', '--not_ubuntu', action='store_true',
                        help='If present, the system sets to allow the '
                        'to run on a non-validated Ubuntu version of LINUX. '
                        'The program has only been tested on Ubuntu LINUX. '
                        'This gives the option to allow for execution on a '
                        'LINUX system that may or may not be Ubuntu without '
                        'requiring validation from the user.')

    # Sets the system to setup for a meddler-in-the-middle (MITM) environment
    parser.add_argument('-c', '--clean', action='store_true',
                        help='If present, the server cleans up the SSL keys '
                        'logging and removes certificates installed by this '
                        'program.')

    # Grabs the values from the command line and returns them as a dictionary
    return vars(parser.parse_args())


def delete_chrome_cert():
    """
    This function deletes the certificate authority (CA) installed for the
    system by this program.
    """
    # If verbose is set
    if verbose:
        # Prints the message
        print('Removing certificate from system.')

    # Sets the command to remove the CA
    command = f'certutil -D -d sql:{directories["nssdb"]} -n "My CA"'
    subprocess.run(command, shell=True, stdout=subprocess.DEVNULL,
                   stderr=subprocess.DEVNULL, check=True)


def clean() -> None:
    """
    This function removes the directories created during the implementation
    and creation of the certificates and keys requried to run a self-signed
    server.
    """
    # If verbose is set
    if verbose:
        # Prints the message
        print('Removing directories created by the system.')

    # Sets the directories to remove
    dirs_to_remove = [directories['rsa'], directories['csr']]

    # Iterates through each directory
    for directory in dirs_to_remove:
        # If the name exists and is a directory
        if is_valid_check(directory, 'dir'):
            # If verbose is set
            if verbose:
                # Prints the message
                print(f'Removing directory: {directory}')
            # Remove the directory and its components
            shutil.rmtree(directory)


def clean_system() -> None:
    """
    This function cleans up the directories created, SSL logging, and the
    certificates installed on the system.
    """
    # Cleans the directories created by this program
    clean()
    # Deletes the Google Chrome certificate installed by this program
    delete_chrome_cert()
    # Removes the SSL key logging from the .bash_aliases file
    remove_sslkeylogfile()
    # Informs the user
    print('Finished cleaning.')
    print('If the meddler-in-the-middle option was set, the terminal must '
          'be closed and reopened in order for the environment variable to '
          'be completely gone to undo this action.')
    # Exits the program
    sys.exit()


def set_sslkeylogfile() -> None:
    """
    This program inserts the variable to export in the .bash_aliases file
    to write SSL keys.
    """
    # Sets the SSL file name
    ssl_file = f'{directories["home"]}/.ssl-key.log'
    # The pattern to look for in the file
    ssl_pattern = f'export SSLKEYLOGFILE={ssl_file}'
    # The alias file
    alias_file = f'{directories["home"]}/.bash_aliases'
    # Open the file for appending
    with open(alias_file, 'a+', encoding='utf-8') as alias:
        # Goes to the beginning of the file
        alias.seek(0)
        # Reads the contents of the file
        file_content = alias.read()
        # If the pattern is not in the file
        if ssl_pattern not in file_content:
            # Write the pattern to the end of the file
            alias.write('\n' + ssl_pattern + '\n')
            # Display a message to the user on how to activate this change
            print('\n\n', '=' * 20, '\nIn order for the ssl key logging to '
                  'work, you must start a new terminal window or execute '
                  f'this command in this terminal:\n\tsource{alias_file}\n',
                  '=' * 20, '\n\n', sep='')


def is_valid_check(value: str, instance: str = 'file') -> bool:
    """
    This function checks to see if a file or directory exists and is either
    a directory or file as expected.
    """
    # If this instance is looking at a file
    if instance == 'file':
        # Sets the function to execute
        func = os.path.isfile
    # If this instance is looking at a directory
    elif instance == 'dir':
        # Sets the function to execute
        func = os.path.isdir
    # Some other uknown value
    else:
        # Returns False to indicate it was invalid
        return False
    # Returns True if all checks were valid and False otherwise
    return all([os.path.exists(value), func(value)])


def remove_sslkeylogfile() -> None:
    """
    This program removes the writing of the SSL key log file from the
    .bash_aliases file as well as removes the log file, if it exists.
    """
    # Sets the SSL file name
    ssl_file = f'{directories["home"]}/.ssl-key.log'
    # The pattern to look for in the file
    ssl_pattern = f'export SSLKEYLOGFILE={ssl_file}'

    # If the name exists and is a file
    if is_valid_check(ssl_file):
        # If verbose is set
        if verbose:
            # Prints the message
            print(f'Removing SSL key log file: {ssl_file}')
        # Remove the file
        os.remove(ssl_file)

    # The alias file
    alias_file = f'{directories["home"]}/.bash_aliases'
    # If the name exists and is a file
    if is_valid_check(alias_file):
        # Open the file for reading
        with open(alias_file, 'r', encoding='utf-8') as alias:
            # Reads the contents of the file
            file_content = alias.read().split('\n')
        # Begins a try-except block
        try:
            # Looks for the pattern in the file content
            # If found, sets to the index in the list found
            index = file_content.index(ssl_pattern)
        # If a ValueError exception was thrown
        except ValueError:
            # Sets the index to be a value that is not found
            index = False
        # If the pattern was found in the file
        if index:
            # Delete the index
            del file_content[index]
            # Opens the file for writing
            with open(alias_file, 'w', encoding='utf-8') as alias:
                # If verbose is set
                if verbose:
                    # Prints the message
                    print('Removed SSL key logging from aliases file: '
                          f'{alias_file}')
                # Writes the remaining data to the file
                alias.write('\n'.join(file_content))


def print_info():
    """
    This function prints final information to the user on how to proceed.
    """
    # Prints the messages
    print('Start your server using the files server.crt and server.key.')
    # If the meddler-in-the-middle switch was activated
    if mitm:
        # Prints the message
        print('To capture packets use a command like:')
        print('\tsudo tcpdump -i lo -w captured_packets.pcap')
        print('Open Google Chrome on the same system that the server is '
              'running.')
        print(f'The SSL keys will be written to file: {directories["home"]}/'
              '.ssl-key.log\n')
        print('As data is written across the server, it will be encrypted.')
        print('Use the SSL keys file and the captured_packets.pcap file in '
              'wireshark during the decryption process.')
        print('Remember, if the .bash_aliases file was modified by this '
              'program, it must be sourced before starting your server and '
              'Google Chrome.')


def set_certificate_params(**kwargs) -> None:
    """
    This function sets up the parameters for the certificate signing.
    """
    # Sets access to the global value
    global params
    # Creates an empty dictionary
    params = {}

    # If the key exists, use that value; otherwise, use the default
    params['country'] = kwargs.get('country', 'UN')
    params['province'] = kwargs.get('province', 'UN')
    params['city'] = kwargs.get('city', 'Nowhere')
    params['org'] = kwargs.get('org', 'None')
    params['email'] = kwargs.get('email', 'admin@localhost')
    params['ou'] = kwargs.get('ou', 'Learning')
    params['algo'] = kwargs.get('algo', 'rsa')
    params['digest'] = kwargs.get('digest', 'sha512')
    params['cn'] = kwargs.get('cn', 'localhost')


def create_vars_file() -> None:
    """
    This function prints the vars file.
    """
    # Sets the data to write
    file_data = f'set_var EASYRSA_REQ_COUNTRY    "{params["country"]}"\n'
    file_data += f'set_var EASYRSA_REQ_PROVINCE   "{params["province"]}"\n'
    file_data += f'set_var EASYRSA_REQ_CITY       "{params["city"]}"\n'
    file_data += f'set_var EASYRSA_REQ_ORG        "{params["org"]}"\n'
    file_data += f'set_var EASYRSA_REQ_EMAIL      "{params["email"]}"\n'
    file_data += f'set_var EASYRSA_REQ_OU         "{params["ou"]}"\n'
    file_data += f'set_var EASYRSA_ALGO           "{params["algo"]}"\n'
    file_data += f'set_var EASYRSA_DIGEST         "{params["digest"]}"\n'
    file_data += f'set_var EASYRSA_REQ_CN         "{params["cn"]}"\n'

    # Opens the file
    with open('vars', 'w', encoding='utf-8') as vfile:
        # Writes the data to the file
        vfile.write(file_data)


def is_linux():
    """
    This function tests to see if the operatins system is a version of LINUX.
    This is the only system in which this program has been tested.
    """
    # If the sysem is LINUX
    if platform.system() == 'Linux':
        # Returns True
        return True
    # Returns False
    return False


def is_ubuntu_linux():
    """
    This function tests to see if the operating system is a version of
    Ubuntu LINUX.
    """
    # Sets the patter to look for
    lsb_pattern = 'DISTRIB_ID=Ubuntu'
    # Sets the file to look for the pattern
    lsb_release_file = '/etc/lsb-release'
    # If the name exists and is a file
    if is_valid_check(lsb_release_file):
        # Opens the file for reading
        with open(lsb_release_file, 'r', encoding='utf-8') as lsb:
            # Reads the data from the file
            lsb_data = lsb.read()
            # If the pattern is in the file data
            if lsb_pattern in lsb_data:
                # Returns True
                return True
    # Returns False
    return False


def run_commands() -> None:
    """
    This function iterates through the commands and executes them according
    to the type in the dictionary.
    """
    # Iterates through each command group after putting them in order
    for key in sorted(commands):
        # If running in verbose mode
        if verbose:
            # If the type is a function
            if commands[key]['type'] == 'function':
                print('function: ', end='')
                try:
                    # Prints the function name being executed to the user
                    print(f'{commands[key]["command"].__name__}')
                except AttributeError:
                    # Prints the function name being executed to the user
                    print(f'{commands[key]["command"].func.__name__}')
            # Otherwise
            else:
                # Displays the command being executed to the user
                print(commands[key]['command'])
        # If the type is shell
        if commands[key]['type'] == 'shell':
            # If the command is a special case
            if 'build-ca' in commands[key]['command']:
                # Execute the command as a pipe
                with subprocess.Popen(commands[key]['command'],
                                      shell=True,
                                      stdin=subprocess.PIPE,
                                      stdout=subprocess.DEVNULL,
                                      stderr=subprocess.DEVNULL) as process:
                    # Pass input to the command
                    process.communicate(f'{params["cn"]}\n'.encode('utf-8'))
            # If the command is a special case
            elif 'sign-req' in commands[key]['command']:
                # Execute the command as a pipe
                with subprocess.Popen(commands[key]['command'],
                                      shell=True,
                                      stdin=subprocess.PIPE,
                                      stdout=subprocess.DEVNULL,
                                      stderr=subprocess.DEVNULL) as process:
                    # Pass input to the command
                    process.communicate('yes\n'.encode('utf-8'))
            # Otherwise
            else:
                # Execute the command
                subprocess.run(commands[key]['command'],
                               shell=True,
                               stdout=subprocess.DEVNULL,
                               stderr=subprocess.DEVNULL,
                               check=True)
            # Sleeps for two tenths of a second in order to ensure that all
            # the commands have finished before continuing; otherwise,
            # Operations will fail
            time.sleep(.2)
        # If the type is a function
        elif commands[key]['type'] == 'function':
            # Call the function
            commands[key]['command']()


def set_directories():
    """
    This function sets up various directories for easy retrieval and
    changing for the future.
    """
    # Sets access to the global variable
    global directories
    # Sets up the required directories
    directories = {'home': f'{Path.home()}',
                   'nssdb': f'{Path.home()}/.pki/nssdb',
                   'rsa': f'{Path.home()}/.easy-rsa',
                   'easy-rsa': '/usr/share/easy-rsa',
                   'pki': f'{Path.home()}/.easy-rsa/pki',
                   'issued': f'{Path.home()}/.easy-rsa/pki/issued',
                   'certs': '/usr/local/share/ca-certificates/',
                   'csr': f'{Path.home()}/.lh_csr',
                   'start': os.getcwd()}


def set_executables():
    """
    This function sets up the executables when they need to be executed in
    a specific way, like a period slash to execute from the current working
    directory for when the period (.) is not in the executor's path.
    """
    # Sets access to the global variable
    global executables
    # Sets up executables
    executables = {'easy-rsa': './easyrsa'}


def make_directory(directory: str) -> None:
    """
    This makes the passed directory if it does not already exist without
    erroring if it does.
    """
    os.makedirs(directory, exist_ok=True)


def change_directory(directory: str) -> None:
    """
    This function changes directory within the program to the passed location.
    """
    os.chdir(directory)


def set_commands():
    """
    This function sets up the commands to execute in the order necessary for
    execution.
    """
    # Sets access to the global variable for changes
    global commands
    # Dictionary of the commands to execute in the appropriate order
    # This dictionary also contains the type of action that will be performed
    # for correct processing within this program.
    # It also contains a description, primarily being used as a comment for
    # individuals reading this.
    # The keys are:
    #   description - provides a short description of what is happening
    #   type - sets the execution type
    #   execute - indicates whether the block of code is to be executed
    #             allows for the mitm switch to not execute the code and other
    #             possible future enhancements
    #   command - the command to perform
    commands = {0: {'description': 'Cleans the written files to run again',
                    'type': 'function',
                    'execute': True,
                    'command': clean},
                1: {'description': 'Updates the current package versions',
                    'type': 'shell',
                    'execute': True,
                    'command': 'sudo apt update'},
                2: {'description': 'Upgrades packages',
                    'type': 'shell',
                    'execute': True,
                    'command': 'sudo apt -y upgrade'},
                3: {'description': 'Installs easy-rsa',
                    'type': 'shell',
                    'execute': True,
                    'command': 'sudo apt install easy-rsa'},
                4: {'description': 'Creates an easy-rsa working directory',
                    'type': 'function',
                    'execute': True,
                    'command': partial(make_directory, directories["rsa"])},
                5: {'description': 'Copies the easy-rsa files',
                    'type': 'shell',
                    'execute': True,
                    'command': fr'cp -pr {directories["easy-rsa"]}/* '
                    f'{directories["rsa"]}/'},
                6: {'description': 'Sets easy-rsa directory permissions',
                    'type': 'shell',
                    'execute': True,
                    'command': f'chmod 700 {directories["rsa"]}'},
                7: {'description': 'Changes directory',
                    'type': 'function',
                    'execute': True,
                    'command': partial(change_directory, directories["rsa"])},
                8: {'description': 'Creates the pki',
                    'type': 'shell',
                    'execute': True,
                    'command': f'{executables["easy-rsa"]} init-pki'},
                9: {'description': 'Creates a certificate with out a password',
                    'type': 'shell',
                    'execute': True,
                    'command': f'{executables["easy-rsa"]} build-ca nopass'},
                10: {'description': 'Creates the vars file',
                     'type': 'function',
                     'execute': True,
                     'command': create_vars_file},
                11: {'description': 'Copies the cert',
                     'type': 'shell',
                     'execute': True,
                     'command': f'sudo cp {directories["pki"]}/ca.crt '
                                f'{directories["certs"]}'},
                12: {'description': 'Adds the certificate to the system',
                     'type': 'shell',
                     'execute': True,
                     'command': 'sudo update-ca-certificates'},
                13: {'description': 'Creates a csr working directory',
                     'type': 'function',
                     'execute': True,
                     'command': partial(make_directory, directories["csr"])},
                14: {'description': 'Changes directory',
                     'type': 'function',
                     'execute': True,
                     'command': partial(change_directory, directories["csr"])},
                15: {'description': 'Creates the server key file',
                     'type': 'shell',
                     'execute': True,
                     'command': 'openssl genrsa -out server.key'},
                16: {'description': 'Creates the server requirement file',
                     'type': 'shell',
                     'execute': True,
                     'command': 'openssl req -new -key server.key -out '
                                'server.req -subj '
                                f'/C={params["country"]}'
                                f'/ST={params["province"]}'
                                f'/L={params["city"]}'
                                f'/O={params["org"]}'
                                f'/OU={params["ou"]}'
                                f'/CN={params["cn"]}'},
                17: {'description': 'Changes directory',
                     'type': 'function',
                     'execute': True,
                     'command': partial(change_directory, directories["rsa"])},
                18: {'description': 'Signs the server csr file',
                     'type': 'shell',
                     'execute': True,
                     'command': f'{executables["easy-rsa"]} import-req '
                                f'{directories["csr"]}/'
                                f'server.req {params["cn"]}'},
                19: {'description': 'Signs the server csr file',
                     'type': 'shell',
                     'execute': True,
                     'command': f'{executables["easy-rsa"]} sign-req server '
                                f'{params["cn"]}'},
                20: {'description': 'Copies the csr file',
                     'type': 'shell',
                     'execute': True,
                     'command': f'cp {directories["csr"]}/server.key '
                                f'{directories["start"]}/server.key'},
                21: {'description': 'Copies the csr file',
                     'type': 'shell',
                     'execute': True,
                     'command': f'cp {directories["issued"]}/localhost.crt '
                                f'{directories["start"]}/server.crt'},
                22: {'description': 'Installs the cert installation utility',
                     'type': 'shell',
                     'execute': True,
                     'command': 'sudo apt install -y libnss3-tools'},
                23: {'description': 'Installs the cert installation utility',
                     'type': 'shell',
                     'execute': True,
                     'command': f'certutil -d sql:{directories["nssdb"]} -A '
                                f'-t TC -n "My CA" -i {directories["pki"]}'
                                '/ca.crt'},
                24: {'description': 'Modifies the aliases file to allow for '
                                    'ssl logging',
                     'type': 'function',
                     'execute': mitm,
                     'command': set_sslkeylogfile},
                25: {'description': 'Displays an instructional message to the '
                                    'user on how to proceed',
                     'type': 'function',
                     'execute': True,
                     'command': print_info}}


def set_mitm(**kwargs) -> None:
    """
    This function sets the value for the meddler-in-the-middle.
    """
    # Sets access to the global variable
    global mitm
    # If the key exists, use that value; otherwise, use the default
    mitm = kwargs.get('mitm', False)


def set_verbose(**kwargs) -> None:
    """
    This function sets the verbose value.
    """
    # Sets access to the global variable
    global verbose
    # If the key exists, use that value; otherwise, use the default
    verbose = kwargs.get("verbose", False)


def setup_environment(args: dict):
    """
    This function sets the require global variables via functions.
    """
    # Sets up the meddler-in-the-middle value
    set_mitm(**args)
    # Sets the executables value
    set_executables()
    # Sets the directories value
    set_directories()
    # Sets the values used in generating a self-signed certificate
    set_certificate_params(**args)
    # Sets if the program should run verbose
    set_verbose(**args)
    # Sets the commands value
    # This one uses the functions above here; therefore, it must be after them
    set_commands()


def can_execute(args: dict) -> bool:
    """
    This function checks to see if the conditions are right for this program
    to execute.
    """
    # Sets the current username
    username = pwd.getpwuid(os.getuid()).pw_name
    # If the current user is a sudo user
    if [g.gr_name for g in grp.getgrall() if all([username in g.gr_mem,
                                                  g.gr_name == 'sudo'])]:
        # If the operating system is LINUX
        if is_linux():
            # If the operating system is not Ubuntu, and the flag to say
            # execute anyway is not set
            if not is_ubuntu_linux() and not args['not_ubuntu']:
                # Sets the message to display
                message = 'You are running LINUX; however, this program has '\
                          'only been tested on Ubuntu LINUX, would you like '\
                          'to continue (y/n): '
                # Sets to an empty response
                response = ''
                # While the response is not valid
                while response not in 'ny':
                    # Retrieves the response from the user
                    response = input(message).casefold()[0]
                # If the response was not to continue
                if response == 'n':
                    # Returns False to say the operating system is not valid
                    return False
            # Returns True to say the operating system is valid
            # Valid == user with sudo privileges and one of the following:
            #   LINUX with bypass from user for not being Ubuntu
            #   or
            #   Ubuntu LINUX
            return True
    # Returns False to indicate the operating system is not valid or the
    # user does not have sudo privileges
    return False


def read_json_data(param_file: str) -> dict:
    """
    This function reads JSON data from a parameter file and returns it.
    """
    # If the file exists
    if is_valid_check(param_file):
        # Opens the json file for reading
        with open(param_file, 'r', encoding='utf-8') as jfile:
            # Returns the read json data
            return json.load(jfile)
    # Returns an empty dictionary
    return {}


def write_template(file: str) -> None:
    """
    This function writes a template JSON file for certificate parameters.
    """
    # Sets the default certificate parameters
    set_certificate_params()
    # Opens the template file for writing
    with open(file, 'w', encoding='utf-8') as template:
        # Writes the default certificate parameters to a JSON file
        json.dump(params, template, indent=4)
    # Exits the program
    sys.exit()


def main() -> None:
    """
    This is the main function that executes the program.
    """
    # Retrieves the commandline arguments
    args = parse_command_line()
    # If a JSON template file is to be written
    if 'tfile' in args and args['tfile']:
        # Writes the default JSON data
        write_template(args['tfile'])

    # If the program can execute
    if can_execute(args):
        # If a parameter file for the certificate signing was supplied in JSON
        if 'file' in args and args['file']:
            # Adds the data from the json file
            args = {**args, **read_json_data(args['file'])}

        # Sets up the required global variables
        setup_environment(args)

        # If the user has chosen to clean the system
        if 'clean' in args and args['clean']:
            # Cleans the syste
            clean_system()

        # Execute the commands
        run_commands()
    # The current user is not a sudo user
    else:
        # Informs the user
        print('Certain commands require the use of sudo. You must be in the '
              'sudo group in order to run this program.')
        # Exits the program
        sys.exit()


# If the program was called by executing its name
if __name__ == '__main__':
    # Executes the main function
    main()

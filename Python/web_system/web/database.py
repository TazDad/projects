#! /usr/bin/env python3.11

"""
This program creates a class that implements a sqlite3 database in Python for
use via web pages.
"""

# ### // ## pylint: disable=R0902
# ### // ## pylint: disable=R0904


# Modules to import
import re
import os
import sqlite3
from string import punctuation as P
from datetime import datetime
import bcrypt


class Database():
    """
    This is a class that implements a database in Python.
    """
    # Sets the attributes to enable hiding
    __initialize_db = False
    __username = ''
    __password = ''
    __pass_hash = ''
    __connection = ''
    __newline_char = '\n'
    __cursor_object = ''
    __valid_user = False
    __user_access_level = 'Invalid User'
    __master_table = 'tblTables'
    __user_table = 'tblUsers'
    __test_takers = 'tblTestResults'

    def __init__(self, database_name='db/web_system_sqlite3.db',
                 username='new', access=''):
        """
        This method initializes a database class in Python.
        """
        # Sets the database name
        self.database_name = database_name
        # Sets to indicate that the database was not just created
        db_initialized = False
        # If the database needs to be initialized
        if not self.is_db_usable():
            # If the OS separator value is in the database name
            if os.sep in self.database_name:
                # Determines the current working directory
                cur_dir = os.getcwd()
                # Steps through each directory in the database name
                for base_dir in self.database_name.split(os.sep)[0:-1]:
                    # Creates the directory found
                    os.mkdir(base_dir)
                    # Changes directory to the directory found
                    os.chdir(base_dir)
                # Returns back to the original directory
                os.chdir(cur_dir)
            # Creates a table to keep track of all the tables in the database
            self.create_master_table()
            # Creates the user table
            self.create_user_table()
            # Sets to indicate that the database was just created
            db_initialized = True
        # Assigns the passed access value
        self.__user_access_level = access
        # Assigns the passed username value
        self.__username = username
        # Sets the connection to the database
        self.__connection = self.database_connection()
        # Sets the cursor object to the database
        self.__cursor_object = self.database_cursor()
        # If the database was initialized this iteration
        if db_initialized:
            # Inserts the newly created tables into the database
            self.insert_into_master_table(self.__user_table,
                                          'Table that tracks the database '
                                          'users',
                                          self.__username)

    def set_newline_char(self, newline_char):
        """
        This method allows the setting of the newline character to either a
        newline or the break for web pages
        """
        # If the passed newline character is in the allowable options
        if newline_char in ['\n', '<br>']:
            # Sets the newline character value
            self.__newline_char = newline_char

    def get_newline_char(self):
        """
        This method retrieves the newline character
        """
        # Returns the newline character value
        return self.__newline_char

    def is_db_usable(self):
        """
        This method validates that a file is usable (readable and wrtitable)
        by the user.
        """
        # Returns the result of testing if the file is readable and writable
        return os.access(self.database_name, os.R_OK | os.W_OK)

    def get_username_rules(self):
        """
        This method displays the rules surrounding username creation.
        """
        # Sets the username rules
        rules = f'Username rules: {self.__newline_char}'
        rules += '  o must be betwen 5 and 24 (inclusive) in '
        rules += f'length {self.__newline_char}'
        rules += '  o must begin with either an uppercase or lowercase '
        rules += f'letter {self.__newline_char}'
        rules += '  o can only contain letters (uppercase or lowercase), '
        rules += f'numbers, digits, and underscores {self.__newline_char}'
        rules += f'  o must not end with an underscore {self.__newline_char}'
        # Returns the username rules
        return rules

    def check_username(self, username):
        """
        This method validates a username.
        """
        # Sets if the username is valid
        status = self.is_valid_username(username)
        # If the username was valid
        if status:
            self.__username = username
        # Returns the status
        return status

    def is_valid_user_password(self):
        """
        This method validates the password entered to see if it matches
        the stored password hash for the user.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash '
                                     f'FROM {self.__user_table} WHERE '
                                     'username=?', (self.__username,))
        # Retrieves the value from the query
        value = self.__cursor_object.fetchone()
        # If a value was returned
        if value:
            # Compares the user password to what was retrieved from the
            # database and returns True of False depending on whether they
            # matched
            return bcrypt.checkpw(self.__password.encode('utf-8'), value[0])
        # If no value was retrieved, return False
        return False

    def insert_user_into_table(self, user_info):
        """
        This method inserts a user into the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'INSERT INTO {self.__user_table} '
                                     '(username, pass_hash,  first_name, '
                                     'last_name, email_address, access_type, '
                                     'reset, date_added, last_login) VALUES ('
                                     ':username, :pass_hash, :first_name, '
                                     ':last_name, :email_address, '
                                     ':access_type, :reset, :date_added,'
                                     ':last_login)',
                                     (user_info['username'],
                                      user_info['pass_hash'],
                                      user_info['first_name'],
                                      user_info['last_name'],
                                      user_info['email_address'],
                                      user_info['access'],
                                      user_info['reset'],
                                      user_info['date_added'],
                                      user_info['last_login']))

        # Commits the query
        self.__connection.commit()

    def update_user_info(self, user_info):
        """
        This method inserts a user into the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'UPDATE {self.__user_table} '
                                     'SET first_name = (?), '
                                     'last_name = (?), email_address = (?), '
                                     'access_type = (?), reset = (?) '
                                     'WHERE username = (?);',
                                     (user_info['first_name'],
                                      user_info['last_name'],
                                      user_info['email_address'],
                                      user_info['access'],
                                      user_info['reset'],
                                      user_info['username']))

        # Commits the query
        self.__connection.commit()

    def any_users_in_user_table(self):
        """
        This method returns True if there are users in the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT username '
                                     f'FROM {self.__user_table}')
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def all_users_in_user_table(self, long=False):
        """
        This method retrieves requested information for all users from
        the database.
        """
        if long:
            # Sets the order of items to retrieve
            order = ['username', 'first_name', 'last_name', 'email_address',
                     'access_type', 'reset', 'date_added', 'last_login']
        else:
            # Sets the order of items to retrieve
            order = ['username', 'first_name', 'last_name']

        # Sets the header formatted of the items retrieved
        order_headers = [' '.join(header.split('_')).title()
                         if '_' in header else header.title()
                         for header in order]
        # SQL statement
        sql = f'SELECT {", ".join(order)} FROM {self.__user_table};'
        # Executes the SQL code
        self.__cursor_object.execute(sql)
        # Returns the header and the retrieved user information
        return (order_headers, self.__cursor_object.fetchall())

    def get_user_data(self, username):
        """
        This method retrieves requested information for the requested user
        from the database.
        """
        # Sets the order of items to retrieve
        order = ['username', 'first_name', 'last_name', 'email_address',
                 'access_type', 'reset', 'date_added', 'last_login']
        # SQL statement
        sql = f'SELECT {", ".join(order)} FROM {self.__user_table} WHERE '
        sql += f'username="{username}";'
        # Executes the SQL code
        self.__cursor_object.execute(sql)
        # Returns the information retrieved for the user as a dictionary
        return dict(zip(order, self.__cursor_object.fetchone()))

    def is_password_reset_required(self):
        """
        This method returns True if the user must reset their password and
        False if they do not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT reset '
                                     f'FROM {self.__user_table} WHERE '
                                     'username=?', (self.__username,))
        # Returns True of the user must reset their password and False if
        # they do not
        return bool(int(self.__cursor_object.fetchone()[0]))

    def is_user_in_user_table(self, user):
        """
        This method returns True if the user is in the database and False if
        it is not.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT pass_hash '
                                     f'FROM {self.__user_table} WHERE '
                                     'username=?', (user,))
        # Returns True if anything was retrieved and False otherwise
        return bool(self.__cursor_object.fetchone())

    def retrieve_user_access_type(self, username):
        """
        This method retrieves the user's access level from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT access_type '
                                     f'FROM {self.__user_table} WHERE '
                                     'username=?', (username,))
        # Returns the user's access type
        return self.__cursor_object.fetchone()[0]

    @staticmethod
    def is_valid_username(username):
        """
        This method checks the username to see if it matches the validation
        criteria.
        """
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_]*$'
        # The value is between 5 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, and underscores
        # The value does not end with an underscore
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(username) > 1 and all([4 < len(username) < 25,
                                          username[0].isalpha(),
                                          re.match(valid_chars, username),
                                          username[-1] != '_'])

    def password_change(self, username, password):
        """
        This method changes the password for the user
        """
        # If the user is changing their password or the user is an admin
        # changing another user and the password is valid
        if all([any([self.__username == username,
                     self.__user_access_level == 'admin']),
                self.is_valid_password(password)]):
            # Retrieves the hash for the password
            new_pass_hash = self.get_password_hash(password)
            # If the password hash is not None (empty)
            if new_pass_hash is not None:
                # Updates the user's password in the database
                self.update_user_password(username, new_pass_hash, "0")
                # Return True to indicate that the password was changed
                return True
        # Return False to indicate that the password was not changed
        return False

    def update_user_password(self, user, pass_hash, reset='0'):
        """
        This method updates the user's password, and reset value.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'UPDATE {self.__user_table} '
                                     'SET pass_hash = (?), '
                                     'reset = (?) WHERE username=(?);',
                                     (pass_hash, reset, user))
        # Commits the change to the database
        self.__connection.commit()

    @staticmethod
    def is_valid_password(password):
        """
        This method checks the password to see if it matches the validation
        criteria.
        """
        # Sets the valid search parameters
        search = '[a-z]{3,}|[A-Z]{3,}|[0-9]{3,}|['+P+'}]{3,}'
        # Sets the valid characters allowed
        valid_chars = '^[a-zA-Z0-9_'+P+']*$'
        # The value is between 12 and 24 inclusive in length
        # The value begins with either an uppercase or lowercase letter
        # The value contains only letters, numbers, punctuation, and
        # underscores
        # The value does not end with a punctuation character
        # The value does not contain three or more characters of the same
        # character set in a row
        # Returns True if the length is greater than one and meets the other
        # criteria or False, otherwise
        return len(password) > 1 and all([11 < len(password) < 25,
                                          password[0].isalpha(),
                                          re.match(valid_chars, password),
                                          re.search(search, password) is None,
                                          password[-1] not in P])

    def get_password_rules(self):
        """
        This method returns the rules for passwords.
        """
        # Sets the password rules to the user
        rules = f'Password rules: {self.__newline_char}'
        rules += '  o must be betwen 12 and 24 (inclusive) in '
        rules += f'length{self.__newline_char}'
        rules += '  o must begin with either an uppercase or lowercase '
        rules += f'letter{self.__newline_char}'
        rules += '  o can only contain character from the character sets: '
        rules += 'letters (uppercase or lowercase), numbers, digits, and '
        rules += f'punctuation characters{self.__newline_char}'
        rules += '  o must not contain any more than two characters from the '
        rules += f'same character set in a row{self.__newline_char}'
        rules += f'  o must not end with punctuation{self.__newline_char}'
        # Returns the rules string
        return rules

    def is_valid_user(self):
        """
        This method checks to see if the user's password is valid.
        """
        # Returns True if the user's password matches, False otherwise
        return self.is_valid_user_password()

    def database_connection(self):
        """
        This method establishes a connecgtion to the database
        """
        # Returns a connection to the database
        return sqlite3.connect(self.database_name)

    def database_cursor(self):
        """
        This method establishes the cursor to a place in the database
        """
        # Returns the cursor to the database
        return self.__connection.cursor()

    def create_user_table(self):
        """
        This method creates the user table within the database.
        """
        # Sets the connection to the database
        data_base = sqlite3.connect(self.database_name)
        # Sets the SQL code to create the database table
        data_base.execute(f'CREATE TABLE IF NOT EXISTS {self.__user_table} '
                          '(user_id INTEGER PRIMARY KEY AUTOINCREMENT, '
                          'username CHAR(25) NOT NULL UNIQUE, '
                          'pass_hash CHAR(128) NOT NULL, '
                          'first_name CHAR(64) NOT NULL, '
                          'last_name CHAR(64) NOT NULL, '
                          'email_address CHAR(128) NOT NULL, '
                          'access_type CHAR(10) check(access_type = "admin" '
                          'or access_type = "user" or '
                          'access_type = "reviewer") '
                          'NOT NULL, reset CHAR(1) '
                          'check(reset = "0" or reset = "1") NOT NULL, '
                          'date_added real NOT NULL, last_login real '
                          'NOT NULL)')
        # Closes the database
        data_base.close()

    def insert_into_master_table(self, table_name, description, admin):
        """
        This method inserts a table name, its description, and the admin
        into the master table of the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'INSERT INTO {self.__master_table} '
                                     '(table_name, table_desc, table_admin) '
                                     'VALUES (?, ?, ?)',
                                     (table_name, description, admin))

        # Commits the query
        self.__connection.commit()

    def table_exists(self, table_name):
        """
        This method inserts a table name, its description, and the admin
        into the master table of the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT name FROM sqlite_master '
                                     'WHERE type="table" AND '
                                     f'name="{table_name}";')

        # Fetches a result, if there are any, then the table exists
        return bool(self.__cursor_object.fetchone())

    def create_master_table(self):
        """
        This method creates the table that keeps track of all the tables
        in the database.
        """
        # Sets the connection to the database
        data_base = sqlite3.connect(self.database_name)
        # Sets the SQL code to create the database table
        data_base.execute(f'CREATE TABLE IF NOT EXISTS {self.__master_table} '
                          '(test_id INTEGER PRIMARY KEY AUTOINCREMENT, '
                          'table_name CHAR(50) NOT NULL UNIQUE, '
                          'table_desc CHAR(128) NOT NULL, '
                          'table_admin CHAR(128) NOT NULL)')
        # Closes the database
        data_base.close()

    def get_test_tables(self, table_admin, admin_required=True):
        """
        This method retrieves the test the user is able to edit or what they
        are able to take.
        """
        if admin_required:
            # Executes the SQL code
            self.__cursor_object.execute('SELECT name FROM sqlite_master, '
                                         f' {self.__master_table} '
                                         'WHERE type="table" and '
                                         'name=table_name '
                                         f'and table_admin="{table_admin}";')
        else:
            # Executes the SQL code
            self.__cursor_object.execute('SELECT name FROM sqlite_master '
                                         'WHERE type="table";')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchall()

        # If there are any results
        if result:
            # Sets the tables to ignore
            tables = [self.__master_table, self.__user_table,
                      'sqlite_sequence', self.__test_takers]

            # Returns a list of all the test names
            return [name[0] for name in result if name[0] not in tables and
                    not name[0].endswith('_results')]
        # Returns an empty list
        return []

    def get_test_description(self, table_admin, table_name):
        """
        This method retrieves the description for the table if the user is an
        admin.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT table_desc '
                                     f'FROM {self.__master_table} '
                                     f'WHERE table_admin = "{table_admin}" '
                                     f'AND table_name = "{table_name}";')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchone()

        # Returns the description of the test or an empty string if not found
        return result[0] if result else ''

    def get_test_id(self, table_admin, table_name):
        """
        This method retrieves the test id for the table if the user is an
        admin.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT test_id '
                                     f'FROM {self.__master_table} '
                                     f'WHERE table_admin = "{table_admin}" '
                                     f'AND table_name = "{table_name}";')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchone()

        # Returns the description of the test or an empty string if not found
        return result[0] if result else ''

    def is_valid_test_admin(self, table_admin, table_name):
        """
        This method retrieves the test the user is able to edit or what they
        are able to take.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT table_name '
                                     f'FROM {self.__master_table} '
                                     f'WHERE table_admin = "{table_admin}" '
                                     f'AND table_name = "{table_name}";')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchone()

        # If there are any results
        if result:
            # Returns True to indicate that the user is an admin for the test
            return True
        # Returns False to indicate that the user is not an admin for the test
        return False

    def get_test_answers(self, table_name):
        """
        This method retrieves the test questions and answers.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT question, answer FROM '
                                     f'{table_name}')

        # Fetches a result, if there are any, then the table exists
        results = self.__cursor_object.fetchall()
        # Returns retrieved data or an empty list if no data was retrieved
        # or the user was not a test admin
        return {result[0]: result[1] for result in results}

    def update_test_taker(self, test_taker, table_name, data):
        """
        This method inserts test taker data into the table.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'UPDATE {self.__test_takers} '
                                     'SET date_completed = '
                                     f'"{datetime.today()}", '
                                     f'correct = "{data["correct"]}", '
                                     f'total = "{data["total"]}", '
                                     f'question_ids = "{data["ids"]}" '
                                     f'WHERE table_name = "{table_name}" and'
                                     f' test_taker = "{test_taker}";')
        # Commits the query
        self.__connection.commit()

    def insert_user_into_test_taker(self, table_admin, table_name,
                                    test_takers):
        """
        This method creates the test taker table within the database to
        control who can take a test.
        """
        # If the user is an administrator of the test
        if self.is_valid_test_admin(table_admin, table_name):
            for test_taker in test_takers:
                # Executes the SQL code
                self.__cursor_object.execute('INSERT INTO '
                                             f'{self.__test_takers} '
                                             '(table_name, test_taker) VALUES '
                                             '(:table_name, :test_taker)',
                                             (table_name, test_taker))
                # Commits the query
                self.__connection.commit()

    def rename_test_results_table(self, table_admin, table_name_orig,
                                  table_name):
        """
        This method renames the test taker table.
        """
        # If the user is an administrator of the test
        if self.is_valid_test_admin(table_admin, table_name):
            # Sets the connection to the database
            data_base = sqlite3.connect(self.database_name)
            # Sets the SQL code to create the database table
            data_base.execute(f'ALTER TABLE {table_name_orig} '
                              f'RENAME TO {table_name};')

            self.create_results_table(table_name, data_base)
            # Closes the database
            data_base.close()

    def get_user_session_id(self, test_taker, table_name):
        """
        This method creates the test taker table within the database to
        control who can take a test.
        """
        # Executes the SQL code
        self.__cursor_object.execute('SELECT session_id FROM '
                                     f'{self.__test_takers} WHERE '
                                     f'table_name = "{table_name}" AND '
                                     f'test_taker = "{test_taker}" AND '
                                     'question_ids IS NULL')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchone()
        if result:
            return result[0]
        return False

    def create_test_taker_table(self, table_admin, table_name):
        """
        This method creates the test taker table within the database to
        control who can take a test.
        """
        # If the user is an administrator of the test
        if self.is_valid_test_admin(table_admin, table_name):
            # Sets the connection to the database
            data_base = sqlite3.connect(self.database_name)
            # Sets the SQL code to create the database table
            data_base.execute('CREATE TABLE IF NOT EXISTS '
                              f'{self.__test_takers} '
                              '(session_id INTEGER PRIMARY KEY AUTOINCREMENT, '
                              'table_name CHAR(50) NOT NULL, '
                              'test_taker CHAR(128) NOT NULL, '
                              'date_completed real, '
                              'correct INTEGER, '
                              'total INTEGER, '
                              'question_ids TEXT)')

            self.create_results_table(table_name, data_base)
            # Closes the database
            data_base.close()

    def insert_into_results_table(self, table_name, session_id, question_ids,
                                  answers, valid_answer):
        """
        This method creates the test taker table within the database to
        control who can take a test.
        """
        for question_id, answer, correct in zip(question_ids, answers,
                                                valid_answer):

            self.__cursor_object.execute('INSERT INTO '
                                         f'{table_name}_results '
                                         '(session_id, question_id, '
                                         'answer, correct) VALUES '
                                         '(:session_id, :question_id, '
                                         ':answer, :correct)',
                                         (session_id, question_id,
                                          answer, correct))
            # Commits the query
            self.__connection.commit()

    def create_results_table(self, table_name, data_base):
        """
        This method creates the test taker table within the database to
        control who can take a test.
        """
        # Sets the SQL code to create the database table
        data_base.execute('CREATE TABLE IF NOT EXISTS '
                          f'{table_name}_results '
                          '(session_id INTEGER NOT NULL, '
                          'question_id INTEGER NOT NULL, '
                          'answer TEXT NOT NULL, '
                          'correct TEXT NOT NULL)')

    def retrieve_test_results(self, test_admin, table_name):
        """
        This method retrieves and formats the test results for the selected
        test name
        """
        # Sets the headers for the columns returned
        headers = [['Username', 'First Name', 'Last Name', 'Test Name',
                    'Date Completed', 'Total Correct', 'Total Questions',
                    'Score', 'Question Asked', 'Test Taker\'s Answer',
                    'Answered Correctly']]

        # If the user is not a test administrator
        if not self.is_valid_test_admin(test_admin, table_name):
            # Returns just the headers
            return headers

        # Sets the order for the data keys retrieved
        base_order = ['username', 'first_name', 'last_name', 'test_name',
                      'date_completed', 'correct', 'total', 'score']

        # Executes the SQL code
        self.__cursor_object.execute(f'SELECT DISTINCT '
                                     f'{self.__test_takers}.test_taker, '
                                     f'{self.__user_table}.first_name, '
                                     f'{self.__user_table}.last_name, '
                                     f'{self.__test_takers}.table_name, '
                                     f'{self.__test_takers}.date_completed, '
                                     f'{self.__test_takers}.correct, '
                                     f'{self.__test_takers}.total, '
                                     f'{self.__test_takers}.session_id '
                                     f'FROM {self.__test_takers} INNER JOIN '
                                     f'{self.__user_table} ON '
                                     f'{self.__test_takers}.test_taker = '
                                     f'{self.__user_table}.username WHERE '
                                     f'{self.__test_takers}.table_name '
                                     f'= "{table_name}";')

        # Fetches the result
        base_questions = self.__cursor_object.fetchall()
        # Turns the retrieved values into a list of lists instead of a list
        # of tuples
        base_questions = [list(base) for base in base_questions]

        # Creates an empty dictionary
        results = {}

        # Steps through the results found
        for base in base_questions:
            # Retrieves the session_id from the results
            session = base.pop()
            # Calculates the individuals percentage score
            base.append(round(base[-2] / base[-1] * 100, 2))
            # Creates the base information for each question retrieved
            results[session] = dict(zip(base_order, base))

        # Executes the SQL code
        self.__cursor_object.execute('SELECT * '
                                     f'FROM {table_name}_results;')

        # Retrieves the answers stored for the test
        answers = self.__cursor_object.fetchall()
        # Turns the retrieved values into a list of lists instead of a list
        # of tuples
        answers = [list(answer) for answer in answers]

        # Executes the SQL code
        self.__cursor_object.execute(f'SELECT {table_name}.question_id, '
                                     f'{table_name}.question FROM '
                                     f'{table_name};')

        # Retrieves the question names within the specified test
        question_names = self.__cursor_object.fetchall()

        # Starts the result with the header
        result = headers

        # Sets the score values
        scores = [headers[0][0:len(base_order)]] + base_questions

        # Steps through each question
        for question in question_names:
            # Steps through each answer
            for answer in answers:
                # Creates an empty group for the data
                setup = []
                # If the session ids in the two groups match
                if question[0] == answer[1]:
                    # Replaces the question id with the question value
                    answer[1] = question[1]
                    # Steps through each base part of the data
                    for base in base_order:
                        # Stores the data
                        setup.append(results[answer[0]][base])
                    # Steps through each result part of the data
                    for answer_value in answer[1:]:
                        # Stores the data
                        setup.append(answer_value)
                # If the question data was stored
                if setup:
                    # Appends the data to the result
                    result.append(setup)

        # Returns the result and the scores
        return result, scores

    def get_test_values(self, table_name):
        """
        This method retrieves the test the user is able to edit or what they
        are able to take.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'SELECT * FROM {table_name}')

        # Fetches a result, if there are any, then the table exists
        result = self.__cursor_object.fetchall()

        # Returns retrieved data or an empty list
        return result if result else []

    def is_able_to_test(self, test_taker, table_name):
        """
        This method tests to see whether the test taker is allowed to take
        the chosen test
        """
        # Placeholder at the moment
        # Returns the test taker's access to take the test
        return bool(test_taker and table_name)

    def get_question_data(self, test_taker, table_name, question):
        """
        This method retrieves the test the user is able to edit or what they
        are able to take.
        """
        # Creates an empty result
        result = []
        # If the user is able to take the test
        if self.is_able_to_test(test_taker, table_name):
            # Executes the SQL code
            self.__cursor_object.execute('SELECT type, options, time FROM '
                                         f'{table_name} WHERE '
                                         f'question="{question}"')

            # Fetches a result, if there are any, then the table exists
            result = self.__cursor_object.fetchone()
        # Returns retrieved data or an empty list if no data was retrieved
        # or the user was not a test admin
        return result

    def create_test_table(self, table_name):
        """
        This method creates the test table within the database.
        """
        # Sets the connection to the database
        data_base = sqlite3.connect(self.database_name)
        # Sets the SQL code to create the database table
        data_base.execute(f'CREATE TABLE IF NOT EXISTS {table_name} '
                          '(question_id INTEGER PRIMARY KEY AUTOINCREMENT, '
                          'type CHAR(20) NOT NULL, '
                          'question TEXT NOT NULL UNIQUE, '
                          'answer TEXT NOT NULL, '
                          'options TEXT, '
                          'time CHAR(4) NOT NULL)')

        # Closes the database
        data_base.close()

    def delete_selected_table(self, test_admin, table_name):
        """
        This method deletes the table from the database if they are an admin.
        """
        # If the user is a test administrator
        if self.is_valid_test_admin(test_admin, table_name):
            # Executes the SQL code
            self.__cursor_object.execute(f'DROP TABLE {table_name};')
            # Commits the query
            self.__connection.commit()

    def delete_from_master_table(self, test_admin, table_name):
        """
        This method deletes the table from the database if they are an admin.
        """
        # If the user is a test administrator
        if self.is_valid_test_admin(test_admin, table_name):
            # Retrieves the test id
            test_id = self.get_test_id(test_admin, table_name)
            # If the test id is valid
            if test_id:
                # Executes the SQL code
                self.__cursor_object.execute('DELETE FROM '
                                             f'{self.__master_table} '
                                             f'WHERE test_id={test_id};')
            # Commits the query
            self.__connection.commit()

    def insert_data_into_test(self, test_admin, table_name, test_info):
        """
        This method inserts test data into the database.
        """
        # If the user is a test administrator
        if self.is_valid_test_admin(test_admin, table_name):
            # Steps through each question in the passed test information
            # and inserts the data into the table
            for question in test_info:
                # Executes the SQL code
                self.__cursor_object.execute(f'INSERT INTO {table_name} '
                                             '(type, question, answer, '
                                             'options, time) VALUES (:type, '
                                             ':question, :answer, :options, '
                                             ':time)',
                                             (test_info[question]['question_'
                                                                  'type'],
                                              question,
                                              test_info[question]['answer'],
                                              test_info[question]['options'],
                                              test_info[question]['time']))
                # Commits the query
                self.__connection.commit()

    def login(self, username, password):
        """
        This method performs the actions to log a user into the database.
        """
        # If the username is not valid
        if not self.check_username(username):
            # Returns False to not allow the user to not login
            return False
        # Sets the password variable
        self.__password = password
        # Sets to see if the password provided matches the stored password
        self.__valid_user = self.is_valid_user()
        # If the provided password does not match the one in the database
        if not self.__valid_user:
            # Returns False to not allow the user to not login
            return False
        # Empties the password and hash values
        self.__password = ''
        # Sets the user's access level
        self.__user_access_level =\
            self.retrieve_user_access_type(self.__username)
        # Updates the last login time for the user
        self.update_last_login(self.__username)
        # Returns True to indicate that the user has logged in
        return True

    @staticmethod
    def get_password_hash(password):
        """
        This method generates a hash of the password using bcrypt
        """
        # If there is a password
        if password:
            # Creates a hash of the password
            return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # Returns None if there was no password
        return None

    def create_user(self, user_info, access='user', reset='1'):
        """
        This method retrieves the username, sets their password, and reset
        value before updating the database.
        """
        # If the username is not valid
        if not self.check_username(user_info['username']):
            # Returns False to not allow the user to be created
            return False
        # Sets the access type possibilities
        access_types = ['admin', 'user', 'reviewer']
        # If no access value was provided in the dictionary
        if 'access' not in user_info or len(user_info['access']) == 0:
            # Sets to the default access value
            user_info['access'] = access
        # If there are no users in the database
        if not self.any_users_in_user_table():
            # Sets to indicate that the user will be an admin
            # The first person will be an admin by default
            user_info['access'] = 'admin'
        # If no reset value was provided in the dictionary
        if 'reset' not in user_info or len(user_info['reset']) == 0:
            # Sets to the default reset value
            user_info['reset'] = reset
        # The user is in the database or the password is not valid or
        # the access value is not valid
        if any([self.is_user_in_user_table(self.__username),
                not self.is_valid_password(user_info['password']),
                user_info['access'] not in access_types]):
            # Returns False to not allow the user to be created
            return False
        # Sets and stores the password hash
        user_info['pass_hash'] = self.get_password_hash(user_info['password'])
        # Sets the date the user was added
        user_info['date_added'] = f'{datetime.today()}'
        # If the username being added is the same as the user that is logged in
        if user_info['username'] == self.__username:
            # Sets the date of the last login value
            user_info['last_login'] = f'{datetime.today()}'
        # Adds the user to the database
        self.insert_user_into_table(user_info)
        # Empties the password and hash values
        self.__password = ''
        # Set to indicaet that the user is valid
        self.__valid_user = True
        # Sets to have access to the user's access level
        self.__user_access_level = access
        # Returns True to indicate that the user was added to the database
        return True

    def update_last_login(self, user):
        """
        This method updates the user's password, and reset value.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'UPDATE {self.__user_table} '
                                     'SET last_login = (?) '
                                     'WHERE username=(?);',
                                     (f'{datetime.today()}', user))

        # Commits the change to the database
        self.__connection.commit()

    def valid_user_values(self, user_data):
        """
        This method checks all the user data values to see if they are valid.
        It returns a tuple that contains True or Fals to indiate that all the
        values are True or not and a list that will contain the field that
        failed, should one fail.
        """
        # Creates empty lists
        valid_values = []
        failures = []

        options = {'username': {'name': 'Username',
                                'func': self.is_valid_username},
                   'password': {'name': 'Password',
                                'func': self.is_valid_password},
                   'first_name': {'name': 'First Name',
                                  'func': self.is_valid_name},
                   'last_name': {'name': 'Last Name',
                                 'func': self.is_valid_name},
                   'email_address': {'name': 'E-mail Address',
                                     'func': self.is_valid_email},
                   'access': {'name': 'Access Type',
                              'func': self.is_valid_access},
                   'reset': {'name': 'Reset Value',
                             'func': self.is_valid_reset}}
        # Steps through each of the keys
        for key in user_data.keys():
            # Test the value to see if it is valid
            valid_values.append(options[key]['func'](user_data[key]))
            # If the value recovered is not valid
            if not valid_values[-1]:
                # Append the proper name
                failures.append(options[key]['name'])
        # Returns True if all values resolved to True; otherwise, it returns
        # False
        return all(valid_values), failures

    @staticmethod
    def is_valid_name(name_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the length of the value is greater than 1; otherwise,
        # return False
        return len(name_value) > 1

    @staticmethod
    def is_valid_email(email_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the length of the value is greater than 4,
        # the value contains a period (.), and the ending matches one
        # of the allowed endings; otherwise, returns False
        return all([len(email_value) > 4, '@' in email_value,
                    '.' in email_value,
                    email_value.split('.')[-1] in ['edu', 'mil', 'com',
                                                   'org']])

    @staticmethod
    def is_valid_access(access_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the value is in the allowed list; otherwise, returns
        # False
        return access_value in ['admin', 'user', 'reviewer']

    @staticmethod
    def is_valid_reset(reset_value):
        """
        This method tests the value to see if it is valid
        """
        # Returns True if the value is in the allowed list; otherwise, returns
        # False
        return reset_value in '01'

    def delete_user_from_table(self, user):
        """
        This method deletes the specified user from the database.
        """
        # Executes the SQL code
        self.__cursor_object.execute(f'DELETE FROM {self.__user_table} WHERE '
                                     'username=(?);', (user,))
        # Commits the change to the database
        self.__connection.commit()

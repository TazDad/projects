#! /usr/bin/env python3.11


"""
This program creates a series of web pages to login and manage a database
for a web site. When logged in, it serves as a way to upload and download
files and a testing system.
"""

# Imports required libraries
import os
import random
from string import ascii_uppercase as U
from string import ascii_lowercase as L
from string import digits as D
from flask import Flask, request, redirect, session, send_file
from web import Database

# Creates an APP for Flask
APP = Flask(__name__, static_url_path="/static")
# Sets the secret key to use and sign cookies
APP.secret_key = 'Database Server'


def get_file_contents(filename):
    """
    This function opens the passed file and returns the contents of the file
    """
    # Sets the filename
    filename = APP.static_url_path + filename
    # Opens the file for reading
    with open("." + filename, "r", encoding="utf-8") as file:
        # Reads the data from the file
        data = file.read()
    # Returns the contents of the file
    return data


# Creates the base page
@APP.route('/')
def index():
    """
    This Flask function serves the landing page for the web system.
    """
    # Returns the retrieved contents of the file
    return get_file_contents("/templates/index.html")


# Creates the admin page
@APP.route('/admin')
def admin():
    """
    This Flask function serves the admin page for the web system.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Returns the retrieved contents of the file
        return get_file_contents("/templates/admin.html")
    # The logged in user is not an admin and is redirected to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates the page to upload a file
@APP.route('/upload', methods=['POST'])
def upload():
    """
    This Flask function serves the page to upload a file.
    """
    # Retrieves the posted form data
    form = request.form
    # Retrieves the filename passed
    file = request.files['file']
    # Sets the filename and path
    filename = 'user_data' + os.sep + form['username']
    filename += os.sep + file.filename
    # If the filename set is not a directory (no filename was supplied)
    if not os.path.isdir(filename):
        # Save the file
        file.save(filename)
    # Returns to the file management page
    return redirect(f'/file_management?username={form["username"]}')


# Creates the page to check the password
@APP.route('/check_password', methods=['POST'])
def check_password():
    """
    This Flask function serves the page to change a password
    """
    # Retrieves the posted form data
    form = request.form
    # Establishes a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])

    # If the passwords match and the password was changed
    if all([form['password'] == form['new_password'],
            data_base.password_change(form['username'], form['password'])]):
        # Redirects to the main page
        return redirect('/main')
    # Redirects to the change password page to allow the user to try again
    return redirect('/change_password?message=<p>Password provided does '
                    'not meet all the requirements. Try again.</p>')


# Creates the page to check the values on the edit page
@APP.route('/check_edit', methods=['POST'])
def check_edit():
    """
    This Flask function processes the edit page to validate the values
    entered into the edit page
    """
    # Retrieves the posted form data into a dictionary to allow for change
    form = dict(request.form)
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])
    # If the password field was left empty
    if len(form['password']) == 0:
        # Delete password from the form data
        del form['password']
    # Tests all the field data values and retrieves the values returns
    valid_field_data, failed_fields = data_base.valid_user_values(form)
    # If all the values entered on the form are valid
    if valid_field_data:
        # If the password is still in the form data
        if 'password' in form:
            # Change the password
            data_base.password_change(form['username'], form['password'])
            # Deletes the password from the form data
            del form['password']
        # Updates the user information with the data provided on the edit page
        data_base.update_user_info(form)
        # Redirects to the main page
        return redirect('/main')

    # One or more values added were incorrect and the user will be redirected
    # back to the edit page to try again
    message = 'Invalid parameters entered. Enter your information and try '\
              f'again. {"Fields" if len(failed_fields) > 1 else "Field"} '\
              f'that contain issues: {", ".join(failed_fields)}'
    return edit(message, form["username"])


# Creates the page to retrieve the password information to change a user's
# password
@APP.route('/change_password', methods=['GET'])
def change_password():
    """
    This Flask function serves the page to enable a password to be changed.
    """
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])
    # Retrieves the passed data
    get = request.args
    # Creates an empty message
    message = ''
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']

    # Returns the retrieved contents of the file
    page = get_file_contents("/templates/change_password.html")
    # Sets the newline character for web display
    data_base.set_newline_char('<br>')
    # Replaces placeholders with appropriate data
    page = page.replace('{message}', message)
    page = page.replace('{rules}', data_base.get_password_rules())
    page = page.replace('{username}', session['username'])
    # Returns page
    return page


# Creates the page to check login credentials
@APP.route('/check_login', methods=['POST'])
def check_login():
    """
    This Flask function processes the data from the login page to allow a
    user to login.
    """
    # Establish a connection to the database
    data_base = Database()
    # Retrieves the posted form data
    form = request.form
    # If the username was in the database with the provided password
    if data_base.login(form['username'], form['password']):
        # Store the username in a cookie
        session['username'] = form['username']
        # Sets a pointer to the database function to retrieve the access
        # type of the user from the database. This was done to facilitate
        # keeping the line that follows below 80 characters for PEP8 compliance
        retrieve = data_base.retrieve_user_access_type
        # Retrieves the user's access level
        session['access'] = retrieve(session['username'])
        # If the directory does not exist
        if not os.path.exists('user_data' + os.sep + session['username']):
            # Create the directory
            os.mkdir('user_data' + os.sep + session['username'])
        # If the user is required to reset their password
        if data_base.is_password_reset_required():
            # Redirects the user to the page to force a password change
            return redirect('/change_password')
        # Redirects to the main page
        return redirect('/main')
    # Redirects to the login page
    return redirect('/login?message=<p>Invalid username or password</p>')


# Creates the login page to the database
@APP.route('/login', methods=['GET'])
def login():
    """
    This Flask function serves a page to allow a user to login to the database.
    """
    # Retrieves the passed data
    get = request.args
    # Creates an empty message
    message = ''
    # If there were arguments passed and a message was passed
    if len(get.keys()) > 0 and 'message' in get.keys():
        # Retrieve the passed message
        message = get['message']
    # Retrieves the contents of the file
    page = get_file_contents("/templates/login.html")
    # Replaces the placeholder with the appropriate data
    page = page.replace('{message}', message)
    # Returns the page
    return page


# Creates a page to allow a user to signup for access to the database
@APP.route('/signup', methods=['GET'])
def signup(form='', message='Welcome. Add your information below to sign up.'):
    """
    This Flask function creates a page to retrieve information for signing
    up a user for access to the database.
    """
    # If no form information was passed
    if form == '':
        # Empties the values
        username = ''
        password = ''
        first_name = ''
        last_name = ''
        email_address = ''
    # Form information was passed
    else:
        # Set the values
        username = form['username']
        password = form['password']
        first_name = form['first_name']
        last_name = form['last_name']
        email_address = form['email_address']

    # Establish a connection to the database
    data_base = Database()
    # Retrieves the template
    page = get_file_contents("/templates/new_user.html")

    # Sets the default access and reset values
    access_input = '<input type="hidden" name="access" value="user">'
    reset_input = '<input type="hidden" name="reset" value="0">'

    # Sets the newline character for web display
    data_base.set_newline_char('<br>')

    # Sets the home button
    home_button = '<td align="center" colspan=2>'
    home_button += '<form action="/">'
    home_button += '<button type="submit">Home</button>'
    home_button += '</td>'

    # Empties the main button
    main_button = ''

    # Replaces the placeholders with the appropriate data
    page = page.replace('{username}', username)
    page = page.replace('{password}', password)
    page = page.replace('{first_name}', first_name)
    page = page.replace('{last_name}', last_name)
    page = page.replace('{email_address}', email_address)
    page = page.replace('{title}', 'Database Signup')
    page = page.replace('{action}', '/check_signup')
    page = page.replace('{button}', 'Signup')
    page = page.replace('{main}', main_button)
    page = page.replace('{home}', home_button)
    page = page.replace('{access_input}', access_input)
    page = page.replace('{reset_input}', reset_input)
    page = page.replace('{username_rules}', data_base.get_username_rules())
    page = page.replace('{rules}', data_base.get_password_rules())
    page = page.replace('{message}', message)

    # Returns the page
    return page


def is_valid_email(email_address):
    """
    This function tests the input to see if it matches the email formatting.
    """
    # Returns True if the value is greater than 4, the @ symbol is in the
    # passed value, a period (.) is in the value, and the end matches one
    # of the values in the list; otherwise, False is returned
    return all([len(email_address) > 4, '@' in email_address,
                '.' in email_address,
                email_address.split('.')[-1] in ['edu', 'mil', 'com', 'org']])


# Creates the page to add the user
@APP.route('/add_user')
def add_user(form='', message='Enter the details for the new user below:'):
    """
    This Flask function serves the page to add a user to the database.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # If no form information was passed
        if form == '':
            # Empties the values
            username = ''
            password = ''
            first_name = ''
            last_name = ''
            email_address = ''
        # Form information was passed
        else:
            # Sets the values
            username = form['username']
            password = form['password']
            first_name = form['first_name']
            last_name = form['last_name']
            email_address = form['email_address']
        # Establish a connection to the database
        data_base = Database()

        # Retrieves the contents of the file
        page = get_file_contents("/templates/new_user.html")

        # Sets the newline character for web display
        data_base.set_newline_char('<br>')

        # Sets the home button
        home_button = '<td align="center" colspan=2>'
        home_button += '<form action="/logout">'
        home_button += '<button type="submit">Logout</button>'
        home_button += '</td>'

        # Sets the main button
        main_button = '<td align="center" colspan=2>'
        main_button = '<form action="/main">'
        main_button = '<button type="submit">Main</button>'
        main_button = '</form>'
        main_button = '</td>'

        # Replaces the placeholders with the appropriate data
        page = page.replace('{username}', username)
        page = page.replace('{password}', password)
        page = page.replace('{first_name}', first_name)
        page = page.replace('{last_name}', last_name)
        page = page.replace('{email_address}', email_address)
        page = page.replace('{title}', 'Add User')
        page = page.replace('{action}', '/check_add')
        page = page.replace('{button}', 'Add User')
        page = page.replace('{home}', home_button)
        page = page.replace('{main}', main_button)
        page = page.replace('{username_rules}', data_base.get_username_rules())
        page = page.replace('{rules}', data_base.get_password_rules())
        page = page.replace('{message}', message)
        page = page.replace('{access_input}', access_dropdown())
        page = page.replace('{reset_input}', reset_dropdown())

        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


def access_dropdown(access='user', style=1):
    """
    This function sets the dropdown options for the access level.
    """
    # If the style for the area is set to 1
    if style == 1:
        # Starts the dropdown area
        dropdown = '<tr>\n<td colspan=2>'
    # All other style formats
    else:
        # Starts the dropdown area
        dropdown = '<tr>\n<td>'

    # Sets the lable for the access area
    dropdown += '<label for="access">Access Level:</label>\n'
    # If the style is not 1
    if style != 1:
        # Close the table data area
        dropdown += '</td><td>'
    # Begins the select area
    dropdown += '  <select name="access" id="access">\n'
    # If the user's access type is an admin
    if access == 'admin':
        # Sets the option
        dropdown += '    <option value="admin" selected>admin</option>\n'
        dropdown += '    <option value="user">user</option>\n'
    # The user is not an admin
    else:
        # Sets the option
        dropdown += '    <option value="admin">admin</option>\n'
        dropdown += '    <option value="user" selected>user</option>\n'
    # Closes the select area
    dropdown += '  </select>\n'

    # If the style is 1
    if style == 1:
        # Close the table data area
        dropdown += '</td>\n'
    # All other style formats
    else:
        # Close the table data area and the row
        dropdown += '</td></tr>\n'

    # Returns the dropdown list
    return dropdown


def reset_dropdown(reset='1', style=1):
    """
    This function sets the dropdown options for the reset options.
    """
    # If the style for the area is set to 1
    if style == 1:
        # Starts the dropdown area
        dropdown = '<td colspan=2>'
    # All other style formats
    else:
        # Starts the dropdown area
        dropdown = '<tr><td>'

    # Starts the reset area
    dropdown += '<label for="reset">Require Reset:</label>\n'

    # If the style is 1
    if style != 1:
        # Close the table data area
        dropdown += '</td><td>'

    # Begins the select area
    dropdown += '  <select name="reset" id="reset">\n'

    # If the reset value is a 1 to force a reset
    if reset == '1':
        # Sets the option
        dropdown += '    <option value="1" selected>Yes</option>\n'
        dropdown += '    <option value="0">No</option>\n'
    # The reset value is not a 1
    else:
        # Sets the option
        dropdown += '    <option value="1">Yes</option>\n'
        dropdown += '    <option value="0" selected>No</option>\n'
    # Closes the select area
    dropdown += '  </select>\n'
    dropdown += '</td></tr>\n'

    # Returns the dropdown list
    return dropdown


# Creates the edit form
@APP.route('/edit')
# def edit(form='', message='', user=''):
def edit(message='', user=''):
    """
    This Flask function serves the form to edit a user's database information.
    """
    # Establish a connection to the database
    data_base = Database(username=session['username'],
                         access=session['access'])

    # If the user is not in the passed data
    if len(user) == 0:
        # Retrieves the user database information based on the user logged in
        user_data = data_base.get_user_data(session['username'])
    # The user is not in the passed data
    else:
        # Retrieves the user database information based on the passed username
        user_data = data_base.get_user_data(user)

    # Retrieves the contents of the file
    page = get_file_contents("/templates/edit.html")

    # Replaces the placeholders with the appropriate data
    page = page.replace('{message}', message)
    page = page.replace('{header}', 'Edit User Information')
    page = page.replace('{username}', user_data['username'])
    page = page.replace('{first_name}', user_data['first_name'])
    page = page.replace('{last_name}', user_data['last_name'])
    page = page.replace('{email_address}', user_data['email_address'])

    # Sets the default access and reset values
    access_data = '<input type="hidden" name="access" value="user">'
    reset_data = '<input type="hidden" name="reset" value="0">'

    # If the logged in user is an admin
    if is_admin_user():
        # Sets the access and reset values to the dropdown option
        access_data = access_dropdown(user_data['access_type'], 2)
        reset_data = reset_dropdown(user_data['reset'], 2)

    # Replaces the placeholders with the appropriate data
    page = page.replace('{access_input}', access_data)
    page = page.replace('{reset_input}', reset_data)

    # Returns the page
    return page


# Creates the function to delete a user from the database
@APP.route('/delete', methods=['GET'])
def delete():
    """
    This Flask function deletes the selected user from the database.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database()
        # Retrieves the passed data
        get = request.args
        # Deletes the selected user from the database
        data_base.delete_user_from_table(get['user_to_delete'])
        # Returns the page
        return redirect('/users')
    # Redirects the user to the login page so they can try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates a function to check the values to sign up
@APP.route('/check_signup', methods=['POST'])
def check_signup():
    """
    This Flask function checks the values entered during signup to see if
    they are legitimate and sign up the user
    """
    # Establish a connection to the database
    data_base = Database()
    # Retrieves the posted form data
    form = request.form
    # Tests all the field data values and retrieves the values returns
    valid_field_data, failed_fields = data_base.valid_user_values(form)
    # If all the values entered on the form are valid
    if valid_field_data:
        # Adds the user to the database
        data_base.create_user(dict(form))
        # Stores the user's username and access information into a cookie
        session['username'] = form['username']
        # Retrieves the access type set by the database. If the user was
        # the first one to be added, the default access type of 'user'
        # is overwritten by the database to make them an admin. This
        # allows for that change to be tracked
        access = data_base.retrieve_user_access_type(session['username'])
        # Sets the user's access type
        session['access'] = access
        # If the user's data directory dode not exist
        if not os.path.exists('user_data' + os.sep + session['username']):
            # Creates the user's data directory
            os.mkdir('user_data' + os.sep + session['username'])
        # If the user is required to reset their password
        if data_base.is_password_reset_required():
            # Returns the page to allow them to change their password
            return redirect('/change_password')
        # Returns the page
        return redirect('/main')
    # Sets the message to display
    message = 'Invalid parameters entered. Enter your information and try '\
              f'again. {"Fields" if len(failed_fields) > 1 else "Field"} '\
              f'that contain issues: {", ".join(failed_fields)}'
    # Retrurns the form and message
    return signup(form, message)


# Creates a function to check the values entered on the add user page
@APP.route('/check_add', methods=['POST'])
def check_add():
    """
    This function validates the values entered on the add user page.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the posted form data
        form = request.form
        # Tests all the field data values and retrieves the values returns
        valid_field_data, failed_fields = data_base.valid_user_values(form)
        # If all the values entered on the form are valid
        if valid_field_data:
            # Adds the user to the database
            data_base.create_user(dict(form))
            # Redirects to the admin page
            return redirect('/admin')
        # Sets the message area
        message = 'Invalid parameters entered. Modify the information and '\
                  'try again. '\
                  f'{"Fields" if len(failed_fields) > 1 else "Field"} '\
                  f'that contain issues: {", ".join(failed_fields)}'
        # Redirects to add a user and passes in the form an message
        return add_user(form, message)
    # Redirects to the main area as the user is not an admin
    return redirect('/main')


# Creates a function to logout a user
@APP.route('/logout')
def logout():
    """
    This Flask function removes the username and access from the cookie
    to log out a user.
    """
    # If the username is in the session
    if 'username' in session:
        # Deletes the username from the session (cookie)
        del session['username']
    # If the access is in the session
    if 'access' in session:
        # Deletes the access from the session (cookie)
        del session['access']
    # Redirects the user to the login page
    return redirect('/login')


def is_admin_user():
    """
    This function checks to see if the logged in user is an admin
    """
    # Returns True if the username and access are in the session and the
    # access is admin; otherwise, returns False
    return all(['username' in session, 'access' in session,
                session['access'] == 'admin'])


# Creates a function to display the user information in the database
@APP.route('/users')
def users():
    """
    This Flask function retrieves all the users from the database and displays
    their information to the admin user that requested it.
    """
    # If the logged in user is an admin
    if is_admin_user():
        # Establish a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the header information and the user data from the
        # database
        header, user_data = data_base.all_users_in_user_table(True)
        # Creates the web page to display
        page = '<html>\n<head><title>User Information</title>\n'
        page += '<link href="static/css/style.css" rel="stylesheet" '
        page += 'type="text/css" />\n'
        page += '</head>\n'
        page += '<body>\n'
        page += '<center><h1><b><u>Database Users</u></b></h1></center>'
        page += '<table align="center">\n'
        page += '<tr><td align="center">\n'
        page += '<form action="/file_management">\n'
        page += '<p><button type="submit">File Management</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '<td align="center">\n'
        page += '<form action="/main">\n'
        page += '<p><button type="submit">Main</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '<td align="center">\n'
        page += '<form action="/logout">\n'
        page += '<p><button type="submit">Logout</button></p>\n'
        page += '</form>\n'
        page += '</td>\n'
        page += '</tr>\n'
        page += '</table>\n'
        page += '<table border="10%" align="center">\n<tr><th align="center">'
        # Joins the header information for display
        page += '</th>\n<th align="center">'.join(header)
        page += '</th>\n'
        # Adds a column to delete the user
        page += '<th align="center">Delete</th>\n'
        # Adds a column to view user data
        page += '<th align="center">Files</th>\n'
        page += '</tr>\n'
        # Steps through each user
        for user in user_data:
            page += '<tr><td>'
            # Joins the user information for display
            page += '</td>\n<td>'.join(user)
            page += '</td>\n'
            # Makes the username a link to allow their information to be
            # edited
            page = page.replace(f'<td>{user[0]}</td>',
                                f'<td><a href="/edit?user={user[0]}">'
                                f'{user[0]}</a></td>\n')
            # Adds a link to allow for the deletion of the user
            page += f'<td><a href="/delete?user_to_delete={user[0]}" '
            page += 'onclick="return confirm(\'Are you sure you want to '
            page += 'delete?\');">Delete</a></td>'
            page += f'<td><a href="/file_management?username={user[0]}">'
            page += 'View</a></td>'
            page += '</tr>\n'
        page += '</table>\n'
        page += '</body>\n</html>'
        # Returns the page
        return page
    # Redirects the user back to the login page to try again
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# Creates a function for the landing page that is displayed after login
@APP.route('/main', methods=['GET'])
def main():
    """
    This Flask function serves as the web system landing page.
    """
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # # Sets the username to retrieve and view files
        # username = session['username']
        # Retrieves the contents of the file
        page = get_file_contents("/templates/main.html")

        # Replaces the placeholders with the appropriate data
        page = page.replace('{user}', session['username'])
        # move the above code to after the username area below?

        # Sets to empty the button areas
        users_button = ''
        admin_button = ''

        # If the logged in user is an admin
        if is_admin_user():
            # Set the admin button
            admin_button += '<form action = "/admin">\n'
            admin_button += '<p><button type="submit">Admin Page'
            admin_button += '</button></p>\n'
            admin_button += '</form>\n'

        # Replaces the placeholders with the appropriate data
        page = page.replace('{users}', users_button)
        page = page.replace('{admin}', admin_button)
        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# Creates a function to manage files
@APP.route('/file_management', methods=['GET'])
def file_management():
    """
    This Flask function serves the file management page.
    """
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Sets the username to retrieve and view files
        username = session['username']
        # Retrieves the contents of the file
        page = get_file_contents("/templates/file_management.html")

        # Replaces the placeholders with the appropriate data
        page = page.replace('{user}', session['username'])

        # Sets to empty the button areas
        users_button = ''
        admin_button = ''

        # If the logged in user is an admin
        if is_admin_user():
            # Retrieves the passed data
            get = request.args
            # If the username was passed
            if 'username' in get:
                # Retrieves the passed username
                username = get['username']

            # Set the admin button
            admin_button += '<form action = "/admin">\n'
            admin_button += '<p><button type="submit">Admin Page'
            admin_button += '</button></p>\n'
            admin_button += '</form>\n'

        # Sets the upload button
        upload_button = '<form action = "/upload" method = "POST" '
        upload_button += 'enctype = "multipart/form-data">'
        upload_button += '  <input type = "file" name = "file" />'
        upload_button += '  <input type = "hidden" name = "username" '
        upload_button += f'value = "{username}"/>'
        upload_button += '  <button type = "submit"/>Upload</button>'
        upload_button += '</form>'

        # Replaces the placeholders with the appropriate data
        page = page.replace('{username}', username)
        page = page.replace('{users}', users_button)
        page = page.replace('{admin}', admin_button)
        page = page.replace('{upload}', upload_button)
        page = page.replace('{files}', get_directory_listing(username))
        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# Creates a function to check the values entered on the add user page
@APP.route('/table_action', methods=['POST'])
def table_action():
    """
    This Flask function validates the values entered on the add user page.
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = dict(request.form)

        # If the action is to create an exam
        if form['action'] == 'create':
            # Executes the function
            return create_test()
        # If the action was to do any other valid option
        if form['action'] in ['modify', 'exam', 'assign', 'results', 'delete']:
            # Executes the function
            return test_choices(form)

    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# Creates a function to check the values entered on the add user page
@APP.route('/delete_test', methods=['POST'])
def delete_test():
    """
    This Flask function performs the necessary actions to delete a test
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = dict(request.form)

        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # If the requesting user is an admin of the test to delete
        if data_base.is_valid_test_admin(session['username'],
                                         form['test_name']):
            # Deletes the table information
            delete_database_data(data_base, session['username'],
                                 form['test_name'], True)
            # Returns to manage the tests
            return redirect('/manage_tests')
        # Returns to manage the tests with a message
        return redirect('/manage_tests?message=The user requesting does '
                        'not have permissions to delete the test.')
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# Creates a function to check the values entered on the add user page
@APP.route('/test_choices', methods=['POST'])
def test_choices(form, message=''):
    """
    This Flask function executes the requested action
    """
    # If the user is logged in
    if 'username' in session:
        # Sets the possible short action names
        action_list = ['create', 'modify', 'exam', 'assign', 'results',
                       'delete']
        # Creates the options for each short action name
        actions = {'create': {'form_action': '/create_test',
                              'button': 'Create Test',
                              'show_tests': False},
                   'modify': {'form_action': '/edit_test',
                              'button': 'Modify Test',
                              'show_tests': True},
                   'exam': {'form_action': '/start_exam',
                            'button': 'Take Test',
                            'show_tests': True},
                   'delete': {'form_action': '/delete_test',
                              'button': 'Delete Test',
                              'show_tests': True},
                   'assign': {'form_action': '/assign_test',
                              'button': 'Assign Test',
                              'show_tests': True},
                   'results': {'form_action': '/show_results',
                               'button': 'Test Results',
                               'show_tests': True}}

        # Defaults the button
        button = 'Take Action'
        # Defaults the form action
        form_action = '/table_action'
        # Sets to not show test names by default
        show_tests = False
        # Creates the page header
        page = page_header(actions[form['action']]['button'])
        # Provides a prompt for the top of the page to the user
        page += '<p>What action do you want to perform?</p>\n'
        # If a message was passed in
        if message:
            # Adds the message to the page
            page += f'{message}\n'

        # Creates a new default message
        message = '<h1><center>There are no tests to select.</center></h1>'

        # Sets a placeholder for the message - it will resolve later
        page += '{message}'
        # Starts the table and action area for the page
        page += '<table align="center"><tr><td>\n<form action="{form_action}" '
        page += 'method="POST">\n<label for="actions">Action:</label></td><td>'
        page += '\n<select name="action" id="actions">\n'

        # Steps through each short action name
        for action in action_list:
            # Displays the options
            page += f'  <option value="{action}"'
            # If the short action name was selected
            if action == form['action']:
                # Adds selected to indicate the option is selected
                page += ' selected'
                # Sets the appropriate show tests value previously defined for
                # the action
                show_tests = actions[action]['show_tests']
                # Sets the appropriate button value previously defined for
                # the action
                button_selected = actions[action]['button']
                # Sets the appropriate form action value previously defined for
                # the action
                form_action_selected = actions[action]['form_action']
            # Sets to display the appropriate button value
            page += f'>{actions[action]["button"]}</option>\n'
        # Closes the selection area, cell, and opens a new cell
        page += '</select>\n</td><td>\n'

        # If tests are to be shown
        if show_tests:
            # Establishes a connection to the database
            data_base = Database(username=session['username'],
                                 access=session['access'])
            if button_selected == 'Take Test':
                # Retrieves the a list of all the tests - will be filtered
                # again later
                test_names = data_base.get_test_tables(session['username'],
                                                       False)

                # Creates a link to the function to shorten the call
                func = data_base.get_user_session_id

                # Tests to see if the user is able to take the test
                test_names = [test_name for test_name in test_names
                              if func(session['username'], test_name)]
            else:
                # Retrieves the tests that the user has administrative
                # privileges over
                test_names = data_base.get_test_tables(session['username'])

            # Empties the message
            message = ''
            # If there were test names found
            if test_names:
                # Sets the button to the option selected
                button = button_selected
                # Sets the form action to the one selected
                form_action = form_action_selected
                # Displays the label area for the test names
                page += '<label for="test_name">Test Name:</label></td><td>\n'
                page += '<select name="test_name" id="test_name">\n'
                # Steps through each test name
                for test in test_names:
                    # If the test is in the form - it has been chosen
                    if test in form:
                        # Sets to indicate it has been selected
                        page += f'  <option value="{test}" selected>{test}'
                        page += '</option>\n'
                    # The test is not in the form - it has not been selected
                    else:
                        # Sets it as an option
                        page += f'  <option value="{test}">{test}</option>\n'
                # Closes the selection area, cell, and starts a new one
                page += '</select>\n</td><td>\n'
        # If the button is set for the last short action option (delete)
        if button == actions[action_list[-1]]['button']:
            # Sets the button to have a JavaScript feature to it in order to
            # provide a double check it is what the user wants to do before
            # the test is deleted. The placeholder will resolve later.
            page += '<button type="submit" value="Go" onclick="return '
            page += 'confirm(\'Confirm Delete?\')"/>{button}</button>\n'
        # The button is not the last one (delete)
        else:
            # Sets the button to display. The placeholder will resolve later.
            page += '<button type = "submit"/>{button}</button>\n'
        # Closes the form, cell, row, and table and adds the page footer
        page += '</form></td></tr></table>\n' + page_footer()
        # Resolves placeholders on the page with values
        page = page.replace('{message}', message)
        page = page.replace('{button}', button)
        page = page.replace('{form_action}', form_action)
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')
        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


def set_question_area(values, question_type='text', qtype_changed=False,
                      question_num=1):
    """
    This function creates the question area for the page
    """
    # Sets to indicaet no errors are found
    errors = False
    # Indicates no question area
    question_area = ''
    # Sets the question type possibilities
    question_types = {'text': set_text,
                      'true or false': set_true_false,
                      'yes or no': set_yes_no,
                      'multiple choice x 4': set_multiple_choice,
                      'multiple choice x 8': set_multiple_choice,
                      'memo': set_memo,
                      'code segment': set_code_segment}

    # If the question type is a multiple choice one
    if question_type.startswith('multiple choice x '):
        # Retrieves the number of options from the question type
        options = int(question_type[-1])
        # Calls the appropriate function to set the data
        question_area, errors = question_types[question_type](values,
                                                              question_num,
                                                              qtype_changed,
                                                              options)
    # It is any other question type
    else:
        # Calls the appropriate function to set the data
        question_area, errors = question_types[question_type](values,
                                                              question_num,
                                                              qtype_changed)

    # Sets the time area
    time_area, time_errors = set_time_area(values, question_num, qtype_changed)
    # Adds the time area to the question area
    question_area += time_area
    # Returns the question area and if there are any errors
    return question_area, any([errors, time_errors])


def set_text(values, question_num, qtype_changed):
    """
    This function sets the text question type
    """
    # Empties the question area
    question_area = ''
    # Sets the keys for the question
    q_value = f'question_{question_num}'
    a_value = f'answer_{question_num}'
    # Sets to indicate no errors were found
    errors = False

    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variables
        question = ''
        answer = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''
        answer = f'{values[a_value]}' if a_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <input type="text" '
    question_area += f'name="question_{question_num}" value="{question}" '
    question_area += 'placeholder="Enter the question to ask." size=60 '
    question_area += '{read_only}'
    question_area += '>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the question
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>'
    question_area += '<tr><td></td><td>\n'
    question_area += 'Answer: <input type="text" '
    question_area += f'name="answer_{question_num}" value="{answer}" '
    question_area += 'placeholder="Enter the expected answer." size=60 '
    question_area += '{read_only}'
    question_area += '>\n'

    # Sets the hidden value key
    key = f'answer_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the answer
    if len(answer) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid answer</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area
    question_area += '</td></tr>\n'

    # Returns the question area and if there were errors
    return question_area, errors


def set_true_false(values, question_num, qtype_changed):
    """
    This function sets the true/false question type
    """
    # Empties the question area
    question_area = ''
    # Sets the keys for the question
    q_value = f'question_{question_num}'
    a_value = f'answer_{question_num}'
    # Sets to indicate no errors were found
    errors = False

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variables
        question = ''
        answer = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''
        answer = f'{values[a_value]}' if a_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <input type="text"'
    question_area += f'name="question_{question_num}" value="{question}" '
    question_area += 'placeholder="Enter the question to ask." size=60 '
    question_area += '{read_only}'
    question_area += '>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the question
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>'
    question_area += '<tr><td></td><td>\n'
    question_area += 'Answer: True <input type="radio" '
    question_area += f'name="answer_{question_num}" value="True" '
    question_area += '{disabled} ' if answer == 'False' else ''
    question_area += 'checked>' if answer == 'True' else '>'
    question_area += '\nFalse <input type="radio" '
    question_area += f'name="answer_{question_num}" value="False" '
    question_area += '{disabled} ' if answer == 'True' else ''
    question_area += 'checked>\n' if answer == 'False' else '>'
    question_area += '\n'

    # Sets the hidden value key
    key = f'answer_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If the answer is not valid
    if answer not in ['True', 'False'] and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">No answer option selected'
        question_area += '</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area
    question_area += '</td></tr>\n'

    # Returns the question area and if there were errors
    return question_area, errors


def set_yes_no(values, question_num, qtype_changed):
    """
    This function sets the yes/no question type
    """

    # Empties the question area
    question_area = ''
    # Sets the keys for the question
    q_value = f'question_{question_num}'
    a_value = f'answer_{question_num}'
    # Sets to indicate no errors were found
    errors = False

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variables
        question = ''
        answer = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''
        answer = f'{values[a_value]}' if a_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <input type="text" '
    question_area += f'name="question_{question_num}" value="{question}" '
    question_area += 'placeholder="Enter the question to ask." size=60 '
    question_area += '{read_only}'
    question_area += '>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the question
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>\n'
    question_area += '<tr><td></td><td>\n'
    question_area += 'Answer: Yes <input type="radio" '
    question_area += f'name="answer_{question_num}" value="Yes" '
    question_area += '{disabled} ' if answer == "No" else ''
    question_area += 'checked>' if answer == "Yes" else '>'
    question_area += '\nNo <input type="radio" '
    question_area += f'name="answer_{question_num}" value="No" '
    question_area += '{disabled} ' if answer == "Yes" else ''
    question_area += 'checked>\n' if answer == "No" else '>\n'

    # If there is a hidden value stored
    key = f'answer_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If the answer is not valid
    if answer not in ['Yes', 'No'] and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">No answer option selected'
        question_area += '</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area
    question_area += '</td></tr>\n'

    # Returns the question area and if there were errors
    return question_area, errors


def set_multiple_choice(values, question_num, qtype_changed, options):
    """
    This function sets the multiple choice question type
    """

    # Empties the question area
    question_area = ''
    # Sets the key for the question
    q_value = f'question_{question_num}'
    # Sets to indicate no errors were found
    errors = False
    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variable
        question = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <input type="text" '
    question_area += f'name="question_{question_num}" value="{question}" '
    question_area += 'placeholder="Enter the question to ask." size=60 '
    question_area += '{read_only}'
    question_area += '>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the question
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>\n'
    question_area += '<tr><td></td><td>'
    question_area += 'Answer Options: (select answer)<br>\n'

    # Retrieves the option area
    option_area, option_errors, selected_valid = set_options(values,
                                                             question_num,
                                                             qtype_changed,
                                                             options)
    # Adds the options area to the question type
    question_area += option_area

    # Sets the hidden value key
    key = f'answer_{question_num}_hid'
    # If the selection area is not valid
    if not selected_valid and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">No option selected</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area
    question_area += '</td></tr>\n'

    # Returns the question area and if there were errors
    return question_area, any([errors, option_errors])


def set_options(values, question_num, qtype_changed, options):
    """
    This function sets up the option area for the multiple choice question
    type
    """

    # Sets to indicate that the selection is not valid
    selected_valid = False
    # Empties the question area
    question_area = ''
    # Sets to indicate that there are no errors
    errors = False

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # Steps through each number of options
    for num in range(options):
        # Sets the keys for the option and answer
        s_value = f'options_{question_num}_{num}'
        a_value = f'answer_{question_num}'
        # If the question type changed
        if qtype_changed:
            # Empties the values
            selected = ''
            answer = ''
        # The question type did not change
        else:
            # Retrieve the values
            selected = f'{values[s_value]}' if s_value in values else ''
            answer = f'{values[a_value]}' if a_value in values else ''

        # Sets the hidden value key
        key = f'answer_{question_num}_hid'
        # If there is a hidden value stored
        if key in values:
            # Store the hidden value
            question_area += f'<input type="hidden" name="{key}" '
            question_area += f'value="{values[key]}">\n'
        # Sets the option labels from the uppercase letters
        question_area += f'Option {U[num]}. <input type="radio" '
        question_area += f'name="answer_{question_num}" '
        question_area += f'value="{U[num]}" '

        # If the answer selected was found
        if answer == U[num]:
            # Mark the option as selected
            question_area += 'checked>\n'
            # Sets to indicate an answer was selected
            selected_valid = True
        # If the answer selection is not what was chosen
        else:
            # Disables the option
            question_area += '{disabled} >\n'

        # If there is no data in the answer
        if len(answer) == 0 and not qtype_changed and not initial:
            # Adds the error message to the question area
            question_area += ' <font color="red">Invalid answer</font>\n'
            # Sets to indicate there was an error
            errors = True

        # Sets the question option area
        question_area += '\n <input type="text" '
        question_area += f'name="options_{question_num}_{num}" size=60'
        question_area += f' value="{selected}" '
        question_area += 'placeholder="Enter answer choice." '
        question_area += '{read_only}'
        question_area += '>'
        question_area += '<br>\n'

    # Returns the question area, if there were any errors and if the selection
    # was found
    return question_area, errors, selected_valid


def set_memo(values, question_num, qtype_changed):
    """
    This function sets up the memo area for the question type
    """

    # Empties the question area
    question_area = ''
    # Sets the keys for the question
    q_value = f'question_{question_num}'
    a_value = f'answer_{question_num}'
    # Sets to indicate no errors were found
    errors = False

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variables
        question = ''
        answer = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''
        answer = f'{values[a_value]}' if a_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <textarea '
    question_area += f'name="question_{question_num}" '
    question_area += 'placeholder="Enter the question to ask."'
    question_area += 'rows="6" cols="60" '
    question_area += '{read_only}'
    question_area += f'>{question}</textarea>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # Adds the error message to the question area
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>'
    question_area += '<tr><td></td><td>\n'
    question_area += f'Answer: <textarea name="answer_{question_num}" '
    question_area += 'placeholder="Enter the expected answer." '
    question_area += 'rows="6" cols="60" '
    question_area += '{read_only}'
    question_area += f'>{answer}</textarea>\n'

    # Sets the hidden value key
    key = f'answer_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If the answer is not valid
    if len(answer) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid answer</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area
    question_area += '</td></tr>'

    # Returns the question area and if there were errors
    return question_area, errors


def set_code_segment(values, question_num, qtype_changed):
    """
    This function sets up the code segment area for the question type
    """

    # Empties the question area
    question_area = ''
    # Sets the keys for the question
    q_value = f'question_{question_num}'
    a_value = f'answer_{question_num}'
    # Sets to indicate no errors were found
    errors = False

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the question type changed
    if qtype_changed:
        # Empties the variables
        question = ''
        answer = ''
    # The question type did not change
    else:
        # Retrieve the values
        question = f'{values[q_value]}' if q_value in values else ''
        answer = f'{values[a_value]}' if a_value in values else ''

    # Sets the question area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question: <textarea '
    question_area += f'name="question_{question_num}" '
    question_area += 'placeholder="Enter the question to ask." '
    question_area += 'rows="10" cols="60" '
    question_area += '{read_only}'
    question_area += f'>{question}</textarea>\n'

    # Sets the hidden value key
    key = f'question_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # If there is no data in the question
    if len(question) == 0 and not qtype_changed and not initial:
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid question</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the answer area
    question_area += '</td></tr>'
    question_area += '<tr><td></td><td>\n'
    question_area += f'Answer: <textarea name="answer_{question_num}" '
    question_area += 'placeholder="Enter the expected output or enter the '
    question_area += 'validation script. If a value is entered here, it '
    question_area += 'is saved and tested against the code; otherwise, '
    question_area += 'a supplied validation script is saved and used to '
    question_area += 'validate the test taker\'s answer."'
    question_area += 'rows="10" cols="60" '
    question_area += '{read_only}'
    question_area += f'>{answer}</textarea>\n'

    # Sets the hidden value key
    key = f'answer_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'

    # Sets the code file area
    code_area, code_errors = code_file_area(values, question_num,
                                            qtype_changed, answer)

    # Adds the code area to the question area
    question_area += code_area

    # Returns the question area and if there were errors found
    return question_area, any([errors, code_errors])


def code_file_area(values, question_num, qtype_changed, answer):
    """
    This function sets up the code file retrieval area
    """
    # Empties the question area
    question_area = ''
    # Sets to indicate no errors were found
    errors = False
    # Sets the keys for the question
    f_value = f'file_{question_num}'
    f_value_hid = f'file_{question_num}_hid'

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # Sets the file value
    file = f'{values[f_value]}' if f_value in values else ''

    # If there is nothing in the answer
    if all([len(answer) == 0, not qtype_changed, len(file) == 0,
            not initial]):
        # Adds the error message to the question area
        question_area += ' <font color="red">Invalid answer</font>\n'
        # Sets to indicate there was an error
        errors = True

    # Closes the question area and starts the file retrieval area
    question_area += '</td></tr>'
    question_area += '<tr><td></td><td>\n'
    question_area += 'Validation file to test submitted code: (required '
    question_area += 'if expected output is not supplied)<br>\n'
    question_area += f'<input type="file" name="file_{question_num}" '
    question_area += '{read_only}'
    question_area += '>\n'

    # If the file is being uploaded
    if f_value in values and len(values[f_value]) > 0:
        # Sets the file to upload
        file_to_upload = values[f_value]
    # If the file was previously identified
    elif f_value_hid in values and len(values[f_value_hid]) > 0:
        # Retrieves the filename
        file_to_upload = values[f_value_hid]
    # No file has been marked for uploading
    else:
        # Empties the variable
        file_to_upload = ''

    # Stores the filename
    if len(answer) == 0:
        question_area += '<input type="hidden" '
        question_area += f'name="file_{question_num}_hid" '
        question_area += f'value="{file_to_upload}">'

    # If the answer is not valid
    if all([len(answer) == 0, len(file) == 0, f_value in values,
            not qtype_changed, not initial]):
        # Adds the error message to the question area
        question_area += ' <font color="red">No file was provided</font>\n'
    # If there is a file to upload
    elif file_to_upload and len(answer) == 0:
        # Displays the file to be uploaded
        question_area += ' <font color="green">File to upload: '
        question_area += f'{file_to_upload}</font>'

    # Closes the question area
    question_area += '</td></tr>\n'

    # Returns the question area and if there are any errors found
    return question_area, errors


def set_time_area(values, question_num, qtype_changed):
    """
    This function sets up the time area
    """
    # Sets to indicate no errors were found
    errors = False
    # Empties the question area
    question_area = ''
    # Sets the key for the question
    time_value = f'time_{question_num}'

    # If the button is equal to set then True; otherwise, False
    initial = values['button'] == 'set' if 'button' in values else False

    # If the key is not in the values or the question type changed
    if time_value not in values or qtype_changed or initial:
        # Empties the variables
        time_message = ''
        time = ''
    # If the value is not empty and it is all digits
    elif f'{values[time_value]}' and all(d in D for d in
                                         f'{values[time_value]}'):
        # Empties the time message
        time_message = ''
        # Stores the time
        time = f'{values[time_value]}'
    # An invalid value was entered
    else:
        # Adds the error message to the question area
        time_message = ' <font color="red">Invalid time value entered.'
        time_message += '</font>\n'
        # Stores the value that was entered
        time = f'{values[time_value]}'
        # Set to indicate errors were found
        errors = True

    # Starts the time area
    question_area += '<tr><td></td><td>\n'
    question_area += 'Question Time Allowed (minutes): <input type="text" '
    question_area += f'name="time_{question_num}" value="{time}" '
    question_area += 'placeholder="Max Time" maxlength=4 size=4 '
    question_area += '{read_only}'
    question_area += '>\n'
    question_area += time_message

    # Sets the hidden value key
    key = f'time_{question_num}_hid'
    # If there is a hidden value stored
    if key in values:
        # Store the hidden value
        question_area += f'<input type="hidden" name="{key}" '
        question_area += f'value="{values[key]}">\n'
    question_area += '</td></tr>\n'

    # Returns the question area and if there are any errors
    return question_area, errors


# Creates a function for test management
@APP.route('/commit_changes')
def commit_changes(test_data):
    """
    This Flask function serves as a way to commit the changes to create a test.
    """
    message = '<center>Invalid user. Login and try again.</center>'
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # Retrieves the data in the proper format
        formatted_data = format_test_data(test_data)

        # Retrieves the posted form data
        form = request.form

        # If the action is present and it is modify
        if 'action' in form and form['action'] == 'modify':
            # Deletes the old database data
            delete_database_data(data_base, session['username'],
                                 test_data['test_name'], False)

        # If there was an original test name and it does not match the new
        # test name
        if 'test_name_orig' in form and all([form['test_name_orig'],
                                             form['test_name_orig'] !=
                                             form['test_name']]):
            # Delete the old database data
            delete_database_data(data_base, session['username'],
                                 form['test_name_orig'], True)

            # Renames the old results table to the new table
            data_base.rename_test_results_table(session['username'],
                                                form['test_name_orig'],
                                                form['test_name'])

        # Insert the table name into the master table
        data_base.insert_into_master_table(test_data['test_name'],
                                           test_data['description'],
                                           session['username'])

        # Creates the test takers and results tables, if they do not exist
        data_base.create_test_taker_table(session['username'],
                                          test_data['test_name'])

        # Create the table
        data_base.create_test_table(test_data['test_name'])

        # Inserts the formatted data into the table
        data_base.insert_data_into_test(session['username'],
                                        test_data['test_name'],
                                        formatted_data)

        # Sets the message to indicate the test was saved
        message = '<h1><b><center>Test saved.</center></b></h1>\n'
        # Removes the temporary directory used for the test name
        remove_test_dir(test_data['test_name'])
        # Removes an empty permanent test directory for the test name
        remove_test_dir(test_data['test_name'], True, True)
        # Returns back to the page to manage tests
        return manage_tests(message)

    # save test to all tests # test name, access
    return redirect(f'/login?message={message}')


def delete_database_data(data_base, username, test_name, delete_dir=False):
    """
    This function performs the required actions to delete the selected test
    from the database and, if chosen, deletes the test directory
    """
    data_base.delete_selected_table(username, test_name)
    data_base.delete_from_master_table(username, test_name)
    remove_test_dir(test_name, delete_dir)
    # Returns control back to the caller
#    return


def remove_test_dir(test_name, if_empty=False, perm=False):
    """
    This function removes the temporary test directory created for the test
    name
    """
    # Sets the base directory for test files
    base_dir = 'test_files'
    # Sets the temporary directory in the test files
    temp_dir = 'temp'
    # Sets the permanent directory in the test files
    perm_dir = 'tests'
    # If the directory to work with is the permanent directory
    if perm:
        # Sets the path for the permanent name
        base_loc = base_dir + os.sep + perm_dir + os.sep + test_name
    # The directory to work with is the temporary directory
    else:
        # Sets the path for the test name
        base_loc = base_dir + os.sep + temp_dir + os.sep + test_name
    # Attempts to remove the directory
    try:
        # If the location exists
        if os.path.isdir(base_loc):
            # Finds any files in the directory
            found_files = get_directory_structure(base_loc)
            if perm and if_empty and found_files:
                # Remove the directory
                os.rmdir(base_loc)
            else:
                # Steps through each file found
                for found_file in found_files:
                    # Removes the file
                    os.remove(found_file)

                # Remove the directory
                os.rmdir(base_loc)
    # If the directory was not empty and could not be removed
    except OSError:
        # Ignore the exception
        pass


def remove_test_file(file, test_name):
    """
    This function removes the specified file from the test name directory
    """
    # Sets the base directory for test files
    base_dir = 'test_files'
    # Sets the permanent directory in the test files
    perm_dir = 'tests'
    # Sets the full permanent directory path
    base_loc = base_dir + os.sep + perm_dir + os.sep + test_name
    # If the directory does not exist
    if os.path.exists(base_loc):
        # Sets the tempary file name
        filename = base_loc + os.sep + file

        # Attempt to remove the file
        try:
            # Removes the file
            os.remove(filename)
        # The file did not exist
        except FileNotFoundError:
            # Ignores the error
            pass


def save_test_file(file, test_name, move=False):
    """
    This function saves the file used in the code segment questions to a
    temporary location when uploaded and moves it to the test area for use
    """
    # Sets the base directory for test files
    base_dir = 'test_files'
    # Sets the temporary directory in the test files
    temp_dir = 'temp'
    # Sets the permanent directory in the test files
    perm_dir = 'tests'
    # Sets to indicate the file has not been saved
    file_saved = False

    # If the file needs to be moved
    if move:

        # Sets the full temporary directory path
        base_old = base_dir + os.sep + temp_dir + os.sep + test_name
        # Sets the full permanent directory path
        base_loc = base_dir + os.sep + perm_dir + os.sep + test_name
        # If the directory does not exist

        # If the path is not a directory
        if not os.path.isdir(base_loc):
            # Create the directory
            os.mkdir(base_loc)

        # Sets the tempary file name
        filename = base_old + os.sep + file
        # Sets the permanent file name
        new_file = base_loc + os.sep + file
        # Attempt to rename the file
        try:
            # Renames the file
            os.rename(filename, new_file)
            # Indicates the file was saved
            file_saved = True
        # The file did not exist
        except FileNotFoundError:
            # Indicates the file was not saved
            file_saved = False
    # The file is to be uploaded to the temporary location
    else:
        # Sets the full temporary directory path
        base_loc = base_dir + os.sep + temp_dir + os.sep + test_name
        # If the directory does not exist
        if not os.path.isdir(base_loc):
            # Create the directory
            os.mkdir(base_loc)
        # Sets the temporary file name
        filename = base_loc + os.sep + file.filename

        # If the filename set is not a directory (no filename was supplied)
        if not os.path.exists(filename):
            # Save the file
            file.save(filename)
            # If the file exists
            if os.path.exists(filename):
                # Sets to indicate the file was saved
                file_saved = True

    # Returns the result of uploading/moving the file
    return file_saved


def format_test_data(data):
    """
    This function formats the test data for easy storage/printing
    """
    # Creates an empty dictionary
    formatted = {}
    # For each question number in the range
    for question_num in range(1, int(data['num_questions']) + 1):
        # Sets the keys to be found
        values = [f'question_type_{question_num}', f'question_{question_num}',
                  f'answer_{question_num}', f'time_{question_num}',
                  f'options_{question_num}']

        # If the question type area has been disabled
        if f'question_type_{question_num}_hid' in data:
            # Retrieve the question type from its hidden value
            data[f'question_type_{question_num}'] = data['question_type_'
                                                         f'{question_num}_hid']
        # Sets the stored values
        question_type = f'{data[values[0]]}'
        question = f'{data[values[1]]}'
        answer = f'{data[values[2]]}'
        time = f'{data[values[3]]}'
        # Sets to indicate there are no options
        options = ''

        # If the question type is multiple choice
        if question_type.startswith('multiple choice x '):
            # Creates an array for the options
            options = []
            # Steps through the possible option range
            for option in range(int(question_type[-1])):
                # Sets the key for the option
                starter = f'{values[4]}_{option}'
                # Stores the data for the option
                options.append(f'{data[starter]}')
            # Join the options as a string
            options = ':::'.join(options)
        # If the question type is a code segment
        elif question_type == 'code segment':
            # Retrieve the file name
            file_place = f'file_{question_num}'
            # Retrieve the hidden file name
            file_place_hid = f'file_{question_num}_hid'
            # If the file name is in the data
            if file_place in data and data[file_place]:
                # Store the file as the answer
                answer = f'file:{data[file_place]}'
            # If the hidden file name is in the data
            elif file_place_hid in data and data[file_place_hid]:
                # Store the hidden file as the answer
                answer = f'file:{data[file_place_hid]}'

        # Sets the question data
        formatted[question] = {'question_type': question_type,
                               'answer': answer,
                               'options': options,
                               'time': time}
    # Returns the formatted question data
    return formatted


def format_questions(questions, form):
    """
    This function formats question data
    """
    # Steps through each question
    for question_num, question in enumerate(questions, start=1):
        # Sets the keys to be create
        values = [f'question_type_{question_num}', f'question_{question_num}',
                  f'answer_{question_num}', f'time_{question_num}',
                  f'options_{question_num}']
        # Sets the question data into form data
        form[values[0]] = question[1]
        form[values[1]] = question[2]
        form[values[2]] = question[3]
        form[values[3]] = question[5]
        form[values[4]] = question[4]
        # Sets the hidden question data into form data
        form[f'{values[0]}_hid'] = question[1]
        form[f'{values[1]}_hid'] = question[2]
        form[f'{values[2]}_hid'] = question[3]
        form[f'{values[3]}_hid'] = question[5]

        # If the question type is multiple choice
        if form[f'{values[0]}'].startswith('multiple choice x '):
            # Creates an array for the options
            options = form[values[4]].split(':::')
            # Steps through the possible option range
            for option_num, option in enumerate(options):
                # Sets the key for the option
                option_key = f'{values[4]}_{option_num}'
                # Retrieves the option value
                form[option_key] = option
        # If the question type is a code segment
        elif form[f'{values[0]}'] == 'code segment':
            # If the answer points to a file name
            if form[f'{values[2]}'].startswith('file:'):
                # Retrieve the file name
                file = form[values[2]].split(':')[1]
                # Sets the filename information into the form
                form[f'file_{question_num}'] = file
                form[f'file_{question_num}_hid'] = file
                # Empties the answer area as this question will be resolved
                # using the program supplied
                form[f'{values[2]}'] = ''

    # Returns the form data
    return form


# Creates a function for test management
@APP.route('/edit_test', methods=['POST'])
def edit_test(message=''):
    """
    This Flask function serves as a way to edit a test.
    """

    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # Sets the valid question types
        question_types = ['text', 'true or false', 'yes or no',
                          'multiple choice x 4', 'multiple choice x 8',
                          'memo', 'code segment']

        # Retrieves the posted form data
        form = dict(request.form)

        # Empties the questions
        questions = []

        # If there is a test name that has been set
        if 'test_name' in form:
            # If the user is not a test admin
            if not data_base.is_valid_test_admin(session['username'],
                                                 form['test_name']):
                # Redirects the user and provide them with a message
                return redirect(f'/manage_tests?message={session["username"]} '
                                'does not have permission to modify test '
                                f'{form["test_name"]}.')

            # Sets the question values
            questions = data_base.get_test_values(form['test_name'])
            # Retrieves the test description
            description = data_base.get_test_description(session['username'],
                                                         form['test_name'])
            # Stores the test name as hidden
            form['test_name_hid'] = form['test_name']
            # Stores the description
            form['description'] = description
            # Stores the description as hidden
            form['description_hid'] = description
            # Stores the number of questions
            form['num_questions_hid'] = str(len(questions))
            # Stores the number of questions as hidden
            form['num_questions'] = str(len(questions))
            # Formats the question data to be treated as though the data
            # was submitted via a form
            form = format_questions(questions, form)

        # Sets the order for the buttons
        order = ('create', 'set', 'assign', 'confirm')
        # Sets the button values and functon to call (key)
        order_dict = {'create': 'Create Test',
                      'set': 'Set Question Types',
                      'assign': 'Assign Values',
                      'confirm': 'Confirm Test Creation'}
        # Sets the header for the page for each stage
        headers = ('What is the name of the test to create?',
                   'Set the question types to create.',
                   'Assign the questions, options, and answers.',
                   'Is everything correct?')

        # Sets the values needed for the page
        page_values = set_values(form, data_base)

        # If there is a message to display
        if message:
            # Appends the original message before the newly found message
            page_values['message'] = message + page_values['message']

        # Sets the current button name
        page_values['button_name'] = form['button'] if 'button' in form else ""
        # Retrieves the new button name

        # If the button name has not been set and the test is not being
        # modified
        if not page_values['button_name'] and form['action'] == 'modify':
            # Sets to indicate that it is an initial run through
            page_values['button_name'] = 'set'

        # Retrieves the page values
        page_values = get_button(page_values, order, form)

        # If the page_value returns as a string, the data has been uploaded
        # and returns the string
        if isinstance(page_values, str):
            # Returns the string
            return page_values

        # Sets the header for the page
        page = page_header('Edit Test', page_values['message'])
        # Sets the body for the page
        page += test_creation_body_top('/create_test')
        # Stores a hidden value to set the action as it is being modified
        page += '<input type="hidden" name="action" value="modify">\n'
        # Sets the questions area for the page
        page_data, page_values = test_creation_questions(form, page_values,
                                                         question_types, True)
        # Adds the question area retrieved to the page
        page += page_data
        # Sets the botton of the page
        page_data, page_values = test_creation_body_bottom(page_values)
        # Adds the body bottom area retrieved to the page
        page += page_data

        # Repaces placeholders with variable data
        page = page.replace('{header_placeholder}',
                            headers[page_values['sequence_order']])
        page = page.replace('{test_name}', page_values['test_name'])
        page = page.replace('{test_name_hid}', page_values['test_name_hid'])
        page = page.replace('{description}', page_values['description'])
        page = page.replace('{description_hid}',
                            page_values['description_hid'])
        page = page.replace('{test_name_orig}', page_values['test_name_orig'])
        page = page.replace('{read_only}', page_values['read_only'])
        page = page.replace('{disabled}', page_values['disabled'])
        page = page.replace('{num_questions}', page_values['num_questions'])
        page = page.replace('{num_questions_hid}',
                            page_values['num_questions_hid'])
        page = page.replace('{button_name}',
                            order[page_values['sequence_order']])
        page = page.replace('{button_value}',
                            order_dict[order[page_values['sequence_order']]])
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')

        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


# Creates a function for test management
@APP.route('/create_test', methods=['POST'])
def create_test(message=''):
    """
    This Flask function serves as a way to create a test.
    """
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # Sets the valid question types
        question_types = ['text', 'true or false', 'yes or no',
                          'multiple choice x 4', 'multiple choice x 8',
                          'memo', 'code segment']

        # Retrieves the posted form data
        form = dict(request.form)

        # Sets the order for the buttons
        order = ('create', 'set', 'assign', 'confirm')
        # Sets the button values and functon to call (key)
        order_dict = {'create': 'Create Test',
                      'set': 'Set Question Types',
                      'assign': 'Assign Values',
                      'confirm': 'Confirm Test Creation'}
        # Sets the header for the page for each stage
        headers = ('What is the name of the test to create?',
                   'Set the question types to create.',
                   'Assign the questions, options, and answers.',
                   'Is everything correct?')

        # Sets the values needed for the page
        page_values = set_values(form, data_base)

        # If there is a message to display
        if message:
            # Appends the original message before the newly found message
            page_values['message'] = message + page_values['message']

        # Sets the current button name
        page_values['button_name'] = form['button'] if 'button' in form else ""
        # Retrieves the new button name

        # Retrieves the page values
        page_values = get_button(page_values, order, form)

        # If the page_value returns as a string, the data has been uploaded
        # and returns the string
        if isinstance(page_values, str):
            # Returns the string
            return page_values

        # Sets the header for the page
        page = page_header('Test Creation', page_values['message'])

        # Sets the body for the page
        page += test_creation_body_top('/create_test')

        # If the page is being modified
        if 'action' in form and form['action'] == 'modify':
            # Sets to keep the page being modified
            page += '<input type="hidden" name="action" value="modify">\n'
        # The page is not being modified
        else:
            # The page is being created
            page += '<input type="hidden" name="action" value="create">\n'
        # Sets the questions area for the page

        # Sets the test question area
        page_data, page_values = test_creation_questions(form, page_values,
                                                         question_types)

        # Adds the question area retrieved to the page
        page += page_data
        # Sets the botton of the page

        # Creates the bottom of the testing area
        page_data, page_values = test_creation_body_bottom(page_values)
        # Adds the body bottom area retrieved to the page
        page += page_data

        # If there are changes that have been made
        if page_values['changes']['question_types']:
            # Unset these values as if this iteration through did not happen
            page_values['sequence_order'] -= 1
            page_values['read_only'] = ''
            page_values['disabled'] = ''
        # If the sequence order becomes less than zero
        if page_values['sequence_order'] < 0:
            # Reset it to zero
            page_values['sequence_order'] = 0
        # Repaces placeholders with variable data
        page = page.replace('{header_placeholder}',
                            headers[page_values['sequence_order']])
        page = page.replace('{test_name}', page_values['test_name'])
        page = page.replace('{javascript}', '')
        page = page.replace('{test_name_orig}', page_values['test_name_orig'])
        page = page.replace('{test_name_hid}', page_values['test_name_hid'])
        page = page.replace('{description}', page_values['description'])
        page = page.replace('{description_hid}',
                            page_values['description_hid'])
        page = page.replace('{read_only}', page_values['read_only'])
        page = page.replace('{disabled}', page_values['disabled'])
        page = page.replace('{num_questions}', page_values['num_questions'])
        page = page.replace('{num_questions_hid}',
                            page_values['num_questions_hid'])
        page = page.replace('{button_name}',
                            order[page_values['sequence_order']])
        page = page.replace('{button_value}',
                            order_dict[order[page_values['sequence_order']]])
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')

        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


def set_values(form, data_base):
    """
    This function sets the initial values for the page
    """
    # Creates a dictionary to store the data
    page_values = {'changes': {'question_types': 0},
                   'test_name': '',
                   'test_name_orig': '',
                   'description': '',
                   'num_questions': ''}
    # The minimum number of question allowed for a test
    min_questions = 1
    # The maximum number of question allowed for a test
    max_questions = 100
    # The minimum number of characters allowed
    min_chars = 3
    # Set to indicate the test name is not valid
    valid_test_name = False
    # Set to indicate the number of questions is not valid
    valid_num_questions = False
    # Set to indicate that no errors were found
    errors = False
    # Empties the message
    message = ''
    # Sets the areas that will be initialized as empty
    values = ('num_questions', 'test_name', 'description', 'read_only',
              'disabled', 'num_questions_hid')

    # Steps through each area to be initialized
    for value in values:
        # Empties each area
        page_values[value] = ''

    # Retrieves the values from the num questions area
    page_values, form, valid_num_questions, new_message, errors =\
        set_num_questions_area(page_values, form, errors, min_questions,
                               max_questions)

    # Saves the new comments
    message += new_message

    # If there was a description
    if 'description' in form:
        # Set the description
        page_values['description'] = form['description']
        # If the description is too short
        if len(page_values['description']) < 5:
            # Sets the message
            message += '<h1><font color="red">The description should be short '
            message += 'and provide a user with information on what the test '
            message += 'is intended to focus on.</font><br></h1>\n'
            # Set to indicate errors were found
            errors = True
    # If there was no description set
    else:
        # Defaults the description to be empty
        form['description'] = ''

    # If there was a hidden description
    if 'description_hid' in form:
        # Retrieve the hidden description
        page_values['description_hid'] = form['description_hid']
        # If the hidden description does not match the current description
        if form['description'] != form['description_hid']:
            # Sets to indicate that a change was made
            page_values['changes']['description'] = True
    # If there was no hidden description
    else:
        # Defaults the hidden description to the current description value
        page_values['description_hid'] = form['description']

    # Retrieves the values from the test name
    page_values, form, valid_test_name, new_message, errors =\
        set_test_name_area(page_values, form, errors, data_base, min_chars)

    # Saves the new comments
    message += new_message

    # Set the values
    page_values['valid_test'] = all([valid_test_name, valid_num_questions])
    page_values['valid_num_questions'] = valid_num_questions
    page_values['message'] = message
    page_values['errors'] = errors

    # Returns the page vaoues retrievd
    return page_values


def set_num_questions_area(page_values, form, errors, min_questions,
                           max_questions):
    """
    This function sets the values for the num_questions area
    """
    # Set to indicate the number of questions entered is invalid
    valid_num_questions = False
    # Creates a default message
    message = ''
    # If there was a number of question provided
    if 'num_questions' in form:
        # Retrieve the number of question provided
        page_values['num_questions'] = form['num_questions']

        # If the value retrieved was not empty, they were all digits, and
        # it is in range
        if page_values['num_questions'] and\
            all(c in D for c in page_values['num_questions']) and\
                min_questions <= int(page_values['num_questions'])\
                <= max_questions:
            # Set to indicate the number of questions entered is valid
            valid_num_questions = True
        # There was a problem with the number of questions entered
        else:
            # Sets the message
            message += '<h1><font color="red">The number of questions is '
            message += 'invalid. The value must be a number ranging from '
            message += f'{min_questions} to {max_questions} (inclusive).'
            message += '</font><br></h1>\n'
            # Set to indicate errors were found
            errors = True
            form['test_name_hid'] = form['test_name']
            form['description_hid'] = form['description']
    # If a number of questions was not provided
    else:
        # Empties the number of questions value
        form['num_questions'] = ''

    # If the number of question was updated
    if 'num_questions_hid' in form:
        # Save the old number of questions
        page_values['num_questions_hid'] = form['num_questions_hid']
        # If the number of questions retrieved is not the same as the
        # previous number of questions found
        if form['num_questions'] != form['num_questions_hid']:
            # Sets to indicate that changes were made to the page
            page_values['changes']['num_questions'] = True
    # The number of questions has never been set
    else:
        # Store the number of questions as a hidden value
        page_values['num_questions_hid'] = form['num_questions']

    # Returns the set data
    return page_values, form, valid_num_questions, message, errors


def set_test_name_area(page_values, form, errors, data_base, min_chars):
    """
    This function sets the values for the test name
    """
    # Creates a default message
    message = ''
    # Set to indicate that the test name is invalid
    valid_test_name = False

    # If there was a test name
    if 'test_name' in form:
        # Set the test name
        page_values['test_name'] = form['test_name']
        # Sets to test if the table name exists in the database
        name_exists = not data_base.table_exists(page_values['test_name'])
        # If the name does not exist and the page is being modified
        if not name_exists and form['action'] == 'modify':
            # Sets to indicate the name exists to allow for modification
            # to happen; otherwise, the next test area will fail as the
            # test already exists and if it exists, it cannot create a new
            # one with the same name
            name_exists = True

        # If the test name is long enough and meets formatting criteria
        if len(page_values['test_name']) > 3 and\
                all([page_values['test_name'][0] in U + L] +
                    [c in D + U + L + '_' for c in page_values['test_name']] +
                    [name_exists]):
            # Set to indicate the test name is valid
            valid_test_name = True
        # There was an error with the test name
        else:
            # Sets the message
            message += '<h1><font color="red">The test name is invalid.<br>It '
            message += f'must be greater than {min_chars} characters in length'
            message += ' and the name must not be one that already exists '
            message += 'within the test system. <br>This test name '
            message += f'{"exists" if name_exists else "does not exist"} '
            message += 'within the test system.</font><br></h1>'
            # Set to indicate that errors were found
            errors = True
    # The test name was not set
    else:
        # Defaults the value to be empty
        form['test_name'] = ''

    # If there was an original test name
    if 'test_name_orig' in form:
        # Reset the original test name to what it was
        page_values['test_name_orig'] = form['test_name_orig']
    # There was not an original test name
    else:
        # Set the original test name to the current test name
        page_values['test_name_orig'] = form['test_name']

    # If there was a test name being hidden
    if 'test_name_hid' in form:
        # Retrieves the hidden test name
        page_values['test_name_hid'] = form['test_name_hid']
        # If the hidden test name is not the same as the current test name
        if form['test_name'] != form['test_name_hid']:
            page_values['changes']['test_name'] = True
    # There was no hidden test name
    else:
        # Store the current test name as the hidden test name
        page_values['test_name_hid'] = form['test_name']

    # Returns the retrieved data
    return page_values, form, valid_test_name, message, errors


def get_button(page_values, order, form):
    """
    This function sets up the button information
    """
    # Initializes the variables
    read_only = ''
    disabled = ''
    sequence_order = 0

    # If the value was set to indicate it is a valid test
    if page_values['valid_test']:
        # If the button option matches
        if page_values['button_name'] == order[0]:
            # Set the sequence order
            sequence_order = 1
        # If the button option matches
        elif page_values['button_name'] == order[1]:
            # Set the sequence order
            sequence_order = 2
        # If the button option matches
        elif page_values['button_name'] == order[2]:
            # Set the sequence order
            sequence_order = 3
            # Indicates items should be read only
            read_only = 'readonly'
            # Indicates items should be disabled
            disabled = 'disabled="true"'
        # If the button option matches
        elif page_values['button_name'] == order[3]:
            # Steps through each question
            for num in range(1, int(page_values['num_questions']) + 1):
                # Sets the old file name, if the file was being replaced
                if f'answer_{num}_hid' in form and\
                  form[f'answer_{num}_hid'].startswith('file:'):
                    remove_test_file(form[f'answer_{num}_hid'].split(':')[1],
                                     form['test_name'])
                # Sets the file key
                f_value = f'file_{num}_hid'
                # If the key is in the form and not empty
                if f_value in form and form[f_value]:
                    # Retrieves the file name
                    file = form[f_value]
                    # Saves the test file
                    save_test_file(file, page_values['test_name'], True)
                    # Sets the file into the form
                    form[f_value] = file
            # Save the data
            return commit_changes(form)
        # Otherwise, an unrecognized value was retrieved
        else:
            # Sets the error message
            error_message = '<center>Invalid command provided.</center>\n'
            # Returns the user back to the start with an error message
            return manage_tests(error_message)

    # Sets the required variable values
    page_values['read_only'] = read_only
    page_values['disabled'] = disabled
    page_values['sequence_order'] = sequence_order

    # Returns the page values
    return page_values


def page_header(title, message=''):
    """
    This function sets the page header
    """
    # Sets the page header information
    page = f'<html><head><title>{title}</title>\n'
    page += '<link href="static/css/style.css" rel="stylesheet" '
    page += 'type="text/css" />\n'
    page += '{javascript}'
    page += '{meta}'
    page += '</head><body>\n'
    page += f'<center><h1><b><u>{title}</u></b></h1></center>\n'
    page += '<table align="center">'
    page += '<tr>'
    page += '<td>\n'
    page += '<form action="/main">\n'
    page += '  <button type="submit">Main</button>\n'
    page += '</form>\n'
    page += '</td>'
    page += '<td>\n'
    page += '<form action = "/edit">\n'
    page += '  <button type="submit">Edit My Information</button>\n'
    page += '</form>\n'
    page += '</td>'

    # If the logged in user is an admin
    if is_admin_user():
        # Set the admin button
        page += '<td>\n'
        page += '<form action = "/admin">\n'
        page += '  <button type="submit">Admin Page</button>\n'
        page += '</form>\n'
        page += '</td>\n'

    page += '<td>\n'
    page += '<form action = "/logout">\n'
    page += '  <button type="submit">Logout</button></p>\n'
    page += '</form>\n'
    page += '</td>'
    page += '</tr>'
    page += '</table>\n'
    # If a message was provided
    if message:
        # Add the message to the page
        page += f'<center>{message}</center>\n'
    # Return the page
    return page


def test_creation_body_top(form_action):
    """
    This function sets the top of the body area
    """
    # Sets the top of the body area
    page = '<p>{header_placeholder}</p>\n'
    page += f'<form action="{form_action}" method="POST" '
    page += 'enctype="multipart/form-data">\n'
    page += '<table align="center"><tr><td>'
    page += 'Test Name:</td><td>\n'
    page += '<input type="text" name="test_name" value="{test_name}" '
    page += 'placeholder="Enter a name for your test" required '
    page += '{read_only}>\n'
    page += '<input type="hidden" name="test_name_hid" '
    page += 'value="{test_name_hid}">\n'
    page += '<input type="hidden" name="test_name_orig" '
    page += 'value="{test_name_orig}">\n'
    page += '</td></tr>\n'

    page += '<tr><td>'
    page += 'Description:</td><td>\n'
    page += '<input type="text" name="description" value="{description}" '
    page += 'placeholder="Enter a description of your test" required '
    page += '{read_only}>\n'
    page += '<input type="hidden" name="description_hid" '
    page += 'value="{description_hid}">\n'
    page += '</td></tr>\n'

    page += '<tr><td>'
    page += 'Number of Questions:</td><td>\n'
    page += '<input type="hidden" name="num_questions_hid" '
    page += 'value="{num_questions_hid}">\n'
    page += '<input type="text" name="num_questions" '
    page += 'value="{num_questions}" '
    page += 'placeholder="Enter the number of questions" size=3 maxsize=3 '
    page += 'required {read_only}>\n'
    page += '</td></tr>\n'

    # Returns the page
    return page


def retrieve_question_value(form, test_value):
    """
    This function retrieves the question value
    """
    # If the key is in the form, return the data; otherwise return an empty
    # string
    return form[test_value] if test_value in form else ''


def retrieve_hidden_question_value(form, test_value, edit_test_data,
                                   test_orig):
    """
    This function retrieves and tests for a hidden question value
    """
    # If the key is in the form
    if test_value in form:
        # Returns the stored data
        return form[test_value]
    if edit_test_data:
        # Returns the stored data
        return form[test_orig]
    # Returns an empty string
    return ''


def test_creation_questions(form, page_values, question_types,
                            edit_test_data=False):
    """
    This function creates the questions for the page
    """
    # Initializes the dictionary to store the retrieved data
    test_values = {}
    # Sets a shorter name for the function for calling it
    select_func = test_creation_questions_select
    # Initializes the page
    page = ''

    # If it is a valid test
    if page_values['valid_test']:
        # Step through each question
        for num in range(1, int(page_values['num_questions']) + 1):
            # Stores the key
            test_values['f_value'] = f'file_{num}'
            # If the key is in the file
            if test_values['f_value'] in request.files:
                # Retrieve the file name
                file = request.files[test_values['f_value']]
                # Save the file name
                save_test_file(file, page_values['test_name'])
                # Store the filename
                form[test_values['f_value']] = file.filename

            # Stores the key
            test_values['o_value'] = f'question_{num}'
            test_values['o_option'] =\
                retrieve_question_value(form, test_values['o_value'])
            # Stores the key
            test_values['p_value'] = f'{test_values["o_value"]}_hid'
            test_values['p_option'] =\
                retrieve_hidden_question_value(form, test_values['p_value'],
                                               edit_test_data,
                                               test_values['o_value'])

            # If the value is in the form and the there is a previous number
            # of questions that does not match
            page_values['changes'][f'question_{num}'] =\
                test_values['p_value'] in form and\
                form[test_values['p_value']] != test_values['o_option']

            # Starts the question area
            page += '<tr><td>\n'
            page += f'<label for="actions_{num}">Question {num}:</label>\n'
            page += '</td><td>\n'
            # If the value is in the form
            if test_values['o_value'] in form or edit_test_data:
                # Store the value as hidden
                page += f'<input type="hidden" name="{test_values["p_value"]}"'
                page += f' value="{test_values["o_option"]}">\n'

            # Set the key
            t_option = f'question_type_{num}'
            # If the key is in the form
            if t_option in form:
                if f'{t_option}_hid' in form:
                    if form[f'{t_option}_hid'] != form[f'{t_option}']:
                        page_values['changes'][t_option] = True
                        page_values['changes']['question_types'] += 1
                    # Stores the data
                    test_values[f'{t_option}_hid'] = form[t_option]
                else:
                    # Stores the data
                    test_values[f'{t_option}_hid'] = ''
                # Store the value as hidden
                page += f'<input type="hidden" name="{t_option}_hid" '
                page += f'value="{form[t_option]}">\n'

            # Sets the question type area
            page += f'<select name="question_type_{num}" '
            page += f'id="question_type_{num}" '
            page += '{disabled}'
            page += '>\n'

            # Empties the selected type
            selected_type = ''
            # Sets the selection area for the questions
            page_select, test_values = select_func(form, num, question_types,
                                                   test_values)
            # Adds the retieved page select data to the page
            page += page_select
            # Closes the select area
            page += '</select>\n</td></tr>\n'
            # If the sequence order is greater than or equal to two
            if page_values['sequence_order'] >= 2:
                # Set the selected type for the question type
                selected_type = form[f'question_type_{num}']
                if f'question_type_{num}' in page_values['changes']:
                    changed = page_values['changes'][f'question_type_{num}']
                else:
                    changed = False

                # Sets the question area
                data, errors = set_question_area(form, selected_type,
                                                 changed, num)
                # Stores the new page data into the page
                page += data

                # If errors were found
                if errors:
                    # Set to indicate there are errors
                    page_values['errors'] = True

    # Returns the page and stored data
    return page, page_values


def test_creation_questions_select(form, num, question_types, test_values):
    """
    This function creates the select options for the question
    """

    # Creates an empty page
    page = ''

    # Steps through the question types
    for question_type in question_types:
        # Sets the key value
        test_values['q_value'] = f'question_type_{num}'

        # If the key value is in the form
        if test_values['q_value'] in form:
            # Store the data
            test_values['q_type'] = form[test_values['q_value']]
        # The key value is not in the form
        else:
            # Store empty data
            test_values['q_type'] = ''

        # If the key is in the data and the option selected matches
        if test_values['q_type'] and form[test_values['q_value']] ==\
                question_type:
            # Marks the option as selected
            page += f'  <option value="{question_type}" selected>'
        # It is one of the other options
        else:
            # Disables the other options and sets their value
            page += '  <option {disabled} '
            page += f'value="{question_type}">'
        # Adds the question type to the drop down
        page += f'{question_type}</option>\n'

    # Returns the page and the test values
    return page, test_values


def test_creation_body_bottom(page_values):
    """
    This function creates the bottom of the page
    """
    # If there are any errors or the page has new queston types
    if page_values['errors']:
        # Empties the variables
        page_values['read_only'] = ''
        page_values['disabled'] = ''
        # Resets the sequence order back one
        page_values['sequence_order'] -= 1
    # Store the bottom of the page
    page = '<td align="center">\n'
    page += '<input type="reset" value="Reset to Defaults" />\n'
    page += '</td>'
    page += '<td align="center">\n'
    page += '<button name="button" type="submit" '
    page += 'value="{button_name}"/>'
    page += '{button_value}</button>\n'
    page += '</td></tr>'
    page += '</form>'
    page += page_footer()
    # Returns the page and the page values
    return page, page_values


def page_footer():
    """
    This function returns the footer for the page
    """
    # Returns the footer information
    return '</body></html>'


# Creates a function to catch exceptions raised on the page
@APP.errorhandler(Exception)
def handle_exception(error):
    """
    This Flask funcation catches exceptions on the page
    """
    return f'{error}'


# Creates a function for test management
@APP.route('/manage_tests')
def manage_tests(message=''):
    """
    This Flask function serves the test management page.
    """
    # If the username is in the session to indicate that the user is logged in
    if 'username' in session:
        # Creates the page
        page = page_header('Test Management')
        page += '<p>What action do you want to perform?</p>\n'
        page += '{message}\n'
        page += '<table align="center"><tr><td>\n'
        page += '<form action="/table_action" method="POST">\n'
        page += '<label for="actions">Action:</label></td><td>\n'
        page += '<select name="action" id="actions">\n'
        page += '  <option value="create">Create Test</option>\n'
        page += '  <option value="modify">Modify Test</option>\n'
        page += '  <option value="exam">Take Test</option>\n'
        page += '  <option value="assign">Assign Test</option>\n'
        page += '  <option value="results">Test Results</option>\n'
        page += '  <option value="delete">Delete Test</option>\n'
        page += '</select>\n'
        page += '</td><td>\n'
        page += '<button type = "submit"/>Take Action</button>\n'
        page += '</form></td></tr>\n'
        page += page_footer()

        # Replaes the placeholder with the variable data
        page = page.replace('{message}', message)
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')
        # Returns the page
        return page
    # Redirects the user to the login page to try again
    return redirect('/login?message=Invalid user. Login and try again.')


def get_directory_listing(username):
    """
    This function performs the necessary actions to walk the user's directory
    and set up the files to download.
    """
    # Gets the path to the logged in user's directory
    path = 'user_data' + os.sep + username
    # Retrieves the file found from the path
    get_files = get_directory_structure(path)
    # If there are no files found
    if len(get_files) == 0:
        # Returns an empty string
        return ''
    # Returns the formatted files for downloading
    return set_directory_structure(get_files, username)


def get_directory_structure(path):
    """
    This function retrieves files and their size found on the system
    starting from the path value.
    """
    # Creates an empty dictionary
    files_found = {}
    # Retrieves the directory path, directories, and files from the os walk
    for walk_info in os.walk(path):
        # Steps through each file found
        for filename in walk_info[2]:
            # Sets the file path and filename
            fname = os.path.join(walk_info[0], filename)
            # If the file is readable
            if os.access(fname, os.R_OK):
                # Reads the file size
                fsize = os.stat(fname).st_size
                # Formats the file size into kilobytes
                files_found[fname] = f'{round(fsize/(1024),3)} Kb'
    # Returns the dictionary of files found
    return files_found


def set_directory_structure(file_list, username):
    """
    This function sets the HTML formatted file structure found on the system
    within the logged in user's path to the user
    """
    # Starts the return value
    return_value = '<table border=0>\n'
    # Sets the header
    return_value += '<tr><td align="center">Filename</td>'
    return_value += '<td align="center">File Size</td>'
    return_value += '<td align="center">Action</td></tr>\n'
    # Steps through each file in the dictionary
    for fname in file_list:
        # Sets the file information
        return_value += '<tr><td>'
        return_value += f'<a href="/download?file={fname}">'
        return_value += f'{fname.split(os.sep)[-1]}</a>'
        return_value += '</td>\n'
        return_value += f'<td align="right">{file_list[fname]}</td>\n'
        return_value += f'<td><a href="/delete_file?file_to_delete={fname}&'
        return_value += f'username={username}" '
        return_value += 'onclick="return confirm(\'Are you sure you want to '
        return_value += 'delete?\');">Delete</a></td>'
        return_value += '<form action="delete_file"</tr>\n'
    # Closes the table
    return_value += '</table>\n'
    # Returns the formatted file structure
    return return_value


# Creates a function to delete files
@APP.route('/delete_file', methods=['GET'])
def delete_file():
    """
    This Flask function deletes the selected file.
    """
    # Retrieves the passed data
    get = request.args
    # Sets the file to delete
    path = get['file_to_delete']

    # Sets the username
    username = get['username']
    # If the path provided is a directory
    if os.path.isdir(path):
        try:
            # Remove the directory
            os.rmdir(path)
        except OSError:
            # Ignores the error
            pass
    # The path provided is a file
    else:
        # Remove the selected file
        os.remove(path)
    # Returns to the file management page
    return redirect(f'/file_management?username={username}')


@APP.route('/download', methods=['GET'])
def download():
    """
    This Flask function downloads the selected file.
    """
    # Retrieves the passed data
    get = request.args
    # Downloads the selected file
    return send_file(get['file'], as_attachment=True)


def set_java_script(milliseconds):
    """
    This function sets the values for the JavaScript area found in the page
    header
    """
    # Sets to force the page to go to the next question/end after the specified
    # amount of time has passed
    java_script = '<script>\n'
    java_script += '    var timer = setTimeout(function() {\n'
    java_script += '      window.location="/display_question"\n'
    java_script += '    }, '
    java_script += f'{milliseconds});\n'
    java_script += f'  var time = {milliseconds / 1000 * 60};\n'
    java_script += '  var countdown_timer = setInterval(function(){\n'
    java_script += '    if(timeleft <= 0){\n'
    java_script += '      clearInterval(countdown_timer);\n'
    java_script += '    }\n'
    java_script += '    document.getElementById("countdown").textContent = '
    java_script += 'time;\n'
    java_script += '    timeleft -= 1;\n'
    java_script += '  }, 1000);\n'

    # Sets to display and update a countdown timer and progress bar
    java_script += f'  ProgressCountdown({milliseconds / 1000}, '
    java_script += '"pageBarCountdown", "pageTextCountdown");\n'
    java_script += '  function ProgressCountdown(time_left, bar, text) {\n'
    java_script += '    return new Promise((resolve, reject) => {\n'
    java_script += '      var countdown_timer = setInterval(() => {\n'
    java_script += '        time_left--;\n'
    java_script += '        document.getElementById(bar).value = time_left;\n'
    java_script += '        document.getElementById(text).textContent = '
    java_script += 'time_left + " Seconds";\n'
    java_script += '        if (time_left <= 0) {\n'
    java_script += '          clearInterval(countdown_timer);\n'
    java_script += '          resolve(true);\n'
    java_script += '        }\n'
    java_script += '      }, 1000);\n'
    java_script += '    });\n'
    java_script += '  }\n'
    java_script += '</script>\n'
    # Returns the JavaScript area
    return java_script


@APP.route('/start_exam', methods=['GET', 'POST'])
def start_exam(message=''):
    """
    This Flask function shows the start page for a test
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = request.form

        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # Retrieves the questions from the database
        questions = data_base.get_test_values(form['test_name'])

        # If no questions were retrieved
        if not questions:
            # Returns to manage the tests
            return redirect('/manage_tests?message="No questions to display."')

        # Stores the queston data with associated id in the session
        session['question_data'] = {question[2]: question[0] for question in
                                    questions}

        # Sets just the questions
        session['questions'] = list(session['question_data'].keys())

        # Shuffles the questions to allow for the questions to be displayed
        # to the user in a random order
        random.shuffle(session['questions'])

        # Sets the ids for the questions in the same order
        session['ids'] = ':'.join([str(session['question_data'][question]) for
                                   question in session['questions']])

        # Sets the range of question possibilities from 1 to n
        session['question_nums'] = list(range(1, len(session['questions'])+1))

        # Stores the test name
        session['test_name'] = form['test_name']

        # Sets the page header area
        page = page_header('Test Start', message)

        # Sets the test start message
        page += test_start_message()

        # Sets the page footer
        page += page_footer()

        # Resolves placeholders stored on the page
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')

        # Returns the page
        return page
    # The logged in user is not an admin and is redirected to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


def test_start_message():
    """
    This function sets the message to display before the user begins the test
    """
    page = '<table width="40%" align="center">\n'
    page += '<tr><td align="center">\n'
    page += 'When the button below is clicked, the test will begin.\n'
    page += 'Each question has a time associated with it.\n'
    page += 'If the question is not answered within this time, '
    page += 'the page automatically moves to the next question until '
    page += 'all the questions are complete. In order to save an answer, '
    page += 'the button for the question must be clicked. This also moves '
    page += 'the page through the questions, until all questions been '
    page += 'displayed.\n'
    page += '</td></tr>\n'
    page += '<tr><td align="center">\n'
    page += 'There is a timer on each page that displays the amout of time'
    page += 'that remains for the question.\n'
    page += '</td></tr>\n'
    page += '<tr><td align="center">\n'
    page += '<b>When you are ready, click the button and begin. '
    page += 'Good Luck.</b>\n'
    page += '</td></tr></table>\n'
    page += '<center>'
    page += '<form action="/display_question">\n'
    page += '<button>Begin Test</button>\n'
    page += '</form>\n'
    page += '</center>\n'
    # Returns the page data
    return page


@APP.route('/finish_test', methods=['GET', 'POST'])
def finish_test(message=""):
    """
    This function finalizes a test taken by a user
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = request.form
        # If there was an answer in the form
        if 'answer' in form:
            # Store the curren answer at the end of the saved answers
            session['answers'] += [form['answer']]

        # Sets the message to display to the user
        message += '<center>Thank you for completing the exam.</center>'
        # Sets the page header
        page = page_header(f'{session["test_name"]} Exam', message)
        # Sets the score and retrieve which questions were answered correctly
        valid_answers = grade_test()
        # Calculates the test taker's score
        score = round(sum(valid_answers) / len(valid_answers) * 100, 2)
        # Steps through each answer area
        for index_value, valid_answer in enumerate(valid_answers):
            # Sets the font based upon whether the question was answered
            # correctly or not
            page += f'<font color={"green" if valid_answer else "red"}>\n'
            # Displays the question in the appropriate font
            page += f'Question {index_value+1}. '
            page += f'{session["questions"][index_value]}<br>\n'
            # Closes the font area
            page += '</font>\n'

        # Sets the passing message for the user
        passed = 'passed' if score >= 70 else 'failed'
        # Sets a dividing area for the score to appear
        page += '<br><hr><br>\n'
        # Sets the message to let the user know their score and whether
        # they passed or not
        page += f'Your score is {score} %. You have {passed}.<br>\n'
        # Sets the page footer
        page += page_footer()

        # Set data values to write
        data_values = {'correct': sum(valid_answers),
                       'total': len(valid_answers),
                       'ids': session['ids']}

        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])

        # Retrieves the session id of the current test for the user
        session_id = data_base.get_user_session_id(session['username'],
                                                   session['test_name'])

        # Inserts the test taker's data into the table
        data_base.update_test_taker(session['username'], session['test_name'],
                                    data_values)

        # Inserts the user's answers into the results table
        data_base.insert_into_results_table(session['test_name'],
                                            session_id,
                                            session['ids'].split(':'),
                                            session['answers'],
                                            valid_answers)

        # Resolves placeholder information
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')

        # Deletes session data used for the test
        del session['questions']
        del session['ids']
        del session['question_nums']
        del session['test_name']
        del session['answers']

        # Returns the page
        return page
    # The logged in user is not an admin and is redirected to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


def grade_test():
    """
    This function grades the test data
    """
    # If the user is logged in
    if 'username' in session:
        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the answers for the test
        results = data_base.get_test_answers(session['test_name'])

        # Calculates whether each questions is valid or not and returns the
        # results as a list with True and False values for each test
        return [session['answers'][index_value] == results[question]
                for index_value, question in enumerate(session['questions'])]

    # The logged in user is not an admin and is redirected to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


@APP.route('/display_question', methods=['GET', 'POST'])
def display_question(message=""):
    """
    This Flask function is used to display an individual question
    """
    # If the user is logged in
    if 'username' in session:
        # Sets the question type possibilities
        question_types = {'text': display_text,
                          'true or false': display_true_false,
                          'yes or no': display_yes_no,
                          'multiple choice x 4': display_multiple_choice,
                          'multiple choice x 8': display_multiple_choice,
                          'memo': display_textarea,
                          'code segment': display_textarea}

        # Retrieves the posted form data
        form = request.form

        # If no answers have been recorded
        if 'answers' not in session:
            # Create a list to store answers
            session['answers'] = []

        # If an answer was submitted
        if 'answer' in form:
            # Append the answer to the stored answers
            session['answers'] += [form['answer']]

        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Remove the next question from the list and store it as an index
        question_index = session['question_nums'].pop(0) - 1
        # Set the question to ask
        question = session['questions'][question_index]
        # Retrieve the information for the question from the database
        question_data = data_base.get_question_data(session['username'],
                                                    session['test_name'],
                                                    question)
        # Set the page header
        page = page_header(f'{session["test_name"]} Exam', message)
        # Sets the JavaScript area
        java_script = set_java_script(int(question_data[-1]) * 1000 * 60)
        # If there are questions left to ask
        if len(session['question_nums']) > 0:
            # Sets the action
            action = '/display_question'
            # Sets the button
            button = 'Next'
        # If there are no more questions to ask after this one
        else:
            # Sets the action
            action = '/finish_test'
            # Sets the button
            button = 'Finish'

        # Sets the meta refresh area - this sets a second way to automatically
        # move the page to the next question
        meta = '<meta http-equiv="refresh" content="'
        meta += f'{int(question_data[-1])*60};'
        meta += f'URL=\'{action}\'">'

        # Display the question number
        page += f'<p><b>Question # {question_index+1}.</b></p>\n'
        # Display the question
        page += f'<p>{question}'

        # If the question does not end with a question mark
        if not question.endswith('?'):
            # Add a question mark
            page += '?'
        # Closes the paragraph area
        page += '</p>\n'

        # Begins the form for the answer area
        page += '<center><form action="{action}" method = "POST" '
        page += 'enctype = "multipart/form-data">\n'

        # If the question type is a multiple choice question
        if question_data[0].startswith('multiple choice x '):
            # Perform the approriate action
            page += question_types[question_data[0]](question_data[1])
        # The question type is not a multiple choice question
        else:
            # Perform the appropriate action
            page += question_types[question_data[0]]()

        # Sets the button area - will resolve later
        page += '<button type="submit">{button}</button>\n'
        page += '</form></center>\n'

        # Display a message to the user to inform them that the page will
        # automaticallly move on with a timer and progress bar
        page += '<br><br><center>Page will automatically continue in:'
        page += '      <h3 id="pageTextCountdown" class="clock">'
        page += f'{int(question_data[-1])*60}</h3>\n'
        page += f'      <progress value="{int(question_data[-1])*60}" '
        page += f'max="{int(question_data[-1])*60}" id="pageBarCountdown">'
        page += '</progress>\n'
        page += '</center>'

        # Sets the page footer
        page += page_footer()
        # Resolves the placeholders
        page = page.replace('{javascript}', java_script)
        page = page.replace('{meta}', meta)
        page = page.replace('{button}', button)
        page = page.replace('{action}', action)
        # Returns the page
        return page
    # Redirects the user back to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


def display_text():
    """
    This function sets the text specific information for the page
    """
    # Sets the text box answer area
    page = '<input type="text" name="answer" placeholder="Answer">\n'
    # Returns the page
    return page


def display_true_false():
    """
    This function sets the true/false specific information for the page
    """
    # Sets the True/False answer area
    page = 'True <input type="radio" name="answer" value="True"> '
    page += 'False <input type="radio" name="answer" value="False">\n'
    # Returns the page
    return page


def display_yes_no():
    """
    This function sets the yes/no specific information for the page
    """
    # Sets the Yes/No answer area
    page = 'Yes <input type="radio" name="answer" value="Yes"> '
    page += 'No <input type="radio" name="answer" value="No">\n'
    # Returns the page
    return page


def display_multiple_choice(options):
    """
    This function sets the multiple choice specific information for the page
    """
    # Retrieves the options from the list
    options = options.split(':::')

    # Starts with an empty page
    page = ''
    # Steps through each number of options
    for index_value, option in enumerate(options):
        # Sets the multiple choice answer area
        page += f'Option {U[index_value]}. <input type="radio" '
        page += f'name="answer" value="{option}" '
    # Returns the page
    return page


def display_textarea():
    """
    This function sets the textarea (memo/code segment) specific information
    for the page
    """
    # Sets the memo/code segment answer area
    page = '<textarea name="answer" placeholder="Enter the expected '
    page += 'answer." rows="6" cols="60"></textarea>\n'
    # Returns the page
    return page


@APP.route('/assign_test', methods=['GET', 'POST'])
def assign_test():
    """
    This Flask function displays the page to select users to take a test
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = request.form
        # Starts the page
        page = page_header('Assign Users')
        page += f'<center><h1>Assign Users For Testing: {form["test_name"]}'
        page += '</h1></center>\n'

        # Establish a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the header information and the user data from the
        # database
        header, user_data = data_base.all_users_in_user_table()
        # Creates the form for submission
        page += '<form action = "/assign_users" method = "POST" '
        page += 'enctype = "multipart/form-data">\n'
        page += '<input type="hidden" name="test_name" '
        page += f'value="{form["test_name"]}">\n'
        page += '<table align="center">\n'
        page += '<tr>\n'
        # Displays the header
        for header_value in header:
            page += f'<th align="center">{header_value}</th>\n'
        # Adds the extra column
        page += '<th>Assign</th>\n'
        page += '</tr>\n'
        # Steps through each user set
        for index_value, user_set in enumerate(user_data):
            # Starts a new row
            page += '<tr>\n'
            # Steps through each user field
            for user_field in user_set:
                # Adds the field
                page += f'<td>{user_field}</td>\n'
                # If the user field is the last one in the list
                if user_field == user_set[-1]:
                    # Add a checkbox to allow for the user selection
                    page += '<td><input type="checkbox" '
                    page += f'name="cb_{index_value}" value="{user_set[0]}">'
                    page += '</td>\n'
            # Closes a row
            page += '</tr>\n'
        # Closes the table
        page += '<tr>\n'
        page += f'<td colspan="{len(header)+1}" align="center">\n'
        page += '<button button="submit">Assign Users</button>\n'
        page += '</td>\n'
        page += '<tr>\n'
        page += '</table>\n'
        page += '</form>'
        # Adds the footer area
        page += page_footer()
        # Resolves the placeholders
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')
        # Returns the page
        return page
    # Redirects the user back to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


@APP.route('/assign_users', methods=['GET', 'POST'])
def assign_users():
    """
    This Flask function assigns users to be able to take a test
    """
    # If the user is logged in
    if 'username' in session:
        # Retrieves the posted form data
        form = request.form
        # Retrieves the selected checkboxes
        users_to_assign = [form[value] for value in form if
                           value.startswith('cb_')]
        # If there are users to assign
        if users_to_assign:
            # Establishes a connection to the database
            data_base = Database(username=session['username'],
                                 access=session['access'])
            # Inserts the users to take the test into the database
            data_base.insert_user_into_test_taker(session['username'],
                                                  form['test_name'],
                                                  users_to_assign)
        # Returns to manage the tests
        return redirect('/manage_tests')
    # Redirects the user back to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


@APP.route('/show_results', methods=['GET', 'POST'])
def show_results():
    """
    This Flask function retrieves the results of the specified test
    """
    # If the user is logged in and is an administrator for the test
    if 'username' in session and is_admin_user():
        # Retrieves the posted form data
        form = request.form
        # Establishes a connection to the database
        data_base = Database(username=session['username'],
                             access=session['access'])
        # Retrieves the test results
        table_data, score_data =\
            data_base.retrieve_test_results(session['username'],
                                            f'{form["test_name"]}')

        # Removes the header from the test results
        header = table_data.pop(0)

        # Removes the score header from the score results
        scores_header = score_data.pop(0) + ['Exam Status']

        # Starts the page
        page = page_header('Test Results')
        page += f'<center>Test Results: {form["test_name"]}</center>\n'

        # Begins the tabs area
        page += '  <div class="tabs">\n'
        page += '    <div class=".tabs__sidebar">\n'
        page += '      <button class="tabs__button" data-for-tab="1">'
        page += 'Percentages</button>\n'
        page += '      <button class="tabs__button" data-for-tab="2">'
        page += 'All Data</button>\n'
        page += '    </div>\n'
        page += '    <div class="tabs__content" data-tab="1">\n'
        page += '      <h2>Percentages</h2>\n'

        # Sets the percentage area
        page += '<table align="center"><tr>\n'
        # Displays the header fields
        for header_field in scores_header:
            page += f'<th align="center">{header_field}</th>\n'
        # Closes the row
        page += '</tr>\n'
        # Steps through the data set
        for data_group in score_data:
            # Begins a new row
            page += '<tr>\n'
            # Steps through each data field
            for index_value, data in enumerate(data_group):
                # If the data field is the last one
                if index_value == len(data_group) - 1:
                    # Adds the data field
                    page += f'<td align="center">{data} %</td>\n'
                    # Adds the exam status
                    page += '<td align="center">'
                    if int(data) >= 70:
                        font = 'green'
                        status = 'Passed'
                    else:
                        font = 'red'
                        status = 'Failed'
                    page += f'<font color={font}>\n'
                    page += f'{status}'
                    page += '</font>'
                    page += '</td>\n'
                # All other data fields
                else:
                    # Add the field data
                    page += f'<td align="center">{data}</td>\n'
            # Closes the row
            page += '</tr>\n'
        # Closes the table
        page += '</table>\n'

        page += '    </div>\n'
        page += '    <div class="tabs__content" data-tab="2">\n'
        page += '      <h2>All Results</h2>\n'

        page += '<table align="center"><tr>\n'
        # Displays the header fields
        for header_field in header:
            page += f'<th align="center">{header_field}</th>\n'
        # Closes the row
        page += '</tr>\n'
        # Steps through the data set
        for data_group in table_data:
            # Begins a new row
            page += '<tr>\n'
            # Steps through each data field
            for index_value, data in enumerate(data_group):
                # If the data field is the last one
                if index_value == len(data_group) - 1:
                    # Adds the checkbox area
                    page += '<td align="center">'
                    page += f'{"No" if data == "0" else "Yes"}'
                    page += '</td>\n'
                elif index_value == 7:
                    # Add the field data
                    page += f'<td align="center">{data} %</td>\n'
                # All other data fields
                else:
                    # Add the field data
                    page += f'<td align="center">{data}</td>\n'
            # Closes the row
            page += '</tr>\n'
        # Closes the table
        page += '</table>\n'

        page += '    </div>\n'
        page += '  </div>\n'
        page += '  <script>\n'
        page += '    function setupTabs () {\n'
        page += '      document.querySelectorAll(".tabs__button").'
        page += 'forEach(button => {\n'
        page += '        button.addEventListener("click", () => {\n'
        page += '          const sideBar = button.parentElement;\n'
        page += '          const tabsContainer = sideBar.parentElement;\n'
        page += '          const tabNumber = button.dataset.forTab;\n'
        page += '          const tabToActivate = tabsContainer.querySelector'
        page += '(`.tabs__content[data-tab="${tabNumber}"]`);\n\n'
        page += '          sideBar.querySelectorAll(".tabs__button").'
        page += 'forEach(button => {\n'
        page += '            button.classList.remove("tabs__button--'
        page += 'active");\n'
        page += '          });\n'
        page += '          tabsContainer.querySelectorAll(".tabs__content").'
        page += 'forEach(tab => {\n'
        page += '            tab.classList.remove("tabs__content--active");\n'
        page += '          });\n'
        page += '          button.classList.add("tabs__button--active");\n'
        page += '          tabToActivate.classList.add("tabs__content--'
        page += 'active");\n'
        page += '        });\n'
        page += '      });\n'
        page += '    }\n'
        page += '    document.addEventListener("DOMContentLoaded", () => {\n'
        page += '      setupTabs();\n\n'
        page += '      document.querySelectorAll(".tabs").forEach('
        page += 'tabsContainer => {\n'
        page += '        tabsContainer.querySelector(".tabs__sidebar '
        page += '.tabs__button").click();\n'
        page += '      });\n'
        page += '    });\n'
        page += '  </script>\n'





        # Closes the page
        page += page_footer()
        # Resolves the placeholder
        page = page.replace('{javascript}', '')
        page = page.replace('{meta}', '')
        # Returns the page
        return page
    # Redirects the user back to the login page
    return redirect('/login?message=<p>Invalid user or invalid '
                    'access level</p>')


# If the program is not being imported
if __name__ == '__main__':
    # If the directory does not exist
    if not os.path.exists('user_data'):
        # Create the directory to store user data
        os.mkdir('user_data')
    # If the directory does not exist
    if not os.path.exists('test_files'):
        # Create the directory
        os.mkdir('test_files')
        os.mkdir('test_files/temp')
        os.mkdir('test_files/tests')
    # Execute Flask to serve the pages
    APP.run(host='0.0.0.0', port=8080)

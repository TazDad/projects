#! /usr/bin/env python3.11

"""
This program is designed to encrypt and decrypt a file with a four digit
passcode
"""

# Modules to import
import os
import sys
import re
from string import ascii_lowercase as L
from string import ascii_uppercase as U
from string import digits as D
from string import whitespace as W
from string import punctuation as P
from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random


def is_file_readable(file):
    """
    This function validates that a file is readable by the user
    Args:
        file is a filename to validate that it is readable by the user
    Returns:
        returns True if the file is readable by the user
        returns False if the file is not readable by the user
    """
    # If the file is readable by the user
    if os.access(file, os.R_OK):
        # Returns True
        return True
    # Returns False
    return False


def is_file_executable(file):
    """
This function validates that a file is executable by the user
    Input:
        file is a filename to validate that it is executable by the user
    Output:
        returns True if the file is executable by the user
        returns False if the file is not executable by the user
    """
    # If the file is readable by the user
    if os.access(file, os.X_OK):
        # Returns True
        return True
    # Returns False
    return False


def encrypt_file(key, filename, output_file):
    """
    This function encrypts a file using AES encryption and the provided
    password
    Args:
        key is the password used to encrypt the file
        filename is the file name to encrypt
        output_file is the encrypted output file
    Returns:
        None
    """
    # The chunk size
    chunksize = 64 * 1024
    # The size of the file to be encrypted
    filesize = str(os.path.getsize(filename)).zfill(16)
    # Creates the initialization vector
    initialization_vector = Random.new().read(16)
    # Sets the encryptor
    encryptor = AES.new(key, AES.MODE_CBC, initialization_vector)
    # Opens the filename for reading in binary mode
    with open(filename, 'rb') as infile:
        # Opens the output_file for writing in binary mode
        with open(output_file, 'wb') as outfile:
            # Writes the encoded file size
            outfile.write(filesize.encode('utf-8'))
            # Writes the random value set
            outfile.write(initialization_vector)
            # Sets up an infinite loop
            while True:
                # Reads a chunk of data from the file
                chunk = infile.read(chunksize)
                # If nothing was read
                if len(chunk) == 0:
                    # Breaks from the while loop
                    break
                # If the chunk size read was not a multiple of 16 bytes
                if len(chunk) % 16 != 0:
                    # Aligns the data
                    chunk += b' ' * (16 - (len(chunk) % 16))
                # Writes the chunk of data to the file
                outfile.write(encryptor.encrypt(chunk))


def decrypt_file(key, filename, output_file):
    """
    This function decrypts a file using AES decryption and the
    provided password
    Args:
        key is the password used to decrypt the file
        filename is the file name to decrypt
        output_file is the decrypted output file
    Returns:
        None
    """
    # The chunk size
    chunksize = 64 * 1024
    # Opens the filename for reading in binary mode
    with open(filename, 'rb') as infile:
        # Reads the file size
        filesize = int(infile.read(16))
        # Reads the initialization vector from the file
        initialization_vector = infile.read(16)
        # Sets the decrypter
        decryptor = AES.new(key, AES.MODE_CBC, initialization_vector)
        # Opens the output_file for writing in binary mode
        with open(output_file, 'wb') as outfile:
            # Sets up an infinite loop
            while True:
                # Reads a chunk of data from the file
                chunk = infile.read(chunksize)
                # If nothing was read
                if len(chunk) == 0:
                    # Breaks from the while loop
                    break
                # Writes out the decrypted chunk of data
                outfile.write(decryptor.decrypt(chunk))
                # Truncates the outfile to the original file size
                outfile.truncate(filesize)


def get_key(password):
    """
    This function formats the password for the encrypter/decrypter
    Args:
        password is the password used for the encryption/decryption
    Returns:
        the password formatted for the encrypter/decrypter
    """
    # Formats the password with utf-8 encoding
    hasher = SHA256.new(password.encode('utf-8'))
    # Returns the digest of the password
    return hasher.digest()


def check_file(file):
    """
    This function validates that the file contains valid characters
    Args:
        file is the decrypted file
    Returns:
        True if the file only contains valid characters
        False if the file contains invalid characters
    """
    # Creates the string of valid charaacters
    characters = L + U + D + W + P
    # Attempts the section of code
    try:
        # Opens the file for reading
        with open(file, encoding='UTF-8') as file_ptr:
            # If there are any characters that are not valid in the file
            if any(c for c in file_ptr.read() if c not in characters):
                # Returns False
                return False
            # Returns True to specify that the file only contains valid
            # characters
            return True

    # If the section of code fails
    except UnicodeDecodeError:
        # Returns false
        return False


def is_valid_pass(password, pass_type):
    """
    This function checks to see if the password is a valid simple or complex
    password.
    Args:
        password is the password to test
        pass_type is the type of password to check against
    Returns:
        True if the password is valid and False otherwise
    """
    # If the password type to check is simple
    if pass_type == 'simple':
        # Returns the test result to see if the password is a simple password
        return is_valid_simple_pass(password)
    # Returns the test result to see if the password is a complex password
    return is_valid_complex_pass(password)


def is_valid_complex_pass(password):
    """
    This function checks the password to see if it matches the validation
    criteria.
    Args:
        password is the password to test
    Returns:
        True if the password is valid and False otherwise
    """
    # Sets the valid search parameters
    search = '[a-z]{3,}|[A-Z]{3,}|[0-9]{3,}|['+P+'}]{3,}'
    # Sets the valid characters allowed
    valid_chars = '^[a-zA-Z0-9_'+P+']*$'
    # The value is between 12 and 24 inclusive in length
    # The value begins with either an uppercase or lowercase letter
    # The value contains only letters, numbers, punctuation, and
    # underscores
    # The value does not end with a punctuation character
    # The value does not contain three or more characters of the same
    # character set in a row
    # Returns True if the length is greater than one and meets the other
    # criteria or False, otherwise
    return len(password) > 1 and all([11 < len(password) < 25,
                                      password[0].isalpha(),
                                      re.match(valid_chars, password),
                                      re.search(search, password) is None,
                                      password[-1] not in P])


def show_password_rules():
    """
    This function displays the rules for a complex password and exits.
    Args:
        None
    Returns:
        None
    """
    rules = 'Simple password rules:\n'
    rules += '  o any four digit integer value where each place is a value'
    rules += 'between 0-9\n\n'
    rules += 'Complex password rules:\n'
    rules += '  o must be betwen 12 and 24 (inclusive)\n'
    rules += '  o must begin with either an uppercase or lowercase\n'
    rules += '  o can only contain character from the character sets:\n'
    rules += '    - letters (uppercase or lowercase)\n'
    rules += '    - numbers\n'
    rules += '    - digits\n'
    rules += '    - punctuation characters\n'
    rules += '  o must not contain any more than two characters from the '
    rules += 'same character set in a row\n'
    rules += '  o must not end with punctuation\n'

    # Shows the password rules to the user
    print(rules)
    # Exits the program
    sys.exit()


def is_valid_simple_pass(password):
    """
    This function validates that the password is a string of four digits.
    Args:
        password is the password to test
    Returns:
        True if the password is valid and False otherwise
    """
    # If the password length is four and all the characters are digits
    if password is not None and len(password) == 4 and \
       all(n in D for n in password):
        # Returns True
        return True
    # Returns False
    return False


def set_parser():
    """
    This function set up the parser for command line arguments.
    Args:
        None
    Returns:
        The established parser.
    """
    # Creates an argument parser
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)

    # Sets the possible input arguments
    parser.add_argument("-t", "--test", dest="pass_test", help="The "
                        "type of password to test against (simple or complex)"
                        ".", metavar="TEST", type=str, default="simple",
                        choices=('simple', 'complex'))
    parser.add_argument("-s", "--show", help="Displays the rules for a "
                        "complex password and exits.", action='store_true')
    parser.add_argument("-c", "--command", dest="command", help="The "
                        "command to perform; either encrypt or decrypt.",
                        metavar="COMMAND", type=str,
                        choices=('encrypt', 'decrypt'))
    parser.add_argument("-p", "--password", dest="password",
                        help="The four digit (including leading 0's) to "
                        "use for the password.", metavar="PASSWORD",
                        type=str)
    parser.add_argument("-f", "--file", dest="file",
                        help="The file to action", metavar="FILE",
                        type=str)
    parser.add_argument("-o", "--outfile", dest="outfile",
                        help="The file to write for encrypted files or "
                        "the base file name for decryption (an underscore"
                        "followed by the password attempted with a .txt "
                        "will be added to the base.", metavar="OUTFILE",
                        type=str)
    parser.add_argument("-q", "--quiet", dest="quiet",
                        help="Do not display messages action messages",
                        action='store_true')
    # Returns the parser value
    return parser


def process_kwargs(options, p_kwargs):
    """
    This function processes the command line arguments that were passed in
    to the original function.

    Args:
        options is the dictionary that stores any keyword value pairs retrieved
        p_kwargs are the keywor value pairs
    Returns:
        a dictionary containing the retrieved keyword/value pairs
    """

    # Sets the allowable options
    possible_options = ('command', 'password', 'file', 'outfile', 'quiet')

    # Steps through any optional arguments passed in
    # If the argument exists, the value is taken over those on the command
    # line; if no value, the command line and/or default values are kept
    options['quiet'] = False
    # Steps through the key/value pairs
    for key, value in p_kwargs.items():
        # If a key is not a valid key
        if key not in possible_options:
            # Informs the user
            print(f"\n\nUnknown argument '{key}' passed, exiting...\n\n")
            # Sets to display the command line help message
            options['show_help'] = True
        # The key is a valid key
        else:
            # If the key being set is quiet
            if key == 'quiet':
                # Sets to not display program execution messages
                options['quiet'] = True
            # The key being set is another valid option
            else:
                # Sets the command line value with the value passed into
                # the function
                options[key] = value
    # Returns the options set
    return options


def set_command_option(command):
    """
    This function checks and sets the command option.
    Args:
        command is the command to test against
    Returns:
        the validated command
    """
    # While the command is not a valid value
    while command not in ['encrypt', 'decrypt']:
        # Retrieves the command to perform from the user
        command = input('Enter an action to perform: (E)ncrypt or '
                        '(D)ecrypt ').casefold()[0]
        # If the value entered begins with an e
        if command == 'e':
            # Sets the value
            command = 'encrypt'
        # If the value entered begins with a d
        elif command == 'd':
            # Sets the value
            command = 'decrypt'
    # Returns the validated command
    return command


def set_file_option(file, command):
    """
    This function checks the file for the action to be performed.
    Args:
        file is the file to perform the action against
        command is the command to test against
    Returns:
        the validated file
    """
    # If the file to action was not entered or does not exist
    while not is_file_readable(file):
        # Retrieves the input file from the user
        file = input(f'File to {command}: ')
    # Returns the validated file
    return file


def set_pass_type_option(pass_type):
    """
    This function checks and sets the password type to use option.
    Args:
        pass_type is the password type to validate it is a valid type
    Returns:
        the validated password type
    """
    # While the password type is not valid
    while pass_type not in ['simple', 'complex']:
        # Retrieves user input
        pass_type = input('Enter the password type (s)imple '
                          'or (c)omplex: ').casefold()[0]
        # If the user entered somethig that begins with c
        if pass_type == 'c':
            # Sets to be complex
            pass_type = 'complex'
        # If the user entered something that begins with s
        elif pass_type == 's':
            # Sets to be simple
            pass_type = 'simple'
    # Returns the validated password type
    return pass_type


def set_password_option(password, command, pass_type):
    """
    This function checks and sets the password.
    Args:
        password is the password to validate
    Returns:
        the validated password
    """
    # If no password was supplied or the password is not valid
    while not is_valid_pass(password, pass_type):
        # Retrieve a new password
        password = input(f'Enter the password used to {command}: ')
    # Returns the validated password
    return password


def parse_command_line(**kwargs):
    """
    This function reads the command line to retrieve the necessary values
    to conduct a port scan. The command line options can be obtained by
    calling the program name with the -h option.

    Args:
        show_help if True is passed in, displays the command line help
        kwargs is an optional dictionary of the keyword/value pairs
               equivalent to those required on the command line
    Returns:
        a dictionary containing the retrieved keyword/value pairs
    """

    # Creates an empty dictionary to store retrieved options
    options = {}

    # Sets the parser
    parser = set_parser()

    # Parses the command line and extracts the variables from the
    # Namespace into a dictionary
    options = vars(parser.parse_args())

    # Defaults to not show the help message
    # Use in input validation
    options['show_help'] = False

    # If keyword/value pairs were passed in
    if len(kwargs.keys()):
        # Sets the options passed in to the function
        options = process_kwargs(options, kwargs)

    # If the user opted to display the complex password rules
    if 'show' in options and options['show']:
        # Execute the function to display the password rules
        show_password_rules()

    # If the command was not set in the options
    if 'command' not in options or options['command'] is None:
        # Sets to be an invalid value
        options['command'] = 'a'

    # If the file was not supplied
    if 'file' not in options or options['file'] is None:
        # Sets to be something that hopefully will not exist
        options['file'] = 'f'

    options['command'] = set_command_option(options['command'])
    options['file'] = set_file_option(options['file'], options['command'])

    # If no outfile was provided
    while 'outfile' not in options or options['outfile'] is None:
        # Retrieves the input file from the user
        options['outfile'] = input('Output filename: ')

    # If the password type to use was not set
    if 'pass_test' not in options or options['pass_test'] is None:
        # Sets the default to be simple
        options['pass_test'] = 'simple'

    # If the password was not set
    if 'password' not in options or options['password'] is None:
        # Sets the password to be invalid
        options['password'] = 'a'

    options['pass_test'] = set_pass_type_option(options['pass_test'])
    options['password'] = set_password_option(options['password'],
                                              options['command'],
                                              options['pass_test'])

    if options['command'] == 'decrypt':
        # Appends the password used during the decryption attempt with .txt
        options['outfile'] = f"{options['outfile']}_{options['password']}.txt"

    # If set to display the help message, display the help message
    if options['show_help']:
        # Display the help message
        parser.print_help()
        # Exits the program
        sys.exit()

    # Returns a dictionary with the keyword and associated value retrieved
    return options


def main():
    """
    This is the main function of the program that calls the parsing for the
    arguments to be set and then performs the chosen action, either
    encryption or decryption.
    Args:
        None
    Returns:
        None
    """
    # Attempts to run the block of code
    try:
        # Retrieves the program options
        options = parse_command_line()

        # If the command is to encrypt
        if options['command'] == 'encrypt':
            # Encrypt the file
            encrypt_file(get_key(options['password']), options['file'],
                         options['outfile'])
            # If the user chose not to see operational messages
            if not options['quiet']:
                # Informs the user
                print('Done.')
        # If the command is to decrypt
        else:
            # Decrypt the file
            decrypt_file(get_key(options['password']), options['file'],
                         options['outfile'])
            # If the file contains valid characters
            if check_file(options['outfile']):
                # If the user chose not to see operational messages
                if not options['quiet']:
                    # Informs the user
                    print('Done.')
            # If the file does not contain all valid characters
            else:
                # If the user chose not to see operational messages
                if not options['quiet']:
                    # Informs the user
                    print('Does not appear to have been decrypted. '
                          'Password may be incorrect, removing file...')
                # If the file is valid
                if is_file_readable(options['outfile']):
                    # Removes the file
                    os.remove(options['outfile'])
    # The user quit the program via CTRL^C
    except KeyboardInterrupt:
        # Informs the user
        print("CTRL^C, exiting...")
        # Exit the program
        sys.exit()


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the program
    main()

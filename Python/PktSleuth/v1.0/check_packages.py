#! /usr/bin/env python3

"""
This module is packaged with PktSleuth to provide a quick, easy way to install
its required packages.
"""

import sys
import subprocess

def install_packages(required_packages: "list[str]") -> None:
    """
    This function iterates through a list of packages and installs each if
    they are not already installed.
    """
    # Iterates through each package name
    for package in required_packages:
        # Begins a try->except block
        try:
            # Attempts to import the package
            __import__(package)
        # The package could not be imported
        except ImportError:
            # Executes the command to install the package
            subprocess.call([sys.executable, '-m', 'pip', 'install',
                             '--upgrade', package], stderr=subprocess.DEVNULL)


install_packages(['PyQt5', 'scapy'])

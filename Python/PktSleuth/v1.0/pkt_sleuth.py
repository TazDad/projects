#! /usr/bin/env python3

# Disables pylint messages
# pylint: disable=R0902
# pylint: disable=R0904
# pylint: disable=R0912
# pylint: disable=R0914
# pylint: disable=R0915
# pylint: disable=E0611
# pylint: disable=C0302
# pylint: disable=C0413


"""
This program is executes PktSleuth, a Python Graphical User Interface (GUI)
designed to allow for the analysis of unknown data within packet data.
"""

# Import required libraries
# If you receive the message that PyQt5 or scapy are not installed, you can
# execute the check_package.py program or uncomment (removing the space as
# well) the import check_packages below. After the first execution, they will
# be installed and this import can be commented out (add space) again.
# import check_packages
import os
import sys
import argparse
import re
import subprocess

# Sets the absolute path of where the program should be located
COMMAND = os.path.abspath(__file__).replace(os.path.basename(__file__),
                                            'check_packages.py')
# This area is to allow for python3.8 version differences on type annotation
# If the version is greater than python3.8
if (sys.version_info.major > 3 or
   (sys.version_info.major >= 3 and sys.version_info.minor > 8)):
    # Import Self from typing
    from typing import Self
    # Command to check if PyQt5 and scapy are installed
# The version of Python is 3.8 or less
else:
    # Need to verify this works on a system that is not Windows WSL Ubuntu
    # where it is not
    # Import TypeVar from typing
    from typing import TypeVar
    # Create a Self for the class
    Self = TypeVar('Self', bound='PktSleuth')
    # Uses the appropriate version of Python to check if PyQt5 and scapy are
    # installed
    COMMAND = f'{sys.executable} {COMMAND}'
# Begins a try->except block
try:
    # Attempts to run the command
    subprocess.run(COMMAND.split(' '), check=False, stdout=subprocess.DEVNULL,
                   stderr=subprocess.DEVNULL)
# The command did not exist
except FileNotFoundError:
    # Inform the user
    print('Unable to execute check_packages.py.')
# The noqa statements disable pycodestyle output, but they have to be inline
# comments on the line where the error occurs with the message
from PyQt5.QtWidgets import (  # noqa: E402
    QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QAction,
    QFileDialog, QMessageBox, QComboBox, QVBoxLayout, QWidget, QPushButton,
    QTextEdit, QLineEdit, QDialog, QLabel, QHBoxLayout, QCheckBox
)
from PyQt5.QtGui import QIcon, QFocusEvent  # noqa: E402
from PyQt5.QtCore import (  # noqa: E402
    Qt, QItemSelection, pyqtSignal, QTimer
)
from scapy.all import raw  # noqa: E402
from scapy.layers.l2 import Ether  # noqa: E402
from scapy.utils import rdpcap  # noqa: E402
from scapy.layers.inet import IP, TCP, UDP, ICMP  # noqa: E402
from scapy.layers.dns import DNS, DNSQR, DNSRR  # noqa: E402
from scapy.layers.inet6 import IPv6  # noqa: E402


class PktSleuth(QMainWindow):
    """
    The main GUI for the hex analyzer.
    """

    # Sets up a new signal
    focus_lost = pyqtSignal()

    def __init__(self: Self, pcap_file: str = None,
                 icon_file: str = 'PktSleuth_icon.ico',
                 program_name: str = 'PktSleuth') -> None:
        """
        Initializer for the GUI
        """
        # Initializes QMainWindow's init
        super().__init__()

        # Sets the main window's title
        self.setWindowTitle(program_name)

        # Main widget and layout
        self.main_widget = QWidget(self)
        self.setCentralWidget(self.main_widget)

        # Creates a layout area for the main window
        layout = QHBoxLayout(self.main_widget)
        # Set to allow the window to expand when maximized
        layout.setContentsMargins(0, 0, 0, 0)

        # Sets to minimize the distinction between the two tables
        layout.setSpacing(0)

        # Create two QTableWidgets: one for frozen columns,
        # one for the scrollable data
        # Frozen column table
        self.frozen_table = QTableWidget(self)
        # Scrollable data table
        self.table_widget = QTableWidget(self)

        # Set the frozne column counts
        self.frozen_table.setColumnCount(1)

        # Add widgets to layout
        layout.addWidget(self.frozen_table)
        layout.addWidget(self.table_widget)

        # Synchronize vertical scrollbars
        self.table_widget.verticalScrollBar().valueChanged.connect(
            self.frozen_table.verticalScrollBar().setValue
        )
        self.frozen_table.verticalScrollBar().valueChanged.connect(
            self.table_widget.verticalScrollBar().setValue
        )

        # Hide the table's vertical header to match the scrollable table
        self.table_widget.verticalHeader().hide()
        self.frozen_table.horizontalHeader().hide()

        # Hides the frozen table by default
        self.frozen_table.setVisible(False)

        # Resize the frozen table's column width so it looks locked in place
        self.frozen_table.setColumnWidth(0, 25)
        self.frozen_table.setFixedWidth(self.frozen_table.columnWidth(0))

        # Set common styles for both tables (adjust as needed)
        self.table_widget.setStyleSheet('QTableWidget { font-family: Arial; '
                                        'font-size: 12pt; padding: 1px; }')
        self.frozen_table.setStyleSheet('QTableWidget { font-family: Arial; '
                                        'font-size: 12pt; padding: 1px; }')

        # Disable the frozen table vertcal scroll bar
        self.frozen_table.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        # Set the window flags to remove the frame
        self.frozen_table.setWindowFlags(Qt.FramelessWindowHint)

        # Set the icon for the main window
        self.setWindowIcon(QIcon(icon_file))
        # Resizes the window to the default size
        self.resize(640, 480)

        # File Menu
        self.file_menu = self.menuBar().addMenu('File')
        # File Menu: Open file
        self.open_file_action = QAction('Open', self)
        self.open_file_action.triggered.connect(self.load_pcap)
        self.file_menu.addAction(self.open_file_action)
        # File Menu: Close file
        self.close_file_action = QAction('Close', self)
        self.close_file_action.triggered.connect(self.close_pcap)
        self.file_menu.addAction(self.close_file_action)
        # File Menu: Exit the program
        self.exit_action = QAction('Exit', self)
        self.exit_action.triggered.connect(self.close_program)
        self.file_menu.addAction(self.exit_action)

        # Edit Menu
        self.edit_menu = self.menuBar().addMenu('Edit')
        # Edit Menu: Search
        self.search_action = QAction('Search', self)
        self.search_action.triggered.connect(self.search_hex_dialog)
        self.edit_menu.addAction(self.search_action)
        # Edit Menu: Make Binary
        self.binary_action = QAction('Binary', self)
        self.binary_action.triggered.connect(self.show_binary_dialog)
        self.edit_menu.addAction(self.binary_action)
        # Edit Menu: Filter
        self.filter_action = QAction('Filter', self)
        self.filter_action.triggered.connect(self.show_filter_dialog)
        self.edit_menu.addAction(self.filter_action)
        # Edit Menu: Defragment
        self.defrag_action = QAction('Defragment', self)
        self.defrag_action.triggered.connect(self.show_fragmentation_dialog)
        self.edit_menu.addAction(self.defrag_action)
        # Edit Menu: Drop Columns
        self.drop_action = QAction('Drop Columns', self)
        self.drop_action.triggered.connect(self.show_dropped_dialog)
        self.edit_menu.addAction(self.drop_action)
        # Edit Menu: Highlight Columns
        self.highlight_action = QAction('Highlight Columns', self)
        self.highlight_action.triggered.connect(self.show_highlight_dialog)
        self.edit_menu.addAction(self.highlight_action)
        # Edit Menu: Reset
        self.reset_action = QAction('Reset', self)
        self.reset_action.triggered.connect(self.reset_display)
        self.edit_menu.addAction(self.reset_action)

        # View Menu
        self.view_menu = self.menuBar().addMenu('View')
        # View Menu: View Text
        self.view_text_action = QAction('View Text', self)
        self.view_text_action.triggered.connect(self.view_text_window)
        self.view_menu.addAction(self.view_text_action)
        # View Menu: PCAP Report
        self.report_action = QAction('PCAP Report', self)
        self.report_action.triggered.connect(self.create_report_window)
        self.view_menu.addAction(self.report_action)

        # About Menu
        self.about_menu = self.menuBar().addMenu('About')
        # About Menu: About
        self.about_action = QAction('About', self)
        self.about_action.triggered.connect(self.about_message)
        self.about_menu.addAction(self.about_action)

        # About Menu: Help
        self.help_action = QAction('Help', self)
        self.help_action.triggered.connect(self.help_message)
        self.about_menu.addAction(self.help_action)

        # Sets the default values
        # Empties the original packet data
        self.original_packets = []
        # Empties the packet data being viewed
        self.packet_view = []
        # Sets the default decoding scheme
        self.decoding_scheme = 'ASCII'
        # Sets for there not to be a text window
        self.text_window = None
        # Sets for there not to be a report window
        self.report_window = None
        # Empties the text widget
        self.text_widget = None
        # Empties the IP/TCP fragmentation label
        self.tcp_label = None
        # Empties the IP/TCP fragmentation checkbox
        self.tcp_checkbox = None
        # Empties the IP/UDP fragmentation label
        self.udp_label = None
        # Empties the IP/UDP fragmentation checkbox
        self.udp_checkbox = None
        # Empties the packet data label
        self.pkt_data_label = None
        # Empties the packet data edit
        self.packet_number_edit = None
        # Default to displaying all data in the text window
        self.text_display_mode = 'All Data'
        # Sets the pcap file to read
        self.pcap_file = pcap_file
        # Sets the fragmentation checkbox selected
        self.frag_checkbox_selected = 0
        # Empties the data used for fragmentation
        self.frag = {}
        # Empties the binary data
        self.binary_data = {}
        # Deactivates unusable menus
        self.menu_control(False)
        # Empties the pcap report
        self.pcap_report = {}
        # Empties the color dropdown
        self.color_dropdown = None

        # Initializes values
        self.hidden_label = ''
        self.start_byte_input = ''
        self.end_byte_input = ''
        self.filter_byte_input = ''
        self.match_byte_input = ''
        self.pattern_input = ''
        self.frag_byte_input = ''
        self.mask_input = ''
        self.start_input = ''
        self.mid_input = ''
        self.end_input = ''
        self.unfrag_input = ''
        self.data_byte_input = ''

        # Sets the color options
        self.color_options = {'Green': Qt.green,
                              'Blue': Qt.blue,
                              'Cyan': Qt.cyan,
                              'Red': Qt.red,
                              'Magenta': Qt.magenta}

        # Mapping of well-known TCP/UDP ports to application protocols
        self.ports = {
            80: "HTTP",
            443: "HTTPS",
            53: "DNS",
            25: "SMTP",
            110: "POP3",
            143: "IMAP",
            21: "FTP",
            22: "SSH",
            23: "Telnet",
            123: "NTP",
            445: "SMB",
            20: "FTP-Data",
            3389: "RDP",
            69: "TFTP",
            5060: "SIP",
        }

        # Load PCAP file if passed in command-line arguments
        if self.pcap_file:
            # Loads the pcap file
            self.load_pcap(self.pcap_file)

        # Sets to allow for selected data changes
        self.table_widget.selectionModel()\
            .selectionChanged.connect(self.handle_selection_changed)

    def handle_selection_changed(self: Self,
                                 deselected: QItemSelection) -> None:
        """
        This method handles selection changes.
        """
        # Check if deselection is complete
        if self.text_window and not deselected.isEmpty():
            # Update the text view or other widgets here
            self.update_text_view()

    def load_pcap(self: Self, file_name: str = None) -> None:
        """
        This method reads and load pcap data for viewing.
        """
        # If the file was not not set
        if not file_name:
            # Retrieve the file name from a selection GUI
            file_name, _ = QFileDialog.getOpenFileName(self, 'Open PCAP File',
                                                       '',
                                                       'PCAP Files (*.pcap '
                                                       '*.pcapng)')
        # If the filename was set
        if file_name:
            # Sets the PCAP file name for usage later
            self.pcap_file = file_name
            # Retrieve the packets from the pcap file
            self.original_packets = rdpcap(file_name)
            # Sets the packets to use for viewing
            self.packet_view = self.original_packets[:]
            # Analyzes the packets to determine the PCAP data
            self.pcap_report = self.analyze_packets()

            # Turns on the horizontal header
            self.frozen_table.horizontalHeader().setVisible(True)
            # Displays the packets
            self.display_packets()
            # If the window is not already maximized
            if not self.isMaximized():
                # After a 100 ms delay, maximize the window
                QTimer.singleShot(100, self.showMaximized)
            # Enables menu control
            self.menu_control()

    def analyze_packets(self: Self) -> dict:
        """
        This method analyzes the packets found in the PCAP file and creates
        report information.
        """
        # Dictionary to hold detailed report information
        report = {
            "total_packets": len(self.original_packets),
            "protocol_counts": {"Ethernet": 0, "IPv4": 0, "IPv6": 0,
                                "TCP": 0, "UDP": 0, "ICMP": 0, "DNS": 0},
            "packet_details": []
        }

        # Iterate over all packets
        for packet in self.original_packets:
            # Packet informationt to store
            packet_info = {
                "src_mac": None,
                "dst_mac": None,
                "ethertype": None,
                "src_ip": None,
                "dst_ip": None,
                "protocol": None,
                "app_protocol": None,
                "payload_size": len(packet),
                "details": ""
            }

            # Check if the packet has an Ethernet layer
            if packet.haslayer(Ether):
                # Increments the layer count
                report["protocol_counts"]["Ethernet"] += 1
                # Retrieves the layer data
                eth_layer = packet[Ether]
                # Retrieves the ethernet source MAC address
                packet_info["src_mac"] = eth_layer.src
                # Retrieves the ethernet destination MAC address
                packet_info["dst_mac"] = eth_layer.dst
                # Sets the next ethernet type value
                packet_info["ethertype"] = hex(eth_layer.type)

                # Check for IPv4, IPv6 encapsulated by Ethernet
                if eth_layer.type == 0x0800:  # IPv4
                    # Increments the layer count
                    report["protocol_counts"]["IPv4"] += 1
                    # Retrieves the layer data
                    ip_layer = packet[IP]
                    # Retrieves the IPv4 source address
                    packet_info["src_ip"] = ip_layer.src
                    # Retrieves the IPv4 destination address
                    packet_info["dst_ip"] = ip_layer.dst

                    # Determine Layer 4 protocol based on IP 'Protocol' field
                    packet_info["protocol"] = (self.
                                               get_ip_protocol(ip_layer.proto))
                    packet_info = self.analyze_transport_layers(packet,
                                                                packet_info,
                                                                report)
                # If the layer is IPv6
                elif eth_layer.type == 0x86DD:  # IPv6
                    # Increments the layer count
                    report["protocol_counts"]["IPv6"] += 1
                    # Retrieves the layer data
                    ipv6_layer = packet[IPv6]
                    # Retrieves the IPv6 source address
                    packet_info["src_ip"] = ipv6_layer.src
                    # Retrieves the IPv6 destination address
                    packet_info["dst_ip"] = ipv6_layer.dst

                    # Determine Layer 4 protocol based on IPv6 'Next Header'
                    # field
                    packet_info["protocol"] = (self.
                                               get_ip_protocol(ipv6_layer.nh))
                    packet_info = self.analyze_transport_layers(packet,
                                                                packet_info,
                                                                report)

            # Check if the packet has an IPv4 layer
            elif packet.haslayer(IP):
                # Increments the layer count
                report["protocol_counts"]["IPv4"] += 1
                # Retrieves the layer data
                ip_layer = packet[IP]
                # Retrieves the IPv4 source address
                packet_info["src_ip"] = ip_layer.src
                # Retrieves the IPv4 destination address
                packet_info["dst_ip"] = ip_layer.dst

                # Determine Layer 4 protocol based on IP 'Protocol' field
                packet_info["protocol"] = self.get_ip_protocol(ip_layer.proto)
                packet_info = self.analyze_transport_layers(packet,
                                                            packet_info,
                                                            report)
            # Check if the packet has an IPv6 layer
            elif packet.haslayer(IPv6):
                # Increments the layer count
                report["protocol_counts"]["IPv6"] += 1
                # Retrieves the layer data
                ipv6_layer = packet[IPv6]
                # Retrieves the IPv6 source address
                packet_info["src_ip"] = ipv6_layer.src
                # Retrieves the IPv6 destination address
                packet_info["dst_ip"] = ipv6_layer.dst

                # Determine Layer 4 protocol based on IPv6 'Next Header' field
                packet_info["protocol"] = self.get_ip_protocol(ipv6_layer.nh)
                packet_info = self.analyze_transport_layers(packet,
                                                            packet_info,
                                                            report)

            # Add packet info to the report
            report["packet_details"].append(packet_info)

        # Returns the data discovered
        return report

    def pkt_summary(self: Self, packet_num: int = 1,
                    text_box: bool = True) -> str:
        """
        This method returns a formatted text value of an individual packet.
        """
        # Begin a try->except block
        try:
            # If the value is to be retrieved from the text box in the GUI
            if text_box:
                # Retrieve the value from the text box and convert it to
                # an integer
                pkt_num = int(self.packet_number_edit.text())
            # The value was passed in
            else:
                # Sets the packet number to the passed in value
                pkt_num = packet_num
        # Catch the ValueError
        except ValueError:
            # Set to the default number
            pkt_num = packet_num

        # Retrieves the information for the packet (GUI is 1-based)
        pkt = self.pcap_report["packet_details"][pkt_num-1]
        # Set the summary information
        summary = f"\nPacket Details ({pkt_num}):\n"
        # If there is information to add
        if pkt['src_mac']:
            # Add the information
            summary += f"  Source MAC: {pkt['src_mac']}\n"
        # If there is information to add
        if pkt['dst_mac']:
            # Add the information
            summary += f"  Destination MAC: {pkt['dst_mac']}\n"
        # If there is information to add
        if pkt['ethertype']:
            # Add the information
            summary += f"  Ethertype: {pkt['ethertype']}\n"
        # If there is information to add
        if pkt['src_ip']:
            # Add the information
            summary += f"  Source IP: {pkt['src_ip']}\n"
        # If there is information to add
        if pkt['dst_ip']:
            # Add the information
            summary += f"  Destination IP: {pkt['dst_ip']}\n"
        # If there is information to add
        if pkt['protocol']:
            # Add the information
            summary += f"  Protocol: {pkt['protocol']}\n"
        # If there is information to add
        if pkt['app_protocol']:
            # Add the information
            summary += f"  Application Protocol: {pkt['app_protocol']}\n"
        # If there is information to add
        if pkt['payload_size']:
            # Add the information
            summary += f"  Payload Size: {pkt['payload_size']} bytes\n"
        # If there is information to add
        if pkt['details']:
            # Add the information
            summary += f"  Details: {pkt['details']}\n"

        # Returns the recovered packet summary
        return summary

    def get_ip_protocol(self: Self, protocol_num: int) -> str:
        """
        This method maps the IP/IPv6 'Protocol' or 'Next Header' field to
        a protocol name.
        """
        # Known protocol values
        protocol_map = {
            1: "ICMP",
            6: "TCP",
            17: "UDP",
            58: "ICMPv6"
        }
        # Returns a known value or unknown
        return protocol_map.get(protocol_num, f"Unknown ({protocol_num})")

    def analyze_transport_layers(self: Self, packet: IP, packet_info: dict,
                                 report: dict) -> None:
        """
        This method analyzes transport layer protocols to update report and
        packet info.
        """
        try:
            # If TCP is present
            if packet.haslayer(TCP):
                # Increments the layer count
                report["protocol_counts"]["TCP"] += 1
                # Sets to the layer data
                tcp_layer = packet[TCP]
                # Sets the protocol found
                packet_info["protocol"] = "TCP"
                # Sets the port information
                packet_info["details"] = (f"Source Port: {tcp_layer.sport}, "
                                          "Destination Port: "
                                          f"{tcp_layer.dport}")

                # Sets the application protocol based on common TCP port
                packet_info["app_protocol"] = (self.ports.get(tcp_layer.sport)
                                               or
                                               self.ports.get(tcp_layer.dport))
            # If UDP is present
            elif packet.haslayer(UDP):
                # Increments the layer count
                report["protocol_counts"]["UDP"] += 1
                # Sets to the layer data
                udp_layer = packet[UDP]
                # Sets the protocol found
                packet_info["protocol"] = "UDP"
                # Sets the port information
                packet_info["details"] = (f"Source Port: {udp_layer.sport}, "
                                          "Destination Port: "
                                          f"{udp_layer.dport}")

                # Sets the application protocol based on common UDP port
                packet_info["app_protocol"] = (self.ports.get(udp_layer.sport)
                                               or
                                               self.ports.get(udp_layer.dport))

                # Check if DNS layer is present
                if packet.haslayer(DNS):
                    # Increments the layer count
                    report["protocol_counts"]["DNS"] += 1
                    # Sets to the layer data
                    dns_layer = packet[DNS]
                    # If it is a DNS Query
                    if dns_layer.qr == 0:
                        # Sets the layer data
                        query = dns_layer[DNSQR]
                        # Appends the details
                        packet_info["details"] += (", DNS Query: "
                                                   f"{query.qname.decode()}")
                    # If it is a DNS Response
                    elif dns_layer.qr == 1:
                        # Sets the layer data
                        response = dns_layer[DNSRR]
                        # Appends the details
                        packet_info["details"] += (", DNS Response: "
                                                   f"{response.rdata}")
            # If ICMP was found
            elif packet.haslayer(ICMP):
                # Increments the layer count
                report["protocol_counts"]["ICMP"] += 1
                # Sets the layer data
                icmp_layer = packet[ICMP]
                # Sets the protocol found
                packet_info["protocol"] = "ICMP"
                # Sets the details
                packet_info["details"] = (f"Type: {icmp_layer.type}, "
                                          f"Code: {icmp_layer.code}")
        except IndexError:
            pass
        # Returns the packet information
        return packet_info

    def pcap_details(self: Self) -> str:
        """
        This method returns a formatted text version of the PCAP information.
        """
        # Returns the string value of the PCAP information
        return ("PCAP Details:\n  Total Packets: "
                f"{self.pcap_report['total_packets']}\n  " +
                '\n  '.join([f'{header}: {value}' for header, value in
                             self.pcap_report["protocol_counts"].items()
                             if value != 0]))

    def create_report_window(self: Self) -> None:
        """
        This method opens the report window.
        """
        # If the report window is open
        if self.report_window:
            # Raises the window to make it visible
            self.report_window.raise_()
            # Return
            return

        # Create the window
        self.report_window = QWidget()
        # Starts a layout
        layout = QVBoxLayout()

        # Sets the PCAP details
        pcap_overview_label = QLabel(self.pcap_details())
        # Adds the label
        layout.addWidget(pcap_overview_label)

        # Sets a layout for the packetn number to display information
        packet_number_layout = QHBoxLayout()
        # Sets the text box to input a packet number
        self.packet_number_edit = QLineEdit(self)
        # Sets a placeholder text value
        self.packet_number_edit.setPlaceholderText('1')
        # Adds a buttons and text box in order
        packet_number_layout.addWidget(QPushButton("<<",
                                                   clicked=self.
                                                   move_to_first_packet))
        packet_number_layout.addWidget(QPushButton("<",
                                                   clicked=self.
                                                   decrement_packet_number))
        packet_number_layout.addWidget(self.packet_number_edit)
        packet_number_layout.addWidget(QPushButton(">",
                                                   clicked=self.
                                                   increment_packet_number))
        packet_number_layout.addWidget(QPushButton(">>",
                                                   clicked=self.
                                                   move_to_last_packet))
        # Adds buttons and text box to the window
        layout.addLayout(packet_number_layout)

        # Sets the summary label for a packet
        self.pkt_data_label = QLabel(self.pkt_summary(), self)
        # Adds the label
        layout.addWidget(self.pkt_data_label)

        # Create save report button
        save_report_button = QPushButton("Save Report")
        save_report_button.clicked.connect(self.save_pcap_report)
        layout.addWidget(save_report_button)

        # Add a submit button to close the window
        close_button = QPushButton('Close')
        close_button.clicked.connect(self.close_report_window)
        layout.addWidget(close_button)

        # Adds the layout to the window
        self.report_window.setLayout(layout)

        # Connect the signal to the update_packet_details method
        self.focus_lost.connect(self.update_packet_details)
        # Set the custom focusOutEvent to emit the focus_lost signal
        self.packet_number_edit.focusOutEvent = self.handle_focus_out

        # Show the report window
        self.report_window.show()

    def close_report_window(self: Self) -> None:
        """
        This method closes the report window.
        """
        # Closes the report window
        self.report_window.close()
        # Sets to say that the window is not open
        self.report_window = None

    def handle_focus_out(self: Self, event: QFocusEvent) -> None:
        """
        This method handles the change of focus from the packet number
        text box in the report window.
        """
        # Says that focus has left
        self.focus_lost.emit()
        # Calls the appropriate method
        QLineEdit.focusOutEvent(self.packet_number_edit, event)

    def update_packet_details(self: Self) -> None:
        """
        This method updates the packet data label in the report window.
        """
        # Begins a try->except block
        try:
            # Retrieve the text value and convert it to an integer
            pkt_num = int(self.packet_number_edit.text())
        # If a ValueError was thrown
        except ValueError:
            # Returns control
            return
        # If the value entered is an available packet number
        if 1 <= pkt_num <= len(self.pcap_report["packet_details"]):
            # Updates the displayed packet data
            self.pkt_data_label.setText(self.pkt_summary())

    def save_pcap_report(self: Self) -> None:
        """
        This method saves the PCAP report to a file.
        """
        # If a pcap file name was set
        if self.pcap_file:
            # Replaces the value after the last period (.) from the pcap
            # file with txt
            default_file = self.pcap_file.rsplit('.', 1)[0] + '_report.txt'
        # The file was not set
        else:
            # Sets a default file name
            default_file = 'output.txt'

        # Brings up a dialog box to retrieve the filename to save
        file_name, _ = QFileDialog.getSaveFileName(self, 'Save As',
                                                   default_file,
                                                   'Text Files (*.txt)')
        # If a file name was entered
        if file_name:
            # Open the file for writing
            with open(file_name, 'w', encoding='utf-8') as rfile:
                # Writes the text to the file
                rfile.write(self.pcap_details())
                for idx in range(1, len(self.pcap_report["packet_details"])+1):
                    rfile.write(self.pkt_summary(idx, False))
        self.report_window.raise_()

    def move_to_first_packet(self: Self) -> None:
        """
        This method sets the value in the packet number text box to be the
        first packet.
        """
        # Sets the text value
        self.packet_number_edit.setText("1")
        # Updates the display
        self.update_packet_details()

    def decrement_packet_number(self: Self) -> None:
        """
        This method decrements the packet number in the text box by 1.
        """
        # Begins a try->except block
        try:
            # Sets the current number to the value in the text box as an
            # integer
            current_number = int(self.packet_number_edit.text())
        # A ValueError was thrown
        except ValueError:
            # Sets the current number to the first one
            current_number = 1

        # If the current number is greater than the first one
        if current_number > 1:
            # Sets the text value to a decremented value
            self.packet_number_edit.setText(str(current_number - 1))
            # Updates the display
            self.update_packet_details()

    def increment_packet_number(self: Self) -> None:
        """
        This method increments the packet number in the text box by 1.
        """
        # Begins a try-excet block
        try:
            # Sets the current number to the value in the text box as an
            # integer
            current_number = int(self.packet_number_edit.text())
        # A ValueError is thrown
        except ValueError:
            # Sets the current number to the first one
            current_number = 1

        # If the current number is less than the maximum number of packets
        if current_number < len(self.pcap_report["packet_details"]):
            # Sets the text value to an incremented value
            self.packet_number_edit.setText(str(current_number + 1))
            # Updates the display
            self.update_packet_details()

    def move_to_last_packet(self: Self) -> None:
        """
        This method sets the value in the packet number text box to be the
        last packet.
        """
        # Determines the number of packets
        last_packet_number = len(self.pcap_report["packet_details"])
        # Sets the text value to the last packet number
        self.packet_number_edit.setText(str(last_packet_number))
        # Updates the displays
        self.update_packet_details()

    def menu_control(self: Self, enable: bool = True) -> None:
        """
        This method enables or disables the specified menus.
        """
        # Enables or disables the edit and the view menu
        self.edit_menu.setEnabled(enable)
        self.view_menu.setEnabled(enable)
        self.close_file_action.setEnabled(enable)

    def clear_table(self: Self) -> None:
        """
        This method clears the data from the table.
        """
        # Clear the table data
        self.table_widget.clear()
        # Clear the table data
        self.frozen_table.clear()
        # Sets the number of rows
        self.table_widget.setRowCount(0)
        # Sets the number of columns
        self.table_widget.setColumnCount(0)
        # Sets the number of rows
        self.frozen_table.setRowCount(0)
        # Sets the number of columns
        self.frozen_table.setColumnCount(0)
        # Sets for the frozen horizontabl scroll bar to always be displayed
        # This aligns the frozen rows to the scrollable rows at all times
        self.frozen_table.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        # Hides the frozen table
        self.frozen_table.setVisible(False)

    def close_pcap(self: Self) -> None:
        """
        This method performs actions to close out the reading of a pcap file.
        """
        # Empties the original packets
        self.original_packets = []
        # Empties the viewing packets
        self.packet_view = []
        # If the window is open
        if self.text_window:
            # Close the window
            self.text_window.close()
        # If the window is open
        if self.report_window:
            # Close the window
            self.report_window.close()
        # Turns off the horizontal header
        self.frozen_table.horizontalHeader().setVisible(False)
        # Set the window to be not open
        self.text_window = None
        # Clear the table
        self.clear_table()
        # Clears the report information
        self.pcap_report = None
        # Disables menus
        self.menu_control(False)
        # Shows the window at the normal size
        self.showNormal()

    def close_actions(self: Self) -> None:
        """
        This method performs actions to close the program.
        """
        # If the widget is open
        if self.table_widget:
            # Close it
            self.table_widget.close()
        # Empty the menu
        self.file_menu = None
        # Empty the menu
        self.edit_menu = None
        # Empty the menu
        self.about_menu = None
        # Empty packet data
        self.original_packets = None
        # Empty the viewing packet data
        self.packet_view = None
        # Empties the decoding scheme
        self.decoding_scheme = None
        # If the widget is open
        if self.text_window:
            # Close it
            self.text_window.close()
        # If the widget is open
        if self.report_window:
            # Close it
            self.report_window.close()
        # Empties the pcap file name
        self.pcap_file = None
        # Empties the fragmenation data
        self.frag = None
        # Empties the binary data
        self.binary_data = None

    def close_program(self: Self) -> None:
        """
        This method performs the actions to close the program.
        """
        # Closes the pcap file
        self.close_pcap()
        # If the widget is open
        if self.text_window:
            # Close it
            self.text_window.close()
        # If the widget is open
        if self.report_window:
            # Close it
            self.report_window.close()
        # Empties the original packets
        self.original_packets = []
        # Empties the viewing packets
        self.packet_view = []
        # Clear all table data
        self.table_widget.clear()
        # Prepares the program for closing
        self.close_actions()
        # Closes the application
        QApplication.quit()

    def show_common_dialog(self: Self, window_type: str = 'B') -> None:
        """
        This method displays the window to determine which column(s) to drop
        or display as binary.
        """
        # Identifies any selected columns
        selected_items = self.table_widget.selectedItems()
        # Empties the start and end values
        start = ''
        end = ''
        # If there are selected items
        if selected_items:
            # Retrieves the information for the first selected column
            first_item = selected_items[0]
            # Retrieves the first column number
            start = first_item.column()
            # If there are more than one columns selected
            if len(selected_items) > 1:
                # Retrieves the information for the last selected column
                last_item = selected_items[-1]
                # Retrieves the last column number
                end = last_item.column()

        # Empties the message
        message = ''

        # If the columns are to be displayed as binary
        if window_type == 'B':
            # Sets the message to display
            message = "to display as binary"
        # The columns are to be dropped
        elif window_type == 'D':
            # Sets the message to display
            message = "to drop"
        # The columns are to be highlighted
        elif window_type == 'H':
            # Sets the message to display
            message = "to highlight"

        # Sets the dialog window
        dialog = QDialog(self)
        # Sets the layout
        layout = QVBoxLayout()

        # Hidden widget to store the window type value
        self.hidden_label = QLabel(window_type)
        # Hides the label
        self.hidden_label.setVisible(False)
        # Adds the label to the dialog box layout
        layout.addWidget(self.hidden_label)

        # Label to request the start byte
        start_byte_label = QLabel(f'Start Byte {message} (Decimal Value '
                                  '0-based):')
        # Box to display any current value and retrieve the chosen value
        self.start_byte_input = QLineEdit(str(start))
        # Adds to the dialog box layout
        layout.addWidget(start_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.start_byte_input)

        # Label to request the end byte
        end_byte_label = QLabel(f'End Byte {message} (Decimal Value 0-based):')
        # Box to display any current value and retrieve the chosen value
        self.end_byte_input = QLineEdit(str(end))
        # Adds to the dialog box layout
        layout.addWidget(end_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.end_byte_input)

        # If the window is for highlighting
        if window_type == 'H':
            # Label to request the end byte
            highlight_label = QLabel('Color to highlight:')
            # Dropdown for color options
            self.color_dropdown = QComboBox()
            # Decoding scheme options
            self.color_dropdown.addItems(list(self.color_options.keys()))
            # Adds the highlight label to the layout
            layout.addWidget(highlight_label)
            # Adds the dropdown box to the layout
            layout.addWidget(self.color_dropdown)

        # Add a submit button to apply the settings
        submit_button = QPushButton('Apply')
        submit_button.clicked.connect(self.apply_common)
        layout.addWidget(submit_button)

        # Add a submit button to clear the settings
        submit_button = QPushButton('Clear')
        submit_button.clicked.connect(self.clear_binary)
        layout.addWidget(submit_button)

        # Add a submit button to close the window
        close_button = QPushButton('Close')
        close_button.clicked.connect(dialog.reject)
        layout.addWidget(close_button)

        # Sets the layout
        dialog.setLayout(layout)
        # Opens the dialog box
        dialog.exec_()

    def show_binary_dialog(self: Self) -> None:
        """
        This method is a way to call the appropriate common dialog for binary
        representation display.
        """
        # Calls the common dialog for binary display
        self.show_common_dialog('B')

    def show_dropped_dialog(self: Self) -> None:
        """
        This method is a way to call the appropriate common dialog for
        dropped display.
        """
        # Calls the common dialog for dropped display
        self.show_common_dialog('D')

    def show_highlight_dialog(self: Self) -> None:
        """
        This method is a way to call the appropriate common dialog for
        dropped display.
        """
        # Calls the common dialog for dropped display
        self.show_common_dialog('H')

    def apply_common(self: Self) -> None:
        """
        This method applies settings for the appropriate common dialog.
        """
        # Retrieves the start column
        start = self.get_value(self.start_byte_input.text())
        # Retrieves the end column
        end = self.get_value(self.end_byte_input.text())
        # Retrieves the hidden label value
        window_type = self.hidden_label.text()
        # If the window type is 'B'
        if window_type == 'B':
            # Sets the display value to be Binary
            display_value = 'Binary'
        # The window type should be dropped
        elif window_type == 'D':
            # Sets the display value to be Dropped
            display_value = 'Dropped'
        else:
            # Sets the display value to be Highlighted
            display_value = 'Highlighted'
        # Sets to indicate no errors were found
        errors_found = False
        # If the start value was empty
        if len(str(start)) == 0:
            # Display an information dialog of the error
            QMessageBox.information(self, f'{display_value} Value Error',
                                    'The start byte must be present.')
            # Sets to indicate that errors were found
            errors_found = True
        # If the start value was not empty and the start value is greater than
        # or equal to the total number of columns or less than zero
        if len(str(start)) > 0 and (start >= self.table_widget.columnCount()
                                    or start < 0):
            # Display an information dialog of the error
            QMessageBox.information(self, f'{display_value} Value Error',
                                    'The start byte must be greater than or '
                                    'equal to 0 and less than the maximum '
                                    'packet size ('
                                    f'{self.table_widget.columnCount()}).')
            # Sets to indicate that errors were found
            errors_found = True
        # If the end value was not empty and the end value is greater than
        # or equal to the total number of columns or less than or equal to
        # the start value
        if len(str(end)) > 0 and (end >= self.table_widget.columnCount() or
                                  end < start):
            # Display an information dialog of the error
            QMessageBox.information(self, f'{display_value} Value Error',
                                    'The end byte must be greater than or '
                                    f'equal to the start byte ({start}) and '
                                    'less than the maximum packet size ('
                                    f'{self.table_widget.columnCount()})')
            # Sets to indicate that errors were found
            errors_found = True
        # If no errors were found

        if not errors_found:
            # Empties the binary data
            self.binary_data = {}
            # If the window type is binary
            if window_type == 'B':
                # Sets the binary data
                self.binary_data = {'start': start, 'end': end}
            # The window type is to drop column(s)
            elif window_type == 'D':
                # Drops the specified columns from the data
                self.apply_dropped(start, end)
        # If the window type is to highlight column(s)
        if window_type == 'H':
            self.apply_highlighted(start, end,
                                   self.color_dropdown.currentText())
        # If the columns are not being highlighted
        else:
            # Displays the packets
            self.display_packets()

    def apply_highlighted(self: Self, start: int, end: int,
                          color: str) -> None:
        """
        This method highlights the columns within the packet data.
        """
        # If the end value was not entered
        if len(str(end)) == 0:
            # Set to be able to highlight the one column
            end = start
        # Iterates through each row
        for row in range(self.table_widget.rowCount()):
            # Iterates through each column
            for col in range(start, end + 1):
                # Sets to modify the column
                item = self.table_widget.item(row, col)
                # Sets the background to yellow
                item.setBackground(self.color_options[color])

    def apply_dropped(self: Self, start: int, end: int) -> None:
        """
        This method drops the specified columns from the data.
        """
        # Sets the new data
        new_data = []

        # If the end value is an integer
        if isinstance(end, int):
            # Sets length of columns to drop
            length_value = end - start + 1
        # end is not an integer
        else:
            # Only one column to be dropped
            length_value = 1
        # Step through each packet in view
        for packet in self.packet_view:
            # Makes the packet data raw
            raw_packet = raw(packet)
            # Skip packets with no data in them after dropping the columns
            if len(raw_packet) <= length_value:
                # Continue processing packets
                continue
            # Skips the columns specified and converts the data back into
            # packet format
            new_data.append(Ether([data for col, data in enumerate(raw_packet)
                                   if not (start == col or
                                           (isinstance(end, int) and
                                            (start <= col <= end)))]))
        # Copies the data set into the viewable data
        self.packet_view = new_data[:]
        # Displays the viewable data
        self.display_packets()

    def show_filter_dialog(self: Self) -> None:
        """
        This method shows and retrieves values to filter the data
        """
        # Retrieves the items selected
        selected_items = self.table_widget.selectedItems()
        # Empties the value and column
        value = ''
        column = ''
        # If there were selected items
        if selected_items:
            # Retrieves the information for the first selected item
            first_item = selected_items[0]
            # Retrieves the value in the cell
            value = first_item.text()
            # Retrieves the column number
            column = first_item.column()

        # Sets the dialog window
        dialog = QDialog(self)
        # Sets the layout
        layout = QVBoxLayout()

        # Sets the label for the byte to filter
        filter_byte_label = QLabel('Filter Byte (Decimal Value 0-based):')
        # Box to display any current value and retrieve the chosen value
        self.filter_byte_input = QLineEdit(str(column))
        # Adds to the dialog box layout
        layout.addWidget(filter_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.filter_byte_input)

        # Sets the label for the value to match
        match_byte_label = QLabel('Match Value (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.match_byte_input = QLineEdit(str(value))
        # Adds to the dialog box layout
        layout.addWidget(match_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.match_byte_input)

        # Add a submit button to apply
        submit_button = QPushButton('Apply')
        submit_button.clicked.connect(self.apply_filter)
        layout.addWidget(submit_button)

        # Add a submit button to clear the filter
        submit_button = QPushButton('Clear Filter')
        submit_button.clicked.connect(self.clear_filter)
        layout.addWidget(submit_button)

        # Add a submit button to close the window
        close_button = QPushButton('Close')
        close_button.clicked.connect(dialog.reject)
        layout.addWidget(close_button)

        # Sets the layout
        dialog.setLayout(layout)
        # Opens the dialog box
        dialog.exec_()

    def apply_filter(self: Self) -> None:
        """
        This method applies the selected filter.
        """
        # Sets the byte to filter
        filter_byte = self.get_value(self.filter_byte_input.text())
        # Sets the value to match
        match = self.get_value(self.match_byte_input.text(), 16)
        # Sets to indicate no errors were found
        errors_found = False
        # If there was no byte or match value entered
        if len(str(filter_byte)) == 0 or len(str(match)) == 0:
            # Display an information dialog of the error
            QMessageBox.information(self, 'Filter Value Error',
                                    'Both the filter byte and the value to '
                                    'match must be present.')
            # Sets to indicate errors were found
            errors_found = True
        # If the filter byte was entered and it is greater than or equal to
        # the total number of columns or less than zero
        if (len(str(filter_byte)) > 0 and
           (filter_byte >= self.table_widget.columnCount() or
           filter_byte < 0)):
            # Display an information dialog of the error
            QMessageBox.information(self, 'Filter Value Error',
                                    'The filter byte must be greater than or '
                                    'equal to 0 and less than the maximum '
                                    'packet size.')
            # Sets to indicate errors were found
            errors_found = True
        # If the a match value was entered and it is greater than the max value
        # of a byte less than zero
        if len(str(match)) > 0 and (match > 0xff or match < 0):
            # Display an information dialog of the error
            QMessageBox.information(self, 'Filter Value Error',
                                    'The match byte must be greater than or '
                                    'equal to 0 and less than 0xff (255).')
            # Sets to indicate errors were found
            errors_found = True
        # If no errors were found
        if not errors_found:
            # Filter and display the packets
            self.filter_packets(filter_byte, match)

    def filter_packets(self: Self, filter_byte: str, match: int) -> None:
        """
        This method filters the packets based on the passed byte and match
        value.
        """
        # If there are no packets to view
        if not self.packet_view:
            # Return
            return
        hex_match = hex(match).replace('0x', '')
        # Store just the packets that match
        self.packet_view = [packet for packet in self.packet_view
                            if len(packet) > filter_byte and
                            packet.build().hex()[filter_byte*2:filter_byte*2+2]
                            == hex_match]
        # Display the packets
        self.display_packets()

    def display_packets(self: Self) -> None:
        """"
        This method displays the packets chosen to view.
        """
        # If there are no packets to view
        if not self.packet_view:
            # Return
            return

        # Clears the table
        self.clear_table()

        # Displayes the frozen table
        self.frozen_table.setVisible(True)

        # Prepare hex data
        hex_data = []

        # Sets for the frozen horizontabl scroll bar to always be displayed
        # This aligns the frozen rows to the scrollable rows at all times
        self.frozen_table.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        # Iterate through the packets to view
        for packet in self.packet_view:
            # Convert it to hex
            packet_hex = packet.build().hex()
            # Step through each value keeping the hex data
            hex_data.append([packet_hex[i:i+2]
                             for i in range(0, len(packet_hex), 2)])

        # Find the longest packet to determine column count
        num_columns = max(len(packet) for packet in hex_data)
        # Sets the number of rows in the data
        num_rows = len(hex_data)

        # Sets the cell padding
        self.table_widget.setStyleSheet("QTableWidget { padding: 0px; }")
        self.frozen_table.setStyleSheet("QTableWidget { padding: 0px; }")

        # Sets the number of rows
        self.table_widget.setRowCount(num_rows)
        self.frozen_table.setRowCount(num_rows)

        # Set number of columns
        self.table_widget.setColumnCount(num_columns)
        self.frozen_table.setColumnCount(1)

        # Set vertical column numbers for the header
        self.table_widget.setHorizontalHeaderLabels([str(i) for i in
                                                     range(num_columns)])

        # Only PKT Length column here
        self.frozen_table.setHorizontalHeaderLabels(["PKT Length"])

        # Fill the table with hex data
        # Iterates through each row
        for row in range(num_rows):
            # Iterates through each column
            for col in range(len(hex_data[row])):
                # Sets the display data
                display_data = hex_data[row][col]
                # If the data is to be presented as binary
                if 'start' in self.binary_data:
                    # if the column falls within the range of the start and
                    # end values
                    if (col == self.binary_data['start'] or
                        ('end' in self.binary_data and
                         isinstance(self.binary_data['end'], int) and
                         self.binary_data['start'] < col <=
                         self.binary_data['end'])):
                        # Sets the display data as binary
                        display_data = f'{int(hex_data[row][col], 16):08b}'
                # Sets the area with the display data
                item = QTableWidgetItem(display_data)
                # Aligns the data to be center aligned
                item.setTextAlignment(Qt.AlignCenter)
                # Adds in the pkt length column for display
                if col == 0:
                    # Sets the area with the display data
                    pkt_length = QTableWidgetItem(str(len(hex_data[row])))
                    # Aligns the data to be center aligned
                    pkt_length.setTextAlignment(Qt.AlignCenter)
                    # Sets the table with the value
                    self.frozen_table.setItem(row, col, pkt_length)
                # Sets the table with the value
                self.table_widget.setItem(row, col, item)

        # Adjust the column widths based on content
        self.table_widget.resizeColumnsToContents()
        self.frozen_table.resizeColumnsToContents()
        # Adjust the row widths based on content
        self.table_widget.resizeRowsToContents()
        self.frozen_table.resizeRowsToContents()

        # Sets the cell padding
        self.table_widget.setStyleSheet("QTableWidget { padding: 0px; }")
        self.frozen_table.setStyleSheet("QTableWidget { padding: 0px; }")

        # Sets the width of the table to be the width of the row number
        # plus the width of the packet length column contents
        self.frozen_table.setFixedWidth(self.frozen_table.columnWidth(0) +
                                        self.frozen_table
                                        .verticalHeader().width())

    def search_hex_dialog(self: Self) -> None:
        """
        This method is the dialog window to allow for searching of hex values
        within the packet data.
        """
        # Empties the prefillsed value(s)
        pre_filled_value = ''
        # Retrieves the selected value(s)
        selected_items = self.table_widget.selectedItems()
        # If there are selected values
        if selected_items:
            # Pre-fill search dialog with selected byte values
            pre_filled_value = ''.join(
                item.text().replace('0x', '').replace('\\x', '')
                .replace('x', '').replace(' ', '')
                for item in selected_items
            )

        # Sets the dialog window
        dialog = QDialog(self)
        # Sets the layout
        layout = QVBoxLayout()

        # Sets the label for the hex value to search for
        pattern_label = QLabel('Search Pattern (2 value hex pairs: Ex: 01 and '
                               'not 1):')
        # Box to display any current value and retrieve the chosen value
        self.pattern_input = QLineEdit(str(pre_filled_value))
        # Adds to the dialog box layout
        layout.addWidget(pattern_label)
        # Adds to the dialog box layout
        layout.addWidget(self.pattern_input)

        # Add a submit button to apply the search
        submit_button = QPushButton('Apply')
        submit_button.clicked.connect(self.search_hex)
        layout.addWidget(submit_button)

        # Add a submit button to clear the search
        submit_button = QPushButton('Clear Search')
        submit_button.clicked.connect(self.clear_search)
        layout.addWidget(submit_button)

        # Add a submit button to close the window
        close_button = QPushButton('Close')
        close_button.clicked.connect(dialog.reject)
        layout.addWidget(close_button)

        # Sets the layout
        dialog.setLayout(layout)
        # Opens the dialog box
        dialog.exec_()

    def search_hex(self: Self) -> None:
        """
        This method retrieves the hex value to search.
        """
        # Retrieves the search pattern
        pattern = self.get_value(self.pattern_input.text(), 16)
        # If there was a search pattern entered
        if pattern:
            # Retrieves the pattern as text
            pattern = self.pattern_input.text()
            # Calls to highlight the pattern within the data
            self.highlight_pattern(pattern)

    def highlight_pattern(self: Self, pattern: str) -> None:
        """
        This method highlights the pattern within the packet data.
        """
        # Iterates through each row
        for row in range(self.table_widget.rowCount()):
            # Sets the row data
            row_data = ''.join([
                f'{self.table_widget.item(row, col).text().casefold():02s}'
                for col in range(1, self.table_widget.columnCount())
                if self.table_widget.item(row, col)
            ])
            # Looks for the pattern in the data
            matches = re.finditer(pattern.casefold(), row_data)
            # Iterates through each match
            for match in matches:
                # Iterates through each column between the match start and
                # match end
                # The +1 allows for skipping the pkt length column
                for col in range(match.start()//2+1, match.end()//2+1):
                    # If it starts on a byte boundary
                    if match.start() % 2 == 0:
                        # Sets to the item found
                        item = self.table_widget.item(row, col)
                        # Sets the background to yellow
                        item.setBackground(Qt.yellow)

    def clear_search(self: Self) -> None:
        """
        This method changes the background color back to white.
        """
        # Iterates through each row
        for row in range(self.table_widget.rowCount()):
            # Iterates through each column
            for col in range(self.table_widget.columnCount()):
                # Sets to the item
                item = self.table_widget.item(row, col)
                # If there is an item
                if item:
                    # Reset the background color to default
                    item.setBackground(Qt.white)

    def show_fragmentation_dialog(self: Self) -> None:
        """
        This method shows the window for the fragmentation dialog.
        """
        # Retrieves the selected items
        selected_items = self.table_widget.selectedItems()
        # Empties the values
        column = ''
        last_column = ''
        # If there were items selected
        if selected_items:
            # Sets the first item
            first_item = selected_items[0]
            # Retrieves the first column number
            column = first_item.column()
            # If there is more than one column
            if len(selected_items) > 1:
                # Sets the last item
                last_item = selected_items[-1]
                # Retrieves the last column number
                last_column = last_item.column()

        # Sets the dialog window
        dialog = QDialog(self)
        # Sets the layout
        layout = QVBoxLayout()

        # Sets the label for the fragmentation byte number
        frag_byte_label = QLabel('Fragmentation Byte (Decimal Value 0-based):')
        # Box to display any current value and retrieve the chosen value
        self.frag_byte_input = QLineEdit(str(column))
        # Adds to the dialog box layout
        layout.addWidget(frag_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.frag_byte_input)

        # Sets the label for the fragmentation mask
        mask_label = QLabel('Fragmentation Mask (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.mask_input = QLineEdit()
        # Adds to the dialog box layout
        layout.addWidget(mask_label)
        # Adds to the dialog box layout
        layout.addWidget(self.mask_input)

        # Sets the label for the start fragmentation mask
        start_label = QLabel('Start Fragmentation Mask (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.start_input = QLineEdit()
        # Adds to the dialog box layout
        layout.addWidget(start_label)
        # Adds to the dialog box layout
        layout.addWidget(self.start_input)

        # Sets the label for the middle fragmentation mask
        mid_label = QLabel('Middle Fragmenation Mask (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.mid_input = QLineEdit()
        # Adds to the dialog box layout
        layout.addWidget(mid_label)
        # Adds to the dialog box layout
        layout.addWidget(self.mid_input)

        # Sets the label for the end fragmentation mask
        end_label = QLabel('End Fragmenation Mask (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.end_input = QLineEdit()
        # Adds to the dialog box layout
        layout.addWidget(end_label)
        # Adds to the dialog box layout
        layout.addWidget(self.end_input)

        # Sets the label for the ungragmented mask
        unfrag_label = QLabel('Unfragmented Fragmenation Mask (Hex Value):')
        # Box to display any current value and retrieve the chosen value
        self.unfrag_input = QLineEdit()
        # Adds to the dialog box layout
        layout.addWidget(unfrag_label)
        # Adds to the dialog box layout
        layout.addWidget(self.unfrag_input)

        # Sets the label for the data start byte
        data_byte_label = QLabel('Data Byte (Decimal Value 0-based):')
        # Box to display any current value and retrieve the chosen value
        self.data_byte_input = QLineEdit(str(last_column))
        # Adds to the dialog box layout
        layout.addWidget(data_byte_label)
        # Adds to the dialog box layout
        layout.addWidget(self.data_byte_input)

        # Sets up the TCP fragmentation checkbox
        self.tcp_label = QLabel('IP/TCP Fragmentation')
        self.tcp_checkbox = QCheckBox()
        self.tcp_checkbox.toggled.connect(lambda checked:
                                          self.on_checkbox_toggled(1, checked))

        # Sets up the UDP fragmentation checkbox
        self.udp_label = QLabel('IP/UDP Fragmentation')
        self.udp_checkbox = QCheckBox()
        self.udp_checkbox.toggled.connect(lambda checked:
                                          self.on_checkbox_toggled(2, checked))

        # Adds the checkboxes to the layout
        layout.addWidget(self.tcp_label)
        layout.addWidget(self.tcp_checkbox)
        layout.addWidget(self.udp_label)
        layout.addWidget(self.udp_checkbox)

        # Add a submit button to apply the settings
        submit_button = QPushButton('Apply')
        submit_button.clicked.connect(self.apply_fragmentation)
        layout.addWidget(submit_button)

        # Add a submit button clear fragmentation
        submit_button = QPushButton('Clear Fragmentation')
        submit_button.clicked.connect(self.clear_fragmentation)
        layout.addWidget(submit_button)

        # Add a submit button to close the window
        close_button = QPushButton('Close')
        close_button.clicked.connect(dialog.reject)
        layout.addWidget(close_button)

        # Sets the layout
        dialog.setLayout(layout)
        # Opens the dialog box
        dialog.exec_()

    def on_checkbox_toggled(self, checkbox_id, checked):
        """
        This method toggles the IP/TCP and IP/UDP checkboxes appropriately
        for defragmentation.
        """
        # Sets the possible states
        states = [False, False, False]
        # Sets the state for the checkbox selected
        states[checkbox_id - 1] = checked
        # If the checkbox_id is less than the current selected one
        # Mark them in reverse order
        # This fixed the issue with the setChecked setting the status and
        # calling this function subsequent times to allow for the checkboxes
        # to be marked correctly the first time
        if checkbox_id <= self.frag_checkbox_selected:
            # Set each checkbox's state
            self.udp_checkbox.setChecked(states[1])
            self.tcp_checkbox.setChecked(states[0])
        # The checkbox is larger than the current selected one
        else:
            # Set each checkbox's state
            self.tcp_checkbox.setChecked(states[0])
            self.udp_checkbox.setChecked(states[1])
        # Updates the checkbox selected
        self.frag_checkbox_selected = checkbox_id

    def calculate_mask_range(self: Self, mask: int) -> tuple:
        """
        This method calculates the minimum and maximum range of the mask
        integer value.
        """
        # If the value is one
        if (mask & -mask) == 1:
            # Returns the minumum and maximum values
            return 0, mask

        # Returns the minumum and maximum values
        return (mask & -mask), mask

    def get_value(self: Self, field: str, base: int = 10) -> int:
        """
        This method retrieves the value entered in a dialog box. It returns
        an int on success and an empty string on failure to convert.
        """
        # Begins a try->except block
        try:
            # If the value starts with 'x'
            if field.casefold().startswith('x'):
                # Remove the 'x'
                field = field[1:]
            # If the value begins with a '0x' or '\x'
            elif (field.casefold().startswith('0x') or
                  field.casefold().startswith('\\x')):
                # Remove them
                field = field[2:]
            # Remove and spaces
            field = field.replace(' ', '')
            # If the base is 10
            if base == 10:
                # Return the value as an integer
                return int(field)
            # Return the value as a hex value
            return int(field, 16)
        # A value error occurred
        except ValueError:
            # Return an empty string
            return ''

    def set_frag_values(self: Self) -> None:
        """
        This method sets the fragmentation data.
        """
        # Empties the fragmenation data
        self.frag = {}
        # Sets each of the values based on the associated text box
        self.frag['frag_byte'] = self.get_value(self.frag_byte_input.text())
        self.frag['frag_mask'] = self.get_value(self.mask_input.text(), 16)
        self.frag['start_value'] = self.get_value(self.start_input.text(), 16)
        self.frag['mid_value'] = self.get_value(self.mid_input.text(), 16)
        self.frag['end_value'] = self.get_value(self.end_input.text(), 16)
        self.frag['unfrag_value'] = self.get_value(self.unfrag_input.text(),
                                                   16)
        self.frag['data_start'] = self.get_value(self.data_byte_input.text())

        # If IP/TCP fragmenation is selected
        if self.tcp_checkbox.isChecked():
            # Subtract the 20 bytes of TCP header from the start as these
            # bytes are data in middle and end packets
            self.frag['data_start'] -= 20
        # If IP/UDP fragmentation is selected
        elif self.udp_checkbox.isChecked():
            # Subtract the 8 bytes of UDP header from the start as these
            # bytes are data in middle and end packets
            self.frag['data_start'] -= 8

        # Sets to indicate errors were not found
        errors_found = False
        # If the value in the field is empty or the value is greater than
        # or equal to the number of columns or the value is less than 0
        if (len(str(self.frag['frag_byte'])) == 0 or
           self.frag['frag_byte'] >= self.table_widget.columnCount() or
           self.frag['frag_byte'] < 0):
            # Displays a message for the error in a dialog box
            QMessageBox.information(self, 'Fragmentation Value Error',
                                    'Invalid fragmentation byte. Must be an '
                                    'integer greater than or equal to 0 and '
                                    'less than the max packet length ('
                                    f'{self.table_widget.columnCount()}).\n'
                                    'It represents the 0-based location of '
                                    'the byte within the packet where the '
                                    'fragmentation control is located.')
            # Sets to indicate errors were found
            errors_found = True
        # If the value in the field is is empty or the value is greather than
        # the maximum value of a byte or less than the minimum value associated
        # with at least two bits controlling fragmentation (0x3)
        if (len(str(self.frag['frag_mask'])) == 0 or
           self.frag['frag_mask'] > 0xff or
           self.frag['frag_mask'] < 0x3):
            # Displays a message for the error in a dialog box
            QMessageBox.information(self, 'Fragmentation Value Error',
                                    'Invalid fragmentation mask. Must be a '
                                    'hex value greater than or equal to 0x3 '
                                    'and less than or equal to 0xff.\nIt '
                                    'represents the hexadecimal value of the '
                                    'bits in the fragmenation byte that '
                                    'control the fragmenation.')
            # Sets to indicate errors were found
            errors_found = True
        # The value was not empty and within a valid range
        else:
            # Retrieve the minimum and maximum values
            min_value, max_value = self.calculate_mask_range(self.
                                                             frag['frag_mask'])
            # If the start value is set and not within the minimum and maximum
            # value range and it is not set to 0x0
            if (len(str(self.frag['start_value'])) > 0 and
                (self.frag['start_value'] < min_value or
                 self.frag['start_value'] > max_value) and
               self.frag['start_value'] != 0):
                # Displays a message for the error in a dialog box
                QMessageBox.information(self, 'Fragmentation Value Error',
                                        'Invalid start value entered. Value '
                                        'must fall within the range of the '
                                        'fragmentation mask entered ('
                                        f'{hex(self.frag["frag_mask"])}).')
                # Sets to indicate errors were found
                errors_found = True
            # If the middle value is set and not within the minimum and maximum
            # value range and it is not set to 0x0
            if (len(str(self.frag['mid_value'])) > 0 and
                (self.frag['mid_value'] < min_value or
                 self.frag['mid_value'] > max_value) and
               self.frag['mid_value'] != 0):
                # Displays a message for the error in a dialog box
                QMessageBox.information(self, 'Fragmentation Value Error',
                                        'Invalid middle value entered. Value '
                                        'must fall within the range of the '
                                        'fragmentation mask entered ('
                                        f'{hex(self.frag["frag_mask"])}).')
                # Sets to indicate errors were found
                errors_found = True
            # If the end value is set and not within the minimum and maximum
            # value range and it is not set to 0x0
            if (len(str(self.frag['end_value'])) > 0 and
                (self.frag['end_value'] < min_value or
                 self.frag['end_value'] > max_value) and
               self.frag['end_value'] != 0):
                # Displays a message for the error in a dialog box
                QMessageBox.information(self, 'Fragmentation Value Error',
                                        'Invalid end value entered. Value '
                                        'must fall within the range of the '
                                        'fragmentation mask entered ('
                                        f'{hex(self.frag["frag_mask"])}).')
                # Sets to indicate errors were found
                errors_found = True
            # If the unfragmented value is set and not within the minimum and
            # maximum value range
            if (len(str(self.frag['unfrag_value'])) > 0 and
                (self.frag['unfrag_value'] < min_value or
                 self.frag['unfrag_value'] > max_value)):
                # Displays a message for the error in a dialog box
                QMessageBox.information(self, 'Fragmentation Value Error',
                                        'Invalid unfragmented value entered. '
                                        'Value must fall within the range of '
                                        'the fragmentation mask entered ('
                                        f'{hex(self.frag["frag_mask"])}).')
                # Sets to indicate errors were found
                errors_found = True
        # Sets the control fields
        control_fields = ['start_value', 'mid_value', 'end_value',
                          'unfrag_value']
        # Retrieves the values for the control fields
        control_values = [self.frag[value] for value in control_fields
                          if self.frag[value]]
        # If the control values are not unique
        if list(sorted(control_values)) != list(set(control_values)):
            # Displays a message for the error in a dialog box
            QMessageBox.information(self, 'Fragmentation Value Error',
                                    'Invalid start, middle, and/or end values '
                                    'entered. Each value must be unique.')
            # Sets to indicate errors were found
            errors_found = True
        # If a start->middle, start->middle->end, or middle->end grouping
        # was not found
        if not any([len(str(self.frag['start_value'])) > 0 and
                    len(str(self.frag['mid_value'])) > 0,
                    len(str(self.frag['mid_value'])) > 0 and
                    len(str(self.frag['end_value'])) > 0,
                    len(str(self.frag['start_value'])) > 0 and
                    len(str(self.frag['mid_value'])) > 0 and
                    len(str(self.frag['end_value'])) > 0]):
            # Displays a message for the error in a dialog box
            QMessageBox.information(self, 'Fragmentation Value Error',
                                    'Invalid start, middle, and/or end values '
                                    'entered. Must enter one of the following '
                                    'groups as hex values:\nstart and middle\n'
                                    'middle and end\nstart, middle and end')
            # Sets to indicate errors were found
            errors_found = True
        # If the fragmenation start value and data start values were entered
        if self.frag['data_start'] and self.frag['frag_byte']:
            # If the data start value falls before the fragmenation byte or
            # the data start value is more than the location of the
            # fragmentation byte
            if (self.frag['data_start'] >= self.table_widget.columnCount() or
               self.frag['data_start'] <= self.frag['frag_byte']):
                # Displays a message for the error in a dialog box
                QMessageBox.information(self, 'Fragmentation Value Error',
                                        'Invalid data start byte. The value '
                                        'must be greater than the '
                                        'fragmentation byte value entered ('
                                        f'{self.frag["frag_byte"]}) and less '
                                        'than the maximum packet size ('
                                        f'{self.table_widget.columnCount()}).')
                # Sets to indicate errors were found
                errors_found = True
        # One or more of the values is empty
        else:
            # Displays a message for the error in a dialog box
            QMessageBox.information(self, 'Fragmentation Value Error',
                                    'Invalid fragmentation byte and data '
                                    'start byte pair. Both areas must contain '
                                    'a valid value.')
            # Sets to indicate errors were found
            errors_found = True
        # If errors were found
        if errors_found:
            # Empty the fragmenation dictionary
            self.frag = {}

    def apply_fragmentation(self: Self) -> None:
        """
        This method applies the fragmentation.
        """
        # Sets the fragmentation data
        self.set_frag_values()
        # If fragmenation data has been set
        if self.frag:
            # Applies the fragmentation
            self.defragment_packets()

    def defragment_packets(self: Self) -> None:
        """
        This method defragments packets based on the specifications entered.
        """
        # Creates a new list to store the packets
        new_data = []

        # Set to indicate a start fragmenation has not been found
        start_found = False
        # Set to indicate a middle fragmenation has not been found
        middle_found = False
        # Iterate through each packet within the view
        for packet in self.packet_view:
            # Creates a raw version of the packet
            raw_packet = raw(packet)
            # If the length of the number of bytes within the packet is less
            # than the starting point of the data
            if len(raw_packet) < self.frag['data_start']:
                # Skip the packet
                continue

            # Handle fragmented packet types based on
            # start/middle/end/unfragment settings
            # Determines the type of packet
            frag_byte = (raw_packet[self.frag['frag_byte']] &
                         self.frag['frag_mask'])
            # If the packet is the start of fragmentation
            if frag_byte == self.frag['start_value']:
                # Start of fragment - keep the entire packet
                new_packet = raw_packet[:]
                # Sets to indicate that a starting fragment was found
                start_found = True
                # Sets to indicate that a middle fragment was not found
                middle_found = False
            # If the packet is the middle of fragmentation
            elif frag_byte == self.frag['mid_value']:
                # If the start has been found
                if start_found:
                    # Middle fragment - only keep data from data_start
                    # Append bytes from the data_start to an ongoing packet
                    new_packet += raw_packet[self.frag['data_start']:]
                # A fragmentation start has not been found
                else:
                    # Start of fragment - keep the entire packet
                    new_packet = raw_packet[:]
                    # Sets to indicate that a starting fragment was found
                    start_found = True
                # Sets to indicate that a middle fragment was found
                middle_found = True
            # If the packet is the end of fragmentation
            elif frag_byte == self.frag['end_value']:
                # If this is the end of a group of middles
                if middle_found:
                    # Append bytes from the data_start to an ongoing packet
                    new_packet += raw_packet[self.frag['data_start']:]
                    # Appends a packetized version of the data to the
                    # running list
                    new_data.append(Ether(new_packet))
                # If the end is also unfragmented
                else:
                    # Appends a packetized version of the data to the
                    # running list
                    new_data.append(Ether(raw_packet))

                # Sets to indicate that a middle fragment was not found
                middle_found = False
                # Sets to indicate that a starting fragment was not found
                start_found = False
            # The packet is unfragmented
            elif frag_byte == self.frag['unfrag_value']:
                # Sets to indicate that a starting fragment was not found
                start_found = False
                # Appends a packetized version of the data to the running list
                new_data.append(Ether(raw_packet))
                # Sets to indicate that a middle fragment was not found
                middle_found = False
        # Sets the data to view
        self.packet_view = new_data[:]
        # Display the unfragmented data
        self.display_packets()

    def reset_display(self: Self) -> None:
        """
        This method resets the packets displayed to the original payload.
        """
        # Clears the binary data
        self.binary_data = {}
        # Sets the packets to view back to the original packets read
        self.packet_view = self.original_packets[:]
        # Displays the packets
        self.display_packets()

    def clear_dropped(self: Self) -> None:
        """
        This method clears dropped columns back to the original payload.
        """
        # Resets the display
        self.reset_display()

    def clear_binary(self: Self) -> None:
        """
        This method clears binary columns back to the original payload.
        """
        # Resets the display
        self.reset_display()

    def clear_filter(self: Self) -> None:
        """
        This method clears the filter back to the original payload.
        """
        # Resets the display
        self.reset_display()

    def clear_fragmentation(self: Self) -> None:
        """
        This method clears fragmentation back to the original payload.
        """
        # Resets the display
        self.reset_display()

    def save_text_to_file(self: Self) -> None:
        """
        This method saves text data to a file.
        """
        # If a pcap file name was set
        if self.pcap_file:
            # Replaces the value after the last period (.) from the pcap
            # file with txt
            default_file_name = self.pcap_file.rsplit('.', 1)[0] + '.txt'
        # The file was not set
        else:
            # Sets a default file name
            default_file_name = 'output.txt'

        # Brings up a dialog box to retrieve the filename to save
        file_name, _ = QFileDialog.getSaveFileName(self, 'Save As',
                                                   default_file_name,
                                                   'Text Files (*.txt)')
        # If a file name was entered
        if file_name:
            # Open the file for writing
            with open(file_name, 'w', encoding='utf-8') as f:
                # Writes the text to the file
                f.write(self.text_widget.toPlainText())

    def view_text_window(self: Self) -> None:
        """
        This method brings up a text window for decoding text.
        """
        # If the text window is not already open
        if not self.text_window:
            # Begin a text window
            self.text_window = QMainWindow(self)
            # Sets the text window title
            self.text_window.setWindowTitle('Decoded Text Window')

            # Text Menu
            # Sets the menu
            text_menu = self.text_window.menuBar().addMenu('File')
            # Sets the action
            save_text_action = QAction('Save As', self.text_window)
            # Sets the function to call when triggered
            save_text_action.triggered.connect(self.save_text_to_file)
            # Adds the action
            text_menu.addAction(save_text_action)

            # Dropdown for decoding scheme
            dropdown = QComboBox(self.text_window)
            # Decoding scheme options
            dropdown.addItems(['ASCII', 'Binary', 'Hex'])
            # Applies the text decoding
            dropdown.currentTextChanged.connect(self.change_decoding_scheme)

            # Dropdown to allow for the selection of which bytes to decode
            # or all of the bytes
            data_selection_dropdown = QComboBox(self.text_window)
            # Dropdown options
            data_selection_dropdown.addItems(['All Data', 'Selected Byte(s)'])
            # What to do when changed
            data_selection_dropdown.currentTextChanged\
                .connect(self.change_text_display_mode)

            # Close button ('X' on the frame)
            close_button = QPushButton('Close Text Window', self.text_window)
            # Closes the window
            close_button.clicked.connect(self.text_window.close)

            # Sets the layout
            layout = QVBoxLayout()
            # Adds the decoding dropdown
            layout.addWidget(dropdown)
            # Adds the selection dropdown
            layout.addWidget(data_selection_dropdown)
            # Adds the close button
            layout.addWidget(close_button)

            # Add the text view for decoded data
            self.text_widget = QTextEdit(self.text_window)
            # Sets the window to read only
            self.text_widget.setReadOnly(True)
            # Adds to the layout
            layout.addWidget(self.text_widget)

            # Creates a container
            container = QWidget()
            # Adds the layout
            container.setLayout(layout)
            # Sets the widget
            self.text_window.setCentralWidget(container)
            # Maximize window
            self.text_window.showMaximized()
        # Raises the window to make it visible
        self.text_window.raise_()
        # Updates the text view
        self.update_text_view()
        # Shows the window
        self.text_window.show()

    def change_text_display_mode(self: Self, mode: str) -> None:
        """
        This method updates the text bytes to decode.
        """
        # Sets the display mode
        self.text_display_mode = mode
        # Updates the text
        self.update_text_view()

    def setup_signals(self: Self) -> None:
        """
        This method sets up the signals for updating the selected text for
        decoding.
        """
        # Connect the selection change to a delayed update
        self.table_widget.selectionModel().selectionChanged\
            .connect(self.update_text_view)

    def update_text_view(self: Self) -> None:
        """
        This method updates the decoded text.
        """
        # Retrieves the selected text
        selected_items = self.table_widget.selectedItems()
        # Empties the decoded text
        decoded_text = ''
        # If the display mode is everything
        if self.text_display_mode == 'All Data':
            # Iterates through each row
            for row in range(self.table_widget.rowCount()):
                # Iterates through each column
                for col in range(self.table_widget.columnCount()):
                    # Retrieves the item
                    item = self.table_widget.item(row, col)
                    # If an item was found
                    if item:
                        # Retrieve the text (byte value)
                        byte_value = item.text()
                        # Decode the byte
                        decoded_text += self.decode_byte(byte_value)
        # The option was to decode selected bytes and there were bytes selected
        elif self.text_display_mode == 'Selected Byte(s)' and selected_items:
            # Decode the selected bytes
            decoded_text = ''.join(self.decode_byte(item.text())
                                   for item in selected_items)
        # Sets the text in the display
        self.text_widget.setText(decoded_text)

    def decode_byte(self: Self, byte_value: str) -> str:
        """
        This function decodes the byte value using the chosen decoding scheme.
        """
        # Retrieve the integer value from the hexadecimal string
        byte_int = int(byte_value, 16)
        # If the decoding scheme is 'ASCII'
        if self.decoding_scheme == 'ASCII':
            # If the byte is a printable ASCII character
            if 32 <= byte_int <= 126 or byte_int in (9, 10, 13):
                # Return the character associated with the integer
                return chr(byte_int)
            # It is not a printable ASCII character
            # Return the hex value
            return f'{byte_int:02x}'
        # If the decoding scheme is 'Binary'
        if self.decoding_scheme == 'Binary':
            # Returns the integer as a zero padded 8-bit binary string
            return bin(byte_int)[2:].zfill(8)
        # If the decoding scheme is 'EBCDIC'
        # Not implemented currently
        if self.decoding_scheme == 'EBCDIC':
            # Begin a try-except block
            try:
                # Beginning EBCDIC map
                # What is the best way to do data conversion?
                ebcdic_map = {129: 'A', 130: 'B', 131: 'C'}
                # Returns the decoded value
                return ebcdic_map.get(byte_int, f'{byte_int}')
            # If the value could not be decoded
            except KeyError:
                # Return the hex value
                return f'{byte_int:02x}'
        # Any other option
        # Return the hex value by default
        return f'{byte_int:02x}'

    def change_decoding_scheme(self: Self, new_scheme: str) -> None:
        """
        This method changes the decoding scheme.
        """
        # Update the decoding scheme
        self.decoding_scheme = new_scheme
        # Refresh the text view
        self.update_text_view()

    def about_message(self: Self) -> None:
        """
        This method displays information about the program.
        """
        # Opens a dialog box with the information
        QMessageBox.about(self, 'About', 'PktSleuth v1.0\nThis tool allows '
                          'you to view and manipulate PCAP files in '
                          'hexadecimal format and decode bytes.\n\n'
                          'Author: Tom Bichard - pktsleuthfeedback@gmail.com')

    def help_message(self: Self) -> None:
        """
        This method displays help information.
        """
        # Opens a dialog box with the information
        QMessageBox.about(self, 'Help', 'On certain systems/environments,'
                          'PktSleuth produces a segmentation fault upon '
                          'closing. It is a known issue where the cause has '
                          'not been identified.')


def parse_command_line() -> None:
    """
    This function parses the command line and returns the recovered pcap
    file, if any was present.
    """
    # Parses the command line
    parser = argparse.ArgumentParser(description="PktSleuth")
    parser.add_argument('pcap_file', nargs='?', help="PCAP file to open")
    parser.add_argument('-i', '--ifile', help="PCAP file to open")
    args = parser.parse_args()
    # Returns the pcap file
    return args.pcap_file or args.ifile


# If the program was executed by calling its name and not imported
if __name__ == '__main__':
    # Retrieves the data file from the command line
    data_file = parse_command_line()
    # IF the environment variable exists, unset it
    os.environ.pop('QT_STYLE_OVERRIDE', None)

    # Begins the application
    app = QApplication(sys.argv)
    # Set the application icon
    icon = os.path.abspath("PktSleuth_icon.png")
    # Sets the window icon
    app.setWindowIcon(QIcon(icon))
    # Begins an instance of PktSleuth
    analyzer = PktSleuth(pcap_file=data_file, icon_file=icon)
    # Shows PktSleuth
    analyzer.show()
    # Exits the application (segment faults on some systems for some reason)
    sys.exit(app.exec_())

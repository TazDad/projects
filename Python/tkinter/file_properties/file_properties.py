#! /usr/bin/env python3.11


"""
This program uses tkinter from Python to create an application that will
allow an individual to load a file into a graphical user interface (GUI) to
view the file properties. The file creation and modification time can be
updated from within the GUI.
"""

# Imports the required libraries
import os
from tkinter import Tk, Entry, Button, Label, filedialog
from datetime import datetime
import humanize


def get_file():
    """
    This function loads the selected file into the GUI, retrieving the
    properties and setting them within the GUI fields.
    """
    # Sets the file times to allow the user to view all files or a
    # pre-defined subset of file times for selection
    file_types = {'All Files': '*',
                  'Text': '.txt .rtf',
                  'Videos': '.webm .mpg .mp2 .mpeg .mpe .mpv .ogg .mp4 .m4p '
                            '.m4v .avi .wmv .mov .qt .flv .swf .avchd',
                  'Images': '.jpg .jpeg .jpe .jif .jfif .jfi .png .gif .webp '
                            '.tiff .tif .psd .raw .arw .cr2 .nrw .k25 .bmp '
                            '.dib .heif .heic .ind .indd .indt .jp2 .j2k .jpf '
                            '.jpx .jpm .mj2 .svg .svgz .ai .eps',
                  'Microsoft Excel': '.xlsx',
                  'Microsoft Excel (97-2003)': '.xls',
                  'Microsoft Word': '.docx',
                  'Microsoft Word (97-2003)': '.doc',
                  'Microsoft PowerPoint': '.pptx',
                  'Microsoft PowerPoint (97-2003)': '.ppt',
                  'Adobe PDF': '.pdf'}

    # Retrieves the filename that the user selected
    filename = filedialog.askopenfilename(initialdir=os.getcwd(),
                                          title='Select a File',
                                          filetypes=list(file_types.items()))

    # If the user entered anything and the value is a valid file
    if len(filename) >= 1 and os.path.isfile(filename):
        # Modifies the label for the file explorer area
        explorer_selection_label.configure(text=filename)

        # Retrieves the file statistics
        file_stats = os.stat(filename)

        # Updates the Entry box with the formatted creation time
        creation_time_entry.insert(0, datetime.
                                   fromtimestamp(file_stats.st_ctime).
                                   strftime("%Y-%m-%d %H:%M:%S.%f"))

        # Updates the Entry box with the formatted modification time
        last_modified_time_entry.insert(0, datetime.
                                        fromtimestamp(file_stats.st_mtime).
                                        strftime("%Y-%m-%d %H:%M:%S.%f"))

        # Updates the file size label with the human readable file size value
        file_size_entry.configure(text=humanize.naturalsize(file_stats.st_size,
                                                            binary=True))

        # Update the labels with the readable, writable, and executable status
        readable_entry.configure(text=str(os.access(filename, os.R_OK)))
        writable_entry.configure(text=str(os.access(filename, os.W_OK)))
        executable_entry.configure(text=str(os.access(filename, os.X_OK)))


def modify_properties():
    """
    This function retrieves the updated properties from the GUI, formats
    them and makes the appropriate changes.
    """
    # Retrieves the formatted creation and modification times separating
    # out the nanoseconds
    created, nano_created = creation_time_entry.get().replace(' ', '-').\
        replace(':', '-').split('.')
    modified, nano_modified = last_modified_time_entry.get().\
        replace(' ', '-').replace(':', '-').split('.')

    # Creates a list of the retrieved values as integers
    created_values = [int(n) for n in created.split("-")]
    modified_values = [int(n) for n in modified.split("-")]

    # Unpacks the lists to send them into datetime to retrieve the timestamp
    # adding the nanoseconds back to the time
    created_time = float(f'{int(datetime(*created_values).timestamp())}.'
                         f'{nano_created}')
    modified_time = float(f'{int(datetime(*modified_values).timestamp())}.'
                          f'{nano_modified}')

    # Updates the modification and creation times
    os.utime(explorer_selection_label['text'], (created_time, modified_time))


if __name__ == '__main__':
    # If the program was called by executing itself

    # Create a new window
    window = Tk()
    # Sets the window title
    window.title('File Properties Modifier')

    # Create the widgets area
    # Creates the button to allow for file selection with the command of the
    # specified function
    explorer_selection = Button(window, text="Select File", command=get_file)
    explorer_selection_label = Label(window, text='', height=1, fg='blue')

    # Creates the label and the Entry box for the retrieve/modified creation
    # time
    creation_time_label = Label(window, text='Creation Time:')
    creation_time_entry = Entry(window, width=50)

    # Creates the label and the Entry box for the retrieve/modified modified
    # time
    last_modified_time_label = Label(window, text='Last Modified Time:')
    last_modified_time_entry = Entry(window, width=50)

    # Creates the labels for the file size area
    file_size_label = Label(window, text='File Size:')
    file_size_entry = Label(window)

    # Creates the labels for the file readable status
    readable_label = Label(window, text='File Readable?')
    readable_entry = Label(window)

    # Creates the labels for the file writable status
    writable_label = Label(window, text='File Writable?')
    writable_entry = Label(window)

    # Creates the labels for the file executable status
    executable_label = Label(window, text='File Executable?')
    executable_entry = Label(window)

    # Creates the button to allow for the file properties to be modified
    modification_button = Button(window, text='Modify File Properties',
                                 width=50, command=modify_properties)

    # Creates the button to exit the program
    exit_button = Button(window, text="Exit", command=exit)

    # Display the widgets area
    # Displays the file explorer area
    explorer_selection.grid(row=0, column=0, sticky='w')
    explorer_selection_label.grid(row=0, column=1)

    # Displays the file creation time area
    creation_time_label.grid(row=1, column=0, sticky='w')
    creation_time_entry.grid(row=1, column=1)

    # Displays the file last modified time area
    last_modified_time_label.grid(row=2, column=0, sticky='w')
    last_modified_time_entry.grid(row=2, column=1)

    # Displays the file size area
    file_size_label.grid(row=3, column=0, sticky='w')
    file_size_entry.grid(row=3, column=1)

    # Displays the file readability status area
    readable_label.grid(row=4, column=0, sticky='w')
    readable_entry.grid(row=4, column=1)

    # Displays the file writability status area
    writable_label.grid(row=5, column=0, sticky='w')
    writable_entry.grid(row=5, column=1)

    # Displays the file executability status area
    executable_label.grid(row=6, column=0, sticky='w')
    executable_entry.grid(row=6, column=1)

    # Displays the modification button
    modification_button.grid(row=7, column=1)

    # Displays the exit button
    exit_button.grid(column=1, row=8)

    # Display the application
    window.mainloop()

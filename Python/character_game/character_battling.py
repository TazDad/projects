#! /usr/bin/env python3.11


"""
This program performs a battle between two characters.
"""


import time
import click
from character_generator import create_character, roll


def clear_screen() -> None:
    """
    This function clears the screen.
    """
    # Clears the screen
    click.clear()


def battle(characters: dict) -> None:
    """
    This function performs a battle between two characters.
    """
    # Sets to indicate no rounds have occurred
    rounds = 0
    # Creates an infinite loop
    while True:
        # Clears the screen
        clear_screen()
        # Informs the user what is happening
        print('Battling...')
        # Gets the strength of each attack
        strength = [roll(6), roll(6)]
        # If the strength's are the same
        if strength[0] == strength[1]:
            # No damage is taken
            continue
        # Retrieves the index of the winner, stronger attack
        winner = strength.index(max(strength))
        # Sets the user index
        loser = (winner + 1) % 2
        # Sets the amount of hit points (offset between hit points)
        hit_points = strength[winner] - strength[loser] + 1
        # Subtracts the hit points from the loser's health
        characters[loser]['health'] -= hit_points
        # Prints the outcome of the battle
        print(f'{characters[winner]["name"]} hit {characters[loser]["name"]} '
              f'for {hit_points} hit points.')
        # Increments the number of rounds
        rounds += 1
        # If the loser has no more health
        if characters[loser]['health'] <= 0:
            # Informs the user who won
            print(f"{characters[winner]['name']} won in {rounds} rounds!")
            # Exits the loop
            break
        # Sleeps for two seconds
        time.sleep(2)


def main() -> None:
    """
    This function executes the functions to create a character.
    """
    # Creates an empty dictionary
    characters = {}

    # Steps through each character
    for index in range(2):
        # Creates the individual character's information
        characters[index] = create_character()

    # Battles between the characters
    battle(characters)


# If the program is not imported
if __name__ == '__main__':
    # Executes the program
    main()

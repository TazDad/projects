#! /usr/bin/env python3.11


"""
This program creates a character for use in a game
"""


# Imports the required libraries
import random


def roll(sides: int) -> int:
    """
    This function rolls the n sided die.
    """
    # Returns a random integer between 1 and the number passed
    return random.randint(1, sides)


def get_value(die1: int, die2: int, add: int) -> int:
    """
    This function returns an integer value based on the roll of two die and
    adding the passed value.
    """
    # Returns the calculated value
    return int(roll(die1) * roll(die2) / 2) + add


def get_character_type() -> str:
    """
    This function determines the character type
    """
    # Sets the character types available
    character_types = {'h': 'Human', 'i': 'Imp', 'w': 'Wizard', 'e': 'Elf',
                       'p': 'Paladin', 'o': 'Orc'}
    # Sets the values to display to the user
    display = [f'({c[0]}){c[1:]}' for c in character_types.values()]
    # Sets the default character type
    character_type = 'a'
    # While the character type is not in the dictionary
    while character_type not in character_types.keys():
        # Gets the character type from the user
        character_type = input('What character type do you want? '
                               f'{", ".join(display)}: ')[0].casefold()
    # Returns the selected character type
    return character_types[character_type]


def get_character_name() -> str:
    """
    This function retrieves the character's name from the user.
    """
    # Retrieves and returns the name of the character from the user
    return input('Name Your Legend: ')


def create_character() -> dict:
    """
    This function executes the functions to create a character
    """
    character = {}

    # Sets the character's name
    character['name'] = get_character_name()

    # Sets the character's type
    character['type'] = get_character_type()

    # Sets the character's health
    character['health'] = get_value(6, 12, 10)

    # Sets the character's strength
    character['strength'] = get_value(6, 12, 12)

    # Returns the character set
    return character


def main() -> None:
    """
    This function executes the program to create a character
    """
    # Prints what the program is to the user
    print('Character Builder')

    # Creates a character
    character = create_character()

    # Steps through each value
    for value in character:
        # Prints the value
        print(f'{value.title()}: {character[value]}')


# If the program is not imported
if __name__ == '__main__':
    main()

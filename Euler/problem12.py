#! /usr/bin/env python3.11


"""
This program solves the Project Euler problem #12: Highly Divisible Triangular
Number.
https://projecteuler.net/problem=12
"""


from euler_lib import function_set as efs


def main(start: int = 1, stop: int = 1000000, greater: int = 500) -> None:
    """
    This function is the main one to determine the triangular number, if any,
    that matches the passed in values
    """
    # Retrieves the triangle number value that matches
    triangle_number = efs.get_first_matching(start, stop, greater)
    # If the result is not None
    if triangle_number:
        # Prints the result
        print(f'The first triangle number within the range {start} - {stop} '
              f'that has {greater} or more divisors is {triangle_number}.')
    # No triangle number was found
    else:
        # Prints the non-result
        print('No triangle number found with divisors greater than or equal '
              f'to {greater} within the range of {start} - {stop}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

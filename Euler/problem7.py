#! /usr/bin/env python3.11


"""
This program determines the nth prime number.
https://projecteuler.net/problem=7
"""


from euler_lib import function_set as efs


def main(stop: int) -> None:
    """
    The main function to determine the nth prime number.
    """
    # Sets the prime number to be the first one
    prime_number = 2
    # Sets the count of prime numbers found
    count = 1
    # While the count of prime numbers found is less than the sequence item
    # trying to find
    while count < stop:
        # Increments to get to the next number in the sequence
        prime_number += 1
        # If the number is prime
        if efs.is_prime(prime_number):
            # Increments the number of the found prime number
            count += 1

    # Informs the user
    print(f'The prime number that is {stop} in the sequence is '
          f'{prime_number}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(10001)

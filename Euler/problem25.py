#! /usr/bin/env python3.11


"""
This program determines the index of the fibonacci sequence where the
fibonacci value is greater than a length.
https://projecteuler.net/problem=25
"""


from euler_lib import function_set as efs


def main(length: int = 1_000) -> None:
    """
    This is the main function to determine the index of the fibonacci sequence
    where the fibonacci value is greater than the passed length.
    """
    # Retrieves the index of the fibonacci sequence whose length is greater
    # than or equal to the chosen length
    index = efs.fibonacci_length(length)
    # Informs the user of the first index within the fibonacci whose length
    # is greater than or equal to a length.
    print('The index value of the fibonacci sequence whose length is greater '
          f'than or equal to {length:,} is {index}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

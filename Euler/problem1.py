#! /usr/bin/env python3.11


"""
This program determines which numbers below 1000 are divisible by 3 or 5
and calculates the sum of these numbers.
https://projecteuler.net/problem=1
"""


from euler_lib import function_set as efs


def main(multiples: list[int], start: int, stop: int,
         conjunction: str) -> None:
    """
    This function performs the actions necessary to sum the numbers in a range
    that are divisible by any value within a list.
    """
    # If it is an empty list or not of type list or any of the elements in the
    # list are not of type int
    if any([len(multiples) == 0, not isinstance(multiples, list),
            not all((isinstance(n, int) for n in multiples))]):
        # Informs the user
        print('Unable to calculate. The list is empty or the value is not '
              'a list of integers.')
    # It is a list of integers
    else:
        # Retrieves a list of integers that are divisible by any of the
        # integers in the list
        numbers = efs.multiple_numbers(multiples, start, stop)
        # Retrieves the total of all the numbers in the list
        total = efs.sum_numbers(numbers)
        # Informs the user
        print(f'The sum of the numbers between {start} and up to {stop} that '
              f'are divisible by '
              f'{efs.make_string_list(multiples, conjunction)} is {total}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main([3, 5], 1, 1000, "or")

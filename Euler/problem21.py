#! /usr/bin/env python3.11


"""
This program calculates the sum of the amicable numbers within a range and
returns the result to the user.
https://projecteuler.net/problem=21
"""


from euler_lib import function_set as efs


def main(start: int = 1, end: int = 10000) -> None:
    """
    This is the main function to calculate the sum of the amicable numbers
    in a range and return the results.
    """
    # Informs the user of the sum of the amicable numbers in the range
    print(f'The sum of the amicable numbers between {start} and {end} is '
          f'{efs.sum_numbers(efs.amicable_numbers(start, end))}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

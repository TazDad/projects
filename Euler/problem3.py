#! /usr/bin/env python3.11


"""
This program determines the largest prime factor of a given number.
https://projecteuler.net/problem=3
"""


from euler_lib import function_set as efs


def main(number: int = 600851475143) -> None:
    """
    This function is the main one to determine the largest prime factor of
    a number.
    """
    # Retrieves the divisors of the number, excluding 1 and itself
    divisors = efs.get_divisors(number)[1:-1]
    # Sets to indicate that a prime factor has not been found
    prime_factor = 0
    # If there were factors found other than 1 and itself
    if len(divisors) > 0:
        # Steps through each factor in reverse order
        for num in reversed(divisors):
            # If the number is prime
            if efs.is_prime(num):
                # Sets the prime factor
                prime_factor = num
                # Breaks from the loop
                break
    # If a prime factor was found
    if prime_factor:
        # Informs the user
        print(f'The largest prime factor of the number {number} is '
              f'{prime_factor}.')
    # If a prime factor was not found
    else:
        # Informs the user
        print(f'There are no prime factors in the number {number}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

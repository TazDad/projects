#! /usr/bin/env python3.11


"""
This program calculates the largest palindrome between two numbers of equal
length.
https://projecteuler.net/problem=4
"""


from euler_lib import function_set as efs


def main(start: int) -> None:
    """
    This is the main function to calculate the largest palindrome from the
    product of two numbers of equal or length.
    """
    # Calculates the largest palendrome between two numbers within a range
    palindrome = efs.largest_palindrome_from_range(start)
    # If the palindrome recovered is not empty
    if palindrome:
        # Informs the user
        print('The largest palindrome found from the product of two numbers '
              f'of equal length starting from the value {start} is '
              f'{palindrome}.')
    # The palindrome is empty
    else:
        # Informs the user
        print('There was no palindrome found within the range starting from '
              f'{start}')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(999)

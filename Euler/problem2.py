#! /usr/bin/env python3.11


"""
This program sums the fibonacci numbers that are less than a specified number
and divisible by a divisor evenly.
https://projecteuler.net/problem=2
"""


from euler_lib import function_set as efs


def main(divisor: int = 2, max_fib: int = 4_000_000):
    """
    This is the main function for the program to calculate the sum of the
    fibnacci numbers less than the maximum fibonacci number that are divisible
    by the specified divisor.
    """
    # Calculates the total
    total = efs.fibonacci_divisors(divisor, max_fib)
    # Informs the user
    print(f'The sum of all fibonacci numbers less than {max_fib:,} where the '
          f'number is evenly divisible by {divisor} is {total}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

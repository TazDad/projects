#! /usr/bin/env python3.11


"""
This program determines the smallest positive number that is evenly divisible
by all of the numbers from the start to the chosen number.
https://projecteuler.net/problem=5
"""


from euler_lib import function_set as efs


def main(start: int, stop: int) -> None:
    """
    The main function to determine the smallest positive number that is
    evenly divisible by all of the numbers from one to the passed number
    """
    # Begins the smallest evenly divisible number
    smallest_evenly_divisible = 1
    # Steps through range of numbers
    for number in range(start, stop + 1):
        # Calculates the least common multiple
        smallest_evenly_divisible = efs.lcm(smallest_evenly_divisible, number)
    # Informs the user
    print('The smallest positive number that is evenly divisible by all the '
          f'numbers from {start} to {stop} is {smallest_evenly_divisible}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(1, 20)

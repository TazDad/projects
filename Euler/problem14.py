#! /usr/bin/env python3.11


"""
This program calculates the total of a group of integers that are stored and
passed in to the main function as a string with the groups of integers
separated by a space. Once the total is calculated, the first n digits of the
total is displayed.
https://projecteuler.net/problem=14
"""


from euler_lib import function_set as efs


def main(start: int = 1, stop: int = 1_000_000) -> None:
    """
    The main function calculates the total of a group of integers stored as a
    string separated by a space. It then provides the first n number of the
    total value to the user.
    """
    # Initializes a list to store the maximum sequence
    max_sequence = []

    # Steps through the specified range
    for number in range(start, stop):
        # Calculates the Collatz Sequence for the specified number
        sequence = efs.collatz_sequence(number)
        # If the new sequence is longer than the current max sequence
        if len(sequence) > len(max_sequence):
            # Copies the current sequence to become the maximum sequence
            max_sequence = sequence[:]

    print(f'The largest Collatz Sequence in the specified range {start} to '
          f'{stop} is {len(max_sequence)} with a value of:')
    print(f'\t{" -> ".join([str(n) for n in max_sequence])}')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

#! /usr/bin/env python3.11


"""
This program determines the maximum product produced by a range of integers
within a large integer for a specific number of integers located within it.
https://projecteuler.net/problem=8
"""


from euler_lib import function_set as efs


def main(items: int, int_string: str) -> None:
    """
    The main function to determine the maximum product that can be produced
    from sequential numbers within a large integer in a provided range.
    """
    # Converts the passed in value to a string, just in case it was passed
    # in as an integer
    int_string = str(int_string)
    # The integer, converted to a list of single digit integers
    int_list = [int(n) for n in int_string]
    # Sets the starting index value
    index = 0
    # Used to store the range that made up the max value
    range_numbers = ''
    # Sets the beginning max value
    max_value = 1
    # While the index value is less than the total number minus those used
    # in the calculation
    while index < len(int_list) - items:
        # Sets the product of the range of numbers
        product = efs.multiply_numbers(int_list[index:index + items])
        # Sets the maximum value between the two numbers
        value = efs.max_number(max_value, product)
        # If the saved maximum value is not equal to the newly calculated
        # maximum value
        if max_value != value:
            # Resets the new maximum value
            max_value = value
            # Sets the range that made up the maximum value
            range_numbers = ' x '.join(int_string[index:index + items])
        # Increments to the new index value
        index += 1
    # Informs the user
    print(f'The maximum range of numbers for a range of {items} items from '
          f'within the number {int_string} is {max_value} calculated '
          f'with this range sequence {range_numbers}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Sets the integer as a string value
    INT_STRING = ('73167176531330624919225119674426574742355349194934'
                  '96983520312774506326239578318016984801869478851843'
                  '85861560789112949495459501737958331952853208805511'
                  '12540698747158523863050715693290963295227443043557'
                  '66896648950445244523161731856403098711121722383113'
                  '62229893423380308135336276614282806444486645238749'
                  '30358907296290491560440772390713810515859307960866'
                  '70172427121883998797908792274921901699720888093776'
                  '65727333001053367881220235421809751254540594752243'
                  '52584907711670556013604839586446706324415722155397'
                  '53697817977846174064955149290862569321978468622482'
                  '83972241375657056057490261407972968652414535100474'
                  '82166370484403199890008895243450658541227588666881'
                  '16427171479924442928230863465674813919123162824586'
                  '17866458359124566529476545682848912883142607690042'
                  '24219022671055626321111109370544217506941658960408'
                  '07198403850962455444362981230987879927244284909188'
                  '84580156166097919133875499200524063689912560717606'
                  '05886116467109405077541002256983155200055935729725'
                  '71636269561882670428252483600823257530420752963450')
    # Executes the main function
    main(13, INT_STRING)

#! /usr/bin/env python3.11


"""
This program calculates a base value to the power of the provided exponent
and sums the individual digits of the result.
https://projecteuler.net/problem=16
"""


from euler_lib import function_set as efs


def main(base: int = 2, exponent: int = 1000) -> None:
    """
    The main function calculates a base value to the power of the provided
    exponent and sums the individual digits of the result.
    """
    # Calculates the base to the power of the provided exponent
    total = efs.get_number_raised(base, exponent)
    # Displays the result
    print(f'The value of {base}^{exponent} is {total}. The sum of the '
          f'individual digits in the number is '
          f'{efs.sum_numbers([int(n) for n in list(str(total))])}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

#! /usr/bin/env python3.11


"""
This program calculates the sum of the numbers within a range that are not
the sum of an abundant number pair.
https://projecteuler.net/problem=23
"""


from euler_lib import function_set as efs


def main(start: int = 1, end: int = 28123) -> None:
    """
    This is the main function to calculate the sum of the non-abundant numbers
    within a range that are not the sum of abundant numbered pairs.
    """
    # Retrieves the abundant numbers within the range
    numbers = efs.get_number_type_in_range(start, end, 'abundant')
    # Sums each element of the abundant numbers list with each element within
    # the list if the sum is less than or equal to the maximum value
    abundant_sums = efs.sum_le(numbers['abundant'], end)
    # Runs through a range of numbers and keeps any of the numbers that is
    # not a calculated sum value of the abundant numbers
    non_abundant_nums = efs.number_not_in_range(abundant_sums, start, end)

    # Informs the user of the numbers that are not the sum of any abundant
    # number pair
    print(efs.sum_numbers(non_abundant_nums))


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

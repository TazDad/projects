#! /usr/bin/env python3.11


"""
This program determines the maximum product produced by a range of integers
within a large integer for a specific number of integers located within it that
touch in any direction.
https://projecteuler.net/problem=11
"""


from euler_lib import function_set as efs


def column_test(start: int, col: int, items: int, data: list[int]):
    """
    This function calculates the positions in the column.
    """
    # Creates an empty list
    positions = []
    # Steps through the possible position locations
    for value in range(start, start+items):
        # Adds the value at the position to the list
        positions.append(data[value][col])
    # Returns the list
    return positions


def diagonal_test_right(start: int, col: int, items: int, data: list[int]):
    """
    This function calculates the positions diagonally down forward.
    """
    # Creates an empty list
    positions = []
    # Steps through the possible position locations
    for index in range(0, items):
        # Adds the value at the position to the list
        positions.append(data[start+index][col+index])
    # Returns the list
    return positions


def diagonal_test_left(start: int, col: int, items: int, data: list[int]):
    """
    This function calculates the positions diagonally down backward.
    """
    # Creates an empty list
    positions = []
    # Steps through the possible position locations
    for index in range(0, items):
        # Adds the value at the position to the list
        positions.append(data[start+index][col-index])
    # Returns the list
    return positions


def get_max_data(values: list[int], max_value: int, data_string: str):
    """
    This function returns the maximum value calculated and the data string
    that produced the value.
    """
    # Calculates the value for the product of the values
    total_value = efs.multiply_numbers(values)
    # Determines the maximum value bewteen the two values
    max_value = efs.max_number(max_value, total_value)
    # If the two values are not the same
    if total_value != max_value:
        # Sets the maximum value to the higher value
        max_value = total_value
        # Sets the data string
        data_string = ' x '.join([str(v) for v in values])
    # Returns the maximum value and the data string
    return max_value, data_string


def main(items: int, per_row: int, int_string: str) -> None:
    """
    The main function to determine the maximum product that can be produced
    from numbers that touch within a large integer in a provided range.
    """
    # Creates a two dimensional array from the input string
    int_list = [[int(n) for n in int_string.split(' ')][i:i+per_row]
                for i in range(0, len([int(n) for n in int_string.split(' ')]),
                               per_row)]
    # Sets a default maximum value
    max_value = 0
    # Creates an empty data string
    range_numbers = ''
    # Calculates the number of rows in the list
    num_rows = len(int_list)
    # Steps through the rows
    for irow, row in enumerate(int_list):
        # Steps through the columns
        for inum in range(per_row):
            # If the column is less than or equal to the number of rows minus
            # the number of items
            if inum <= (per_row - items):
                # Sets the appropriate values
                values = row[inum:inum+items]
                # Calculates the maximum value and the data string for the
                # values
                max_value, range_numbers = get_max_data(values, max_value,
                                                        range_numbers)
            # If the column is less than or equal to the number of rows and
            # the row is less than or equal to the number of items per row
            # minus the number of items
            if inum <= (per_row - items) and irow <= (num_rows - items):
                # Sets the appropriate values
                values = diagonal_test_right(irow, inum, items, int_list)
                # Calculates the maximum value and the data string for the
                # values
                max_value, range_numbers = get_max_data(values, max_value,
                                                        range_numbers)
            # If the column is greater than or equal to the number of rows and
            # the row is less than or equal to the number of items per row
            # minus the number of items
            if inum >= (per_row - items) and irow <= (num_rows - items):
                # Sets the appropriate values
                values = diagonal_test_left(irow, inum, items, int_list)
                # Calculates the maximum value and the data string for the
                # values
                max_value, range_numbers = get_max_data(values, max_value,
                                                        range_numbers)
            # If the row is less than or equal to the number of items per
            # row minus the number of items
            if irow <= (num_rows - items):
                # Sets the appropriate values
                values = column_test(irow, inum, items, int_list)
                # Calculates the maximum value and the data string for the
                # values
                max_value, range_numbers = get_max_data(values, max_value,
                                                        range_numbers)

    # Informs the user
    print(f'The maximum range of numbers for a range of {items} items from '
          f'within the number {int_string} is {max_value} calculated '
          f'with this range sequence {range_numbers}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Sets the integer as a string value
    INT_STR = ('08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 '
               '49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 '
               '81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 '
               '52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 '
               '22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 '
               '24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 '
               '32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 '
               '67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 '
               '24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 '
               '21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 '
               '78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 '
               '16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 '
               '86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 '
               '19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 '
               '04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 '
               '88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 '
               '04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 '
               '20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 '
               '20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 '
               '01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48')
    # Executes the main function
    main(4, 20, INT_STR)

#! /usr/bin/env python3.11


"""
This program creates a dictionary of the word value of numbers from 1 to
10,000 and returns one to the range requested. It then sums the number of
letters (minus the word "and", spaces, and the hyphen (-)) to provide the
total number of characters of the words within the specified range.
https://projecteuler.net/problem=17
"""


import sys
from euler_lib import function_set as efs


def numbers_to_words(max_key: int) -> dict:
    """
    This function creates a dictionary that has numbers as keys and the word
    version of that number as the value.
    """
    # Creates the unique ones, teens, and tens area
    base_values = {'ones': {1: 'one',
                            2: 'two',
                            3: 'three',
                            4: 'four',
                            5: 'five',
                            6: 'six',
                            7: 'seven',
                            8: 'eight',
                            9: 'nine'},
                   'teens': {11: 'eleven',
                             12: 'twelve',
                             13: 'thirteen',
                             14: 'fourteen',
                             15: 'fifteen',
                             16: 'sixteen',
                             17: 'seventeen',
                             18: 'eighteen',
                             19: 'nineteen'},
                   'tens': {10: 'ten',
                            20: 'twenty',
                            30: 'thirty',
                            40: 'forty',
                            50: 'fifty',
                            60: 'sixty',
                            70: 'seventy',
                            80: 'eighty',
                            90: 'ninety'}}

    # Creates the hundreds place holder
    places = {100: 'hundred'}

    # Sets the base number area
    base_numbers = {**base_values['ones'], **base_values['teens']}

    # Sets the rest of the values under 100
    # Steps through the values in the tens dictionary
    for ten, phrase1 in base_values['tens'].items():
        # Sets the value for the ten
        base_numbers[ten] = phrase1
        # If the value is 10
        if ten == 10:
            # Steps through the values in the teens dictionary
            for teen, phrase2 in base_values['teens'].items():
                # Sets the value for the teens
                base_numbers[teen] = phrase2
        # All other tens values
        else:
            # Steps through the values in the ones dictionary
            for one, phrase3 in base_values['ones'].items():
                # Sets the value for the ones
                base_numbers[ten+one] = phrase1 + '-' + phrase3

    # Sets the hundreds base dictionary
    hundreds = {**base_numbers}

    # Sets the rest of the values under 1000
    # Steps through the values in the places dictionary
    for place, phrase1 in places.items():
        # Sets the value for the hundreds
        hundreds[place] = phrase1
        # Steps through the ones range
        for index in range(1, 10):
            # Sets the hundreds
            hundreds[place*index] = base_values['ones'][index] + ' ' + phrase1
            # Steps through the numbers in the base numbers dictionary
            for number, phrase2 in base_numbers.items():
                # Sets the other values in the hundreds
                hundreds[place*index+number] = base_values['ones'][index] +\
                                               ' ' + phrase1 + ' and ' +\
                                               phrase2

    # Sets the thousands base dictionary
    thousands = {**hundreds}
    # Creates the thousands place holder
    places = {1000: 'thousand'}

    # Sets the rest of the values under 10000
    # Steps through the value in the places dictionary
    for place, phrase1 in places.items():
        # Sets the value for the thousands
        thousands[place] = phrase1
        # Steps through the ones range
        for index in range(1, 10):
            # Sets the thousands
            thousands[place*index] = base_values['ones'][index] + ' ' + phrase1
            # Steps through the numbers in the base hundreds dictionary
            for number, phrase2 in hundreds.items():
                # Sets the other values in the thousands
                thousands[place*index+number] = base_values['ones'][index] +\
                                                ' ' + phrase1 + ' ' + phrase2

    # Returns the dictionary key/value pairs that are less than the value
    # passed in
    return {key: value for key, value in thousands.items() if key < max_key}


def main(number: int = 1000) -> None:
    """
    The main function sums the total number of letters used in the word values
    of a number minus spaces, hyphens (-), and the word and within the
    specified range.
    """
    # If the number passed in is greater than or equal to the maximum set
    # within the numbers_to_words function
    if number >= 10000:
        # Informs the user that the number is outside the scope
        print(f'{number} is outside the scope of this program, exiting...')
        # Exits the program
        sys.exit()

    # Gets the dictionary of the set of numbers
    numbers = numbers_to_words(number)
    # Prints the total number of charaters found
    print(efs.sum_numbers([len(numbers[num].replace('and', '')
                           .replace('-', '').replace(' ', ''))
                           for num in range(1, number)]))


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

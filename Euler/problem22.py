#! /usr/bin/env python3.11


"""
This program reads a file that contains names separated by a comma and enclosed
within double quotes to calculate a name score - the sum of each letter of the
name and its position the alphabet multiplied by its position within the
list of sorted names - for each name. All the name scores calculated are summed
and the total returned.
https://projecteuler.net/problem=22
"""


from euler_lib import function_set as efs


def read_data_file(file: str, separator: str) -> list:
    """
    This function reads a data file, removes the double quotes surrounding
    the names, separates the names into a list, and returns the names in a
    sorted list.
    """
    return sorted(open(file, 'r', encoding='UTF-8').read().
                  replace('"', '').split(separator))


def main(file: str = 'euler_names_problem_22.txt',
         separator: str = ',') -> None:
    """
    This is the main function that reads a file that contains names separated
    by a comma and enclosed within double quotes to calculate a name score -
    the sum of each letter of the name and its position the alphabet multiplied
    by its position within the list of sorted names - for each name. All the
    name scores calculated are summed and the total returned.
    """
    # Retrieves a list of all the names found in the file
    first_names = read_data_file(file, separator)
    # Calculates the name score for each name in the list
    name_scores = [efs.sum_numbers([efs.position_in_alphabet(c) for c in name])
                   * position for position, name in
                   enumerate(first_names, start=1)]
    # Informs the user
    print(f'The file, {file}, contained {len(first_names)} names.\nThe total '
          'of the name scores calculated for each name is '
          f'{efs.sum_numbers(name_scores):,}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

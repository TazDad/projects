#! /usr/bin/env python3.11


"""
This program steps through numbers in a range, if the number is prime it is
added to the prime numbers found in the sequence before it.
https://projecteuler.net/problem=10
"""


from euler_lib import function_set as efs


def main(start: int = 2, stop: int = 2000000) -> None:
    """
    The main function to add the prime numbers from the start to the end value.
    """
    # Initializes the total value
    total = 0
    # Steps through the numbers in the range
    for number in range(start, stop):
        # If the number is prime
        if efs.is_prime(number):
            # Adds it to the total
            total += number
    # If the total value is not zero
    if total:
        # Informs the user
        print(f'The summation of the prime numbers from {start} - {stop} is '
              f'{total}.')
    # Prime numbers were found and added together
    else:
        # Informs the user
        print('There are no prime number in the provided range {start} - '
              f'{stop}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

#! /usr/bin/env python3.11


"""
This program is to build a package for code reuse within the Euler projects.
"""


import datetime
import string


def sum_numbers(numbers: list[int]) -> int:
    """
    This function returns the sum of the numbers in a list.
    """
    # Sums the numbers in a list and returns the value
    return sum(numbers)


def multiple_numbers(multiples: list[int], start: int, stop: int) -> list[int]:
    """
    This function calculates a list of numbers in a range that are divisible
    by any of the numbers in the range.
    """
    # Returns the list of numbers within the range that is divisible by any
    # of the numbers in the multiples list
    return [n for n in range(start, stop) for m in multiples if n % m == 0]


def make_string_list(multiples: list[int], conjunction: str) -> str:
    """
    This function combines the elements of a list into a string separated by a
    comma (,) and a space, adding a comma before the passed conjunction if
    there are three or more items.
    """
    # Sets the value to return as the first element
    data = f'{multiples[0]}'

    # If this is the only item in the list, return it
    if len(multiples) == 1:
        # Returns the data string
        return data

    # If there are more than two values in the list
    if len(multiples) > 2:
        # Concatenate all the values except for the last one together with
        # a comma and a space adding a final comma for appending the last
        # item later
        data = ', '.join([f'{n}' for n in multiples[:-1]]) + ','

    # Returns the comma separated string separated by the conjunction
    return f'{data} {conjunction} {multiples[-1]}'


def fibonacci(number: int) -> int:
    """
    This function calculates and returns the fibonacci number at the index
    location.
    """
    # If the number is less than or equal to one
    if number <= 1:
        # Return the number
        return number
    # Returns the fibonacci number at the index
    return fibonacci(number - 1) + fibonacci(number - 2)


def fibonacci_divisors(divisor: int, max_fib: int) -> int:
    """
    This function calculates and returns the total of all the fibonacci
    numbers from zero to the end value or the maximum allowed fibonacci number.
    """
    # Sets the total
    total = 0
    # Steps through each index value
    for index in range(max_fib):
        # Retrieves the fibonacci number at the index
        result = fibonacci(index)
        # If the fibonacci number is larger than the maximum fibonacci number
        if result > max_fib:
            # Breaks from the loop
            break
        # If the fibonacci number is evenly divisible by the divisor
        if (result % divisor) == 0:
            # Adds the result to the total
            total += result
    # Returns the total calculated
    return total


def get_divisors(num: int) -> list[int]:
    """
    This function returns the divisors of the number.
    """
    # Sets the divisors to be 1 and the number passed in.
    divisors = [1, num]
    # Sets the index to the next index to add a divisor
    index = 1
    # Steps through the range of possible divisor halves
    for value in range(2, int(num ** (1 / 2)) + 1):
        # Calculates the numerator and any remainder
        (numerator, remainder) = divmod(num, value)
        # If there is no remainder
        if not remainder:
            # Inserts the first part of the divisor
            divisors.insert(index, value)
            # If the value and the numerator is not the same
            if value != numerator:
                # Inserts the second part of the divisor
                divisors.insert(len(divisors) - 1, numerator)
            # Increments to the next index location
            index += 1
    # Returns the list of divisors
    return divisors


def is_prime(num: int) -> bool:
    """
    This function returns True if a number is prime and False if it is not.
    """
    # If the length of the divisors found for the number is two, then there is
    # only 1 and the number itself as a divisor; making it a prime number
    return len(get_divisors(num)) == 2


def get_triangle_number(num: int) -> int:
    """
    This function returns the triangle number at the specified location.
    """
    # Calculates and returns the triangle number at the location
    return (num * (num + 1)) // 2


def get_first_matching(start: int, stop: int, greater: int) -> int:
    """
    This function steps through a range to calculate the triangle number at
    the location.
    """
    # Sets the triangle number to be None
    triangle = 0
    # Steps through the range
    for value in range(start, stop):
        # Gets the triangle number at the position
        triangle = get_triangle_number(value)
        # Retrieves the divisors for the triangle number
        divisors = get_divisors(triangle)
        # If the number of divisors is greater than or equal to the value
        # assigned
        if len(divisors) >= greater:
            # Breaks from the loop
            break
    # Returns the triangle number value calculated
    return triangle


def is_palindrome(value) -> bool:
    """
    This function tests the passed in value to see if it is a palindrome.
    """
    # Tests the value to see if they are the same forwards and backwards
    # and returns True if it is and False if it is not
    return str(value) == str(value)[::-1]


def largest_palindrome_from_range(start: int) -> None:
    """
    This function determines the largest palindrome produced between the
    product of two numbers with the same number of characters.
    """
    # Sets to indicate a palindrome was not found
    palindrome = 0
    # Sets the stop value
    stop = int('1' + '0' * (len(str(start)) - 1))
    # Sets the first number to multiply
    for first_num in range(start, stop, -1):
        # Sets the second number to multiply
        for second_num in range(first_num, stop, -1):
            # If the product of the two numbers is a palindrome
            if is_palindrome(first_num * second_num):
                # Set the palindrome to the product of the two numbers
                palindrome = first_num * second_num
                # Breaks from the loop
                break
        # If a palindrome was calculated
        if palindrome:
            # Breaks from the loop
            break
    # Returns the calculated palindrome
    return palindrome


def gcd(value1: int, value2: int) -> int:
    """
    This function returns the greatest common denominator between two numbers.
    """
    # Returns the greatest common denominator
    return value2 and gcd(value2, value1 % value2) or value1


def lcm(value1: int, value2: int) -> int:
    """
    This function returns the least common multiple between two numbers.
    """
    # Returns the least common multiple
    return int(value1 * value2 / gcd(value1, value2))


def get_number_raised(number: int, exponent: int = 2) -> int:
    """
    This function returns a value that is raised to the passed in power.
    """
    # Returns the number raised to the exponent passed in
    return number ** exponent


def sum_squared(num_list: list[int]) -> int:
    """
    This function sums the squares of a list of numbers and returns the sum
    of their squares.
    """
    # Returns the sum of a range of squared numbers
    return sum_numbers([get_number_raised(num) for num in num_list])


def multiply_numbers(num_list: list[int]) -> int:
    """
    This function determines the produce of a list of numbers.
    """
    # Sets the beginning product
    product = 1
    # Steps through each number in the list
    for number in num_list:
        # Calculates the product
        product *= number
    # Returns the product
    return product


def max_number(num1: int, num2: int) -> int:
    """
    This function returns the largest value between two numbers.
    """
    # If the first number is larger than the second; otherwise, returns the
    # second number
    return num1 if num1 > num2 else num2


def get_number_root(number: int, exponent: int = 2) -> float:
    """
    This function determines the root value of a number to an exponent.
    """
    # Returns the number raised to the exponent passed in
    return number ** (1 / exponent)


def pythagorean_triplet(start_a: int, stop) -> list[int]:
    """
    This function is a generator that calculates yields the next next
    pythagorean triplet from an initial starting point. It will retrieve
    the next one in the sequence each time that the generator is called.
    """
    # Steps through each possible a value in the range
    for a_num in range(start_a, stop + 1):
        # Squares the a value
        a_squared = get_number_raised(a_num)
        # Steps through each possible b value in the range
        for b_num in range(a_num + 1, stop + 1):
            # Squares the b value
            b_squared = get_number_raised(b_num)
            # Takes the square rood of the addition of a and b and performs
            # a divmod to see if the number is a whole number
            c_num, remainder = divmod(get_number_root(a_squared +
                                                      b_squared), 1)
            # If there is no remainder
            if remainder == 0:
                # Sets the sequence to for a, b, and c
                sequence = [a_num, b_num, int(c_num)]
                yield sequence


def collatz_sequence(number: int) -> list:
    """
    This function calculates the Collatz Sequence from the starting number
    provided.
    """
    # Begins the sequence
    sequence = [number]
    # While the number is greater than one
    while number > 1:
        # If the number is even
        if number % 2 == 0:
            # Determine the integer value of the division by 2
            number //= 2
        # The number is odd
        else:
            # Calculates the number multiplied by three adding one
            number = number * 3 + 1
        # Appends the next value in the sequence
        sequence.append(number)
    # Returns the final sequence
    return sequence


def factorial(number: int) -> int:
    """
    This function calculates the factorial of a given number.
    """
    # If the number is less than or equal to one, return 1
    if number <= 1:
        return 1
    # Returns the number multiplied by the result of the one before it
    return number * factorial(number - 1)


def paths_in_grid(grid: int) -> int:
    """
    This function calculates and returns the number of paths through a grid.
    """
    # Calculates and returns the number of paths through the grid size
    return factorial(grid + grid) // (factorial(grid) * factorial(grid))


def is_triangular(number: int) -> bool:
    """
    This function determines if a number triangular in nature.
    """
    # Steps through the possible number range
    for num in range(2, number + 1):
        # Calculates the sum of the range
        value = sum_numbers(range(1, num))
        # If the value is equal to the number passed
        if value == number:
            # It is a triangular number, return True
            return True
        # If the value is greater than the number passed
        if value > number:
            # Break from the loop
            break
    # The number was not a triangle number, return False
    return False


def max_path(data: list[int]) -> list[int]:
    """
    This function traverses a triangular list of integers to determine the
    maximum values through the data set down a binary path.
    """
    # Sets the result to be the first element in the data set
    results = [data[0]]
    # Sets the row length for the second row
    row_length = 2
    # Sets the first index in the second row
    index = 1
    # While the index is less than the amount of items in the data set
    while index < len(data):
        # If the first element is greater than or equal to the second element
        if data[index] >= data[index + 1]:
            # Stores the greater element
            results.append(data[index])
        # The second element is greater than the first
        else:
            # Stores the greater element
            results.append(data[index + 1])
            # Increments the index value
            index += 1
        # Sets to the base location in the next row
        index += row_length
        # Sets the row length for the next row
        row_length += 1
    # Returns the stored results
    return results


def is_leap_year(year: int) -> bool:
    """
    This function returns True if the year is a leap year and False if it
    is not.
    """
    # If the year is evenly divisible by 400 or the year is not evenly
    # divisible by 100 and evenly divisible by 4
    if year % 100 != 0 and year % 4 == 0 or year % 400 == 0:
        # The year is a leap year, return True
        return True
    # The year is not a leap year, return False
    return False


def set_days_in_months(year: int) -> dict:
    """
    This function returns the number of days in each month based upon the
    year passed in
    """
    # Creates a list that contains the number of days in each month
    days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    # If the year is a leap year
    if is_leap_year(year):
        # Increments February by one to account for leap year
        days[1] += 1
    # Return the list of days in each month
    return days


def count_days_in_time(start_date: datetime, day_to_match: int,
                       weekday_to_match: int, end_date: datetime) -> int:
    """
    This function steps through a start date to an end date to return the
    number of times a weekday occurs on a specified day of the month.
    """
    # Sets the number of days found
    num_days = 0
    # Steps through the years in the date range
    for year in range(start_date.year, end_date.year + 1):
        # Steps through days in each month
        for month in set_days_in_months(year):
            # If the current day is equal to the chosen weekday and day
            # of the week
            if start_date.day == day_to_match and\
               start_date.weekday() == weekday_to_match:
                # Increments the number of days found that matched
                num_days += 1
            # Increments by the number of days in the month
            start_date += datetime.timedelta(days=month)
    # Returns the number of days matched
    return num_days


def get_day_name(weekday: int) -> str:
    """
    This function returns the weekday name based upon its integer value.
    """
    # Sets the weekday names
    weekday_names = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
                     'Saturday', 'Sunday']
    # Returns the name of the weekday at the index within the list
    return weekday_names[weekday]


def is_amicable(number: int) -> bool:
    """
    This function checks to see if a number is amicable, the sum of the proper
    divisors - all divisors except for the original number - of a number and
    the sum of all the proper divisors of the resultant sum is equal to the
    original value.
    """
    # Determines the amicable sum of the passed number
    amicable_sum = sum_numbers(get_divisors(number)[0:-1])
    # Returns True if the original value is equal to the sum of the amicable
    # divisors of the amicable sum of the original number
    return number == sum_numbers(get_divisors(amicable_sum)[0:-1])


def amicable_numbers(start: int, end: int, inclusive: bool = False) -> int:
    """
    This function returns a list of amicable numbers within a range.
    """
    # Creates a list of numbers that are amicable within the given range
    return [number for number in range(start, end + int(inclusive))
            if is_amicable(number)]


def position_in_alphabet(character: str) -> int:
    """
    This function returns the position of a character within the alphabet.
    """
    # Returns the position of a character within the alphabet
    return string.ascii_uppercase.index(character.upper()) + 1


def get_number_type(number: int) -> bool:
    """
    This function checks to see if a number is amicable, the sum of the proper
    divisors - all divisors except for the original number - of a number and
    the sum of all the proper divisors of the resultant sum is equal to the
    original value.
    """
    # Determines the amicable sum of the passed number
    perfect_sum = sum_numbers(get_divisors(number)[0:-1])
    if perfect_sum == number:
        data_type = 'perfect'
    elif perfect_sum < number:
        data_type = 'deficient'
    else:
        data_type = 'abundant'

    # Returns True if the original value is equal to the sum of the amicable
    # divisors of the amicable sum of the original number
    return data_type


def get_number_type_in_range(start: int, end: int,
                             number_type: str = 'all') -> dict:
    """
    This function runs through a range of numbers. It stores the number in
    an array within a dictionary sorted by the number type. The number types
    that can be retrieved are:
                    perfect     is a number where the sum of its proper
                                divisors is exactly equal to the number
                    deficient   is a number where the sum of its proper
                                divisors is less than the number
                    abundant    is a number where the sum of its proper
                                divisors is greather than the number
                    all         all three types
    """
    # Sets the types of numbers to return based upon the type passed in
    number_types = [t for t in ['perfect', 'deficient', 'abundant'] if
                    number_type in ['all', 'perfect', 'deficient', 'abundant']]
    # Creates an empty dictionary
    numbers = {}
    # Steps through each number in the range
    for num in range(start, end + 1):
        # Sets the number type
        n_type = get_number_type(num)
        # If the number type retrieved is one that will be returned
        if n_type in number_types:
            # If the number type has not been found yet
            if n_type not in numbers:
                # Creates a list within the dictionary for the number to be
                # stored
                numbers[n_type] = []
            # Adds the number to the list
            numbers[n_type].append(num)
    # Returns the number's dictionary
    return numbers


def sum_le(numbers: list[int], max_value: int) -> set:
    """
    This function sums each element of a list with itself and returns it if
    the value is less than or equal to the maximum value. The data is returned
    as a set to ensure that only one sum value of the same value is returned.
    """
    # Returns a set of the sum of each element within in the list if the sum
    # is less than the maximum value
    return {x + y for x in numbers for y in numbers if x + y <= max_value}


def number_not_in_range(numbers: list[int], start: int,
                        max_value: int) -> list[int]:
    """
    This function returns a list of integers within a range where the number
    is not in the passed list.
    """
    # Returns a list of integers in a range where the integer is not in the
    # passed list
    return [n for n in range(start, max_value + 1) if n not in numbers]


def set_factoradic_lexicographic_permutation(position: int) -> list:
    """
    This function calculates the nth factoradic lexicographic permutation for
    a group of numbers.
    """
    # Sets the starting number to perform division
    divisor = 1
    # Creates an empty list to store the permutation
    values = []
    # Creates an infinite loop
    while True:
        # Retrieves the quotient and remainder of the division
        (quotient, remainder) = divmod(position, divisor)
        # If both the remainder and the quotient are not empty
        if remainder + quotient != 0:
            # Resets the position as the quotient
            position = quotient
            # Inserts the remainder into the permutation list
            values.insert(0, remainder)
        # If the remainder and the quotient are empty
        else:
            # Breaks the loop
            break
        # Increments the divisor
        divisor += 1
    # Returns the factoradic lexicographic permutation
    return values


def retrieve_factoradic_lexicographic_permutation(permutation: list[int],
                                                  data_values: list) -> list:
    """
    This function retrieves the factoradic lexicographic permutation elements
    within a list.
    """
    # Converts the data to a list of strings. This allows for the input to
    # be a variety of input values.
    data_values = [str(c) for c in data_values]
    # Returns the factoradic lexicographic permutation elements from the data
    return ''.join([str(data_values.pop(value)) for value in permutation])


def fibonacci_length(length: int) -> int:
    """
    This function returns the index of the first fibonacci number whose length
    is greater than or equal to the length being checked against.
    """
    # Sets the starting point within the fibonacci sequence
    sequence = [0, 1]
    # Sets the starting index
    index = 0
    # Sets the fibonacci number
    fib_number = sum_numbers(sequence)
    # While the length of the fibonacci number is less than the value passed in
    while len(str(fib_number)) < length:
        # Sets to the next index
        index += 1
        # Sets the next fibonacci in the sequence
        sequence = [sequence[1], fib_number]
        # Sets to the next fibonacci number
        fib_number = sum_numbers(sequence)
    # Returns the index
    return index


def max_repeating_value(number: int) -> int:
    """
    This function determines the largest repeating sequence within a number.
    """
    value = str(number)
    max_length = 1
    found_value = ""
    for index in range(2, len(value) // 2 + 1):
        length = 1
        while value.startswith(value[0: index] * (length + 1)):
            length += 1
        if length > max_length:
            max_length = length
            found_value = value[0: index] * (length + 1)
    return found_value

#! /usr/bin/env python3.11


"""
This program calculates the factorial of a number and then sums the individual
numbers of the result.
https://projecteuler.net/problem=20
"""


from euler_lib import function_set as efs


def main(number: int = 100) -> None:
    """
    This is the main function to calculate the number of days that a day of
    the week occurs on a specific day of the week.
    """
    # Calculates the factorial of the specified number
    result = efs.factorial(number)
    # Calculates the sum of the individual numbers
    total = efs.sum_numbers([int(n) for n in str(result)])
    # Informs the user
    print(f'The the sum of the numbers in 100! = {result} is {total}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

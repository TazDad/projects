#! /usr/bin/env python3.11


"""
This program calculates the factoradic lexicographic permutation location for
a data set.
https://projecteuler.net/problem=24
"""


from euler_lib import function_set as efs


def main(data_values: list, number: int = 1_000_000) -> None:
    """
    This is the main function that calculates the factoradic lexicographic
    permutation location for a data set.
    """
    # Sets the factoradic lexicographic permutation for the data set
    indexes = efs.set_factoradic_lexicographic_permutation(number)
    # Retrieves the value of the factoradic lexicographic permutation for the
    # data set
    value = efs.retrieve_factoradic_lexicographic_permutation(indexes,
                                                              data_values)
    # Informs user of the factoradic lexicographic permutation value
    print('The factoradic lexicographic permutation value for the values '
          f'in {data_values} is {value}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(list(range(0, 10)))

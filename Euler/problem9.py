#! /usr/bin/env python3.11


"""
This program determines the maximum product produced by a range of integers
within a large integer for a specific number of integers located within it.
https://projecteuler.net/problem=9
"""


from euler_lib import function_set as efs


def main(start_a: int, value: int) -> None:
    """
    The main function to determine the maximum product that can be produced
    from sequential numbers within a large integer in a provided range.
    """
    # Begins the pythagorean triplet generator
    triplets = efs.pythagorean_triplet(start_a, value // 2 + 1)
    # Gets the first pythagorean triplet in the sequence
    sequence = list(next(triplets, None))

    # While the pythagorean triplet values do not add up to the passed value
    while sequence is not None and efs.sum_numbers(sequence) != value:
        # Gets the next pythagorean triplet sequence
        sequence = next(triplets, None)
        # If the sequence is not None
        if sequence is not None:
            # Convert the generator value to a list
            sequence = list(sequence)

    # If the sequence is None
    if sequence is None:
        # Informs the user
        print('There is not a pythagorean triplet whose values add up to '
              f'{value}.')
    # The sequence is not empty
    else:
        # Informs the user
        print(f'The pythagorean triplet whose values add up to {value} is '
              f'a = {sequence[0]}, b = {sequence[1]}, c = {sequence[2]}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(1, 1000)

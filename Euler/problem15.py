#! /usr/bin/env python3.11


"""
This program calculates the number of paths trhough a square grid of n size.
https://projecteuler.net/problem=15
"""


from euler_lib import function_set as efs


def main(grid_size: int = 20) -> None:
    """
    The main function calculates the number of paths through a square grid.
    """
    # Informs the user
    print(f'The number of paths through a {grid_size} x {grid_size} grid is '
          f'{efs.paths_in_grid(grid_size)}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

#! /usr/bin/env python3.11


"""
This program calculates the number of times that a day of the week occurs
on a specified day between two date ranges.
https://projecteuler.net/problem=19
"""


import datetime
from euler_lib import function_set as efs


def main(start_date: datetime, day_to_match: int,
         weekday_to_match: int, end_date: datetime) -> int:
    """
    This is the main function to calculate the number of days that a day of
    the week occurs on a specific day of the week.
    """
    # Retrieves the number based on the specified data
    num_sundays = efs.count_days_in_time(start_date, day_to_match,
                                         weekday_to_match, end_date)
    # Informs the user
    print(f'The number of times that {efs.get_day_name(weekday_to_match)} '
          f'occurred on day {start_date.day} of the month between '
          f'{start_date.strftime("%B")} {start_date.day}, {start_date.year} '
          f'and {end_date.strftime("%B")} {end_date.day}, {end_date.year} '
          f'is {num_sundays}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(datetime.date(1900, 1, 1), 1, 6, datetime.date(2000, 12, 31))

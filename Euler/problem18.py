#! /usr/bin/env python3.11


"""
This program creates a dictionary of the word value of numbers from 1 to
10,000 and returns one to the range requested. It then sums the number of
letters (minus the word "and", spaces, and the hyphen (-)) to provide the
total number of characters of the words within the specified range.
https://projecteuler.net/problem=18
"""


from euler_lib import function_set as efs


def main(int_string: str) -> None:
    """
    This main function validates that a string of integers separated by spaces
    is triangular and then traverses through the rows to determine the maximum
    values along a touching path along the pyramid. It then sums the values
    to determine the total of the path.
    """
    # Converts the string of integers separated by a space into a list of
    # integers
    int_list = [int(n) for n in int_string.split(' ')]
    # If the list of integers is triangular
    if efs.is_triangular(len(int_list)):
        # Retrieves the maximum data path in the triangular integer set
        data_path = efs.max_path(int_list)
        print('The maximum data path recovered is: '
              f'{efs.make_string_list(data_path, "and")}. The sum of these '
              f'integers is {efs.sum_numbers(data_path)}.')
    # If the list of integers is not triangular
    else:
        # Informs the user
        print('The integer list is not a triangular number set, exiting...')


# If the program was executed and not imported
if __name__ == '__main__':
    # Sets the string of integers
    INT_STR = ('75 95 64 17 47 82 18 35 87 10 20 04 82 47 65 '
               '19 01 23 75 03 34 88 02 77 73 07 63 67 99 65 '
               '04 28 06 16 70 92 41 41 26 56 83 40 80 70 33 '
               '41 48 72 33 47 32 37 16 94 29 53 71 44 65 25 '
               '43 91 52 97 51 14 70 11 33 28 77 73 17 78 39 '
               '68 17 57 91 71 52 38 17 14 91 43 58 50 27 29 '
               '48 63 66 04 68 89 53 67 30 73 16 69 87 40 31 '
               '04 62 98 27 23 09 70 98 73 93 38 53 60 04 23')

    # Executes the main function
    main(INT_STR)

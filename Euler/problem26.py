#! /usr/bin/env python3.11


"""
This program determines the index of the fibonacci sequence where the
fibonacci value is greater than a length.
https://projecteuler.net/problem=26
"""


from euler_lib import function_set as efs

def get_long_division_cycle_length(denominator: int) -> int:
    numerator = 10
    digits = []
    remainders = []
    cur = numerator
    while True:
        digit = cur // denominator
        cur = cur % denominator
        if cur in remainders:
            break
        digits.append(digit)
        remainders.append(cur)
        cur *= 10
        if cur == 0:
            return 0
        while cur < denominator:
            cur *= 10
            digits.append(0)
    while remainders[0] != cur:
        remainders.pop(0)
    return len(remainders)

def solution_long_division() -> int:
    cycle_lengths = {denominator: get_long_division_cycle_length(denominator) for denominator in range(983, 984)}
    return sorted(cycle_lengths.items(), key=lambda item: item[1])[-1][0]


def main(start: int = 983, end: int = 984) -> None:
    """
    This is the main function to determine the index of the fibonacci sequence
    where the fibonacci value is greater than the passed length.
    """
    ranges = []
    max_value = ""
    value_number = 1
    for value in range(start, end):
        number = f'{1 / value:.70f}'.split('.')[1]
        print(len(str(int((10000000000000000000000000000000 ** 10) / value))), value)
        print(number, 'number')
        found_value = efs.max_repeating_value(int(number))
        if len(found_value) > len(max_value):
            value_number = value
            max_value = found_value
    print(value_number, max_value)
    print(solution_long_division())


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main()

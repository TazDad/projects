#! /usr/bin/env python3.11


"""
This program calculates the difference between the sum of the squares of a
range of numbers and the sum of a range of numbers squared.
https://projecteuler.net/problem=6
"""


from euler_lib import function_set as efs


def main(start: int, stop: int) -> None:
    """
    The main function to calculate the difference between the sum of the
    squares of a range of numbers and the sum of a range of numbers squared.
    """
    # Sets the range of numbers
    number_list = list(range(start, stop + 1))
    # Sets the squared value of the sum of a range of numbers
    summed_square = efs.get_number_raised(efs.sum_numbers(number_list))
    # Calculates the addition of a range of numbers that are squared
    squared_sum = efs.sum_squared(number_list)
    # Informs the user
    print('The difference between the square of a sum of a range of integers '
          'and the square of the sum of a range of integers for the range '
          f'{start} and {stop} is {summed_square - squared_sum}.')


# If the program was executed and not imported
if __name__ == '__main__':
    # Executes the main function
    main(1, 100)

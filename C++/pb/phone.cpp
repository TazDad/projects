#include <iostream>
#include <map>
#include <string>
#include <regex>
#include "phone.h"
#include "common.h"

std::map<char, std::string> phone_types {{'1', "Home"}, {'2', "Cell"},
                                         {'3', "Mobile"}, {'4', "Work"},
                                         {'5', "Office"}, {'6', "Voice"},
                                         {'7', "Fax"}};


void Phone_Data::print_phone_types()
{
    std::cout << "Key\tType" << std::endl << "---\t----" << std::endl;
    for(auto const& [key, value]: phone_types)
    {
        std::cout << key << "\t" << value << std::endl;
    }
    std::cout << std::endl;
}

char Phone_Data::get_phone_type()
{
    char phone_type = '0';

    clear_screen();
    print_phone_types();
    while (phone_types.find(phone_type) == phone_types.end())
    {
        std::cout << "Enter the number for the phone type: ";
        std::cin >> phone_type;
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return(phone_type);
}

std::string Phone_Data::get_phone_number()
{
    const std::regex phone_pattern{"\\(?\\d{3}[-\\) ]+\\d{3}[- ]?\\d{4}"};
    std::string phone_number = "";
    
    while (! std::regex_match(phone_number, phone_pattern))
    {
        clear_screen();
        std::cout << "Enter phone number formatted (123) 456-7890: ";
        getline(std::cin, phone_number);
    }
    return(phone_number);
}

void Phone_Data::get_numbers()
{
    char get_phone = 'y';

    while (get_phone == 'y' || get_phone == 'Y')
    {
        this->add_phone(get_phone_type(), get_phone_number());
        std::cout << "Would you like to add another phone number? ";
        std::cin >> get_phone;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}


// Function to add new phone data.
void Phone_Data::add_phone(char type, std::string number)
{
    // Create the new phone number.
    Phone* new_phone = new Phone(type, number);

    // If this is the first number
    if (head == NULL) {
        head = new_phone;
        last = head;
        return;
    }

    last->next = new_phone;
    last = new_phone;
}

Phone* Phone_Data::get_head()
{
    return head;
}

void Phone_Data::delete_all_numbers()
{
    // Sets the current position to be the first entry
    Phone *delete_position = this->phone_list->head;

    // Sets the previous position to be NULL
    Phone *delete_position = NULL;

    // Step through each phone number.
    while (current_position != NULL)
    {
        current_position = current_position->next;
        delete delete_position;
    }
}

// Function to delete the provided phone number
void Phone_Data::delete_phone(std::string number)
{
    // Sets the current position to be the first entry
    Phone *current_position = head;

    // Sets the previous position to be NULL
    Phone *previous_position = NULL;

    // If there are no phone numbers
    if (head == NULL)
    {
        // Informs the user
        std::cout << "There are no phone numbers." << std::endl;
        // Returns control
        return;
    }

    // Step through each phone number.
    while (current_position != NULL)
    {
        // If the current phone number is equal to the one being deleted
        if (current_position->number.compare(number) == 0)
        {
            // If the current position is the first entry
            if (current_position == head)
            {
                // Sets the first entry to be the next entry
                head = head->next;
                if (head == NULL) {
                    last = NULL;
                } else {
                    last = head->next;
                }
            }
            // The phone number to delete is not the first entry
            else
            {
                // Sets the previous position's next value to the current
                // position's next value
                previous_position->next = current_position->next;
            }
            if (current_position == last) {
                last = previous_position;
            }
            // Delete's the memory for the current phone number
            delete current_position;
            // Returns control
            return;
        }
        // Sets the previous position to the current phone number
        previous_position = current_position;
        // Sets the current position to the next phone number
        current_position = current_position->next;
    }
}

// Function to print the phone numbers.
void Phone_Data::print_numbers()
{
    // Sets the current position to be the first entry
    Phone* current_position = head;

    // Check to see if there are any phone numbers
    if (current_position == NULL)
    {
        // Informs the user
        std::cout << "There are no phone numbers to display." << std::endl;
        // Returns control
        return;
    }

    // Traverse the phone numbers
    while (current_position != NULL)
    {
        // Prints the information for the phone number at the current position
        std::cout << current_position->number << " ";
        std::cout << phone_types[current_position->type] << std::endl;
        // Sets the current position to the next phone number
        current_position = current_position->next;
    }
}

#ifndef PERSON_H_
#define PERSON_H_

#include <map>
#include "phone.h"

// Creates a character map (dictionary) of the possible phone types
// and their associated name

extern std::map<char, std::string> person_commands;
extern std::map<char, std::string> modify_commands;

class Person {
private:
    std::string dob;
    std::string first_name;
    std::string last_name;
    std::string address;
    std::string city;
    std::string state;
    std::string zip;
    int id;
	Phone_Data phone_list;

public:
    Person *next;
    // Function to retrieve values for a person
    void add_person();

    // Function to retrieve an individual's name
    std::string get_name(std::string);

    // Function to retrieve an individual's name
    std::string get_address();

    // Function to retrieve an individual's name
    std::string get_zip(std::string);

    // Function to retrieve an individual's name
    void set_city_state_zip(std::string);

    void print_person();
    void increment_id();
    int get_id();
    void set_id();

    // Constructor
    Person() {
        // Calls the function to retrieve information for an individual
        this->set_id();
    }

    // Constructor
    Person(int passed_id) {
        // Calls the function to retrieve information for an individual
        this->id = passed_id;
        this->add_person();
    }
    std::string get_value(char);

    void delete_all_numbers();
};



// Linked list class to implement a linked list
class Contact {
private:
    Person* head;
    Person* last;
    int num_contacts;
    int id;

public:
    // Default constructor
    Contact()
    {
        head = NULL;
        last = NULL;
        num_contacts = 1;
        id = 1;
    }

    bool process_command(char);
    void menu();
    void print_commands();
    // Function to add a phone number
    void add_contact();

    // Function to print the phone numbers
    Person* display_contacts();

    void display_contact(int);

    // Function to delete the phone number
    void delete_contact(int);

    // Function to set a phone number
    int set_id();
    int get_id();
    void increment_id();
    void print_contacts();
    bool is_id_valid(int);
};

#endif // PERSON_H_

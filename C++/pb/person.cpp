#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <curl/curl.h>
#include "person.h"
#include "phone.h"
#include "common.h"

std::map<char, std::string> person_commands {{'1', "Add Contact"},
                                             {'2', "Modify Contact"},
                                             {'3', "Delete Contact"},
                                             {'4', "Save Contacts"},
                                             {'5', "View Contacts"},
                                             {'6', "Exit (with save)"},
                                             {'7', "Exit (without save)"}};


std::map<char, std::string> modify_commands {{'1', "First Name"},
                                             {'2', "Last Name"},
                                             {'3', "DOB"},
                                             {'4', "Address"},
                                             {'5', "Zip"},
                                             {'6', "Phone"},
                                             {'7', "Return to main menu"}};


static size_t write_callback(void *contents, size_t size, size_t nmemb,
                             void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::string Person::get_value(char value)
{
    std::string return_value;
    switch (value)
    {
    case 'F':
    case 'f':
        return_value = this->first_name;
        break;
    case 'L':
    case 'l':
        return_value = this->last_name;
        break;
    }
    return(return_value);
}

void Person::set_id()
{
    this->id = 1;
}

int Person::get_id()
{
    return(this->id);
}

std::string Person::get_name(std::string prompt)
{
    std::string name;
    
    std::cout << "Enter the individual's " << prompt << " name: ";
    std::cin >> name;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return title_case(name);
}

void Person::add_person()
{
    this->first_name = this->get_name("First");
    this->last_name = this->get_name("Last");
    phone_list.get_numbers();
    this->address = this->get_address();
    this->set_city_state_zip("");
//    this->print_person();
}

std::string Person::get_address()
{
    std::string street_address;
    while (street_address.length() < 5)
    {
        clear_screen();
        std::cout << "Enter the individual's street address: ";
        getline(std::cin, street_address);
    }
    return(title_case(street_address));
}

std::string Person::get_zip(std::string zip_code)
{
    const std::regex zip_pattern{"\\d{5,5}"};

    while (zip_code.length() != 5 || ! std::regex_match(zip_code, zip_pattern))
    {
        clear_screen();
        std::cout << "Enter 5-digit USA zip code: ";
        getline(std::cin, zip_code);
    }
    return(zip_code);
}

void Person::print_person()
{
    clear_screen();
    std::cout << "ID: " << this->id << std::endl;
    std::cout << "Name: " << this->first_name << " ";
    std::cout << this->last_name << std::endl;
    phone_list.print_numbers();
    std::cout << "Address:" << std::endl;
    std::cout << "\t" << this->address << std::endl;
    std::cout << "\t" << this->city << ", " << this->state;
    std::cout << " " << this->zip << std::endl;
}

void Person::delete_all_numbers()
{
    Phone_Data *current_position = this->phone_list.get_head();

    current_position->delete_all_numbers();
/*
    // Sets the current position to be the first entry
    Phone *current_position = this->phone_list.get_head();

    // Sets the previous position to be NULL
    Phone *delete_position = NULL;

    // Step through each phone number.
    while (current_position != NULL)
    {
        current_position = current_position->next;
        delete delete_position;
    }
*/
}


void Person::set_city_state_zip(std::string zip_code)
{
    int start;
    int stop;
    bool process = false;
    std::string trigger = "Data source:";
    std::string start1 = "<td align=center>";
    std::string stop1 = "</font></td><td align=center>";
    CURL *curl;
//    CURLcode res;
    std::string site_data;

    // remove
    this->zip = this->get_zip(zip_code);
    this->city = "Cambridge";
    this->state = "OH";
    return;

    while (! process)
    {
        this->zip = this->get_zip(zip_code);

        std::string search = "is not currently assigned by the US Postal"
                             "Service to any city";


        std::string zip_api = ("https://www.zipinfo.com/cgi-local/zipsrch.exe"
                               "?zip=" + this->zip + "&Go=Go");

        curl = curl_easy_init();
        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, zip_api.c_str());

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &site_data);
//            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);

            if (site_data.find(search) == std::string::npos)
            {
                zip_code = "";
                process = true;
            }
        }
    }

    start = site_data.find(start1,
                           site_data.find(trigger)) + start1.length();
    stop = site_data.find(stop1, start);

    this->city = site_data.substr(start, stop-start);

    start = stop + stop1.length();
    stop = site_data.find(stop1, start);

    this->state = site_data.substr(start, stop-start);
}

int Contact::get_id()
{
    return(id);
}

bool Contact::is_id_valid(int check_id)
{
    // Sets the current position to be the first entry
    Person *current_position = head;

    // Step through each contact.
    while (current_position != NULL)
    {
        if (current_position->get_id() == check_id)
        {
            return true;
        }
        // Sets the current position to the next phone number
        current_position = current_position->next;
    }
    return false;
}

int Contact::set_id()
{
    bool valid_id = false;
    int retrieve_id;
    // If there are no contacts
    if (head == NULL)
    {
        // Informs the user
        std::cout << "There are no contacts." << std::endl;
        // Returns control
        return 0;
    }

    // Sets the current position to be the first entry
    Person *current_position = head;

    // Step through each phone number.
    while (current_position != NULL)
    {
        std::cout << current_position->get_id() << std::endl;
        // Sets the current position to the next phone number
        current_position = current_position->next;
    }
    while (! valid_id)
    {
        std::cout << "Enter the id of a contact to modify: " << std::endl;
        std::cin >> retrieve_id;
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return retrieve_id;
}

void Contact::increment_id()
{
    id += 1;
}

// Function to add new phone data.
void Contact::add_contact()
{
    clear_screen();
    // Create the new phone number.
    Person *new_contact = new Person(get_id());
    // If this is the first number
    if (head == NULL) {
        head = new_contact;
        last = head;
    }
    else
    {
        last->next = new_contact;
        last = new_contact;
    }
    increment_id();
}

// Function to delete the provided phone number
void Contact::delete_contact(int contact_id)
{
    // Sets the current position to be the first entry
    Person *current_position = head;

    // Sets the previous position to be NULL
    Person *previous_position = NULL;

    // If there are no phone numbers
    if (head == NULL)
    {
        // Informs the user
        std::cout << "There are no contacts." << std::endl;
        // Returns control
        return;
    }

    // Step through each contact.
    while (current_position != NULL)
    {
        // If the current phone number is equal to the one being deleted
        if (current_position->get_id() == contact_id)
        {
            // If the current position is the first entry
            if (current_position == head)
            {
                // Sets the first entry to be the next entry
                head = head->next;
                if (head == NULL) {
                    last = NULL;
                } else {
                    last = head->next;
                }
            }
            // The phone number to delete is not the first entry
            else
            {
                // Sets the previous position's next value to the current
                // position's next value
                previous_position->next = current_position->next;
            }
            if (current_position == last) {
                last = previous_position;
            }
            current_position->delete_all_numbers();
            // Delete's the memory for the current phone number
            delete current_position;
            std::cout << "The entry has been deleted." << std::endl;
            hit_enter();
            // Returns control
            return;
        }
        // Sets the previous position to the current phone number
        previous_position = current_position;
        // Sets the current position to the next phone number
        current_position = current_position->next;
    }
}

// Function to print the contacts.
void Contact::print_contacts()
{
    // Sets the current position to be the first entry
    Person* current_position = head;
    char display = 'y';

    // Check to see if there are any phone numbers
    if (current_position == NULL)
    {
        // Informs the user
        std::cout << "There are no contacts to display." << std::endl;
        // Returns control
        return;
    }

    // Traverse the contactss
    while (current_position != NULL && (display == 'y' || display == 'Y'))
    {
        // Prints the information for the phone number at the current position
        current_position->print_person();
        // Sets the current position to the next contact
        current_position = current_position->next;
        if (current_position ==  NULL)
        {
            std::cout << "This was the last contact." << std::endl;
            hit_enter();
        }
        else
        {
            std::cout << "Continue (y/n): ";
            std::cin >> display;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
    }
}

void Contact::print_commands()
{
    std::cout << "Key\tCommand" << std::endl << "---\t-------" << std::endl;
    for(auto const& [key, value]: person_commands)
    {
        std::cout << key << "\t" << value << std::endl;
    }
    std::cout << std::endl;
}

Person* Contact::display_contacts()
{
    // Sets the current position to be the first entry
    Person* current_position = head;
//    char display = 'y';

    // Check to see if there are any phone numbers
    if (current_position == NULL)
    {
        // Informs the user
        std::cout << "There are no contacts to display." << std::endl;
        // Returns control
        return NULL;
    }

    // Traverse the contactss
    while (current_position != NULL)
    {
        std::cout << "ID: " << current_position->get_id() << "\tFirst Name: ";
        std::cout << current_position->get_value('F') << "\tLast Name: ";
        std::cout << current_position->get_value('L') << std::endl;
        // Sets the current position to the next contact
        current_position = current_position->next;
    }
    return head;
}

bool Contact::process_command(char command)
{
//    Person *contact = NULL;
    int contact_id;
    bool exit_condition = true;
    switch (command)
    {
    case '1':
        this->add_contact();
        break;
    case '2':
        display_contacts();
        std::cout << "What is the ID of the contact that would you like to edit? ";
        std::cin >> contact_id;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        break;
    case '3':
        display_contacts();
        std::cout << "What is the ID of the contact to delete? ";
        std::cin >> contact_id;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        delete_contact(contact_id);
        break;
    case '4':
        std::cout << "Choice is 4" << std::endl;
        break;
    case '5':
        std::cout << "Choice is 5" << std::endl;
        print_contacts();
        break;
    case '6':
        std::cout << "Choice is 6" << std::endl;
        exit_condition = false;
        break;
    case '7':
        std::cout << "Choice is 7" << std::endl;
        exit_condition = false;
        break;
    default:
        exit_condition = false;
        break;
    }
    return(exit_condition);
}

void Contact::menu()
{
    bool more_commands = true;
    char command;
    while (more_commands)
    {
        command = '0';
        clear_screen();
        this->print_commands();
        while (person_commands.find(command) == person_commands.end())
        {
            std::cout << "What action would you like to perform? ";
            std::cin >> command;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        more_commands = process_command(command);
    }
}

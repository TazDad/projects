#ifndef PHONE_H_
#define PHONE_H_

#include <map>
#include "common.h"

// Creates a character map (dictionary) of the possible phone types
// and their associated name
extern std::map<char, std::string> phone_types;

// Phone number class to represent a node of the linked list
class Phone
{
//private:

public:
    std::string number;
    char type;
    Phone *next;

    // Default constructor
    Phone()
    {
        next = NULL;
    }

    // Instantiated constructor
    Phone(char type, std::string number)
    {
        this->type = type;
        this->number = number;
        this->next = NULL;
        //this->prev = NULL;
    }
};

// Linked list class to implement a linked list
class Phone_Data {
private:
    Phone* head;
    Phone* last;

public:
    // Default constructor
    Phone_Data()
    {
        head = NULL;
        last = NULL;
    }

    Phone* get_head();

    // Function to add a phone number
    void add_phone(char, std::string);

    // Function to print the phone numbers
    void print_numbers();

    // Function to delete the phone number
    void delete_phone(std::string);

    // Function to set a phone number
    void get_numbers();

    // Function to display the phone type options
    void print_phone_types();

    // Function to get the type of phone
    char get_phone_type();

    // Function to get the formatted phone number
    std::string get_phone_number();

    void delete_all_numbers();
};

#endif // PHONE_H_

#ifndef COMMON_H_
#define COMMON_H_

void clear_screen();

void hit_enter();

std::string title_case(std::string);

#endif // COMMON_H_

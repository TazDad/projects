#include <iostream>
#include <string>
#include <algorithm>

void clear_screen()
{
    std::cout << "\033[2J\033[1;1H";
}

void hit_enter()
{
    std::cout << "Hit enter to continue.";
    fflush(stdin);
//    std::cin.get();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}

std::string title_case(std::string data)
{
    std::transform(data.begin(), data.end(), data.begin(), 
                   [](unsigned char c){ return std::tolower(c); });

    bool space_found = true;
    for (size_t i = 0 ; i < data.length() ; i++)
    {
        if (space_found) {
            data[i] = toupper(data[i]);
            space_found =  false;
        }
        else if (data[i] == ' ')
        {
            space_found = true;
        }
    }
    return(data);
}

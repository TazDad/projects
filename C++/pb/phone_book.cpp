#include "person.h"
#include "common.h"
#include <iostream>
#include <string>

// To compile: g++ -Wall -g -std=c++17 phone_book.cpp -o phone_book -lcurl

int main() {
    Contact contacts;
    contacts.menu();
    return(0);
}

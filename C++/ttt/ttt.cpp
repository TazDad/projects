#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <iterator>
#include <time.h>
#include <cstring>
#include <bits/stdc++.h>
#include "ttt.h"

// Sets to allow the standard included packages to be listed without the .h
using namespace std;

void set_board(string board[][3], string master[][3]) {
    // This function is used to reset the game board
    // Steps through the three rows
    for(int row = 0; row < 3; row++) {
        // Steps through the three columns in each row
        for(int col = 0; col < 3; col++) {
            // Reassigns the value for each element
            board[row][col] = master[row][col];
        }
    }
}

int num_matches(string data, char character) {
    // This function counts the number of times a character appears in a string
    // Sets to indicate that the character has not been found
    int count = 0;
    // Iterates through a string
    for(int i=0; i<(int)data.length(); i++) {
        // If the character matches the test character
        if(data[i] == character) {
            // Increment the counter
            count++;
        }
    }
    // Return the counter/number of times the test character appeared in the
    // string
    return count;
}

void set_stats(string filename, int* stats, player players[2],
               bool write_data) {
    // This function reads and/or writes the file that contains the statistics
    // to retrieve statistics for the players or write the game statistics out
    // to the file
    // Sets the infile stream
    fstream infile;
    // Sets the outfile stream
    fstream outfile;
    // Used when retrieving fields from the statistics file
    string field;
    // Used to indicate that the statistics were found or not
    bool found = false;
    // Used to keep track of the number of fields found in the file
    int count = 0;
    // Sets the two possible name combinations for comparing
    string names[2] = {players[0].name + ":" + players[1].name,
                       players[1].name + ":" + players[0].name};

    // If the statistics are to be written
    if(write_data) {
        // Attempts to open a temporary file for writing
        outfile.open(filename + ".tmp", ios::out); 

        // If the file could not be opened for writing
        if(! outfile.is_open()) {
            // Sets to indicate that data will not be written
            write_data = false;
        }
    }
    
    // Opens the statistics file for reading
    infile.open(filename, ios::in); 

    // If the statistics file could be opened for reading
    if(infile.is_open()) {
        // Creates to allow a line to be read from a file line by line
        string line;
        // Retrieves a line from the file
        while(getline(infile, line)) {
            // If the line retrieved is greater than twelve characters in
            // length and the separator is found four times on the line
            // indicating the line has five fields
            if(line.length() > 12 && num_matches(line, ':') == 4) {
                // If the line begins with the first name combination
                if(line.substr(0, (int)names[0].length()) == names[0]) {
                    // If data is to be written to the file
                    if(write_data) {
                        // Writes the current statistics to the outfile
                        outfile << players[0].name << ":" <<
                                   players[1].name << ":" <<
                                   stats[0] << ":" <<
                                   stats[1] << ":" << stats[2] << endl;
                    // Reads data from statistics file
                    } else {
                        // Sets a stringstream to separate the line based
                        // upon the separator
                        stringstream data(line);  
                        // Sets to indicate no fields have been found on the
                        // line
                        count = 0;
                        // While fields can be retrieved from the line
                        while(getline(data, field, ':')) {
                            // If the names have been passed and the statistics
                            // are being retrieved
                            if(count >= 2) {
                                // Retrieves and sets the statistic found
                                stats[count-2] = stoi(field);
                            }
                            // Increments the fields retrieved
                            count++;
                        }
                    }
                    // Sets to indicate a record was found
                    found = true;
                // If the line begins with the second name combination
                } else if(line.substr(0, (int)names[1].length()) == names[1]) {
                    // If data is to be written to the file
                    if(write_data) {
                        // Writes the current statistics to the outfile
                        outfile << players[1].name << ":" <<
                                   players[0].name << ":" <<
                                   stats[1] << ":" <<
                                   stats[0] << ":" << stats[2] << endl;
                    // Reads data from statistics file
                    } else {
                        // Writes the current statistics to the outfile
                        stringstream data(line);  
                        // Sets to indicate no fields have been found on the
                        // line
                        count = 0;
                        // While fields can be retrieved from the line
                        while(getline(data, field, ':')) {
                            // If the first statistics area has been found
                            if(count == 2) {
                                // Retrieves and sets the statistic found
                                stats[1] = stoi(field);
                            // If the second statistics area has been found
                            } else if(count == 3) {
                                // Retrieves and sets the statistic found
                                stats[0] = stoi(field);
                            // If the third statistics area has been found
                            } else if(count == 4) {
                                // Retrieves and sets the statistic found
                                stats[2] = stoi(field);
                            }
                            // Increments the fields retrieved
                            count++;
                        }
                    }
                    // Sets to indicate a record was found
                    found = true;
                // If a matched statistic was not found and data is to be
                // written
                } else if(write_data) {
                    // Writes the line to the file
                    outfile << line << endl;
                }
            }
        }
        // Closes the infile stream
        infile.close();
    }
    // If the statistics file is to be written
    if(write_data) {
        // If a a game statistics record could not be found
        if(! found) {
            // Writes the current game statistics to the file
            outfile << players[0].name << ":" << players[1].name << ":" <<
                       stats[0] << ":" << stats[1] << ":" << stats[2] << endl;
        }
        // Closes the output file stream
        outfile.close();
        // Renames the temporary output file to the original file name
        rename((filename + ".tmp").c_str(), filename.c_str());
    }
    // If the infile stream is open
    if(infile.is_open()) {
        // Closes the infiel stream
        infile.close();
    }
}

void title_case(string &s, int length) { 
    // This function converts a string to title case
    // Steps through each character in the string
    for (int index = 0; index < length; index++) {
        // If it is the first character of the string
        if(index == 0) {
            // Converts the character to uppercase
            s[index] = (char)toupper(s[index]);
        // Any other character in the string
        } else {
            // converts the character to lowercase
            s[index] = (char)tolower(s[index]);
        }
    }
      
}

int max_number(int num1, int num2) {
    // This function compares two integers and returns the larger of the two
	if(num1 > num2) {
        // Returns the larger number
		return num1;
	}
    // Returns the larger number
	return num2;
}

void print_char(string to_print, int times) {
    // This function prints a string the specified number of times
    // Steps through each time the string is to be printed
    for(int index = 0; index < times; index++) {
        // Prints the string
        cout << to_print;
    }
}

int number_length(int number) {
    // This function determines the number of character (length) of an integer
    // Creates the variable to store the length of the integer
    int length;
    // Sets the length to be a minimum of one and increments while the integer
    // is greater than 0
    for(length = 1; number > 0; length++) {
        // Decreases the integer by a factor of 10
        number /= 10;
    }
    // Returns retrieved length of the integer
    return length;
}

void display_board(string board[][3], player players[2], int stats[3]) {
    // This function displays the statistics and game board
    // Determines the maximum size between the player names
    int size = max_number((int)players[0].name.length(),
                          (int)players[1].name.length()) + 2;
    // Prints the space character the found number of times
    print_char(" ", size);
    // Prints the rest of the header
    cout << " Wins  Losses  Draws" << endl;

    // Prints the statistics information for player 1
    cout << players[0].name;
    print_char(" ", size - (int)players[0].name.length());
    print_char(" ", 4-number_length(stats[0]));
    cout << " " << stats[0] << " ";
    print_char(" ", 6-number_length(stats[1]));
    cout << " " << stats[1] << " ";
    print_char(" ", 5-number_length(stats[2]));
    cout << " " << stats[2] << endl;

    // Prints the statistics information for player 2
    cout << players[1].name;
    print_char(" ", size - (int)players[1].name.length());
    print_char(" ", 4-number_length(stats[1]));
    cout << " " << stats[1] << " ";
    print_char(" ", 6-number_length(stats[0]));
    cout << " " << stats[0] << " ";
    print_char(" ", 5-number_length(stats[2]));
    cout << " " << stats[2] << endl << endl;

    // Steps through the three rows
    for(int row = 0; row < 3; row++) {
        // Prints the space character the found number of times
        print_char(" ", size);
        // Prints part of the game board
        cout << "     |     |" << endl;
        // Steps through the three columns in each row
        for(int col = 0; col < 3; col++) {
            // If the last column
            if(col == 2) {
                // Prints the statistics with a new line character
                cout << "  " << board[row][col] << endl;
            // Any other column
            } else {
                // If the first element of the column
                if(col == 0) {
                    // Prints the space character the found number of times
                    print_char(" ", size);
                }
                // Prints the statistics
                cout << "  " << board[row][col] << "  |";
            }
        }
        // Prints the space character the found number of times
        print_char(" ", size);
        // Prints part of the game board
        cout << "     |     |" << endl;
        // If it is not the last row
        if(row < 2) {
            // Prints the space character the found number of times
            print_char(" ", size);
            // Prints a spacer
            cout << "-----------------";
        }
        // Prints a newline
        cout << endl;
    }
}

string get_name(int player) {
    // This function retrieves and sets a name
    // Location to store the name
    string name;
    // A do-while loop to retrieve a name while its length is less than three
    do {
        // Prompts the user
        cout << "Enter player " << player << "'s name: ";
        // Retrieves the name of the player
        getline(cin, name);
    } while(name.size() < 3);
    // Converts the name to title case
    title_case(name, (int)name.length());
    // Returns the recovered name
    return name;
}

int num_players() {
    // This function retrieves the number of players in the game
    // Location to store the retrieved number of players
    string players;
    // A do-while loop to retrieve the number of players while the response
    // is not a one or a two
    do {
        // Prompts the user
        cout << "Enter the number of players: ";
        // Retrieves the value from the player
        getline(cin, players);
    } while(players != "1" && players != "2");
    // Returns the value as an integer
    return stoi(players);
}

bool get_response(string prompt) {
    // This function retrieves a yes or no response from the user and returns
    // the result of a boolean test
    // Location to store the response
    string response;
    // A do-while loop to retrieve a yes or no response
    do {
        // Prompts the player
        cout << prompt;
        // Retrieves the value from the player
        getline(cin, response);
    } while(response != "y" && response != "n");
    // Returns the result of a boolean test to see if the player responded
    // with a 'y'
    return response == "y";
}

void get_player_pieces(string name, string *pieces) {
    // This function sets the correct order of the game pieces
    string prompt = name + ", would you like to be X's? (y/n): ";
    // If the player answered the prompt with an 'n'
    if(! get_response(prompt)) {
        // Changes the order of the player pieces
        pieces[0] = "O";
        pieces[1] = "X";
    }
}

int get_player_turn(string name) {
    // This function retrieves and sets the player to go first
    // Sets the prompt to display to the player
    string prompt = name + ", would you like to go first? (y/n): ";
    // If the player answered the prompt with a 'y'
    if(get_response(prompt)) {
        // Returns to indicate that player one is to go first
        return 0;
    // The player answered the prompt with a 'n'
    } else {
        // Returns to indicate that player two is to go first
        return 1;
    }
}

void clear_screen() {
    // This function clears the screen
    // Clears the screen
    cout << "\e[1;1H\e[2J";
}

int next_player(int turn) {
    // This function sets play to the next player
    // Sets play to the next player
    return (turn + 1) % 2;
}

bool valid_position(char value, string board[][3], int *move) {
    // This function tests if a position is available to play
    // If the value to test is an integer
    if(isdigit(value)) {
        // Steps through the three rows
        for(int row = 0; row < 3; row++) {
            // Steps through the three columns in each row
            for(int col = 0; col < 3; col++) {
                // If the position has not been played
                if(value == board[row][col][0]) {
                    // Sets the move to play
                    move[0] = row;
                    move[1] = col;
                    // Returns true to indicaet the test was valid
                    return true;
                }
            }
        }
    }
    // Returns false to indicate the test was invalid
    return false;
}

bool get_player_move(string board[][3], string name, int *move) {
    // This function retrieves a position to play from the player
    // Sets the prompt to display to the user
    string prompt = name + ", where would you like to play? ";
    // Location to store the response from the user
    string response;
    // Sets to indicate the position to play is invalid
    bool position = false;
    // A do-while loop to retrieve a position to play from the user
    do {
        // Prompts the player
        cout << prompt;
        // Retrieves the response from the player
        getline(cin, response);
        // Sets the position to play and determines if it is valid
        position = valid_position(response[0], board, move);
    } while(! position);
    // Returns if the position was valid
    return position;
}

bool player_win(string piece, string board[][3]) {
    // This function determines if the piece being tested has won
    // Sets the winning combination
    string combo = piece + piece + piece;
    // If any of the positions are winning combinations
    if(board[0][0] + board[0][1] + board[0][2] == combo ||
       board[0][0] + board[1][0] + board[2][0] == combo ||
       board[0][0] + board[1][1] + board[2][2] == combo ||
       board[0][1] + board[1][1] + board[2][1] == combo ||
       board[0][2] + board[1][1] + board[2][0] == combo ||
       board[0][2] + board[1][2] + board[2][2] == combo ||
       board[1][0] + board[1][1] + board[1][2] == combo ||
       board[2][0] + board[2][1] + board[2][2] == combo) {
        // Returns true
        return true;        
    }
    // Returns false
    return false;
}

bool player_can_win(string piece, string board[][3], int *move) {
    // This function determines if the piece being tested has any location
    // that it can win and returns a boolean value along with the location
    // to play in move
    // Sets the paired combination
    string combo = piece + piece;
    // Sets to indicate the player cannot win
    bool player_can_win = false;

    // If any of the combinations for position "0" allow the player to win
    if((board[0][1] + board[0][2] == combo ||
        board[1][0] + board[2][0] == combo ||
        board[1][1] + board[2][2] == combo) &&
       (board[0][0] == "0")) {
        // Sets the valid move
        move[0] = 0;
        move[1] = 0;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "1" allow the player to win
    } else if((board[0][0] + board[0][2] == combo ||
               board[1][1] + board[2][1] == combo) &&
              (board[0][1] == "1")) {
        // Sets the valid move
        move[0] = 0;
        move[1] = 1;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "2" allow the player to win
    } else if((board[0][0] + board[0][1] == combo ||
               board[1][1] + board[2][0] == combo ||
               board[1][2] + board[2][2] == combo) &&
              (board[0][2] == "2")) {
        // Sets the valid move
        move[0] = 0;
        move[1] = 2;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "3" allow the player to win
    } else if((board[0][0] + board[2][0] == combo ||
               board[1][1] + board[1][2] == combo) &&
              (board[1][0] == "3")) {
        // Sets the valid move
        move[0] = 1;
        move[1] = 0;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "4" allow the player to win
    } else if((board[0][0] + board[2][2] == combo ||
               board[0][1] + board[2][1] == combo ||
               board[0][2] + board[2][0] == combo ||
               board[1][0] + board[1][2] == combo) &&
              (board[1][1] == "4")) {
        // Sets the valid move
        move[0] = 1;
        move[1] = 1;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "5" allow the player to win
    } else if((board[0][2] + board[2][2] == combo ||
               board[1][0] + board[1][1] == combo) &&
              (board[1][2] == "5")) {
        // Sets the valid move
        move[0] = 1;
        move[1] = 2;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "6" allow the player to win
    } else if((board[0][0] + board[1][0] == combo ||
               board[0][2] + board[1][1] == combo ||
               board[2][1] + board[2][2] == combo) &&
              (board[2][0] == "6")) {
        // Sets the valid move
        move[0] = 2;
        move[1] = 0;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "7" allow the player to win
    } else if((board[0][1] + board[1][1] + board[2][1] == combo ||
               board[2][0] + board[2][1] + board[2][2] == combo) &&
              (board[2][1] == "7")) {
        // Sets the valid move
        move[0] = 2;
        move[1] = 1;
        // Set to indicate the piece can win
        player_can_win = true;
    // If any of the combinations for position "8" allow the player to win
    } else if((board[0][0] + board[1][1] == combo ||
               board[0][2] + board[1][2] == combo ||
               board[2][0] + board[2][1] == combo) &&
              (board[2][2] == "8")) {
        // Sets the valid move
        move[0] = 2;
        move[1] = 2;
        // Set to indicate the piece can win
        player_can_win = true;
    }
    // Returns if the player can win
    return player_can_win;
}

void get_computer_move(string board[][3], string pieces[2], int *move) {
    // This function sets the location for the computer to play
    // Sets to indicate that the position is invalid
    bool position = false;
    // If the computer can win
    if(player_can_win(pieces[1], board, move)) {
        // Returns control to end the function
        return;
    }
    // If the player can win
    if(player_can_win(pieces[0], board, move)) {
        // Returns control to end the function
        return;
    }
    // If the middle position is open
    if(board[1][1] == "4") {
        // Sets the move to the middle position
        move[0] = 1;
        move[1] = 1;
        // Returns control to end the function
        return;
    }
    // A do-while loop to retrieve a random location for the computer to play
    do {
        // Converts a random integer location to a char without compiler
        // warnings to retrieve a valid position
        position = valid_position((char)((rand() % 9) + '0'), board, move);
    } while(! position);
}

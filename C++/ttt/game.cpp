#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <iterator>
#include <time.h>
#include <cstring>
#include <typeinfo>
#include <ctime>
#include "ttt.h"

// Sets to allow the standard included packages to be listed without the .h
using namespace std;

int main() {
    // This is the main function to execute the game
    // Seeds the random number sequencer
    srand((unsigned int)time(NULL));
    // Sets the statistics file name
    string filename = "ttt.dat";
    // Location to store the position to play
    int move[2];
    // Sets the game session
    game game_session;
    // Sets the number of players in the game
    game_session.players = num_players();
    // Sets the area to store player information
    player players[2];
    // Retrieves the name of player 1
    players[0].name = get_name(1);
    // Sets the game pieces
    get_player_pieces(players[0].name, game_session.pieces);
    // Sets the player to go first
    game_session.start = get_player_turn(players[0].name);
    // If there is only one player in the game
    if(game_session.players == 1) {
        // Sets player two to be the computer
        players[1].name = "Computer";
    // If there are two players
    } else {
        // Retrieves the name of player two
        players[1].name = get_name(2);
    }
    // Retrieves the statistics from the statistics file
    set_stats(filename, game_session.stats, players, false);
    // While the players want to play the game
    while(game_session.play_game) {
        // Copies the array to reset the board
        set_board(game_session.board, game_session.master_board);
        // Sets the players turn to the starting player
        game_session.turn = game_session.start;
        // Changes the starting player to the next player to allow for it
        // to change at the start of each game
        game_session.start = next_player(game_session.turn);
        // Resets to play the game
        game_session.play_game = true;
        // Resets to indicate that the game is not over
        game_session.game_over = false;
        // Resets the number of plays in the game
        game_session.plays = 0;
        // While the game is not over
        while(! game_session.game_over) {
            // Clears the screen
            clear_screen();
            // Displays the game board
            display_board(game_session.board, players, game_session.stats);
            // If the player is the computer
            if(players[game_session.turn].name == "Computer") {
                // Retrieves the move for the computer
                get_computer_move(game_session.board, game_session.pieces,
                                  move);
            // Any other player
            } else {
                // Retrieve the move for the player
                get_player_move(game_session.board,
                                players[game_session.turn].name, move);
            }
            cout << move[0] << " && " << move[1] << endl;
            // Sets the position to the appropriate piece for the player
            game_session.board[move[0]][move[1]] =
                game_session.pieces[game_session.turn];
            // Tests to see if the game is over because a player won
            game_session.game_over =
                player_win(game_session.pieces[game_session.turn],
                           game_session.board);
            // Increments the number of plays in the game
            game_session.plays++;
            // If the game is over
            if(game_session.game_over) {
                // Sets the message for the end of the game
                game_session.message = players[game_session.turn].name +
                                       " won the game.";
                // Increments the appropriate winning player's statistics
                game_session.stats[game_session.turn]++;
            // If the number of plays meets or exceeds the maximium number of
            // possible plays in a game
            } else if(game_session.plays > 8) {
                // Sets the message for the end of the game
                game_session.message = "The game ended in a tie.";
                // Sets to indicate the game is over
                game_session.game_over = true;
                // Increments the draw value
                game_session.stats[2]++;
            // The game is not over
            } else {
                // Changes play to the next player
                game_session.turn = next_player(game_session.turn);
            }
        }
        // Clears the screen
        clear_screen();
        // Displays the game board
        display_board(game_session.board, players, game_session.stats);
        // Displays the end of game message
        cout << game_session.message << endl;
        // Determines if there will be another game
        game_session.play_game =
            get_response("Would you like to play again? ");
    }
    // Writes the statistics to the statistics file
    set_stats(filename, game_session.stats, players, true);
    // Exits the game
    exit(0);
}

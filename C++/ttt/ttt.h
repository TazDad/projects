#include <string>
#include <iostream>

using namespace std;

struct game {
    string master_board[3][3] =
        {{"0", "1", "2"},
         {"3", "4", "5"},
         {"6", "7", "8"}};
    string message;
    string board[3][3];
    int plays = 0;
    int players = 1;
    int start = 0;
    int turn;
    int stats[3] = {0, 0, 0};
    string pieces[2] = {"X", "O"};
    bool game_over = false;
    bool play_game = true;
};

struct player {
    string name;
};

void set_board(string[][3], string[][3]);

int num_matches(string, char);

void set_stats(string, int*, player[2], bool);

void title_case(string&, int);

int number_length(int);

//void display_board(string[][3], string, string);
void display_board(string[][3], player[2], int[3]);

string get_name(int player);

int num_players();

bool get_response(string);

void get_player_pieces(string, string*);

int get_player_turn(string);

void clear_screen();

int next_player(int);

bool get_player_move(string[][3], string, int*);

bool valid_position(char, string[][3], int*);

bool player_win(string, string[][3]);

void get_computer_move(string[][3], string[2], int*);

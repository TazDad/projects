// Imports the required packages
use std::{
    io,
    fs,
    fs::File,
    io::{BufReader, BufRead},
    path::Path,
    io::Write,
};
use rand::Rng;

fn main() {
    /*
    Main function to execute the program.
    */
    // Clears the screen
    clear_screen();
    // Sets the filename to read records from
    let filename: &str = "ttt.dat";
    // Sets the number of players
    let num_players: u8 = get_num_players();
    // Creates the empty array for the names
    let mut players: [String; 2] = Default::default();
    // Retrieves the name for player 1
    players[0] = get_player_name(1);
    // If the number of players is one
    if num_players == 1 {
        // Sets the player 2 name to the computer
        players[1] = "Computer".to_string();
    // The number of players is anything other than one
    } else {
        // Retrieves the name for player 2
        players[1] = get_player_name(2);
    }
    // Sets the game pieces
    let mut pieces: [String; 2] = ["X".to_string(), "O".to_string()];
    // If the player does not want to be X
    if ! get_player_piece(pieces[0].clone()) {
        // Swap the pieces
        pieces[0] = "O".to_string();
        pieces[1] = "X".to_string();
    }
    // Sets the player's turn
    let mut turn:usize = (! get_player_turn()) as usize;
    // Reads the stats from the file name
    let mut stats:[usize; 3] = set_stats(filename, players.clone());
    // Sets to allow game play
    let mut play_game: bool = true;
    // While the game is to be played
    while play_game {
        // Clears the screen
        clear_screen();
        // Sets the game board
        let mut game_board: [String; 9] = ["0".to_string(), "1".to_string(),
                                           "2".to_string(), "3".to_string(),
                                           "4".to_string(), "5".to_string(),
                                           "6".to_string(), "7".to_string(),
                                           "8".to_string()];
        // Player position to mark
        let mut position: usize;
        // Sets to indicate the game is not over
        let mut game_over: bool = false;
        // Sets the number of turns
        let mut num_turns: usize = 0;
        // While the game is not over
        while ! game_over {
            // Clears the screen
            clear_screen();
            // Displays the game board
            display_board(players.clone(), stats, game_board.clone());
            // If the player is the computer
            if players[turn] == "Computer" {
                // Retrieve the position for the computer to play
                position = computer_play(pieces.clone(), game_board.clone());
            // The player is not the computer
            } else {
                // Retrieve the position to play from the user
                position = player_play(players[turn].clone(),
                                       game_board.clone());
            }
            // Marks the game board with the player's piece
            game_board[position] = pieces[turn].clone();
            // Increments the number of turns in the game
            num_turns += 1;
            // If a player has won
            if check_win(pieces[turn].clone(), game_board.clone()) {
                // Clears the screen
                clear_screen();
                // Increments the proper stats by one
                stats[turn] += 1;
                // Displays the game board
                display_board(players.clone(), stats, game_board.clone());
                // Prints the reason a game ends
                println!("Congratulations, {} won the game.", players[turn]);
                // Sets to indicate the game is over
                game_over = true;
            // If the maximum number of plays have been played
            } else if num_turns > 8 {
                // Increments the proper stats by one
                stats[2] += 1;
                // Clears the screen
                clear_screen();
                // Displays the game board
                display_board(players.clone(), stats, game_board.clone());
                // Prints the reason the game ended
                println!("The game ended in a tie.");
                // Sets to indicate the game is over
                game_over = true;
            // The game is not over or not a draw
            } else {
                // Changes to the next player
                turn = (turn + 1) % 2;
            }
        }
        // Determines if the user wants to play again
        play_game = get_response(
                        format!("Would you like to play again (y/n): "));
        // If the user selected to continue playing
        if play_game {
            // Swap the player's turns
            turn = (turn + 1) % 2;
        }
    }
    // Writes the statistics to the file
    let _ = write_records(filename, players, stats);
}
fn check_win(piece: String, locations:[String; 9]) -> bool {
    /*
    This function checks the possible winning combinations to see if the
    specified player won
    */
    // Sets the three pieces together value
    let combo: String = format!("{}{}{}", piece, piece, piece);
    // If any of the winning combinations was three of the specified character
    if format!("{}{}{}", locations[0], locations[1], locations[2]) == combo ||
       format!("{}{}{}", locations[0], locations[3], locations[6]) == combo ||
       format!("{}{}{}", locations[0], locations[4], locations[8]) == combo ||
       format!("{}{}{}", locations[1], locations[4], locations[7]) == combo ||
       format!("{}{}{}", locations[2], locations[4], locations[6]) == combo ||
       format!("{}{}{}", locations[2], locations[5], locations[8]) == combo ||
       format!("{}{}{}", locations[3], locations[4], locations[5]) == combo ||
       format!("{}{}{}", locations[6], locations[7], locations[8]) == combo {
        // The player won, return true
        return true;
    }
    // The player did not win, return false
    return false;
}
fn player_play(player: String, locations:[String; 9]) -> usize {
    /*
    This function retrieves a position for the player to play
    */
    // Gets a location from the user
    let mut location:usize = get_player_position(player.clone());
    // While the location chosen is a character piece
    while locations[location] == "X" || locations[location] == "O" {
        // Retrieves a location from the user
        location = get_player_position(player.clone());
    }
    // Returns the retrieved location
    return location;
}
fn computer_play(pieces:[String; 2], locations:[String; 9]) -> usize {
    /*
    This function sets the position to play by the computer
    */
    // If the middle space is open
    if locations[4] != "X" && locations[4] != "O" {
        // Return the position of the middle space
        return 4;
    } 
    // Retrieve a location that the computer can win, if any
    let mut location: usize = player_can_win(pieces[1].clone(),
                                             locations.clone());
    // If a valid index was recovered and the index location is not one
    // of the character pieces
    if location < 9 && locations[location] != "X" &&
                       locations[location] != "O" {
        // Return the location index
        return location;
    }
    // Retrieve a location that the player can win, if any
    location = player_can_win(pieces[0].clone(), locations.clone());
    // If a valid index was recovered and the index location is not one
    // of the character pieces
    if location < 9 && locations[location] != "X" &&
                       locations[location] != "O" {
        // Return the location index
        return location;
    }
    // While the location is not valid or the position to mark has already
    // been marked
    while location >= 9 || locations[location] == "X" ||
                           locations[location] == "O" {
        // Set a random index location
        location = rand::thread_rng().gen_range(0..9);
    }
    // Return the location index
    return location;
}
fn player_can_win(piece: String, locations:[String; 9]) -> usize {
    /*
    This function determines the location at which the specified player can
    win, if any
    */
    // Sets the combination for two locations marked together with the same
    // specified piece
    let combo: String = format!("{}{}", piece, piece);
    // Set the default location
    let mut location:usize = 9;
    // If the grouping contains a pair of the specified piece
    if (format!("{}{}", locations[1], locations[2]) == combo ||
        format!("{}{}", locations[3], locations[6]) == combo ||
        format!("{}{}", locations[4], locations[8]) == combo) &&
        locations[0] == "0" {
        // Sets the location that will allow the user to win
        location = 0;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[2]) == combo ||
               format!("{}{}", locations[4], locations[7]) == combo) &&
               locations[1] == "1" {
        // Sets the location that will allow the user to win
        location = 1;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[1]) == combo ||
               format!("{}{}", locations[4], locations[6]) == combo ||
               format!("{}{}", locations[5], locations[8]) == combo) &&
               locations[2] == "2" {
        // Sets the location that will allow the user to win
        location = 2;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[6]) == combo ||
               format!("{}{}", locations[4], locations[5]) == combo) &&
               locations[3] == "3" {
        // Sets the location that will allow the user to win
        location = 3;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[8]) == combo ||
               format!("{}{}", locations[1], locations[7]) == combo ||
               format!("{}{}", locations[2], locations[6]) == combo ||
               format!("{}{}", locations[3], locations[5]) == combo) &&
               locations[4] == "4" {
        // Sets the location that will allow the user to win
        location = 4;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[2], locations[8]) == combo ||
               format!("{}{}", locations[3], locations[4]) == combo) &&
               locations[5] == "5" {
        // Sets the location that will allow the user to win
        location = 5;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[3]) == combo ||
               format!("{}{}", locations[2], locations[4]) == combo ||
               format!("{}{}", locations[7], locations[8]) == combo ) &&
               locations[6] == "6"{
        // Sets the location that will allow the user to win
        location = 6;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[1], locations[4]) == combo ||
               format!("{}{}", locations[6], locations[8]) == combo) &&
               locations[7] == "7" {
        // Sets the location that will allow the user to win
        location = 7;
    // If the grouping contains a pair of the specified piece
    } else if (format!("{}{}", locations[0], locations[4]) == combo ||
               format!("{}{}", locations[2], locations[5]) == combo ||
               format!("{}{}", locations[6], locations[7]) == combo) &&
               locations[8] == "8" {
        // Sets the location that will allow the user to win
        location = 8;
    }
    // Returns the location to play
    return location;
}
fn get_player_position(player: String) -> usize {
    /*
    Retrieves which piece the player wants to be.
    */
    // Creates a string for the buffer
    let mut buffer = String::new();
    // Creates the position to play as invalid
    let mut position: usize = 9;
    // While the buffer is empty, the position is greater than 8 or is not
    // an integer
    while buffer.is_empty() || position > 8 {
        // Clears the buffer
        buffer = "".to_string();
        // Prints the specified message
        print_message(format!("{}, where would you like to play? ", player));
        // Retrieves the data from the command line
        let _ = std::io::stdin().read_line(&mut buffer);
        // Removes the newline from the input
        buffer = remove_newline(buffer);
        // If the input was only one line
        if buffer.len() == 1 {
            // If the character is a number
            if buffer.chars().nth(0).unwrap().is_numeric() {
                // Sets to indicate an integer was provided
                position = buffer.parse::<usize>().unwrap();
            }
        }
    }
    // Returns the position entered by the user
    return buffer.parse::<usize>().unwrap();
}
fn display_board(players:[String; 2], stats:[usize; 3],
                 locations:[String; 9]) {
    /*
    This function displays the game board to the user.
    */
    // Sets a max size of the player names
    let mut max: usize = players[0].len();
    // If the name of player two is longer
    if players[1].len() > max {
        // Sets the max size of player names to player two
        max = players[1].len();
    }
    // Adds a buffer of two characters
    max += 2;
    // Prints the game board
    println!("{:<max$} Wins  Losses  Draws", " ");
    println!("{:<max$} {:^4}  {:^6}  {:^5}", players[0], stats[0], stats[1],
                                             stats[2]);
    println!("{:<max$} {:^4}  {:^6}  {:^5}", players[1], stats[1], stats[0],
                                             stats[2]);
    println!("\n     |     |");
    println!("{:^5}|{:^5}|{:^5}", locations[0], locations[1], locations[2]);
    println!("     |     |");
    println!("-----------------");
    println!("     |     |");
    println!("{:^5}|{:^5}|{:^5}", locations[3], locations[4], locations[5]);
    println!("     |     |");
    println!("-----------------");
    println!("     |     |");
    println!("{:^5}|{:^5}|{:^5}", locations[6], locations[7], locations[8]);
    println!("     |     |\n");
}
fn write_records(filename: &str, players:[String; 2],
                 stats:[usize; 3]) -> io::Result<()> {
    /*
    This function writes the player names and game statistics to a file.
    */
    // Retrieves the lines from the stat file
    let lines = read_stat_file(filename);
    // Sets to indicate that a match for the statistics was not found
    let mut line_written: bool = false;
    // Create a file to write the statistics
    let mut stat_file = File::create(format!("{}.tmp", filename)).
                        expect("Unable to create file.");
    // Steps through each line recovered from the file
    for line in lines {
        // If the line does not contain four colons
        if line.matches(":").count() != 4 {
            // Skip the line
            continue;
        }
        // Splits the line into fields
        let values = line.split(":").collect::<Vec<&str>>();
        // If a statistic was found with the first player and second player
        // in any order
        if (values[0] == players[0] && values[1] == players[1]) ||
           (values[0] == players[1] && values[1] == players[0]) {
            // Writes the game statistics to the file
            stat_file.write(format!("{}:{}:{}:{}:{}", players[0], players[1],
                                    stats[0],
                                    stats[1],
                                    stats[2]).as_bytes()).
                            expect("write failed");
            // Set to indicate that the statistic was written to the file
            line_written = true;
        }
    }
    // If the statistic has not been written to the file, a game between the
    // players was not found and needs to be written
    if ! line_written {
        // Writes the game statistics to the file
        stat_file.write(format!("{}:{}:{}:{}:{}", players[0], players[1],
                                stats[0], stats[1], stats[2]).as_bytes()).
                        expect("write failed");
    }
    // Renames the temporary file to the permanent file
    fs::rename(format!("{}.tmp", filename), filename)?;
    // Returns the IO result
    Ok(())
}
fn read_stat_file(filename: &str) -> Vec<String> {
    /*
    This function reads the statistics file and returns the lines
    */
    // If the file exists
    if Path::new(&filename).exists() {
        // Opens the file for reading
        let file = File::open(filename).expect("no such file");
        // Reads the file
        let buf = BufReader::new(file);
        // Retrieves the data from the file
        buf.lines()
            .map(|l| l.expect("Could not parse line"))
            .collect()
    // The file does not exist
    } else {
        // Returns an empty vector of strings
        return Vec::new();
    }
}
fn set_stats(filename: &str, players:[String; 2]) -> [usize; 3] {
    /*
    This function reads the stats from the file looking for 
    */
    // Retrieves the lines from the stat file
    let lines = read_stat_file(filename);
    // Sets an array to store the statistics
    let mut stats:[usize; 3] = [0; 3];
    // Steps through each line recovered from the file
    for line in lines {
        // If the line does not contain four colons
        if line.matches(":").count() != 4 {
            // Skip the line
            continue;
        }
        // Splits the line into fields
        let values = line.split(":").collect::<Vec<&str>>();
        // If a statistic was found with the first player and second player
        // in any order
        if (values[0] == players[0] && values[1] == players[1]) ||
           (values[0] == players[1] && values[1] == players[0]) {
            // Stores the found statistics as integers
            stats[0] = values[2].parse().unwrap();
            stats[1] = values[3].parse().unwrap();
            stats[2] = values[4].parse().unwrap();
        }
    }
    // Returns the found statistics
    stats
}
fn clear_screen() {
    /*
    This function clears the terminal.
    */
    // Clears the terminal
    print!("\x1B[2J\x1B[1;1H");
}
fn print_message(message: String) {
    /*
    Prints a message to the user.
    */
    // Prints the passed message
    print!("{}", message);
    // Flushes the print to have it display without a newline
    io::stdout().flush().unwrap();    
}
fn get_player_piece(piece: String) -> bool {
    /*
    Retrieves if the player wants to be the specified piece.
    */
    // Gets the response
    return get_response(format!("Would you like to be {}'s (y/n): ", piece));
}
fn get_player_turn() -> bool {
    /*
    Retrieves if the player wants to go first.
    */
    // Gets the response
    return get_response(format!("Would you like to go first (y/n): "));
}
fn get_response(message: String) -> bool {
    /*
    Retrieves which piece the player wants to be.
    */
    // Creates a string for the buffer
    let mut buffer = String::new();
    // Sets to indicate the answer is false
    let mut value:bool = false;
    // While the buffer is empty, the length is not y or n
    while buffer.is_empty() || ! ((buffer.len() == 2) &&
          (buffer.eq("y\n") || buffer.eq("n\n"))) {
        // Clears the buffer
        buffer = "".to_string();
        // Prints the specified message
        print_message(message.clone());
        // Retrieves the data from the command line
        let _ = std::io::stdin().read_line(&mut buffer);
        buffer.make_ascii_lowercase();
    }
    // If the user entered a y
    if buffer == "y\n" {
        // Sets the value to true
        value = true;
    }
    // Returns the value
    return value;
}
fn get_num_players() -> u8 {
    /*
    Retrieves the number of players from the user.
    */
    // Creates a string for the buffer
    let mut buffer = String::new();
    // While the buffer is empty, the length is not 2 (allows for newline) and
    // the value is not a 1 or a 2
    while buffer.is_empty() || ! ((buffer.len() == 2) &&
          (buffer.eq("1\n") || buffer.eq("2\n"))) {
        // Clears the buffer
        buffer = "".to_string();
        // Prints the specified message
        print_message(format!("How many players (1 or 2): "));
        // Retrieves the data from the command line
        let _ = std::io::stdin().read_line(&mut buffer);
    }
    // Sets the number of players passed in
    let players = buffer.trim().parse::<u8>().unwrap();
    // Returns the number of players
    return players;
}
fn get_player_name(player: u8) -> String {
    /*
    Retrieves the name of a player.
    */
    // Defaults a name to be empty
    let mut name:String = String::new();
    // While the name is not empty or the value stored in the name is less
    // than three characters in length
    while name.is_empty() || name.len() < 3 {
        // Dereferences the variable to empty the string
        name = "".to_string();
        // Prints the specified message
        print_message(format!("Enter the name for player {player}: "));
        // Reads the input from the user and stores it in the variable
        std::io::stdin().read_line(&mut name).unwrap();
        // Dereferences the variable to remove the newline character
        name = remove_newline(name.to_string());
    }
    // Capitalizes just the first character in the string and makes all the
    // other characters lowercase
    capitalize_first(&mut name);
    // Returns the retrieved name
    name
}
fn remove_newline(string: String) -> String {
    /*
    Removes the newline character from a string.
    */
    // Creates a variable to modify the data
    let mut data: String = string;
    // Sets the size of the string
    let mut str_len:usize = data.len();
    // If the string ends with a newline
    if data.ends_with("\n") {
        // Subtracts one from the string length
        str_len -= 1;
        // Truncates the string to get rid of the newline
        data.truncate(str_len);
    }
    // Returns the data with a newline removed, if it was there
    return data;
}
fn capitalize_first(string: &mut String) {
    /*
    Capitalizes the first character in a string and makes the rest lowercase.
    */
    // Sets the size of the string
    let str_len:usize = string.len();
    // The next two statements converts the string to where the first
    // character is uppercase and the others are all lowercase
    string[0..1].make_ascii_uppercase();
    string[1..str_len].make_ascii_lowercase();
}

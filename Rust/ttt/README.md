This implements Tic-Tac-Toe in the rust programming language.
In order to play, rust must be installed on your system.

To build the game, type: `cargo build`

The executable is then built in the target/debug directory as ttt.

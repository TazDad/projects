
// Imports the java util package
import java.util.*;  

// Imports input and output classes
import java.io.*;

// Creates a class to execute the game of Tic-Tac-Toe
public class ttt {
    static void display_gameboard(String[] names, int[] records,
                                  char[][] gameboard) {
        // This method displays the Tic-Tac-Toe game board.
        // Prints the formatted statistics
        print_stats(names, records);
        // Steps through each row
        for (int row=0; row<3; row++) {
            // Prints the game board
            System.out.println("     |     |");
            System.out.printf("  %s  |  %s  |  %s%n",
                              gameboard[row][0],
                              gameboard[row][1],
                              gameboard[row][2]);
            System.out.println("     |     |");
            // If the row is less than two
            if (row < 2) {
                // Prints the dividing line
                System.out.println("----------------");
            }
        }
        // Prints a newline character
        System.out.println();
    }
    public static String set_name(int player) {  
        // This method retrieves a name of a player
        // Sets the standard input access
        Scanner sc = new Scanner(System.in);
        // Sets a beginning name
        String name = "";
        // Starts a do while loop
        do {
            // Prints the message to the user
            System.out.print("Enter player " + player + "'s name: ");
            // Retrieves the value entered by the user
            name = sc.nextLine();
          // While the name entered is less than 3 characters long
        } while (name.length() < 3);
        // Sets the name to title case
        name = name.substring(0,1).toUpperCase() +
               name.substring(1).toLowerCase();
        // Returns the retrieved name
        return name;
    }
    public static boolean is_numeric(String num_as_string) {
        // This method tests to see if a value stored as a string is a number
        // If the string is empty
        if ((num_as_string == null) || (num_as_string.length() == 0)) {
            // Returns false
            return false;
        }
        // Starts a try-except block
        try {
            // Convert the string to an integer
            int num = Integer.parseInt(num_as_string);
        // If the string could not be converted to an integer
        } catch (NumberFormatException nfe) {
            // Returns false
            return false;
        }
        // Returns true
        return true;
    }
    public static int set_players() {
        // This method sets the number of players playing the game
        // Sets the standard input access
        Scanner sc = new Scanner(System.in);
        // Sets a beginning number of player to an invalid value
        String players = "0";
        // Start a do-while loop
        do {
            // Print a message to the user
            System.out.print("Enter the number of players (1 or 2): ");
            // Retrieves the input from the user
            players = sc.nextLine();
        // While the user did not enter anything , it was not an integer, and
        // it was not a 1 or 2
        } while(((players.length() == 0) || (players == null)) ||
                ((!is_numeric(players))) ||
                ((Integer.parseInt(players) < 1) ||
                 (Integer.parseInt(players) > 2)));
        // Returns the value entered as an integer
        return Integer.parseInt(players);
    }
    public static boolean get_response(String message) {
        // This method retrieves a response from the user
        // Sets the standard input access
        Scanner sc = new Scanner(System.in);
        // Sets a default invalid response
        String response = "a";
        // Begins a do-while loop
        do {
            // Prints the message to the user
            System.out.print(message);
            // Retrieves the response
            response = sc.nextLine();
            // If nothing was entered
            if ((response == null) || (response.length() == 0)) {
                // Returns to an invalid response
                response = "a";
            // Something was entered by the user
            } else {
                // Converts the response to lower case
                response = response.toLowerCase();
                // Retrieves the first character
                response = response.charAt(0) + "";
            }
        // If the response is not a y or n
        } while((!response.equals("y")) && (!response.equals("n")));
        // If the response retrieved is a y
        if (response.equals("y")) {
            // Returns true
            return true;
        }
        // Returns false
        return false;
    }
    public static int count_chars(String data, String match) {
        // This method counts the number of times a character occurs within
        // a string
        // Initializes the number of times the character occurs to 0
        int count = 0;
        // Steps through the string
        for (int i = 0; i < data.length(); i++) {
            // If the character matches
            if (data.substring(i, i+1) == match) {
                // Increments the counter
                count++;
            }
        }
        // Returns the number of times the character was found
        return count;
    }
    public static int[] read_records(File file, String[] names)
        throws IOException {
        // This method reads the records file
        // Initializes the buffer for reading
		BufferedReader buffer;
        // Initializes the statistics
        int stats[] = {0, 0, 0};
        // Creates a buffer
        buffer = new BufferedReader(new FileReader(file));
        // Reads the line entered by the user
        String line = buffer.readLine();
        // Creates a new string array
        String[] line_values = {"", "", "", "", ""};
        // While the line is not null (end of the file is reached)
        while (line != null) {
            // If the line length is less than 11 and the character is not
            // found five times in the line recovered
            if ((line.length() < 11) && (count_chars(line, ":") != 5))  {
                // Read the next line from the file
                line = buffer.readLine();
                // Continues/skips/reads the next line
                continue;
            }
            // Splits the line based on the colon
            line_values = line.split(":");
            // If the name pair is found in either order on the lilne
            if (((names[0].equals(line_values[0])) &&
                 (names[1].equals(line_values[1]))) ||
                ((names[0].equals(line_values[1])) &&
                 (names[1].equals(line_values[0])))) {
                // Retrieves the statistics
                stats[0] = Integer.parseInt(line_values[2]);
                stats[1] = Integer.parseInt(line_values[3]);
                stats[2] = Integer.parseInt(line_values[4]);
                // Breaks from reading from the file
                break;
            }
            // Read the next line
            line = buffer.readLine();
        }
        // Closes the buffer
        buffer.close();
        // Returns the statistics
        return stats;
    }
    public static int set_player_turn(String name) {
        // This method sets the player's turn
        // If the user entere a y to the message
        if (get_response(name + ", would you like to go first? (y/n): ")) {
            // Returns a 0
            return 0;
        }
        // Returns a 1
        return 1;
    }
    public static char[][] set_gameboard() {
        // This method creates the game board
        // Creates the game board
        char[][] game_board = {{'0', '1', '2'},
                               {'3', '4', '5'},
                               {'6', '7', '8'}};
        // Returns the game board
        return game_board;
    }
    public static int next_player(int turn) {
        // This method sets the player's turn to the next player
        // Returns the next player's turn
        return (turn + 1) % 2;
    }
    public static char[] set_player_pieces(char[] pieces, String name) {
        // This method sets the player pieces
        // Creates a temporary character to swap the pieces
        char swap = 'a';
        // If the user does not want to be the first character
        if (! get_response(name + ", would you like to be " + pieces[0] +
                           "'s? (y/n): ")) {
            // Stores the first character
            swap = pieces[0];
            // Replaces the first character
            pieces[0] = pieces[1];
            // Sets the second character to the saved first character
            pieces[1] = swap;
        }
        // Returns the pieces
        return pieces;
    }
    public static int[][] set_positions() {
        // This method sets the indexes of the positions to play
        // Initializes the indexes of the positions to play
        int[][] positions = {{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2},
                             {2,0}, {2,1}, {2,2}};
        // Returns the positions to play
        return positions;
    }
    public static void clear_screen() {
        // This method clears the terminal screen
        // Clears the terminal
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
    public static void set_ctrl_c_interrupt() {
        // This method serves as a way to catch the CTRL^C and exit quietly
        // Adds a runtime hook as a Thread
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                // Function to run when CTRL^C is hit
                // Clears the screen
                clear_screen();
            }
        });
    }
    public static void write_records(File file, String[] names,
                                     int[] records) throws IOException {
        // This method writes the statistics to the records file
        // Creates an outfile pointer
        File outfile = new File(file + ".tmp");
        // Creates a variable that indicates if a matching record was found
        // Set by default to indicate there is not a matching record
        boolean line_found = false;
        // Sets the line to write
        String new_data_line = names[0] + ":" + names[1] + ":" +
                               String.valueOf(records[0]) + ":" +
                               String.valueOf(records[1]) + ":" +
                               String.valueOf(records[2]);
        // Creates a new string array
        String[] line_values = {"", "", "", "", ""};
        // Creates a buffer for writing data
        BufferedWriter data = new BufferedWriter(new FileWriter(outfile));
        // Creates a buffer for reading data
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        // Reads a line
        String line = buffer.readLine();
        // While the line is not null (end of the file is reached)
        while (line != null) {
            // If the line length is less than 11 and the character is not
            // found five times in the line recovered
            if ((line.length() < 11) && (count_chars(line, ":") != 5))  {
                // Read the next line from the file
                line = buffer.readLine();
                // Continues
                continue;
            }
            // Splits the line based on the colon
            line_values = line.split(":");
            // If the name pair is found in either order on the lilne
            if (((names[0].equals(line_values[0])) &&
                 (names[1].equals(line_values[1]))) ||
                ((names[0].equals(line_values[1])) &&
                 (names[1].equals(line_values[0])))) {
                // Sets the line to the new data line
                line = new_data_line;
                // Sets to indicate that the line was found
                line_found = true;
            }
            // Writes the data line to the file
            data.write(String.format(line + "%n"));
            // Reads the next line
            line = buffer.readLine();
        }
        // If the line was not found
        if (! line_found) {
            // Writes the new line to the file
            data.write(String.format(new_data_line));
        }
        // Closes the buffer
        buffer.close();
        // Closes the data buffer
        data.close();
        // Renames the temporary file back to the original file
        outfile.renameTo(file);
    }
    public static void print_stats(String[] names, int[] records) {
        // This method prints the statistics to the terminal formatted
        // Sets the length of the first name
        int size = names[0].length();
        // If the length of the second name is greater than the first one
        if (names[1].length() > size) {
            // Sets the new length to be the max size between the names
            size = names[1].length();
        }
        // Adds two to provide spacing
        size += 2;
        // Sets the string format area to format the name
        String format = "%-" + size + "s";
        // Prints the formatted statistics
        System.out.printf(format, "");
        System.out.printf("          ");
        System.out.printf("Win\tLoss\tDraw%n");
        System.out.printf("Player 1: ");
        System.out.printf(format, names[0]);
        System.out.printf("%d\t%d\t%d%n", records[0],
                          records[1], records[2]);
        System.out.printf("Player 2: ");
        System.out.printf(format, names[1]);
        System.out.printf("%d\t%d\t%d%n%n", records[1],
                          records[0], records[2]);
    }
    public static int[] get_computer_play(char[][] board, char[] pieces) {
        // This method gets the computer's move
        // Initializes a random number generation
        Random rand = new Random(System.currentTimeMillis());        
        // Sets to see if either play can win with the computer being tested
        // first
        int[][] positions = {player_can_win(board, pieces, 1),
                             player_can_win(board, pieces, 0)};
        // Sets the possible locations
        int[][] possible = {{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2},
                            {2,0}, {2,1}, {2,2}};
        // For each possible position array
        for (int[] position : positions) {
            // If the first element is greater than 0, a valid position was
            // found, and the position is not one of the pieces
            if ((position[0] >= 0) &&
                (board[position[0]][position[1]] != pieces[0]) &&
                (board[position[0]][position[1]] != pieces[1])) {
                // Return the position to play
                return position;
            }
        }
        // Starts with the middle position
        int num = 4;
        // While the position has been played
        while ((board[possible[num][0]][possible[num][1]] == pieces[0]) ||
               (board[possible[num][0]][possible[num][1]] == pieces[1])) {
            // Retrieves a new position
            num = rand.nextInt(9);
        }
        // Returns the position that has not been played
        return possible[num];
    }
    public static boolean check_win(char[][] board, char piece) {
        String location = piece + "" + piece + "" + piece + "";
        String[] winning_combos = {
            board[0][0] + "" + board[0][1] + "" + board[0][2] + "",
            board[0][0] + "" + board[1][0] + "" + board[2][0] + "",
            board[0][0] + "" + board[1][1] + "" + board[2][2] + "",
            board[0][1] + "" + board[1][1] + "" + board[2][1] + "",
            board[0][2] + "" + board[1][2] + "" + board[2][2] + "",
            board[0][2] + "" + board[1][1] + "" + board[2][0] + "",
            board[1][0] + "" + board[1][1] + "" + board[1][2] + "",
            board[2][0] + "" + board[2][1] + "" + board[2][2] + ""};
        for (int i=0; i<8; i++) {
            if (winning_combos[i].equals(location)) {
                return true;
            }
        }
        return false;
    }
    public static int[] player_can_win(char[][] board, char[] pieces,
                                       int turn) {
        // This function determines if the passed player has any locations
        // in which they can win
        // Defaults the position to be invalid
        int[] no_pos = {-1, -1};
        // Sets the value to test against (two of the player's pieces together)
        String location = pieces[turn] + "" + pieces[turn] + "";
        // Sets the values of the possible winning locations on the board
        String[][] positions = {{board[0][1] + "" + board[0][2] + "",
                                 board[1][0] + "" + board[2][0] + "",
                                 board[1][1] + "" + board[2][2] + ""},
                                {board[0][0] + "" + board[0][2] + "",
                                 board[1][1] + "" + board[2][1] + ""},
                                {board[0][0] + "" + board[0][1] + "",
                                 board[1][2] + "" + board[2][2] + "",
                                 board[1][1] + "" + board[2][1] + ""},
                                {board[0][0] + "" + board[2][0] + "",
                                 board[1][1] + "" + board[1][2] + ""},
                                {board[0][0] + "" + board[2][2] + "",
                                 board[0][1] + "" + board[2][1] + "",
                                 board[0][2] + "" + board[2][0] + "",
                                 board[1][0] + "" + board[1][2] + ""},
                                {board[0][2] + "" + board[2][2] + "",
                                 board[1][0] + "" + board[1][1] + ""},
                                {board[0][0] + "" + board[1][0] + "",
                                 board[0][2] + "" + board[1][1] + "",
                                 board[2][1] + "" + board[2][2] + ""},
                                {board[0][1] + "" + board[1][1] + "",
                                 board[2][0] + "" + board[2][2] + ""},
                                {board[0][0] + "" + board[1][1] + "",
                                 board[0][2] + "" + board[1][2] + "",
                                 board[2][0] + "" + board[2][1] + ""}};

        // Sets the index of the possible positions to play
        int[][] winning = {{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2},
                           {2,0}, {2,1}, {2,2}};

        // Steps through each possible position
        for (int i=0; i<9; i++) {
            // Steps through each possible combination to win
            for (String pos : positions[i]) {
                // If the position has two of the player's tokens and
                // the space to win is empty
                if ((pos.equals(location)) &&
                    (board[winning[i][0]][winning[i][1]] != pieces[0]) &&
                    (board[winning[i][0]][winning[i][1]] != pieces[1])) {
                    // Returns the location to be marked
                    return winning[i];
                }
            }
        }
        // Returns the invalid position
        return no_pos;
    }
    public static int[] get_player_pos(char[][] board, char piece) {
        // This method retrieves the position where the player wants to play
        // Sets the default position to the middle of the board
        int[] pos = {1,1};
        // Sets all the possible valid locations
        int[][] possible = {{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2},
                            {2,0}, {2,1}, {2,2}};

        // Sets to be an invalid location to play
        String num = "9";
        // Sets the standard input access
        Scanner sc = new Scanner(System.in);
        // Begins a do-while loop
        do {
            // Prints a message to the user
            System.out.print("Enter the number of the position to play: ");
            // Retrieves input from the user
            num = sc.nextLine();
        // While the user did not enter anything, the value is a number, the
        // value is a number between 0 and 8, and the position does not have a
        // valid token already in it
        } while(((num.length() == 0) || (num == null)) ||
                ((!is_numeric(num))) ||
                ((Integer.parseInt(num) < 0) ||
                 (Integer.parseInt(num) > 8)) ||
                 (board[possible[Integer.parseInt(num)][0]][possible[Integer.parseInt(num)][1]] == piece));
        // Returns the array to the position found
        return possible[Integer.parseInt(num)];
    }
    public static void main(String[] args) {
        // This method is the main function that runs the program
        // Begins a try-except block
        try {
            // Starts the CTRL^C interrupt
            set_ctrl_c_interrupt();
            // Sets the records file
            File data_file = new File("ttt.dat");
            // Sets the number of players in the game
            int num_players = set_players();
            // Sets the names of the players
            String[] player_names = {set_name(1), "Computer"};
            // If there are two playerss
            if (num_players == 2) {
                // Sets the name of the second player
                player_names[num_players-1] = set_name(num_players);
            }
            // Creates an empty statistics array
            int[] stats = {};
            // Sets the first player
            int player_turn = set_player_turn(player_names[0]);
            // Sets to indicate the the player wants to play the game
            boolean play_game = true;
            // Sets to indicate that the game is not over
            boolean game_over = false;
            // Sets to indicate that no moves have been made in the game
            int num_moves = 0;
            // Sets the character tokens to use in the game
            char[] pieces = {'X', 'O'};
            // Sets the order of the tokens based on the first player's input
            pieces = set_player_pieces(pieces, player_names[0]);
            // Sets a placeholder for why a game ends
            String end_type = "";
            // Establishes a placeholder for the gameboard
            char[][] gameboard = {{}};
            // Established a placeholder for the location for the player to
            // place their token
            int[] player_pos = {};
            // Changes the player's turn to the next player. This is done here
            // To allow the change to happen again immediately after as the
            // game begins. Each time a game begins, the player that starts
            // the game changes
            player_turn = next_player(player_turn);
            // While the game is to be played
            while (play_game) {
                // Clears the screen
                clear_screen();
                // Sets the statistics for the players
                stats = read_records(data_file, player_names);
                // Sets to indicate no moves have been made in the game
                num_moves = 0;
                // Creates a clean gameboard
                gameboard = set_gameboard();
                // Sets to indicate that the game is not over
                game_over = false;
                // Changes the player's turn
                player_turn = next_player(player_turn);
                // While the game is not over
                while (! game_over) {
                    // Clears the screen
                    clear_screen();
                    // Displays the game board
                    display_gameboard(player_names, stats, gameboard);
                    // If the player is the computer
                    if (player_names[player_turn].equals("Computer")) {
                        // Retrieves the position for the computer to play
                        player_pos = get_computer_play(gameboard, pieces);
                    // The player is a person
                    } else {
                        // Retrieves the position from the player where they
                        // want to play
                        player_pos = get_player_pos(gameboard,
                                                    pieces[player_turn]);
                    }
                    // Marks the location with the player's token
                    gameboard[player_pos[0]][player_pos[1]] = pieces[player_turn];
                    // If the user won
                    if (check_win(gameboard, pieces[player_turn])) {
                        // Sets the reason why the game ended
                        end_type = player_names[player_turn] + " won.";
                        // Sets to end the game
                        game_over = true;
                        // Increments the players total wins by one, which is
                        // also the losses for the other player
                        stats[player_turn] += 1;
                    }
                    // Increments the number of moves playedd
                    num_moves++;
                    // If the number of moves played is greater than 8, all
                    // positions have been marked and the game is a draw
                    if (num_moves > 8) {
                        // Sets the reason why the game ended
                        end_type = "The game is a draw.";
                        // Sets to end the game
                        game_over = true;
                        // Increments the number of draw for the players
                        stats[2] += 1;
                    }
                    // If the game is not over
                    if (! game_over) {
                        // Changes to the next player's turn
                        player_turn = next_player(player_turn);
                    }
                }
                // Writes the statistics to the record file
                write_records(data_file, player_names, stats);
                // Clears the screen
                clear_screen();
                // Displays the game board
                display_gameboard(player_names, stats, gameboard);
                // Prints the reason that the game ended
                System.out.println(end_type);
                // Retrieves whether or not the players want to play again
                play_game = get_response("Would you like to play again " +
                                         "(y/n)? ");
            }
        // Catches an IOException
        } catch (IOException exception) {
            // Informs the user that an IOException occurred and provides
            // them with the error message
            System.out.println("IOException: " + exception.getMessage());
        }
    }
}


#! /usr/bin/env perl

# This program implements the game of Tic-Tac-Toe.

# Libraries used
use warnings;
use strict;
use English qw<$PROGRAM_NAME>;
use Config;
use Storable qw(dclone); 
use File::Copy;

# Sets the signal handler to catch a ^C to exit smoothly
$SIG{INT} = \&close_gracefully;

sub init_vars {
    # This subroutine is used to set up all the necessary values used in the
    # game

    # Used to seed the rand function
    srand (time ^ ($$ + ($$ << 11)));
    # Calls the subroutine to clear the screen
    clear_screen();
    # Creates a hash to store all the required variables
    my %vars = ("filename"     => "ttt.dat",
                "players"      => get_num_players(),
                "names"        => [get_player_name("What is Player 1's name? "
                                                  ), "Computer"],
                "end_type"     => "",
                "moves"        => 0,
                "turn"         => 0,
                "play_game"    => 1,
                "game_over"    => 0,
                "stats"        => [0, 0, 0],
                "master_board" => [["0", "1", "2"],
                                   ["3", "4", "5"],
                                   ["6", "7", "8"]],
                "game_board"   => [["0", "1", "2"],
                                   ["3", "4", "5"],
                                   ["6", "7", "8"]],
                "pieces"       => ["X", "O"],
                "positions"    => {"0" => [0,0],
                                   "1" => [0,1],
                                   "2" => [0,2],
                                   "3" => [1,0],
                                   "4" => [1,1],
                                   "5" => [1,2],
                                   "6" => [2,0],
                                   "7" => [2,1],
                                   "8" => [2,2]
                                  }
               );

    # Sets the value to determine the player that gets to go first
    $vars{'turn'} = int(! get_response("$vars{'names'}[0], would you like " .
                                       "to go first? (Y/N): "));

    # If the first player stored does not want to be the piece associated with
    # their position
    if (int(! get_response("$vars{'names'}[0], would you like to be " .
                           "$vars{'pieces'}[0]'s: "))) {
        # Creates a temporary piece
        my $piece = $vars{'pieces'}[0];
        # Stores the second piece as the first piece
        $vars{'pieces'}[0] = $vars{'pieces'}[1];
        # Stores the temporary piece as the secone piece
        $vars{'pieces'}[1] = $piece;
    }
    # If there are two players
    if ($vars{'players'} == 2) {
        # Retrieve the second player's name
        $vars{'names'}[1] = get_player_name("What is Player 2's name? ");
    }
    # Retrieves game statistics stored in the specified file
    $vars{'stats'} = read_stats($vars{'filename'}, $vars{'names'});

    # Returns the hash to the calling section of code
    return(%vars);
}

sub close_gracefully {
    # This function is used to process when the player hits ^C
    
    # Clears the screen
    clear_screen();
    # Prompts the player; thanking them for playing
    print("Thank you for playing.\n");
    exit(1);
}

sub clear_screen {
    # This subroutine is used to clear the screen

    # If the operating system is LINUX
    if ($Config{osname}) {
        # Executes the shell command to clear
        system("clear");
    # Other operating systems
    # Note: This was built and set up primarily for LINUX. Executing this on
    # other operating systems could crash the program if the cls command does
    # not exist
    } else {
        # Executes the shell command to clear
        system("cls");
    }
}

sub read_stats {
    # This subroutine reads the statistics file to determine if a record has
    # been stored already for the players. If it has, the saved record set is
    # retrieved, formatted, and returned

    # Sets the separator value used in the file to separate fields
    my $sep = ":";
    # If the file is able to be opened for reading
    if (open(my $fh, '<:encoding(UTF-8)', $_[0])) {
        # Steps through each line in the file
        while (my $row = <$fh>) {
            # Removes the newline character
            chomp($row);
            # If the length of the line is greater than 12
            if (length($row) > 12) {
                # Splits the line and stores the value as an array
                my @names = split(/$sep/, $row);
                # If the name combination is found
                if ("$_[1][0]$_[1][1]" eq "$names[0]$names[1]") {
                    # Sets the retrieved stats
                    my @stats = ($names[2], $names[3], $names[4]);
                    # Returns the set stats
                    return(\@stats);
                # If the name combination is found in reverse
                } elsif ("$_[1][0]$_[1][1]" eq "$names[1]$names[0]") {
                    # Sets the retrieved stats
                    my @stats = ($names[3], $names[2], $names[4]);
                    # Returns the set stats
                    return(\@stats);
                }
            }
        }
    }

    # Sets the stats to return
    my @stats = (0, 0, 0);
    # Returns the set stats
    return(\@stats);
}

sub write_stats {
    # This function writes the records to the records file

    # Sets the separator value used in the file to separate fields
    my $sep = ":";
    # Sets the newline with the current stats stored from the games just played
    my $new_line = join("$sep", @{$_[1]}, @{$_[2]});
    # Opens a temporary version of the statistics file for writing or dies
    open(FH, '>', "$_[0].tmp") or die "unable to write to file: $!";
    # If the file is able to be opened for reading
    if (open(my $fh, '<:encoding(UTF-8)', $_[0])) {
        # Steps through each line in the file
        while (my $row = <$fh>) {
            # Removes the newline character
            chomp($row);
            # If the length of the line is greater than 12
            if (length($row) > 12) {
                # Splits the line and stores the value as an array
                my @names = split(/$sep/, $row);
                # If the name combination is found in either order
                if ("$_[1][0]$_[1][1]" eq "$names[0]$names[1]" or
                    "$_[1][0]$_[1][1]" eq "$names[1]$names[0]") {
                    # Prints the newly created line to the file
                    print(FH "$new_line\n");
                # The two players have never played against one another
                } else {
                    # Writes the original record to the file
                    print(FH "$row\n");
                }
            }
        }
    }
    # Closes the file handle
    close(FH);
    # Moves the temporary file to the original file or dies
    move("$_[0].tmp", "$_[0]") or die "move failed: $!";
}

sub get_num_players {
    # This subroutine retrieves the number of players in the game

    # Sets the response to be an invalid response
    my $response = "a";
    # While the response is not a 1 or 2
    while ($response !~ /^[12]$/) {
        # Prompts the player to enter a value
        printf("Enter the number of players (1 or 2): ");
        # Retrieves the value from the player
        $response = <>;
        # Removes the newline character
        chomp($response);
        # If the player entered at least one character
        if (length($response) > 0) {
            # Retrieves the first character only
            $response = substr($response, 0, 1);
        }
    }
    # Returns the integer value of the value entered
    return(int($response));
}

sub check_win {
    # This subroutine checks the game board to see if the passed in player's
    # token has any combinations that allow them to win

    # If any of the possible winning combinations are filled only with the
    # player's token passed in
    if ("$_[0][0][0]$_[0][0][1]$_[0][0][2]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][1][0]$_[0][1][1]$_[0][1][2]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][2][0]$_[0][2][1]$_[0][2][2]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][0][0]$_[0][1][0]$_[0][2][0]" eq "$_[1]$_[1]$_[1]" or
        "$_[0][0][1]$_[0][1][1]$_[0][2][1]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][0][2]$_[0][1][2]$_[0][2][2]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][0][0]$_[0][1][1]$_[0][2][2]" eq "$_[1]$_[1]$_[1]" or 
        "$_[0][0][2]$_[0][1][1]$_[0][2][0]" eq "$_[1]$_[1]$_[1]"
       ) {
        # Returns 1 to indicate True
        return(1);
    }
    # If the code makes it here, it returns 0 to indicate False
    return(0);
}

sub get_player_name {
    # This subroutine retrieves a player name from the player

    # Sets the name to be empty
    my $name = "";
    # While the length is less than 2
    while (length($name) < 2) {
        # Prompts the player for input
        printf("%s", $_[0]);
        # Retrieves player input
        $name = <>;
        # Removes the newline character
        chomp($name);
    }
    # Returns the name in title case, with only the first character capitalized
    return(ucfirst(lc($name)));
}

sub get_response {
    # This subroutine retrieves a yes or no response from the player and
    # returns a 1 (yes) or 0 (no) depending on the value entered
    
    # Sets the response to be an invalid value by default
    my $response = "a";
    # While the response is not a n or y
    while ($response !~ /^[ny]$/) {
        # Prompts the player for input
        printf("%s", $_[0]);
        # Retrieves the player's response
        $response = <>;
        # Removes the newline character from the player's input
        chomp($response);
        # If the player entered a value
        if (length($response) > 0) {
            # Retrieves just the first character from the player's input and
            # makes it lowercase
            $response = lc(substr($response, 0, 1));
        }
    }
    # If the player entered a y
    if ($response eq "y") {
        # Returns a 1 to indicate the player entered a y
        return(1);
    }
    # Returns a 0 to indicate the player entered a n
    return(0);
}

sub display_stats {
    # This function displays the formatted game statistics

    # Sets the format of the data
    format STDOUT =
@<<<<<<<<<<<< @>>>>> @>>>>>>> @>>>>>>
"Player:",     "Wins:",  "Losses:","Draws:"
-------------------------------------
@<<<<<<<<<<<< @||||| @|||||||  @|||||
$_[0][0],     $_[1][0], $_[1][1], $_[1][2]
@<<<<<<<<<<<< @||||| @|||||||  @|||||
$_[0][1],     $_[1][1], $_[1][0], $_[1][2]

.
    # Writes the formatted game statistics to standard out
    write()
}

sub display_board {
    # This function displays the game board to the player
    
    # Calls the subroutine to display the formatted statistics
    display_stats($_[0], $_[1]);
    # Steps through the first array dimension
    for my $row (0..2) {
        # Prints the value
        print("     |     |\n");
        # Steps through the second array dimension
        for my $col (0..2) {
            # Prints the value
            print("  $_[2][$row][$col]  ");
            # If the column is not the last one of the row
            if ($col != 2) {
                # Prints the value
                print("|");
            # If the column is the last one of the row
            } else {
                # Prints the value
                print("\n");
            }
        }
        # Prints the value
        print("     |     |\n");
        # If the row is not the last one
        if ($row < 2) {
            # Prints the value
            print("-----------------\n");
        # If the row is the last one
        } else {
            # Prints the value
            print("\n");
        }
    }
}

sub player_move {
    # This subroutine retrieves the position from the player where they want to
    # play their piece on the game board

    # Set to indicate the move is invalid
    my $valid_move = 0;
    #  Set to indicate an invalid value for the location
    my $location = -1;
    # While the move is not valid
    while (! $valid_move) {
        # Prints the message to the player
        print("$_[1], enter the location to play: ");
        # Retrieves input from the player
        $location = <>;
        # Removes the newline character
        chomp($location);
        # If the location retrieved is a valid value
        if ($location =~ /^[0-8]$/) {
            # If the position on the game board is the location value, meaning
            # both players have not played in this location yet
            if ($_[0][$_[2]{$location}[0]][$_[2]{$location}[1]] =~
                /^$location$/) {
                # Returns the location retrieved
                return($location);
            }
        }
    }
    # Returns an invalid character
    return(-1);
}

sub computer_move {
    # This function calculates the computer player's move
    
    # Sets the location value that would allow the computer to win, if any
    # such value existed
    my $possible_win = win_possible($_[0], $_[1][1], $_[2]);
    # If the value retrieved is valid
    if (int($possible_win) >= 0) {
        # Returns the retrieved value, to set where the computer will play
        return($possible_win);
    }
    
    # Sets the location value that would allow the player to win, if any
    # such value existed
    $possible_win = win_possible($_[0], $_[1][0], $_[2]);
    # If the value retrieved is valid
    if (int($possible_win) >= 0) {
        # Returns the retrieved value, to set where the computer will play
        return($possible_win);
    }
    # If the center position is still available for play
    if ($_[0][1][1] eq "4") {
        # Returns the retrieved value, to set where the computer will play
        return("4");
    }
    # Performs a do while loop to set a valid random value to play
    do {
        # Sets the random integer value retrieved as a string
        $possible_win = "" . int(rand(9));
        # While the position set has already been played
    } while ($_[0][$_[2]{$possible_win}[0]][$_[2]{$possible_win}[1]] !~
             /^[0-8]$/);
    # Returns the valid position to play
    return($possible_win);
}

sub win_possible {
    # This subroutine checks to see if any place on the board would allow the
    # passed player to win and return that position as a place to play

    # Sets the winning combination hash
    my %winning_combos = ("0" => ["$_[0][0][1]$_[0][0][2]",
                                  "$_[0][1][0]$_[0][2][0]",
                                  "$_[0][1][1]$_[0][2][2]"],
                          "1" => ["$_[0][0][0]$_[0][0][2]",
                                  "$_[0][1][1]$_[0][2][1]"],
                          "2" => ["$_[0][0][0]$_[0][0][1]",
                                  "$_[0][1][2]$_[0][2][2]",
                                  "$_[0][1][1]$_[0][2][0]"],
                          "3" => ["$_[0][0][0]$_[0][2][0]",
                                  "$_[0][1][1]$_[0][1][2]"],
                          "4" => ["$_[0][0][0]$_[0][2][2]",
                                  "$_[0][0][1]$_[0][2][1]",
                                  "$_[0][0][2]$_[0][2][0]",
                                  "$_[0][1][0]$_[0][1][2]"],
                          "5" => ["$_[0][0][2]$_[0][2][2]",
                                  "$_[0][1][0]$_[0][1][1]"],
                          "6" => ["$_[0][0][0]$_[0][1][0]",
                                  "$_[0][1][1]$_[0][0][2]",
                                  "$_[0][2][1]$_[0][2][2]"],
                          "7" => ["$_[0][0][1]$_[0][1][1]",
                                  "$_[0][2][0]$_[0][2][2]"],
                          "6" => ["$_[0][0][0]$_[0][1][1]",
                                  "$_[0][0][2]$_[0][1][2]",
                                  "$_[0][2][0]$_[0][2][1]"]
                         );

    # Steps through each key (position) in the hash
    foreach my $position (keys %winning_combos) {
        # Steps through each winning combination possibility
        for my $combo (@{$winning_combos{$position}}) {
            # If the winning combination matches the passed player's piece and
            # the position is still open for play
            if ($combo eq "$_[1]$_[1]" and
                $_[0][$_[2]{$position}[0]][$_[2]{$position}[1]] =~ /^[0-8]$/) {
                # Returns the position that would allow the passed player to
                # play
                return($position);
            }
        }
    }
    # Returns an invalid character
    return(-1);
}

sub play_game {
    # This function executes the procedures to play the game

    # Sets the game hash
    my %game_vars = init_vars();
    # Sets the move that the player is making to be an invalid character
    my $move = "";
    # While the player chooses to play the game
    while ($game_vars{'play_game'}) {
        # Performs a deep copy of the master game board to the game play game
        # board to allow it to be reset for a new game
        $game_vars{'game_board'} = dclone($game_vars{'master_board'});
        # Sets to indicate that the game is not over
        $game_vars{'game_over'} = 0;
        # Sets to indicate that no moves have been played in the game
        $game_vars{'moves'} = 0;
        # Sets to indicate no reason as to why the game concluded
        $game_vars{'end_type'} = "";
        # While the game is not over
        while (! $game_vars{'game_over'}) {
            # Clears the screen
            clear_screen();
            # Displays the game board
            display_board($game_vars{'names'}, $game_vars{'stats'},
                          $game_vars{'game_board'});
            # If the player is the Computer
            if ($game_vars{'names'}[$game_vars{'turn'}] eq "Computer") {
                # Retrieves the position for the computer to play
                $move = computer_move($game_vars{'game_board'},
                                      $game_vars{'pieces'},
                                      \%{$game_vars{'positions'}});
            # The player is a human
            } else {
                # Retrieves the postion from the player on where they want to
                # play on the game board
                $move = player_move($game_vars{'game_board'},
                                    $game_vars{'names'}[$game_vars{'turn'}],
                                    \%{$game_vars{'positions'}});
            }
            # Sets the player's piece into the position chosen
            $game_vars{'game_board'}[$game_vars{'positions'}{$move}[0]]
                [$game_vars{'positions'}{$move}[1]] =
                $game_vars{'pieces'}[$game_vars{'turn'}];
            # Increments the number of moves played within the game
            $game_vars{'moves'} += 1;
            # If the number of moves in the game is greater than or equal to
            # five, as there can be no winner before this value
            if ($game_vars{'moves'} >= 5) {
                # Checks the gameboard to see if the current player won
                $game_vars{'game_over'} = check_win($game_vars{'game_board'},
                                     $game_vars{'pieces'}[$game_vars{'turn'}]);
                # If the current player won the game
                if ($game_vars{'game_over'}) {
                    # Sets why the game ended to be a win
                    $game_vars{'end_type'} = "win";
                    # Increments the current player's win statistic
                    # This also increments the other player's loss statistic
                    $game_vars{'stats'}[$game_vars{'turn'}] += 1;
                # If the maximum number of moves have been reached in the game
                } elsif ($game_vars{'moves'} == 9) {
                    # Sets to indicate that the game is over
                    $game_vars{'game_over'} = 1;
                    # Sets to indicate that the game ended as a draw
                    $game_vars{'end_type'} = "draw";
                    # Increments the draw statistic
                    $game_vars{'stats'}[2] += 1;
                }
            }
            # If the game is not over
            if (! $game_vars{'game_over'}) {
                # Changes the turn value to be the other player's turn
                $game_vars{'turn'} = int(! $game_vars{'turn'});
            }
        }
        # Clears the screen
        clear_screen();
        # Displays the board
        display_board($game_vars{'names'}, $game_vars{'stats'},
                      $game_vars{'game_board'});
        # If the game ended because of a win
        if ($game_vars{'end_type'} eq "win") {
            # Tell the player who won the game
            print($game_vars{'names'}[$game_vars{'turn'}] .
                  " won the game.\n\n");
        # Otherwise, the game ended as a draw
        } else {
            # Tell the player that the game ended in a draw
            print("The game ended in a draw.\n\n");
        }
        # Retrieves input from the player on whether they want to play again
        $game_vars{'play_game'} = get_response("Would you like to play " .
                                               "again? (y/n): ");
        # If the player opted to play again
        if ($game_vars{'play_game'}) {
            # Changes the turn value to be the other player's turn
            $game_vars{'turn'} = int(! $game_vars{'turn'});
        }
    }
    # Write the game statistics to the file
    write_stats($game_vars{'filename'}, $game_vars{'names'},
                $game_vars{'stats'});
}

# If the program is being executed by calling the program name
if ( $PROGRAM_NAME eq __FILE__ ) {
    # Plays the game
    play_game();
}

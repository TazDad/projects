// Sets to tell tell the go compiler that this is a program that can be
// compiled and executed
package main

// Import required packages
import (
    "fmt"
    "strings"
    "regexp"
    "strconv"
    "os"
    "math/rand"
)

// Create a struct to use for game specific data
type game struct {
    plays       int
    board       [9]string
    game_over   bool
    play_game   bool
}

// Create a struct to use for player data
// Though the wins, losses, and draws are used as the wins/losses and
// losses/wins for each player, this sets it up to allow for the easy
// addition of additional records to keep totals across all the games
// that the user has played with any player
type player struct {
    name    string
    wins    int
    losses  int
    draws   int
    piece   string
}

func get_input(prompt string) string {
    // This function retrieves input from the user
    var value string
    // Prints the passed in prompt to the user
    fmt.Print(prompt)
    // Retrieves input from the user
    fmt.Scanln(&value)
    // Returns the value entered on the line by the user as a string
    return value
}

func get_response(prompt string) bool {
    // This function retrieves the results of a boolean test of whether or not
    // the user entered a yes or no response
    // Begins an infinite loop
    for {
        // Retrieves the input ande makes it lowercase
        response := strings.ToLower(get_input(prompt))
        // Tests to see if the values match
        match, _ := regexp.MatchString("^[ny]", response)
        // If the values match
        if match {
            // If the response was a yes response return true; otherwise false
            return response == "y"
        }
    }
}

func get_name(player int) string {
    // This function retrieves a player's name
    // Sets the prompt value to display
    prompt := fmt.Sprintf("Enter player %d name: ", player)
    // Begins an infinite loop
    for {
        // Retrieves a name from the user
        name := get_input(prompt)
        // If the length of the name entered is greater than or equal to three
        if len(name) >= 3 {
            // Returns the name in title case
            return strings.Title(name)
        }
    }
}

func get_num_players() int {
    // This function retrieves the number of players
    // Sets the prompt value to display
    prompt := "How many players? "
    // Begins an infinite loop
    for {
        // Retrieves the number of players from the user
        players := get_input(prompt)
        // Tests to see if the values match
        match, _ := regexp.MatchString("^[12]", players)
        // If the values match
        if match {
            // Returns the value as an integer
            return string_to_int(players)
        }
    }
}

func next_player(player int) int {
    // This function changes whose turn it is
    // Returns the next player
    return (player + 1) % 2
}

func string_to_int(value string) int {
    // This function converts a string number into an integer
    number, _ := strconv.Atoi(value)
    // Returns the converted integer
    return number
}

func read_stats(file string, player1 string, player2 string) [3]int {
    // This function reads a file looking for the players to retrieve
    // stored statistics, if they exit
    // Creates an empty statistics set
    stats := [3]int {0, 0, 0}
    // Reads the data from the file
    data, err := os.ReadFile(file)
    // If the file was unable to be read
    if err != nil {
        // Returns the statistics
        return stats
    }
    // Separates the lines recovered from the file based upon a newline
    // character
    lines := strings.Split(string(data), "\n")

    // Steps through each line recovered from the file
    for index := 0; index < len(lines); index++ {
        // Tests to see if the record is found as player1:player2
        match, _ := regexp.MatchString(fmt.Sprintf("^%s:%s:", player1,
                                                              player2),
                                       lines[index])
        // If the record matched
        if match {
            // Separates the line to retrieve the records
            line := strings.Split(string(lines[index]), ":")
            // Retrieves the statistics value
            stats[0] = string_to_int(line[2])
            stats[1] = string_to_int(line[3])
            stats[2] = string_to_int(line[4])
            // Returns the statistics
            return stats
        }
        // Tests to see if the record is found as player2:player1
        match, _ = regexp.MatchString(fmt.Sprintf("^%s:%s:", player2, player1),
                                      lines[index])
        // If the record matched
        if match {
            // Separates the line to retrieve the records
            line := strings.Split(string(lines[index]), ":")
            // Retrieves the statistics value
            stats[0] = string_to_int(line[3])
            stats[1] = string_to_int(line[2])
            stats[2] = string_to_int(line[4])
            // Returns the statistics
            return stats
        }
    }
    // Returns the statistics
    return stats
}

func write_stats(file string, player1 string, player2 string, stats [3]int) {
    // This function writes the statistics to the file
    // Reads the data from the file
    data, err := os.ReadFile(file)
    // If the file was unable to be read
    if err != nil {
        // Returns
        return
    }
    // Creates a temporary outfile
    outfile, err := os.Create(file + ".tmp")
    // If the file was unable to be read
    if err != nil {
        // Returns
        return
    }

    // Separates the lines recovered from the file based upon a newline
    // character
    lines := strings.Split(string(data), "\n")

    // Steps through each line recovered from the file
    for index := 0; index < len(lines); index++ {
        // If the length of the line is less than 13 characters, which is
        // the minimum length that a valid record should be or the line
        // does not contain four colons (:), which means it does not have
        // the five required fields
        if len(lines[index]) < 13 || strings.Count(lines[index], ":") != 4 {
            // Continues the loop
            continue
        }
        // Tests to see if the record is found as player1:player2
        match, _ := regexp.MatchString(fmt.Sprintf("^%s:%s:", player1,
                                                              player2),
                                       lines[index])
        // If the record matched
        if match {
            // Replaces the retrieved data line with new data from this game
            lines[index] = fmt.Sprintf("%s:%s:%d:%d:%d", player1, player2,
                                                         stats[0], stats[1],
                                                         stats[2])
        }
        // Tests to see if the record is found as player2:player1
        match, _ = regexp.MatchString(fmt.Sprintf("^%s:%s:", player2, player1),
                                      lines[index])
        // If the record matched
        if match {
            // Replaces the retrieved data line with new data from this game
            lines[index] = fmt.Sprintf("%s:%s:%d:%d:%d", player2, player1,
                                                         stats[1], stats[0],
                                                         stats[2])
        }
        // Writes the records to the outfile
        _, err = outfile.WriteString(lines[index] + "\n")
    }
    // Renames the temporary file to the original file
    os.Rename(file + ".tmp", file)
}

func set_player_turn(name string) int {
    // This function determines if the first player would like to go first
    prompt := fmt.Sprintf("%s, would you like to go first? (y/n): ", name)
    // If the response entered evaluated to true
    if get_response(prompt) {
        // Returns a zero to indicate that the player will go first
        return 0
    // The response evaluated to false
    } else {
        // Returns a zero to indicate that the player will go second
        return 1
    }
}

func set_player_pieces(name string) (string, string) {
    // This function determines if the first player would like to be X's
    prompt := fmt.Sprintf("%s, would you like to be X's? (y/n): ", name)
    // If the response entered evaluated to true
    if get_response(prompt) {
        // Returns the player pieces in the desired order
        return "X", "O"
    // The response evaluated to false
    } else {
        // Returns the player pieces in the desired order
        return "O", "X"
    }
}

func get_player_position(name string, board [9]string) int {
    // This function retrieves the position that the player would like to play
    // Sets the prompt to display to the user
    prompt := fmt.Sprintf("%s, where would you like to play? ", name)
    // Sets the default position to be an invalid value
    position := ""
    // Begins an infinite loop
    for {
        // Retrieves a position from the user
        position = get_input(prompt)
        // If the user entered a value between 0 and 8 (inclusive)
        match, _ := regexp.MatchString("^[0-8]$", position)
        // If the position retrieved is valid and the position attempting
        // to play has not been played
        if match && board[string_to_int(position)] == position {
            // Returns the integer value of the position entered
            return string_to_int(position)
        }
    }
}

func clear_screen() {
    // This function clears the screen
    // Clears the screen
    fmt.Println("\x1B[2J\x1B[1;1H")
}

func player_wins(board [9]string, piece string) bool {
    // This function determines if the piece provided has been played in
    combo := fmt.Sprintf("%s%s%s", piece, piece, piece)
    // If any of the winning combinations was three of the specified character
    if fmt.Sprintf("%s%s%s", board[0], board[1], board[2]) == combo ||
       fmt.Sprintf("%s%s%s", board[0], board[3], board[6]) == combo ||
       fmt.Sprintf("%s%s%s", board[0], board[4], board[8]) == combo ||
       fmt.Sprintf("%s%s%s", board[1], board[4], board[7]) == combo ||
       fmt.Sprintf("%s%s%s", board[2], board[4], board[6]) == combo ||
       fmt.Sprintf("%s%s%s", board[2], board[5], board[8]) == combo ||
       fmt.Sprintf("%s%s%s", board[3], board[4], board[5]) == combo ||
       fmt.Sprintf("%s%s%s", board[6], board[7], board[8]) == combo {
        // The player won, return true
        return true
    }
    // The player did not win, return false
    return false
}

func max_number(num1 int, num2 int) int {
    // This function compares two integers and returns the larger of the two
	if num1 > num2 {
        // Returns the larger number
		return num1
	}
    // Returns the larger number
	return num2
}

func display_board(players [2]string, board [9]string, stats [3]int) {
    // This function displays the game board
    // Retrieves the maximum size between the names of the players
    size := max_number(len(players[0]), int(len(players[1]))) + 2
    // Prints the game board    
    fmt.Println(strings.Repeat(" ", size), "Wins  Losses  Draws")
    fmt.Printf("%*s %4d  %6d  %5d\n", -size, players[0], stats[0],
                                    stats[1], stats[2])
    fmt.Printf("%*s %4d  %6d  %5d\n\n", -size, players[1], stats[1],
                                      stats[0], stats[2])

    fmt.Printf("%s     |     |\n", strings.Repeat(" ", size))
    fmt.Printf("%s  %s  |  %s  |  %s\n", strings.Repeat(" ", size), board[0],
                                         board[1], board[2])
    fmt.Printf("%s     |     |\n", strings.Repeat(" ", size))
    fmt.Printf("%s-----------------\n", strings.Repeat(" ", size))
    fmt.Printf("%s     |     |\n", strings.Repeat(" ", size))
    fmt.Printf("%s  %s  |  %s  |  %s\n", strings.Repeat(" ", size), board[3],
                                         board[4], board[5])
    fmt.Printf("%s     |     |\n", strings.Repeat(" ", size))
    fmt.Printf("%s-----------------\n", strings.Repeat(" ", size))
    fmt.Printf("%s     |     |\n", strings.Repeat(" ", size))
    fmt.Printf("%s  %s  |  %s  |  %s\n", strings.Repeat(" ", size), board[6],
                                         board[7], board[8])
    fmt.Printf("%s     |     |\n\n", strings.Repeat(" ", size))
}

func computer_play(board [9]string, pieces [2]string) int {
    // This function sets a move for the computer
    // If the center position is open
    if board[4] == "4" {
        // Return the position of the middle space
        return 4
    }

    // Retrieve a position that the computer can win, if any
    position := player_can_win(board, pieces[1])
    // If the position is not a nine
    if position != 9 {
        // Return the position
        return position
    }
    // Retrieve a position that the player can win, if any
    position = player_can_win(board, pieces[0])
    // If the position is not a nine
    if position != 9 {
        // Return the position
        return position
    }

    // Begins an infinite loop
    for {
        // Set a random index position between 0 and 8
        position = rand.Intn(9)
        // If the position is between 0 and 8
        if position >= 0 && position <= 8 &&
           board[position] != "X" && board[position] != "O" {
            // Breaks the loop
            break
        }
    }
    // Return the position index
    return position
}

func player_can_win(board [9]string, piece string) int {
    // This function checks to see if the passed in piece can win
    // Sets the piece for two positions
    combo := fmt.Sprintf("%s%s", piece, piece)
    // Sets the position to be invalid
    position := 9
    // If the grouping contains a pair of the specified piece
    if((fmt.Sprintf("%s%s", board[1], board[2]) == combo ||
        fmt.Sprintf("%s%s", board[3], board[6]) == combo ||
        fmt.Sprintf("%s%s", board[4], board[8]) == combo) &&
       (board[0] == "0")) {
        // Sets the position that will allow the user to win
        position = 0
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[2]) == combo ||
               fmt.Sprintf("%s%s", board[4], board[7]) == combo) &&
              (board[1] == "1")) {
        // Sets the position that will allow the user to win
        position = 1
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[1]) == combo ||
               fmt.Sprintf("%s%s", board[4], board[6]) == combo ||
               fmt.Sprintf("%s%s", board[5], board[8]) == combo) &&
              (board[2] == "2")) {
        // Sets the position that will allow the user to win
        position = 2
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[6]) == combo ||
               fmt.Sprintf("%s%s", board[4], board[5]) == combo) &&
              (board[3] == "3")) {
        // Sets the position that will allow the user to win
        position = 3
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[8]) == combo ||
               fmt.Sprintf("%s%s", board[1], board[7]) == combo ||
               fmt.Sprintf("%s%s", board[2], board[6]) == combo ||
               fmt.Sprintf("%s%s", board[4], board[5]) == combo) &&
              (board[4] == "4")) {
        // Sets the position that will allow the user to win
        position = 4
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[2], board[8]) == combo ||
               fmt.Sprintf("%s%s", board[3], board[4]) == combo) &&
              (board[5] == "5")) {
        // Sets the position that will allow the user to win
        position = 5
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[3]) == combo ||
               fmt.Sprintf("%s%s", board[2], board[4]) == combo ||
               fmt.Sprintf("%s%s", board[7], board[8]) == combo) &&
              (board[6] == "6")) {
        // Sets the position that will allow the user to win
        position = 6
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[1], board[4]) == combo ||
               fmt.Sprintf("%s%s", board[6], board[8]) == combo) &&
              (board[7] == "7")) {
        // Sets the position that will allow the user to win
        position = 7
    // If the grouping contains a pair of the specified piece
    } else if((fmt.Sprintf("%s%s", board[0], board[4]) == combo ||
               fmt.Sprintf("%s%s", board[2], board[5]) == combo ||
               fmt.Sprintf("%s%s", board[6], board[7]) == combo) &&
              (board[8] == "8")) {
        // Sets the position that will allow the user to win
        position = 8
    }
    // Returns the position to play
    return position
}

func main() {
    // This is the main function that executes the program
    // Retrieves the number of players in the game
    num_players := get_num_players()
    // Creates player 1
    player1 := player{}
    // Sets the name for player 1
    player1.name = get_name(1)
    // Creates player 2
    player2 := player{}
    // If the number of players is one
    if num_players == 1 {
        // Sets the name to be the computer
        player2.name = "Computer"
    // The number of players is two
    } else {
        // Sets the name for player 2
        player2.name = get_name(2)
    }
    // Sets the turn for the player
    turn := set_player_turn(player1.name)
    // Sets to the next player; this allows for the chaning 
    turn = next_player(turn)
    // Retrieves the statistics from the records file
    stats := read_stats("ttt.dat", player1.name, player2.name)
    // Sets the statistics for each player
    player1.wins = stats[0]
    player2.losses = stats[0]
    player1.losses = stats[1]
    player2.wins = stats[1]
    player1.draws = stats[2]
    player2.draws = stats[2]
    // Sets the pieces used by the players in the game
    player1.piece, player2.piece = set_player_pieces(player1.name)
    // Sets a place holder for a message at the end of the game
    var message string
    // Sets a place holder for the player's move
    var move int
    // Begins an infinite loop
    for {
        // Creates a new game
        game_setting := game{0, [9]string{"0", "1", "2", "3", "4",
                                          "5", "6", "7", "8"}, false, true}
        // Sets the player to the next player before beginning a new game
        turn = next_player(turn)
        // Begins an infinite loop
        for {
            // Clears the screen
            clear_screen()
            // Display the game board
            display_board([2]string {player1.name, player2.name},
                          game_setting.board,
                          [3]int {player1.wins, player1.losses, player1.draws})
            // If the player's turn is the first player
            if turn == 0 {
                // Retrieves the position to play entered by the user
                move = get_player_position(player1.name, game_setting.board)
                // Places the player's piece on the game board
                game_setting.board[move] = player1.piece
                // Checks to see if the player won
                game_setting.game_over = player_wins(game_setting.board,
                                                     player1.piece)
                // If the game is over
                if game_setting.game_over {
                    // Sets the message to display as to why the game is over
                    message = fmt.Sprintf("%s won the game.", player1.name)
                    // Updates the statistics
                    player1.wins++
                    player2.losses++
                }
            // The player's turn is the second player
            } else {
                // If player two is the computer
                if player2.name == "Computer" {
                    // Retrieves the position to play for the computer
                    move = computer_play(game_setting.board,
                                         [2]string{player1.piece,
                                                   player2.piece})
                // Player two is not the computer
                } else {
                    // Retrieves the position to play entered by the user
                    move = get_player_position(player2.name,
                                               game_setting.board)
                }
                // Places the player's piece on the game board
                game_setting.board[move] = player2.piece
                // Checks to see if the player won
                game_setting.game_over = player_wins(game_setting.board,
                                                     player2.piece)
                // If the game is over
                if game_setting.game_over {
                    // Sets the message to display as to why the game is over
                    message = fmt.Sprintf("%s won the game.", player2.name)
                    // Updates the statistics
                    player2.wins++
                    player1.losses++
                }
            }
            // Increments the number of plays that have occured in the game
            game_setting.plays++
            // If a player won
            if game_setting.game_over {
                // Breaks from the loop
                break
            }
            // If the maximum number of plays has been played
            if game_setting.plays > 8 {
                // Updates the players' draws
                player1.draws++
                player2.draws++
                // Sets the message as to why the game ended
                message = fmt.Sprintf("The game ended in a draw.")
                // Breaks from the loop
                break
            }
            // Sets the turn to the next player
            turn = next_player(turn)
        }
        // Clears the screen
        clear_screen()
        // Displays the game board
        display_board([2]string {player1.name, player2.name},
                      game_setting.board,
                      [3]int {player1.wins, player1.losses, player1.draws})
        // Prints the reason as to why the game ended
        fmt.Println(message)
        // Retrieves whether to play the game again
        game_setting.play_game = get_response("\nWould you like to play " +
                                              "again (y/n): ")
        // If the game should not be played again
        if ! game_setting.play_game {
            // Breaks from the loop
            break
        }
    }
    // Writes the game statistics to the file
    write_stats("ttt.dat", player1.name, player2.name,
                [3]int {player1.wins, player1.losses, player1.draws})
}

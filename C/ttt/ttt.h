#ifndef HEADER_FILE
#define HEADER_FILE

#define MAX_PLAYERS 2
#define NAME_SIZE 51
#define BUF_SIZE 255

struct game;

struct player;

void title_case(char*);

void get_name(char**, int);

int get_num_players();

void set_gameboard(char*);

int num_matches(char*, char);

void clear_screen();

void set_stats(char*, int*, char*, char*, bool);

bool get_input(char*);

bool play_again();

int get_player_turn(char*);

void get_player_pieces(char*, char*);

int max_number(int, int);

void display_board(char*, char*, int[], char[]);

char* concat_chars(int, char, char, char);

int player_can_win(char*, char);

int get_computer_move(char*, char*);

bool player_wins(char[], char);

int change_turn(int);

int get_position(char*);

int get_player_move(char*, char*);

#endif


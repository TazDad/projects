// Sets the required libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "ttt.h"

void title_case(char *s) {
    // This function converts a string to title case
    // If the string is empty, return from the function
    if (s == NULL) return;

    // Stheps through each character in the string
    for (char *p = s; *p != '\0'; ++p)
        // Converts the character accordingly
        *p = (p == s || *(p-1) == ' ') ? (char)toupper(*p) : (char)tolower(*p);
}

void get_name(char **value, int player) {
    // Prints a prompt to the user
    printf("Enter player %d's name: ", player);
    // A do-while loop to retrieve a string until the string length is greater
    // than or equal to three
    do {
        // Retrieves a string from the user
        scanf("%s", *(value));
    } while(strlen(*(value)) < 3);
    // Converts a string to title case
    title_case(*(value));
}

int get_num_players() {
    // This function retrieves the number of players in the game
    // A variable to store the retrieved data
    char value;
    // Prompts the user
    printf("Enter the number of player: ");
    // A do-while loop to retrieve a the number of players; either one or two
    do {
        // Retrieves a character from the user
        value = (char)getc(stdin);
        // Flushes the rest of the input
        fflush(stdin);
    } while (value != '1' && value != '2');
    // Retrieves the integer value
    return value - '0';
}

void set_gameboard(char* board) {
    // This function sets the game board
    // Steps through the possible indexes
    for(int i=0; i<9; i++) {
        // Assigns the character value of the integer to the board
        board[i] = (char)(i + '0');
    }
}

int num_matches(char* data, char character) {
    // This function counts the number of times a character appears in a string
    // Sets to indicate that the character has not been found
    int count = 0;
    // Iterates through a string
    for(int i=0; i<(int)strlen(data); i++) {
        // If the character matches the test character
        if(data[i] == character) {
            // Increment the counter
            count++;
        }
    }
    // Return the counter/number of times the test character appeared in the
    // string
    return count;
}

void clear_screen() {
    // This function clears the screen
    // Clears the screen
    printf("\e[1;1H\e[2J");
}

void set_stats(char* filename, int* stats, char* name1, char* name2,
               bool write_data) {
    // This function reads/writes the statistics fil
    // Used in strtok to split a string on a separator
    char* token;
    // Sets the separator
    char separator[2] = ":";
    // Sets to find allow for the names to be found in either order in
    // the records file
    int matched = 0;
    // Sets to indicate the index used in the stats retrieval
    int index = 0;
    // Sets to indicate a record was found
    bool match_found = false;
    // Sets to indicate the record has not been written
    bool written = false;
    // Is used to indicate which order the names were found in the records file
    char order[3];
    // Used for writing to a temporary file
    char* temp_file = malloc(sizeof(char) * (strlen(filename) + 5));

    // Sets an input file pointer
    FILE *file = NULL;
    // Sets an output file pointer
    FILE *outfile = NULL;
    // Character array for each line read
    char line[255] = "";
    // Sets the temporary file name
    sprintf(temp_file, "%s.tmp", filename);
    // If the output file cannot be written
    if((outfile = fopen(temp_file, "w")) == NULL) {
        // Sets to indicate that the statistics will not be written to a file
        write_data = false;
    }
    // If the statistics file is opened for reading
    if((file = fopen(filename, "r")) != NULL) {
        // While a line can be read
        while(fgets(line, BUF_SIZE, file) != NULL) {
            // If the line is greater than 12 characer and the colon can be
            // found in the string exactly four times
            if(strlen(line) > 12 && num_matches(line, ':') == 4) {
                // If a match has not been found
                if(!match_found) {
                    // Remove the newline character
                    line[strlen(line)-1] = '\0';
                    // Starts tokenizing the string
                    token = strtok(line, separator);
                    // Sets to indicate a match has not been found
                    matched = 0;
                    // Sets the index to zero
                    index = 0;
                    // While the token still has data
                    while(token != NULL) {
                        // If the matches is less than two indicating the
                        // names area has not been passed
                        if(matched < 2) {
                            // If the name of player 1 is found
                            if(strncmp(token, name1, strlen(name1)) == 0) {
                                // Sets the order value where found
                                order[matched++]='a';
                            }
                            // If the name of player 2 is found
                            if(strncmp(token, name2, strlen(name2)) == 0) {
                                // Sets the order value where found
                                order[matched++]='b';
                            }
                        // The names area has been passed
                        } else if(matched == 2) {
                            // If the current game records have not been
                            // written
                            if(!write_data) {
                                // If player 1 was the first name found
                                if(order[0] == 'a') {
                                    // Sets the record found for the index
                                    stats[index] = atoi(token);
                                // Player 2 was found first
                                } else {
                                    // If the first record was not retrieved
                                    if(index == 0) {
                                        // Stores it as the second index
                                        stats[1] = atoi(token);
                                    // If the first record has been retrieved
                                    } else if(index == 1) {
                                        // Store it as the first index
                                        stats[0] = atoi(token);
                                    // The first two indexes have been written
                                    } else {
                                        // Retrieves the draws record value
                                        stats[index] = atoi(token);
                                    }
                                }
                                // Increments the stats index
                                index++;
                            }
                            // Sets to indicate the recordsd have been
                            // retrieved
                            match_found = true;
                        }
                        // Retrieves the next tokenized string
                        token = strtok(NULL, separator);
                    }
                }
                // If data is to be written
                if(write_data) {
                    // If a record was found and the new data has not been
                    // written
                    if(matched && !written) {
                        // If player 1 was retrieved first
                        if(order[0] == 'a') {
                            // Writes the data to the file
                            fprintf(outfile, "%s:%s:%d:%d:%d\n", name1, name2,
                                                                 stats[0],
                                                                 stats[1],
                                                                 stats[2]);
                        // Player 2 was retrieved first
                        } else {
                            // Writes the data to the file
                            fprintf(outfile, "%s:%s:%d:%d:%d\n", name2, name1,
                                                                 stats[1],
                                                                 stats[0],
                                                                 stats[2]);
                        }
                        // Set to indicate that the record has been written
                        written = true;
                    // Prints data that is not matched
                    } else {
                        fprintf(outfile, "%s\n", line);
                    }
                }
            }
        }
        // If the data is to be written        
        if(write_data) {
            // If the data has not been written
            if(!written) {
                // Writes the data to the file
                fprintf(outfile, "%s:%s:%d:%d:%d\n", name1, name2, stats[0],
                                                     stats[1], stats[2]);
            }
            // Closes the outfile
            fclose(outfile);
            // Renames the temp file to the original file
            rename(temp_file, filename);
        }
        // Closes the statistics file
        fclose(file);
    }
    // If a records match has not been found
    if(!match_found) {
        // Store empty statistics
        stats[0] = 0;
        stats[1] = 0;
        stats[2] = 0;
    }
    // Frees the memory of the temp file
    free(temp_file);
}

bool get_input(char* prompt) {
    // This function retrieves a yes or no response from a user
    // Location to store the response
    char value;
    // Prompts the user
    printf("%s", prompt);
    // A do-while loop to retrieve a y/n response
    do {
        // Retrieves a character from the user
        value = (char)getc(stdin);
        // Sets the character to lower case
        value = (char)tolower(value);
        // Flushes the rest of the input, if any
        fflush(stdin);
    } while (value != 'y' && value != 'n');
    // Returns the boolean test compared to a y
    return value == 'y';
}

bool play_again() {
    // This function determines if the players want to play again
    // Allocates memory for the prompt
    char *prompt = malloc(sizeof(char) * 37);
    // Sets the prompt
    sprintf(prompt, "Would you like to play again? (y/n): ");
    // Sets to indicate that the user does not want to play again
    bool result = false;
    // If the user entered y
    if(get_input(prompt)) {
        // Sets to indicate that the user wants to play again
        result = true;
    }
    // Frees the memory used for the prompt
    free(prompt);
    // Returns the result
    return result;
}

int get_player_turn(char* name) {
    // This function determines the turn of the player to start
    // Allocates memory for the prompt
    char *prompt = malloc(sizeof(char) * (strlen(name) + 37));;
    // Sets the prompt
    sprintf(prompt, "%s, would you like to go first? (y/n): ", name);
    // Sets to indicate that the user does not want to go first
    int result = 1;
    // If the user entered y
    if(get_input(prompt)) {
        // Sets to indicate that the user wants to go first
        result = 0;
    }
    // Frees the memory used for the prompt
    free(prompt);
    // Returns the result
    return result;
}

void get_player_pieces(char* name, char* pieces) {
    // This function sets the character pieces
    // Allocates memory for the prompt
    char *prompt = malloc(sizeof(char) * (strlen(name) + 35));;
    // Sets the prompt
    sprintf(prompt, "%s, would you like to be X's? (y/n): ", name);
    // If the user did not enter y
    if(!get_input(prompt)) {
        // Changes the order of the pieces
        pieces[0] = 'O';
        pieces[1] = 'X';
    }
    // Frees the memory used for the prompt
    free(prompt);
}

int max_number(int num1, int num2) {
    // This function compares two integers and returns the larger of the two
	if(num1 > num2) {
        // Returns the larger number
		return num1;
	}
    // Returns the larger number
	return num2;
}

void display_board(char* name1, char* name2, int stats[], char board[]) {
    // This function displays the game board
    // Retrieves the maximum size between the names of the players
    int size = max_number((int)strlen(name1), (int)strlen(name2)) + 2;
    // Prints the game board
    printf("%*c Wins  Losses  Draws\n", size, ' ');

    printf("%*s %4d  %6d  %5d\n", -size, name1, stats[0], stats[1], stats[2]);
    printf("%*s %4d  %6d  %5d\n", -size, name2, stats[1], stats[0], stats[2]);

    printf("\n%*c     |     |\n", size, ' ');
    printf("%*c  %c  |  %c  |  %c\n", size, ' ', board[0], board[1], board[2]);
    printf("%*c     |     |\n", size, ' ');
    printf("%*c-----------------\n", size, ' ');
    printf("%*c     |     |\n", size, ' ');
    printf("%*c  %c  |  %c  |  %c\n", size, ' ', board[3], board[4], board[5]);
    printf("%*c     |     |\n", size, ' ');
    printf("%*c-----------------\n", size, ' ');
    printf("%*c     |     |\n", size, ' ');
    printf("%*c  %c  |  %c  |  %c\n", size, ' ', board[6], board[7], board[8]);
    printf("%*c     |     |\n\n", size, ' ');
}

char* concat_chars(int size, char piece1, char piece2, char piece3) {
    // This function concatenates two or three characters together
    // Allocates the memory for the concatenated data
    char *combo = malloc(sizeof(char) * size);
    // If the size to create is two
    if(size == 2) {
        // Sets the new string
        sprintf(combo, "%c%c", piece1, piece2);
    // The size is three
    } else {
        // Sets the new string
        sprintf(combo, "%c%c%c", piece1, piece2, piece3);
    }
    // Returns the new string
    return combo;
}

int player_can_win(char *board, char piece) {
    // This function checks to see if the passed in piece can win
    // Allocates the memory for the two pieces
    char *combo = malloc(sizeof(char) * 2);
    // Sets the piece for two positions
    sprintf(combo, "%c%c", piece, piece);

    // Sets the position to be invalid
    int position = 9;

    // If the winning combinations associated with the '0' value has the
    // two associated locations filled in with the associated player's piece
    if((strncmp(concat_chars(2, board[1], board[2], '0'), combo, 2) == 0 ||
        strncmp(concat_chars(2, board[3], board[6], '0'), combo, 2) == 0 ||
        strncmp(concat_chars(2, board[4], board[8], '0'), combo, 2) == 0) &&
       (board[0] == '0')) {
        // Sets the location where the user could win
        position = 0;
    // If the winning combinations associated with the '1' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[2], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[4], board[7], '0'), combo,
                                                                 2) == 0) &&
              (board[1] == '1')) {
        // Sets the location where the user could win
        position = 1;
    // If the winning combinations associated with the '2' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[1], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[4], board[6], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[5], board[8], '0'), combo,
                                                                 2) == 0) &&
              (board[2] == '2')) {
        // Sets the location where the user could win
        position = 2;
    // If the winning combinations associated with the '3' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[6], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[4], board[5], '0'), combo,
                                                                 2) == 0) &&
              (board[3] == '3')) {
        // Sets the location where the user could win
        position = 3;
    // If the winning combinations associated with the '4' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[8], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[1], board[7], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[2], board[6], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[4], board[5], '0'), combo,
                                                                 2) == 0) &&
              (board[4] == '4')) {
        // Sets the location where the user could win
        position = 4;
    // If the winning combinations associated with the '5' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[2], board[8], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[3], board[4], '0'), combo,
                                                                 2) == 0) &&
              (board[5] == '5')) {
        // Sets the location where the user could win
        position = 5;
    // If the winning combinations associated with the '6' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[3], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[2], board[4], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[7], board[8], '0'), combo,
                                                                 2) == 0) &&
              (board[6] == '6')) {
        // Sets the location where the user could win
        position = 6;
    // If the winning combinations associated with the '7' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[1], board[4], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[6], board[8], '0'), combo,
                                                                 2) == 0) &&
              (board[7] == '7')) {
        // Sets the location where the user could win
        position = 7;
    // If the winning combinations associated with the '8' value has the
    // two associated locations filled in with the associated player's piece
    } else if((strncmp(concat_chars(2, board[0], board[4], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[2], board[5], '0'), combo,
                                                                 2) == 0 ||
               strncmp(concat_chars(2, board[6], board[7], '0'), combo,
                                                                 2) == 0) &&
              (board[8] == '8')) {
        // Sets the location where the user could win
        position = 8;
    }
    // Frees the memory used by the combo
    free(combo);
    // Returns the position to play
    return position;
}

int get_computer_move(char *board, char *pieces) {
    // This function sets a move for the computer
    // If the center position is open
    if(board[4] == '4') {
        // Return the position of the middle space
        return 4;
    }
    int position;
    // Retrieve a position that the computer can win, if any
    position = player_can_win(board, pieces[1]);
    // If the position is not a nine
    if(position != 9) {
        // Return the position
        return position;
    }
    // Retrieve a position that the player can win, if any
    position = player_can_win(board, pieces[0]);
    // If the position is not a nine
    if(position != 9) {
        // Return the position
        return position;
    }
    // A do-while loop to retrieve a random integer between 0 and 8 until
    // the position the position has not already been played
    do {
        // Set a random index position between 0 and 8
        position = rand() % 9;
    } while(board[position] != (position + '0'));
    // Return the position index
    return position;
}

bool player_wins(char board[], char piece) {
    // This function determines if the piece provided has won
    char *combo = malloc(sizeof(char) * 3);
    // Sets the winning string
    sprintf(combo, "%c%c%c", piece, piece, piece);
    // Sets to indicate that the player has not won
    bool result = false;
    // If any of the winning combinations was three of the specified character
    if(strncmp(concat_chars(3, board[0], board[1], board[2]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[0], board[3], board[6]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[0], board[4], board[8]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[1], board[4], board[7]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[2], board[4], board[6]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[2], board[5], board[8]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[3], board[4], board[5]), combo, 3) == 0 ||
       strncmp(concat_chars(3, board[6], board[7], board[8]), combo, 3) == 0) {
        // The player won, return true
        result = true;
    }
    // Frees the memory used by the combo
    free(combo);
    // Returns the result of the test
    return result;
}

int change_turn(int turn) {
    // This function changes to the next player
    // Returns the next player
    return (turn + 1) % 2;
}

int get_position(char* prompt) {
    // This function retrieves a location to play from the user
    // Sets the character value to retrieve
    char value;
    // Prompts the user
    printf("%s", prompt);
    // A do-while loop to retrieve an index to play for the user
    do {
        // Retrieves a character from a user
        value = (char)getc(stdin);
        // Flushes the rest of the standard input
        fflush(stdin);
    } while(! isdigit(value) || value == '9');
    // Returns the integer value
    return value - '0';
}

int get_player_move(char *name, char* board) {
    // This function retrieves the position that the player would like to play
    // Allocates the memory for the prompt
    char *prompt = malloc(sizeof(char) * (strlen(name) + 32));
    // Sets the prompt to display to the user
    sprintf(prompt, "%s, where would you like to play? ", name);

    // Sets the position value to store
    int position;
    // A do-while loop to retrieve a position to play that has not already been
    // played
    do {
        // Retrieves the position
        position = get_position(prompt);
    } while(board[position] != (position + '0'));
    // Frees the memory
    free(prompt);
    // Returns the position retrieved
    return position;
}

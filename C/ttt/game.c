// Sets the required libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "ttt.h"

// Defines a structure for game data
typedef struct game {
    char board[10];
    int plays;
    bool game_over;
    bool play_game;
} game;

// Defines a structure for player data
typedef struct player {
    char *name;
    int wins;
    int losses;
    int draws;
} player;

int main(int argc, char* argv[]) {
    // The main function to execute the game
    // Seeds the random generator
    srand((unsigned int)time(NULL));
    // Character string for the message
    char *message;
    // Creates an array of structs for the players
    player players[MAX_PLAYERS];
    // Creates a game session
    game game_session;
    // Creates a move value
    int move;
    // Creates the array of ints to be used for the statistics
    int stats[3] = {0, 0, 0};
    // Retrieves the number of players in the game
    int num_players = get_num_players();
    // Sets the initial character pieces
    char pieces[2] = {'X', 'O'};

    // Allocates memory for a message area
    message = malloc(sizeof(char) * NAME_SIZE);
    // Allocates memory for the player names
    players[0].name = malloc(sizeof(char) * NAME_SIZE);
    players[1].name = malloc(sizeof(char) * NAME_SIZE);
    // Sets to playe the game
    game_session.play_game = true;
    // Retrieves the name of player 1
    get_name(&players[0].name, 1);
    // Sets the player pieces
    get_player_pieces(players[0].name, pieces);
    // Sets the starting player
    int start = get_player_turn(players[0].name);
    // Sets to allow for changing turns
    int turn;
    // If the number of players is two
    if(num_players == 2) {
        // Retrieves the name of player two
        get_name(&players[1].name, 2);
    // The number of players it one
    } else {
        // Sets the name of player two to be the computer
        strncpy(players[1].name, "Computer", 9);
    }
    // Sets the statistics
    set_stats("ttt.dat", stats, players[0].name, players[1].name, false);
    // While the game is to be played
    while(game_session.play_game) {
        // Sets the gameboard
        set_gameboard(game_session.board);
        // Sets that the game should be played
        game_session.play_game = true;
        // Sets to indicate the game is over
        game_session.game_over = false;
        // Sets the number of plays in the game
        game_session.plays = 0;
        // Sets the player's turn to the start value
        turn = start;
        // Sets the start to the next player
        start = change_turn(turn);
        // While the game is not over
        while(! game_session.game_over) {
            // Clear the screen
            clear_screen();
            // Displays the game board
            display_board(players[0].name, players[1].name, stats,
                          game_session.board);
            // If the player's turn is the computer
            if(strncmp(players[turn].name, "Computer", 8) == 0) {
                // Retrieves the computer's move
                move = get_computer_move(game_session.board, pieces);
            // The player is human
            } else {
                // Retrieves the player's move
                move = get_player_move(players[turn].name, game_session.board);
            }
            // Sets the position retrieved to the current player's piece
            game_session.board[move] = pieces[turn];
            // Checks to see if a player has won
            game_session.game_over = player_wins(game_session.board,
                                                 pieces[turn]);
            // Increments the number of plays
            game_session.plays++;
            // If the game is over
            if(game_session.game_over) {
                // Sets the message of how the game ended
                sprintf(message, "%s won the game.", players[turn].name);
                // Increments the current player's winning statistics
                stats[turn]++;            
            // All the positions have been played
            } else if(game_session.plays > 8) {
                // Sets the message of how the game ended
                sprintf(message, "The game ended in a tie.");
                // Increments the draws total
                stats[2]++;
                // Sets that the game is over
                game_session.game_over = true;
            // The game is still in session
            } else {
                // Changes to the next player's turn
                turn = change_turn(turn);
            }
        }
        // Clears the screen
        clear_screen();
        // Displays the board to the user
        display_board(players[0].name, players[1].name, stats,
                      game_session.board);
        // Prints the reason why the game ended
        printf("%s\n", message);
        // Determines if the game should be played again
        game_session.play_game = play_again();
    }
    // Writes the updated statistics to the file
    set_stats("ttt.dat", stats, players[0].name, players[1].name, true);
    // Frees the memory of allocated data
    free(players[0].name);
    free(players[1].name);
    free(message);
    // Exits the program
    exit(0);
}

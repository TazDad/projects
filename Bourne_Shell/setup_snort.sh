#! /bin/sh
exit
# This program must be executed by a sudo user
# This program installs snort 3.3.0.0 and required files by performing the
# following actions:
# 	Updates the packages on the system
# 	Installs required packages required for snort installation
#   Installs some optional packages required for snort installation
# 	Updates the packages on the system
#   Downloads snort
#	Installs snort
#	Configures the required directories and files
#   Updates the snort.lua file
#	Installs the community rules
#	Adds a local rule for testing
# 	Removes files and directories used for installation

start_install() {
	# Informs the user
	echo "Installing snort. This process takes about an hour."

	# Informs the user
	echo "Changing directory to $run_dir"
	# Changes the directory to the run directory
	cd $run_dir 1>/dev/null 2>&1
}

install_system_libraries() {
	# Informs the user
	echo "Installing required packages"
	# Installs the required and some optional packages
	# If they are already on the system they will not be installed again
	echo -n "[ 0%]"
	sudo apt-get install -y git cmake build-essential libpcap-dev openssl libssl-dev liblzma-dev 1>/dev/null 2>> $log_file
	echo -n "\r[ 15%]"
	sudo apt-get install -y libpcre3-dev libdumbnet-dev bison flex zlib1g-dev g++ libdnet asciidoc 1>/dev/null 2>> $log_file
	echo -n "\r[ 30%]"
	sudo apt-get install -y dblatex autotools-dev autoconf libtool libnet1-dev automake 1>/dev/null 2>> $log_file
	echo -n "\r[ 45%]"
	sudo apt-get install -y luajit pkg-config xorg-dev libglib2.0-dev libcmocka-dev hwloc cpputest 1>/dev/null 2>> $log_file
	echo -n "\r[ 60%]"
	sudo apt-get install -y iconv libml libunwind lzma source-highlight w3m uuid uuid-dev 1>/dev/null 2>> $log_file
	echo -n "\r[ 75%]"
	sudo apt-get install -y libboost-all-dev ragel lzma source-highlight w3m uuid libsqlite3-dev 1>/dev/null 2>> $log_file
	echo -n "\r[ 90%]"
	sudo apt-get install -y libnetfilter-queue-dev libhwloc-dev 1>/dev/null 2>> $log_file
	echo "\rFinished installing system libraries"
}

update_system() {
	# Informs the user
	echo "Updating/upgrading system"
	# Updates and upgrades the system packages
	sudo apt-get update 1>/dev/null 2>> $log_file
	sudo apt-get -y upgrade 1>/dev/null 2>> $log_file
}

setup_environment() {
	# Informs the user
	echo "Setting up environment"
	# Sets up the system environment to find some of the installed packages
	libtoolize --force 1>/dev/null 2>> $log_file
}

install_safec() {
    # Informs the user
	echo "Downloading the safec package"
	# Downloads the file
	wget http://downloads.sourceforge.net/project/safeclib/libsafec-10052013.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the downloaded file
	tar -zxvf libsafec-10052013.tar.gz 1>/dev/null 2>> $log_file
	# Informs the user
	echo "Configuring and installing the safec package"
	# Changes directory to the new directory
	cd libsafec-10052013 2>/dev/null
	# Runs the configure script
	./configure 1>/dev/null 2>> $log_file
	# Compiles the components
	make 1>/dev/null 2>> $log_file
	# Installs the package
	sudo make install 1>/dev/null 2>> $log_file
	# Changes the directory up a level
	cd .. 2>/dev/null
}

install_gperf() {
    # Informs the user
	echo "Downloading the gperf package"
	# Downloads the file
	git clone https://github.com/gperftools/gperftools.git 1>/dev/null 2>> $log_file
	# Changes directory to the new directory
	cd gperftools 2>/dev/null
	# Informs the user
	echo "Configuring and installing the gperf package"
	# Creates the configuration file
	autoreconf -i  1>/dev/null 2>> $log_file
	# Runs the configure script
	./configure 1>/dev/null 2>> $log_file
	# Compiles the components
	make -j $(nproc) 1>/dev/null 2>> $log_file
	# Installs the package
	sudo make install 1>/dev/null 2>> $log_file
	# Changes the directory up a level
	cd .. 2>/dev/null
}

install_libdaq() {
	# Informs the user
	# This is listed above to install; however, during initial testing, it did not work for snort so this is here
	echo "Downloading the libdaq package"
	# Gets the source file
	git clone https://github.com/snort3/libdaq.git 1>/dev/null 2>> $log_file
	# Changes the directory to the downloaded location
	cd libdaq 1>/dev/null 2>&1
	# Informs the user
	echo "Configuring and installing the libdaq package"
	# Sets up required package files and values
	./bootstrap 1>/dev/null 2>> $log_file
	# Configures for installation
	./configure --prefix=/usr/local/lib/daq_s3 1>/dev/null 2>> $log_file
	# Installs the package
	sudo make install 1>/dev/null 2>> $log_file
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
	# Verifies that libdaq was installed
	verify_libdaq
}

verify_libdaq() {
	# If the file does not exist
	if [ ! -f "/usr/lib/libdaq.so.3" ]; then
		# If the file exists
		if [ -f "/usr/local/lib/daq_s3/lib/libdaq.so.3" ]; then
			# Set a symbolic link to the file location
			sudo ln -s /usr/local/lib/daq_s3/lib/libdaq.so.3 /usr/lib/libdaq.so.3 1>/dev/null 2>&1
		# If this file exists
		elif [ -f "/usr/local/lib/libdaq.so.3" ]; then
			# Set a symbolic link to the file location
			sudo ln -s /usr/local/lib/libdaq.so.3 /usr/lib/libdaq.so.3 1>/dev/null 2>&1
		fi
	fi
	# Sets up a value to allow for the system to know where libdaq was installed
	echo "/usr/local/lib/daq_s3/lib" | sudo tee -a /etc/ld.so.conf.d/my_libdaq.conf 1>/dev/null 2>&1
}

install_hwloc() {
	# Informs the user
	echo "Downloading the hwloc package"
	# Downloads the package
	wget https://download.open-mpi.org/release/hwloc/v2.11/hwloc-2.11.1.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the package
	tar -zxvf hwloc-2.11.1.tar.gz 1>/dev/null 2>> $log_file
	# Changes to the downloaded directory
	cd hwloc-2.11.1 1>/dev/null 2>&1
	# Informs the user
	echo "Configuring and installing the hwloc package"
	# Configures for installation
	./configure 1>/dev/null 2>> $log_file
	# Creates files for installation
	make 1>/dev/null 2>> $log_file
	# Installs the files
	sudo make install 1>/dev/null 2>> $log_file
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
}

install_luajit() {
	# Informs the user
	echo "Downloading the luajit package"
	# Downloads the package
	git clone https://luajit.org/git/luajit.git 1>/dev/null 2>> $log_file
	# Changes directory to the package location
	cd luajit 1>/dev/null 2>&1
	# Informs the user
	echo "Configuring and installing the luajit package"
	# Creates files for installation
	make 1>/dev/null 2>> $log_file
	# Installs the files
	sudo make install 1>/dev/null 2>> $log_file
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
}

install_ragel() {
    # Informs the user
	echo "Downloading the ragel package"
	# Downloads the file
	wget http://www.colm.net/files/ragel/ragel-6.10.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the downloaded file
	tar -zxvf ragel-6.10.tar.gz 1>/dev/null 2>> $log_file
	# Informs the user
	echo "Configuring and installing the ragel package"
	# Changes directory to the new directory
	cd ragel-6.10 2>/dev/null
	# Runs the configure script
	./configure 1>/dev/null 2>> $log_file
	# Compiles the components
	make 1>/dev/null 2>> $log_file
	# Installs the package
	sudo make install 1>/dev/null 2>> $log_file
	# Changes the directory up a level
	cd .. 2>/dev/null
}

download_boost() {
	# Downloading the boost package
	wget https://boostorg.jfrog.io/artifactory/main/release/1.67.0/source/boost_1_67_0.tar.gz 1>/dev/null 2>> $log_file
	tar -zxvf boost_1_67_0.tar.gz 1>/dev/null 2>> $log_file
	echo "$run_dir/boost_1_67_0"
}

install_hyperscan() {
	# Installs the ragel package
	install_ragel
	# Retrieves the boost libraries
	boost_dir=$(download_boost)
	# Informs the user
	echo "Downloading the hyperscan package"
	# Downloads the package
	git clone https://github.com/intel/hyperscan.git 1>/dev/null 2>> $log_file
	# Informs the user
	echo "Configuring and installing the hyperscan package"
	# Creates a directory to build the package
	mkdir hyper-build 2>/dev/null
	# Changes to the package build directory
	cd hyper-build 2>/dev/null
	# Prepares the files for building
	cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DBOOST_ROOT=$boost_dir ../hyperscan 1>/dev/null 2>> $log_file
	# Creates the files for installation
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make -j $(nproc) 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Installs the package
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make install 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
}

install_flatbuffers() {
	# Informs the user
	echo "Downloading the flatbuffers package"
	# Downloads the package
	wget https://github.com/google//flatbuffers/archive/v1.9.0.tar.gz -O flatbuffers-1.9.0.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the downloaded file
	tar -zxvf flatbuffers-1.9.0.tar.gz 1>/dev/null 2>> $log_file
	# Informs the user
	echo "Configuring and installing the flatbuffers package"
	# Creates a directory to build the package
	mkdir flat-build 2>/dev/null
	# Changes to the package build directory
	cd flat-build 2>/dev/null
	# Prepares the files for building
	cmake ../flatbuffers-1.9.0 1>/dev/null 2>> $log_file
	# Creates the files for installation
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make -j $(nproc) 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Installs the package
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make install 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
}

install_libml() {
	# Informs the user
	echo "\nDownloading the libml package"
	# Downloads the package
	git clone https://github.com/snort3/libml.git 1>/dev/null 2>> $log_file
	# Informs the user
	echo "Configuring and installing the libml package"
	# Creates a directory to build the package
	mkdir libml-build 2>/dev/null
	# Changes to the package build directory
	cd libml-build 2>/dev/null
	# Prepares the files for building
	cmake ../libml 1>/dev/null 2>> $log_file
	# Creates the files for installation
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make -j $(nproc) 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Installs the package
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make install 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Returns up a directory
	cd .. 1>/dev/null 2>&1
}

runtime_config() {
	# Informs the user
	echo "\nConfiguring run-time bindings"
	# Configures the run-time bindings
	sudo ldconfig 1>/dev/null 2>&1
}

fix_pthread() {
	# Iterates through each of the files to replace
	for file in `grep -l lpthread build/* build/*/* 2>>/dev/null`
	do
		# Replaces all occurrences of lpthread with pthread
		sed -i "s/lpthread/pthread/g" "$file" 1>/dev/null 2>> $log_file
	done

	# Informs the user
	echo "Making snort"
	# Creates the files for building
	cmake build 1>/dev/null 2>> $log_file
}

# Do not need this anymore
fix_daq() {
	# Sets up the variables to insert the DAQ lines
	# The call to cmake in the line
	cmake_call='cmake "$gen" \\'
	# The lines to insert after the call to cmake
	cmake_daq='    -D DAQ_LIBRARIES=\/usr\/local\/lib\/daq_s3\/lib \\\n    -D DAQ_INCLUDE_DIR=\/usr\/local\/lib\/daq_s3\/include \\'
	# Finds the matching line and inserts the new information
	sed -i "s/$cmake_call/$cmake_call\n$cmake_daq/" "configure_cmake.sh"
}

install_snort() {
	# Informs the user
	echo "Downloading snort"
	# Sets the version to download
	version='3.3.0.0'
	# Downloads the package
	wget https://github.com/snort3/snort3/archive/refs/tags/$version.tar.gz 1>/dev/null 2>> $log_file
	# Renames the file downloaded
	mv $version.tar.gz snort3-$version.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the file
	tar -zxvf snort3-$version.tar.gz 1>/dev/null 2>> $log_file
	# Changes directory to the package directory
	cd snort3-$version 1>/dev/null 2>&1
	# Fixes DAQ location issues for snort
	# Do not need this anymore
	#fix_daq
	# Informs the user
	echo "Configuring snort"
	# Configures files for installation
	./configure_cmake.sh --prefix=/usr/local --with-daq-libraries=/usr/local/lib/daq_s3/lib --with-daq-includes=/usr/local/lib/daq_s3/include 1>/dev/null 2>> $log_file

	# On the initial system, it kept calling lpthread, which was not available.
	# This area supplied a work around to install
	# This commented out function is needed if the pthread is set up to lpthread instead and it does not work
	#fix_pthread

	# Changes directory to the build directory
	cd build 1>/dev/null 2>&1
	# Informs the user
	echo "Installing snort (Be patient. This takes several minutes.)"
	# Creates the files for installation
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make -j $(nproc) 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Installs the package
	# Displays the percentage complete along the way if the value is presented during the make process
	stdbuf -o0 sudo make install 2>/dev/null | awk '{match($0, /^\[[ 1][0-9][0-9]%\]/, m); if (m[0]) {FS="]"; split($0, fields, FS); ORS=""; print "\r" fields[1]"]" }}'
	# Returns up two levels
	cd ../.. 2>/dev/null
}

snort_setup() {
	# Informs the user
	echo "\nSetting access to snort"
	# Sets snort to be accessed from within a path already setup in the user environment
	sudo ln -s $snort_bin/snort /usr/sbin/snort  1>/dev/null 2>&1
}

get_snort_rules() {
	# Informs the user
	echo "Downloading snort rules"
	# Downloads the snort rules
	wget https://www.snort.org/downloads/community/snort3-community-rules.tar.gz 1>/dev/null 2>> $log_file
	# Untars and unzips the snort rules
	tar -zxvf snort3-community-rules.tar.gz 1>/dev/null 2>> $log_file
	# Makes the snort rules directories
	sudo mkdir $install_dir/etc/rules 2>/dev/null
	sudo mkdir $install_dir/etc/builtin_rules 2>/dev/null
	sudo mkdir $install_dir/etc/so_rules 2>/dev/null
	sudo mkdir $install_dir/etc/lists 2>/dev/null
	# Copies the downloaded rules to the snort rules directory
	sudo cp snort3-community-rules/snort3-community.rules $install_dir/etc/rules 1>/dev/null 2>> $log_file
	sudo cp snort3-community-rules/sid-msg.map $install_dir/etc/rules 1>/dev/null 2>> $log_file
}

# Do not need this anymore
get_network_info() {
	# Gets the local IP address
	ipadd=$(ip add | grep brd | grep inet | sed 's/\// /g' | awk -F' ' '{print $2}')
	# Gets the cidr notation
	cidr=$(ip add | grep brd | grep inet | sed 's/\// /g' | awk -F' ' '{print $3}')
	# Gets the interface (must be connected to internet)
	interface=$(ip route get 8.8.8.8 | head -1 | awk -F' ' '{print $5}')
	# Sets the max padding that could be allowed
	padding="00000000000000000000000000000000"

	# Saves the current separator
	OLD_IFS=$IFS
	# Sets the new sepearator
	IFS='.'
	# Creates an empty variable for the binary representation
	bin_rep=''
	# Iterates through the IP address separating on the new separator
	for num in $ipadd
	do
		# Convert the decimal value to binary formatting it in 8-bit places padded with 0s
		bin_value=$(printf "%08d" $(echo "obase=2; $num" | bc))
		# If the variable is empty
		if [ -z "${#bin_rep}" ]
		then
		    # Sets the first value
		    bin_rep=$bin_value
		# The variable is not empty
		else
		    # Appends the new value onto the current value
		    bin_rep="$bin_rep$bin_value"
		fi
	done
	# Sets the separator back to the original one
	IFS=$OLD_IFS

	# Sets the binary subnet mask (without periods)
	subnet_bin=$(echo $bin_rep | cut -c 1-$(($cidr)))$(echo $padding | cut -c 1-$((${#padding}-$cidr)))

	# Sets the empty network address
	network=""
	# Sets the first iteration of the loop
	times=1
	# Sets the starting value for the cut
	start=1
	# While the number of times through the loop is less than or equal to four
	while [ $times -le 4 ] ; do
		# Sets the end bits to take
		bits=$(expr $times \* 8)
		# Retrieves the appropriate 8-bits of the binary number from the binary subnet mask
		num=$(echo $subnet_bin | cut -c $(($start))-$(($bits)))
		# Converts the binary bits to decimal
		dec=$(echo "ibase=2; $num" | bc)
		# If the variable is empty
		if [ -z "$network" ]; then
		    # Sets the first part of the network
		    network=$dec
		# The variable is not empty
		else
		    # Appends the next part of the network address
		    network="$network.$dec"
		fi
		# Increments the iterations through the loop
		times=$(expr $times + 1)
		# Sets the new starting location within the subnet_bin
		start=$(expr $start + 8)
	done
	echo "$interface $cidr $network"
}

# Do not need this anymore
write_configuration() {
	# Retrieves the network information
	network_info=$(get_network_info)
	# Splirts the value into separate variables
	set -- $network_info
	# Sets the retrieved interface
	interface=$1
	# Sets the retrieved CIDR notation
	cidr=$2
	# Sets the retrieved network information
	network=$3
	# Informs the user
	# Prints to stderr to avoid it being captured in the return
	echo "Creating snort configuration file" >&2
	# Sets the snort local rules file name
	snort_rules="/etc/snort/rules/local.rules"
	# Sets the snort configuration file name
	snort_conf="/etc/snort/snort.conf"
	# Writes the snort configuration file
	echo '# Configuration file for snort 3\n' | sudo tee $snort_conf 2>/dev/null 2>&1
	echo '# Define the location of the rules file' | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo "include $snort_rules" | sudo tee -a $snort_conf 1>/dev/null 2>&1
	echo '\n# Network interfaces to monitor' | sudo tee -a $snort_conf 1>/dev/null 2>&1
	echo "interface $interface" | sudo tee -a $snort_conf 1>/dev/null 2>&1
	echo '\n# Internal network addresses' | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo "internal $network/$cidr" | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo '\n# Exterrnal network addresses' | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo "external any" | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo '\n# Logging configuration' | sudo tee -a $snort_conf 2>/dev/null 2>&1
	echo 'log alert /var/log/snort/alert.log' | sudo tee -a $snort_conf 1>/dev/null 2>&1
	# Treats the printing of the snort rules file name as a return value
	echo "$snort_rules"
}

write_local_rule() {
	# Informs the user
	echo "Creating local snort rule"
	# Creates an intitial rule within the local rules file to detect ICMP packets
	echo 'alert icmp any any -> any any (msg:"Alert: ICMP Echo request (ping)"; sid:10000001; rev:1;)' | sudo tee $rules_file 1>/dev/null 2>&1
}

modify_configuration() {
	# Sets the snort configuration file
	config_file="$config_dir/snort.lua"
	# Sets the original configuration file backup nane
	config_orig="$config_dir/snort.lua.orig"
	# Moves the original file to a new name
	sudo mv $config_file $config_orig 2>/dev/null
	# Sets up the new ips configuration data
	ips_line="ips =\n{\n    enable_builtin_rules = true,\n    include = RULE_PATH .. '\/local.rules',\n    variables = default_variables\n}"

	# Removes the -- (commenting feature for lua) from the alert to the console and modifies the ips data
	cat $config_orig | sed 's/--alert_fast/alert_fast/' | sed -z "s/ips =[^\}]*\}/$ips_line/" | sudo tee -a $config_file 1>/dev/null 2>&1
}

clean_up() {
	# Informs the user
	echo "Cleaning up files"
	# Removes directories and files used during installation
	sudo rm -fr snort3-3.3.0.0 snort3-3.3.0.0.tar.gz hwloc-2.11.1 libdaq luajit 2>/dev/null
	sudo rm -fr hwloc-2.11.1.tar.gz snort3-community-rules.tar.gz libml-build libml hyperscan hyper-build 2>/dev/null
	sudo rm -fr snort3-community-rules ltmain.sh libsafec-10052013.tar.gz libsafec-10052013 gperftools 2>/dev/null
	sudo rm -fr ragel-6.10 ragel-6.10.tar.gz boost_1_67_0.tar.gz boost_1_67_0 2>/dev/null
	sudo rm -fr flatbuffers-1.9.0.tar.gz flatbuffers-1.9.0 flat-build 2>/dev/null
}

execution_duration() {
	# Retrieves the passed in start time
	start_time="$1"
	# Get the end time in seconds since the epoch
	end_time=$(date +%s)
	# Calculate the duration
	duration=$((end_time - start_time))

	# Convert duration to hours, minutes, seconds
	hours=$((duration / 3600))
	minutes=$((duration % 3600 / 60))
	seconds=$((duration % 60))

	# Print the duration or program execution
	printf "Script runtime: %02d:%02d:%02d (HH:MM:SS)\n" $hours $minutes $seconds
}

setup_env_variables() {
	# If the evironment values have not already been set
	if [ ! -n "$LUA_SET" ]; then
		# Adds the snort LUA paths to the environment variables
		sh -c "echo 'export LUA_SET=1' >> ~/.bash_aliases"
		sh -c "echo 'export LUA_PATH=\$install_dir/include/snort/lua/\?.lua\;\;' >> ~/.bash_aliases"
		sh -c "echo 'export SNORT_LUA_PATH=\$install_dir/etc' >> ~/.bash_aliases"
		# Sets to allow sudo to keep these environment variables
		echo "Defaults env_keep += \"LUA_PATH SNORT_LUA_PATH\"" | sudo tee -a /etc/sudoers 1>/dev/null 2>> $log_file
		# Informs the user
		echo "In order to ensure that snort works, either close and reopen this terminal window or execute: source ~/.bash_aliases"
		echo "The source will need to be done in any current active terminal."
	fi
}

install_system_packages() {
	update_system
	install_system_libraries
	update_system
	setup_environment
}

install_ind_packages() {
	install_safec
	install_gperf
	install_flatbuffers
	install_libdaq
	install_hwloc
	install_luajit
	install_hyperscan
	install_libml
	runtime_config
}

install_snort_package() {
	install_snort
	snort_setup
	get_snort_rules
	write_local_rule
	modify_configuration
}

execute_program() {
	# Executes the functions to install snort and all the required/optional packages
	start_install
	install_system_packages
	install_ind_packages
	install_snort_package
	setup_env_variables

	# Cleans up after install
	clean_up
}

if [ `groups | grep -c sudo` -ne 1 ]; then
	echo "This program must be executed by someone with sudo permissions, exiting..."
	exit
fi

# Sets the start time in seconds since the epoch
start_time=$(date +%s)

# Sets the directory to use during installation
run_dir="$HOME/Downloads"
# Sets the errors log file
# With the way the program is currently set up, this needs to be a global variable like it is
# Most of the functions use this file
log_file="$run_dir/snort_install.log"
# Sets the number of processes to use during make commands to speed them up
nproc=5
# Sets the snort installation directory
install_dir="/usr/local"
# Sets the snort configuration directory
config_dir="$install_dir/etc/snort"
# Sets the snort binary directory
snort_bin="$install_dir/bin"
# Sets up the rules file
rules_file="$install_dir/etc/rules/local.rules"

execute_program

# Prints the execution time of the program
execution_duration $start_time

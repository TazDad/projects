#! /bin/sh

# This program determines the network address based upon the information returned
# from the command "ip add". It expects that the IPv4 address is directly followed
# by the CIDR address in order to determine the network address.

# Gets the local IP address
ipadd=$(ip add | grep brd | grep inet | sed 's/\// /g' | awk -F' ' '{print $2}')
# Gets the cidr notation
cidr=$(ip add | grep brd | grep inet | sed 's/\// /g' | awk -F' ' '{print $3}')
# Sets the max padding that could be allowed
padding="00000000000000000000000000000000"

# Saves the current separator
OLD_IFS=$IFS
# Sets the new sepearator
IFS='.'
# Creates an empty variable for the binary representation
bin_rep=''
# Iterates through the IP address separating on the new separator
for num in $ipadd
do
    # Convert the decimal value to binary formatting it in 8-bit places padded with 0s
    bin_value=$(printf "%08d" $(echo "obase=2; $num" | bc))
    # If the variable is empty
    if [ -z "${#bin_rep}" ]
    then
        # Sets the first value
        bin_rep=$bin_value
    # The variable is not empty
    else
        # Appends the new value onto the current value
        bin_rep="$bin_rep$bin_value"
    fi
done
# Sets the separator back to the original one
IFS=$OLD_IFS

# Sets the binary subnet mask (without periods)
subnet_bin=$(echo $bin_rep | cut -c 1-$(($cidr)))$(echo $padding | cut -c 1-$((${#padding}-$cidr)))

# Sets the empty network address
network=""
# Sets the first iteration of the loop
times=1
# Sets the starting value for the cut
start=1
# While the number of times through the loop is less than or equal to four
while [ $times -le 4 ] ; do
    # Sets the end bits to take
    bits=$(expr $times \* 8)
    # Retrieves the appropriate 8-bits of the binary number from the binary subnet mask
    num=$(echo $subnet_bin | cut -c $(($start))-$(($bits)))
    # Converts the binary bits to decimal
    dec=$(echo "ibase=2; $num" | bc)
    # If the variable is empty
    if [ -z "$network" ]; then
        # Sets the first part of the network
        network=$dec
    # The variable is not empty
    else
        # Appends the next part of the network address
        network="$network.$dec"
    fi
    # Increments the iterations through the loop
    times=$(expr $times + 1)
    # Sets the new starting location within the subnet_bin
    start=$(expr $start + 8)
done

# Prints the network address
echo "$network"

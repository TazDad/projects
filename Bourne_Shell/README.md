This repository is for Bourne Shell programs that I have created.

Here is a brief synopsis of each one:

1. setup_snort.sh
- This program is designed for Ubuntu. It is for someone with sudo permissions. It performs the necessary actions to install the required and some of the suggested packages required for snort 3.3.0.0. After they have been installed, it installs snort version 3.3.0.0 and the community rules.

2. get_network_addr.sh
- This program is designed for Ubuntu. It reads the output of the "ip add" command to retrieve the IPv4 address of the system and the CIDR notation. It uses this information to calculate the network address that the IPv4 address of the system belongs.